﻿var mongoose = require('mongoose');

var officeStaffSchema = new mongoose.Schema({

    userRef: { type: mongoose.Schema.Types.ObjectId, ref: 'user', required: true },

    //subjects: [require('../models/subject.server.model').subjectSchema],

    //fatherName: String,
    //birthDate: String,
    //joinedDate: String,
    designation: String,
    qualification: String,
    experience: String,
    //contactNo: String,

    createdBy: {
        type: mongoose.Schema.Types.ObjectId
    },
    createdDate: {
        type: Date
    },
    updatedBy: {
        type: mongoose.Schema.Types.ObjectId
    },
    updatedDate: Date
});

mongoose.model('officeStaff', officeStaffSchema);