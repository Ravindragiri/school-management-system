﻿var mongoose = require('mongoose');

var parentSchema = new mongoose.Schema({

    user: { type: mongoose.Schema.Types.ObjectId, ref: 'user', required: true },

    fatherName: String,
    fatherOccupation: String,
    fatherContactNumber: String,

    motherName: String,
    motherOccupation: String,
    motherContactNumber: String,

    createdBy: {
        type: mongoose.Schema.Types.ObjectId
    },
    createdDate: {
        type: Date
    },
    updatedBy: {
        type: mongoose.Schema.Types.ObjectId
    },
    updatedDate: Date

});

mongoose.model('parent', parentSchema);