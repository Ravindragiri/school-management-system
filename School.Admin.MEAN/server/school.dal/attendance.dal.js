﻿var Q = require('q');
var mongoose = require('mongoose')
require('../models/student.server.model');
require('../models/attendance.server.model');
require('../models/user.server.model');
var _student = mongoose.model('student');
var _attendance = mongoose.model('attendance');
var _user = mongoose.model('user');
var ObjectId = mongoose.Types.ObjectId;

require('../../server/school.common/helpers/deferredHelper');

var dal = {};

dal.update = update;
//dal.updateAll = updateAll;
dal.count = count;
dal.add = add;
dal.getattendanceById = getattendanceById;
dal.removeByDate = removeByDate;
dal.removeById = removeById;
dal.getAttendances = getAttendances;
dal.searchAttendances = searchAttendances;
dal.getAllAttendances = getAllAttendances;
dal.getAttendanceDetail = getAttendanceDetail;
dal.getAttendancesByUserId = getAttendancesByUserId;
dal.getAttendanceByStudentId = getAttendanceByStudentId;
dal.getById = getById;

module.exports = dal;

function update(studentModel, studentId, attendanceId, userId) {

    var deferred = Q.defer();
    //TODO
    _student.findById(studentId, function (err, data) {
        if (err) {
            console.log(err);
            throw err;
        }

        var attendance = data.attendances.id(attendanceId);

        if (attendance) {
            try {

                var attendanceModel = studentModel.attendances[0];
                //get changed dates only

                attendance.status = attendanceModel.status ? attendanceModel.status : attendance.status;
                attendance.remarks = attendanceModel.remarks ? attendanceModel.remarks : attendance.remarks;
                attendance.attendanceDate = attendanceModel.attendanceDate ? attendanceModel.attendanceDate : attendance.attendanceDate;
                attendance.academicYear = attendanceModel.academicYear ? attendanceModel.academicYear : attendance.academicYear;

                //attendance.updatedBy = loggedInUserId;
                attendance.updatedDate = new Date();

                data.save();
            }
            catch (error) {
                console.log(error);
            }
        }
        else {
            studentModel.attendances.forEach(function (attendance) {
                attendance._id = new ObjectId; 
                data.attendances.push(attendance);
            });
            data.save();
        }
    });
    return deferred.promise;
}

function count(studentId) {

    var deferred = Q.defer();

    _student.findById(studentId, function (err, data) {

            if (err) {
                deferred.reject(err);
            }
            if (data) {
                deferred.resolve(data.attendances.length);
            } else {
                deferred.resolve();
            }
        });
    return deferred.promise;
}

function add(studentId, attendanceDTO) {

    var attendance = new _attendance(attendanceDTO);
    attendance._id = new ObjectId;
    //attendance.createdBy = loggedInUserId;
    attendance.createdDate = new Date();

    var deferred = Q.defer();

    _student
        .findByIdAndUpdate({ _id: studentId }, { $push: { "attendances": attendance } }, { safe: true, upsert: true }, function (err, result) {

            if (err) deferred.reject(err);

            if (result) {
                deferred.resolve(result);
            } else {
                deferred.resolve();
            }
        });
    return deferred.promise;
}

//function add(studentId, userId, attendanceDTO) {

//    var attendance = new _attendance(attendanceDTO);
//    attendance._id = new ObjectId;
//    attendance.createdBy = userId;
//    attendance.createdDate = new Date();

//    var deferred = Q.defer();

//    _student
//    .findByIdAndUpdate({ _id : studentId, userId : userId } , { $push: { "attendances": attendance } } , { safe: true, upsert: true }, function (err, result) {

//        if (err) deferred.reject(err);

//        if (result) {
//            deferred.resolve(result);
//        } else {
//            deferred.resolve();
//        }
//    });
//    return deferred.promise;
//}

function getattendanceById(attendanceId) {

    var deferred = Q.defer();

    _attendance
        .findOne({ _id: attendanceId })
        .exec(function (err, result) {
            execHelper(deferred, err, result);
        });
    return deferred.promise;
}

function removeByDate(studentId, attendanceDate) {

    var gteDate = new Date(attendanceDate);
    var ltDate = new Date();
    ltDate.setDate(gteDate.getDate() + 1);

    var deferred = Q.defer();

    _student
        .update
        ({ _id: studentId },
        {
            $pull: { attendances: { "attendanceDate": { "$gte": gteDate, "$lt": ltDate } } }
        })
        .exec(function (err, student) {
            if (err) {
                deferred.reject(err);
            }
            if (student) {
                console.log('attendance successfully deleted!');
                deferred.resolve(student);
            } else {
                deferred.resolve();
            }
        });
    return deferred.promise;
}

function removeById(studentId, attendanceId) {

    var deferred = Q.defer();

    _student
        .update
        ({ _id: studentId },
        {
            $pull: { attendances: { "_id": attendanceId } }
        })
        .exec(function (err, student) {
            if (err) {
                deferred.reject(err);
            }
            if (student) {
                console.log('attendance successfully deleted!');
                deferred.resolve(student);
            } else {
                deferred.resolve();
            }
        });
    return deferred.promise;
}

function getAttendances(searchCriteria) {

    var deferred = Q.defer();

    var str = searchCriteria.orderBy.split("_");
    var sortObject = {};
    //var sortType = req.params.sortType;
    //var sortDir = req.params.sortDirection;
    var sortType = str[0];
    var sortDir = str[1] == "DESC" ? -1 : 1;
    sortObject[sortType] = sortDir;

    _user
        .aggregate(
        [
            {
                $match: {
                    'userType': searchCriteria.userType
                }
            },
            {
                $project: {
                    _id: 1,
                    phoneNumber: 1,
                    province: 1,
                    city: 1,
                    address: 1,
                    pinCode: 1,
                    firstName: 1,
                    simCountryISO2: 1,
                    loc: 1,
                    distance: 1,
                    email: 1,
                    attendances: 1,
                    createdDate: 1
                }
            },
            { "$sort": sortObject },
            { "$limit": searchCriteria.pageSize },
            { "$skip": (searchCriteria.page - 1) * searchCriteria.pageSize }
        ])
        .exec(function (err, result) {
            execHelper(deferred, err, result);
        });
    return deferred.promise;
}

//TODO
function searchAttendances(searchCriteria) {

    var deferred = Q.defer();

    var sortObject = {};
    if (!searchCriteria.orderBy) {
        searchCriteria.orderBy = "attendanceDate_DESC";
    }
    var str = searchCriteria.orderBy.split("_");
    //var sortType = req.params.sortType;
    //var sortDir = req.params.sortDirection;
    var sortType = str[0];
    var sortDir = str[1] == "DESC" ? -1 : 1;
    sortObject[sortType] = sortDir;

    var searchText = searchCriteria.searchText;

    _user
        .find({})
        .populate({
            path: 'attendances',
            select: '_id title description'
        })
        .select('_id firstName phoneNumber simCountryISO2 province city address pinCode loc email phoneNumber avatarUrl attendances')
        .find({
            $and: [
                {
                    '$or': [
                        //{ 'lastName': new RegExp(searchText, 'i') },
                        //{ 'firstName': new RegExp(searchText, 'i') },
                        { 'attendances.title': new RegExp(searchText, 'i') }
                        //{ 'attendances.description': new RegExp(searchText, 'i') }
                    ]
                },
                { 'userType': searchCriteria.userType }
            ]
        })
        //.find({
        //    '$or': [
        //        { 'lastName': new RegExp(searchText, 'i') },
        //        { 'firstName': new RegExp(searchText, 'i') },
        //        { 'attendances.title': new RegExp(searchText, 'i') }, 
        //        { 'attendances.description': new RegExp(searchText, 'i') },
        //        { 'province': new RegExp(searchText, 'i') },
        //        { 'city': new RegExp(searchText, 'i') },
        //        { 'address': new RegExp(searchText, 'i') }
        //    ]
        //})
        //.find({ 'userType': searchCriteria.userType })
        .sort(sortObject)
        .limit(searchCriteria.pageSize)
        .skip((searchCriteria.page - 1) * searchCriteria.pageSize)
        .lean()
        .exec(function (err, result) {
            execHelper(deferred, err, result);
        });
    return deferred.promise;
}

function getAllAttendances(searchCriteria) {

    var deferred = Q.defer();

    var sortObject = {};
    if (!searchCriteria.orderBy) {
        searchCriteria.orderBy = "distance_DESC";
    }
    var str = searchCriteria.orderBy.split("_");
    //var sortType = req.params.sortType;
    //var sortDir = req.params.sortDirection;
    var sortType = str[0];
    var sortDir = str[1] == "DESC" ? -1 : 1;
    sortObject[sortType] = sortDir;

    _student
        .find({})
        .populate({
            path: 'userRef',
            select: '_id firstName phoneNumber simCountryISO2 province city address pinCode loc email userType attendances',
            match: {
                userType: searchCriteria.userType
            }
        })
        .populate({
            path: 'attendances'
        })
        .select('_id currentClass currentSection currentRollNo userRef attendances')
        //.find({ 'userType': searchCriteria.userType })
        .sort(sortObject)
        .limit(searchCriteria.pageSize)
        .skip((searchCriteria.page - 1) * searchCriteria.pageSize)
        .lean()
        .exec(function (err, result) {
            execHelper(deferred, err, result);
        });
    return deferred.promise;
}

function getAttendanceDetail(userId) {

    var deferred = Q.defer();

    _student
        .find({})
        .select('_id userRef attendances')
        .populate({
            path: 'userRef',
            select: '_id firstName phoneNumber email simCountryISO2 province city address pinCode',
            match: { _id: userId }
        })
        .populate({
            path: 'attendances',
            select: '_id attendanceDate status'
        })
        .lean()
        .exec(function (err, result) {
            execHelper(deferred, err, result);
        });
    return deferred.promise;
}

function getById(studentId, attendanceId) {

    var deferred = Q.defer();

    _student
        .aggregate(
        //{ $match: { _id: ObjectId(studentId) } },
        { $unwind: '$attendances' },
        { $match: { "attendances._id": { $eq: ObjectId(attendanceId) } } },
        {
            $project: {
                "_id": 1,
                "attendances._id": 1,
                "attendances.status": 1,
                "attendances.attendanceDate": 1,
                "attendances.academicYear": 1
            }
        })
        .exec(function (err, result) {
            execHelper(deferred, err, result);
        });
    return deferred.promise;
}

function getAttendancesByUserId(userId, page, pageSize) {

    var deferred = Q.defer();

    _user
        .findOne({ _id: userId })
        .select('_id attendances')
        .populate({
            path: 'attendances',
            select: '_id title description'
        })
        .limit(pageSize)
        .skip(pageSize * page)
        .sort('-_id')
        .exec(function (err, result) {
            execHelper(deferred, err, result);
        });
    return deferred.promise;
}

function getAttendanceByStudentId(studentId) {

    var deferred = Q.defer();

    _student
        .findOne({ _id: studentId})
        .populate({
            path: 'userRef',
            select: '_id firstName phoneNumber simCountryISO2 province city address pinCode loc email userType attendances',
        })
        .populate({
            path: 'attendances'
        })
        .select('_id currentClass currentSection currentRollNo userRef attendances')
        //.find({ 'userType': searchCriteria.userType })
        .lean()
        .exec(function (err, result) {
            execHelper(deferred, err, result);
        });
    return deferred.promise;
}