﻿module.exports = function (app) {

    var officeStaffController = require('../controllers/officeStaff.server.controller');
    app.get('/officeStaff/index', officeStaffController.index);
    app.get('/officeStaff/register', officeStaffController.renderRegister);
    //app.get('/officeStaff/profile', officeStaffController.renderProfile);
    app.get('/officeStaff/myProfile', officeStaffController.renderMyProfile);
};