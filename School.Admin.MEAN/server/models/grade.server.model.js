﻿var mongoose = require('mongoose');

var gradeSchema = new mongoose.Schema({
    name: String,
    gradePoint: Number,
    markFrom: Number,
    markUpto: Number,
    comments: String
});

exports.gradeSchema = gradeSchema;
mongoose.model('grade', gradeSchema);