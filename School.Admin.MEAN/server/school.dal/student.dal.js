﻿var Q = require('q');
var _ = require('lodash');
var mongoose = require('mongoose'),
    async = require('async');

require('../school.common/appConstants');

require('../models/student.server.model');
require('../models/user.server.model');

var ObjectId = mongoose.Types.ObjectId;

var _parents = mongoose.model('parent');
var _students = mongoose.model('student');
var _user = mongoose.model('user');

require('../../server/school.common/helpers/deferredHelper');

var studentDal = {};

studentDal.add = add;
studentDal.getStudentById = getStudentById;
studentDal.searchStudents = searchStudents;
studentDal.getStudentContactDetailById = getStudentContactDetailById;
studentDal.getStudentsByAnyCriteria = getStudentsByAnyCriteria;
studentDal.getStudentProfileInfo = getStudentProfileInfo;
studentDal.getStudentByUserId = getStudentByUserId;
studentDal.count = count;
//studentDal.getStudentById = getStudentById;
studentDal.mobileUpdateProfile = mobileUpdateProfile;
//studentDal.getStudentIdByUserId = getStudentIdByUserId;
studentDal.insert = insert;
studentDal.update = update;
studentDal.mobileRegister = mobileRegister;
//studentDal.getAdmissionNumber = getAdmissionNumber;
//studentDal.getRollno = getRollno;
studentDal.getAllStudents = getAllStudents;
studentDal.getAllStudentsByClassAndSection = getAllStudentsByClassAndSection;
studentDal.remove = remove;
studentDal.updateProfile = updateProfile;

function add(studentModel) {

    var deferred = Q.defer();

    studentModel
        .save(function (err, result) {
            if (err) {
                console.log(err);
                deferred.reject(err);
            }

            if (result) {
                deferred.resolve(result);
            } else {
                deferred.resolve();
            }
        });

    return deferred.promise;
}

function addParent(parentModel) {

    var deferred = Q.defer();

    parentModel
        .save(function (err, result) {
            if (err) deferred.reject(err);

            if (result) {
                deferred.resolve(result);
            } else {
                deferred.resolve();
            }
        });

    return deferred.promise;
}

function searchStudents(pagingSortingParams, searchCriteria) {

    var deferred = Q.defer();

    _user
        .find({})
        //.populate({ path: 'user' })
        .populate({ path: 'exams' })
        //.populate({ path: 'followers' })
        //.populate({ path: 'favorites' })
        //.populate({ path: 'messages' })
        //.populate({ path: 'profileVisitors' })
        //.populate({ path: 'sharedProfiles' })
        .find({ 'userType': 1 })
        .select('_id province city address pinCode avatarUrl exams')
        .exec(function (err, result) {
            execHelper(deferred, err, result);
        });
    return deferred.promise;
}

function getStudentById(_id) {

    var deferred = Q.defer();

    _user
        .findOne({ '_id': _id })
        .populate({ path: 'user' })
        .populate({ path: 'exams' })
        //.populate({ path: 'followers' })
        //.populate({ path: 'favorites' })
        //.populate({ path: 'messages' })
        //.populate({ path: 'profileVisitors' })
        //.populate({ path: 'sharedProfiles' })
        .select('_id province city address pinCode avatarUrl exams')
        .exec(function (err, result) {
            execHelper(deferred, err, result);
        });
    return deferred.promise;
}

function getStudentContactDetailById(userId) {

    var deferred = Q.defer();

    _user
        .findOne({ _id: userId })
        .select('_id firstName address city pinCode province phoneNumber email avatarUrl')
        .exec(function (err, result) {
            execHelper(deferred, err, result);
        });
    return deferred.promise;
}

//function getStudentContactDetailById(studentId) {

//    var deferred = Q.defer();

//    _students
//    .findOne({ _id : studentId })
//    .select('_id address user')
//    .populate({
//        path: 'user',
//        select: 'firstName PhoneNumber email'
//    })
//    .exec(function (err, result) {
//      execHelper(deferred, err, result);
//    });
//    return deferred.promise;
//}

function mobileRegister(studentModel, userModel) {

    var userDal = require('./user.dal.js');
    var deferred = Q.defer();

    async.waterfall(
        [
            function (callback) {

                userModel.createdDate = new Date();

                userDal.add(userModel)
                    .then(function (user) {

                        if (user) {

                            studentModel.user = user._id;
                            studentModel.createdBy = user._id;
                            studentModel.createdDate = new Date();
                            callback(null, studentModel);
                        } else {
                            deferred.resolve();
                        }
                    })
                    .catch(function (err) {
                        if (err) deferred.reject(err);
                    });
            },
            function (studentModel, callback) {
                add(studentModel)
                    .then(function (student) {

                        if (student) {
                            var student = _.pick(student.toObject(), ['_id', 'province', 'city', 'address', 'pinCode', 'avatarUrl', 'exams']);
                            deferred.resolve(student);
                        } else {
                            deferred.resolve();
                        }
                    })
                    .catch(function (err) {
                        if (err) deferred.reject(err);
                    });
            }
        ], function (errs, results) {
            if (errs) {
                async.each(results, rollback, function () {
                    console.log('Rollback done.');
                    res.status(400).send(errs);
                });
            } else {
                console.log('Done.');
                deferred.resolve(results);
            }
        });

    function rollback(doc, callback) {
        if (!doc) { callback(); }
        else {
            userModel.findByIdAndRemove(doc._id, function (err, doc) {
                console.log('Rolled-back document: ' + doc);
                callback();
            });
        }
    }
    return deferred.promise;
}

function getStudentsByAnyCriteria(searchCriteria) {

    var deferred = Q.defer();

    var str = searchCriteria.orderBy.split("_");
    var sortObject = {};
    //var sortType = req.params.sortType;
    //var sortDir = req.params.sortDirection;
    var sortType = str[0];
    var sortDir = str[1] == "DESC" ? -1 : 1;
    sortObject[sortType] = sortDir;

    _user
        .aggregate(
        [
            //{
            //    '$geoNear': {
            //        'near': [searchCriteria.coords[0], searchCriteria.coords[1]],
            //        'spherical': true,
            //        'distanceField': 'dist',
            //        'maxDistance': searchCriteria.distance,
            //        'distanceMultiplier': 6371
            //    }
            //},
            {
                $match: {
                    $and: [
                        { 'userType': searchCriteria.userType },
                        {
                            $or: [
                                { 'country': new RegExp(searchCriteria.location, 'i') },
                                { 'province': new RegExp(searchCriteria.location, 'i') },
                                { 'city': new RegExp(searchCriteria.location, 'i') },
                                { 'address': new RegExp(searchCriteria.location, 'i') }
                                //{ 'geocoderAddress': new RegExp(searchCriteria.location, 'i') },
                                //{ 'geocoderCountry': new RegExp(searchCriteria.location, 'i') }
                            ]
                        },
                        {
                            $or: [{ 'exams.title': new RegExp(searchCriteria.searchText, 'i') },
                                { 'exams.description': new RegExp(searchCriteria.searchText, 'i') }]
                        }
                    ]
                }
            },
            //{
            //    $match: {
            //        $or: [
            //            { 'exams.title': new RegExp(searchCriteria.searchText, 'i') }, 
            //            { 'exams.description': new RegExp(searchCriteria.searchText, 'i') },
            //            { 'country': new RegExp(searchCriteria.searchText, 'i') },
            //            { 'province': new RegExp(searchCriteria.searchText, 'i') },
            //            { 'city': new RegExp(searchCriteria.searchText, 'i') },
            //            { 'address': new RegExp(searchCriteria.searchText, 'i') },
            //            { 'geocoderAddress': new RegExp(searchCriteria.location, 'i') },
            //            { 'geocoderCountry': new RegExp(searchCriteria.location, 'i') }
            //            { 'lastName': new RegExp(searchCriteria.searchText, 'i') },
            //            { 'firstName': new RegExp(searchCriteria.searchText, 'i') }
            //        ]
            //    }
            //},
            {
                $project: {
                    _id: 1,
                    province: 1,
                    city: 1,
                    address: 1,
                    pinCode: 1,
                    firstName: 1,
                    //address: 1,
                    //simCountryISO2: 1,
                    loc: 1,
                    dist: 1,
                    email: 1,
                    exams: 1
                }
            },
            { "$sort": sortObject },
            { "$limit": searchCriteria.pageSize },
            { "$skip": (searchCriteria.page - 1) * searchCriteria.pageSize }
        ])
        .exec(function (err, result) {
            execHelper(deferred, err, result);
        });

    /*
    _user
    .find({})
    //.select('_id province city address pinCode firstName phoneNumber simCountryISO2 loc email exams')
    .populate({
        path: 'exams',
        select: '_id title description'
    })
    //.populate({
    //    path: 'user',
    //    select: '_id firstName phoneNumber simCountryISO2 loc email'
    //})
    //.populate({ path: 'user' })
    .populate({ path: 'followers' })
    .populate({ path: 'favorites' })
    .populate({ path: 'messages' })
    .populate({ path: 'profileVisitors' })
    //.populate({ path: 'sharedProfiles' })
    .find({
        $and: [
            {
                $or: [
                    { 'exams.title': new RegExp(searchText, 'i') }, 
                    { 'exams.description': new RegExp(searchText, 'i') },
                    { 'country': new RegExp(searchText, 'i') },
                    { 'province': new RegExp(searchText, 'i') },
                    { 'city': new RegExp(searchText, 'i') },
                    { 'address': new RegExp(searchText, 'i') },
                    { 'geocoderAddress': new RegExp(searchText, 'i') },
                    { 'geocoderCountry': new RegExp(searchText, 'i') },
                    { 'lastName': new RegExp(searchText, 'i') },
                    { 'firstName': new RegExp(searchText, 'i') },
                ]
            }
        ]
    })
    .find({ 'userType': searchCriteria.userType })
    //.find({
    //    'loc' : {
    //        '$geoNear': {
    //            'near': {
    //                'type': 'Point',
    //                'coordinates': [searchCriteria.coords[1] , searchCriteria.coords[0]]
    //            },
    //            'spherical': true, 
    //            'distanceField': 'dist',
    //            'maxDistance': searchCriteria.distance
    //        }
    //    }
    //})

    //.find({
    //    'loc' : {
    //        "$nearSphere": {
    //            "$geometry": {
    //                type: "Point", 
    //                'coordinates': [searchCriteria.coords[1], searchCriteria.coords[0]],
    //                    //near: [searchCriteria.coords[1], searchCriteria.coords[0]],
    //            },
    //            'distanceField': "dist",
    //            'distanceMultiplier': 6371,
    //            'maxDistance': searchCriteria.distance,
    //            //query: { type: "public" },
    //            //includeLocs: "location",
    //            //uniqueDocs: true, 
    //            //num: 5,
    //            'spherical': true
    //        }
    //    }
    //})

    .select('_id province city address pinCode firstName phoneNumber simCountryISO2 loc email exams')
    .populate('dist')
    .exec(function (err, result) {
	    execHelper(deferred, err, result);
    });
    */
    return deferred.promise;
}

function getStudentProfileInfo(userId) {

    var deferred = Q.defer();

    _user
        .findOne({ _id: userId })
        .populate({ path: 'exams' })
        //.populate({ path: 'followers' })
        //.populate({ path: 'favorites' })
        //.populate({ path: 'messages' })
        //.populate({ path: 'profileVisitors' })
        //.populate({ path: 'sharedProfiles' })
        .exec(function (err, result) {
            execHelper(deferred, err, result);
        });
    return deferred.promise;
}

//function getStudentProfileInfo(userId) {

//    var deferred = Q.defer();

//    _students
//    .findOne({})
//    .populate({
//        path: 'user',
//        match : { _id : userId },
//    })
//    .populate({ path: 'user' })
//    .populate({ path: 'exams' })
//    .populate({ path: 'followers' })
//    .populate({ path: 'favorites' })
//    .populate({ path: 'messages' })
//    .populate({ path: 'profileVisitors' })
//    .populate({ path: 'sharedProfiles' })
//    .exec(function (err, student) {

//        if (err) {
//            deferred.reject(err);
//        }
//        if (student) {
//            deferred.resolve(student);
//        } else {
//            deferred.resolve();
//        }
//    });
//    return deferred.promise;
//}

function getStudentByUserId(userId) {

    var deferred = Q.defer();

    _students
        .findOne({ userRef: ObjectId(userId) })
        .populate({
            path: 'userRef',
            select: '_id firstName lastName birthDate phoneNumber bloodGroup email username address city province pinCode'
        })
        .select('_id currentClass currentSection currentRollNo userRef')
        .exec(function (err, result) {
            execHelper(deferred, err, result);
        });
    return deferred.promise;
}

//function getStudentIdByUserId(userId) {

//    var deferred = Q.defer();

//    _students
//    .findOne({ 'user._id' : userId })
//    .select('_id')
//    .exec(function (err, student) {

//        if (err) {
//            deferred.reject(err);
//        }
//        if (student) {
//            deferred.resolve(student);
//        } else {
//            deferred.resolve();
//        }
//    });
//    return deferred.promise;
//}

const MongoClient = require('mongodb').MongoClient;
var db;

MongoClient.connect('mongodb://127.0.0.1/School', (err, client) => {
    if (err) return console.log(err)
    db = client.db('School') // whatever your database name is
})

function count() {

    var deferred = Q.defer();

    db.collection('students').count(function (err, count) {
        if (err) {
            deferred.reject(err);
        }
        if (count) {
            deferred.resolve(count);
        } else {
            deferred.resolve();
        }
    });
    return deferred.promise;
}

//function count() {

//    var deferred = Q.defer();

//    _students
//        .count({})
//        .exec(function (err, count) {

//            if (err) {
//                deferred.reject(err);
//            }
//            if (count) {
//                deferred.resolve(count);
//            } else {
//                deferred.resolve();
//            }
//        });
//    return deferred.promise;
//}

function getStudentById(studentId) {

    var deferred = Q.defer();

    _students
        .findOne({ '_id': studentId })
        .populate({ path: 'user' })
        .populate({ path: 'exams' })
        //.populate({ path: 'messages' })
        .exec(function (err, result) {
            execHelper(deferred, err, result);
        });
    return deferred.promise;
}

function mobileUpdateProfile(updateProfileModel) {

    var deferred = Q.defer();
    //TODO password check add future
    _user.findOne({ _id: updateProfileModel._id }, function (err, user) {
        if (err)
            deferred.reject(err);

        if (user) {
            //if (updateProfileModel.loginType == loginTypeEnum.SocialAccount) {
            user.email = updateProfileModel.email ? updateProfileModel.email : user.email;
            user.phoneNumber = updateProfileModel.phoneNumber ? updateProfileModel.phoneNumber : user.phoneNumber;
            //}

            user.firstName = updateProfileModel.firstName ? updateProfileModel.firstName : user.firstName;
            user.lastName = updateProfileModel.lastName ? updateProfileModel.lastName : user.lastName;

            user.address = updateProfileModel.address ? updateProfileModel.address : user.address;
            user.city = updateProfileModel.city ? updateProfileModel.city : user.city;
            user.province = updateProfileModel.province ? updateProfileModel.province : user.province;
            user.pinCode = updateProfileModel.pinCode ? updateProfileModel.pinCode : user.pinCode;
            //user.country = updateProfileModel.country ? updateProfileModel.country : user.country;
            //user.countryISO2 = updateProfileModel.countryISO2 ? updateProfileModel.countryISO2 : user.countryISO2;
            //user.countryDialCode = updateProfileModel.countryDialCode ? updateProfileModel.countryDialCode : user.countryDialCode;

            user.avatarUrl = updateProfileModel.avatarUrl ? updateProfileModel.avatarUrl : user.avatarUrl;
            //user.AvatarMimeType = mimeType;
            //user.AvatarFileName = fileName;

            var ObjectId = mongoose.Types.ObjectId;
            for (var i = 0; i < updateProfileModel.exams.length; i++) {
                updateProfileModel.exams[i]._id = new ObjectId;
                updateProfileModel.exams[i].createdBy = updateProfileModel._id;
                updateProfileModel.exams[i].createdDate = new Date();
            }
            user.exams = updateProfileModel.exams ? updateProfileModel.exams : user.exams;
            user.isProfileIncomplete = true;

            user.updatedBy = updateProfileModel._id;
            user.updatedDate = new Date();

            user.save(function (err, result) {
                if (err) deferred.reject(err);

                if (result) {
                    result = _.pick(result.toObject(), ['_id', 'username', 'email', 'isProfileIncomplete', 'firstName', 'phoneNumber', 'province',
                        'city', 'address', 'pinCode', 'countryDialCode', 'avatarUrl', 'exams']);
                    deferred.resolve(result);
                } else {
                    deferred.resolve();
                }
            });
        }
    });
    return deferred.promise;
}

function insert(studentModel, parentModel, userModel) {

    var userDal = require('./user.dal.js');
    var deferred = Q.defer();

    async.waterfall(
        [
            function (callback) {

                userModel.createdDate = new Date();

                userDal.add(userModel)
                    .then(function (user) {

                        if (user) {

                            //studentModel.user._id = user._id;
                            studentModel.userRef = user._id;
                            parentModel.userRef = user._id;

                            studentModel.createdBy = user._id;
                            studentModel.createdDate = new Date();
                            callback(null, parentModel);
                        } else {
                            deferred.resolve();
                        }
                    })
                    .catch(function (err) {
                        if (err) {
                            console.log(err);
                            deferred.reject(err);
                        }
                    });
            },

            function (parentModel, callback) {
                addParent(parentModel)
                    .then(function (parent) {

                        if (parent) {
                            studentModel.parentRef = parent._id;
                            callback(null, studentModel);
                        } else {
                            deferred.resolve();
                        }
                    })
                    .catch(function (err) {
                        if (err) {
                            console.log(err);
                            callback(err, userModel);
                            deferred.reject(err);
                        }
                    });
            },

            function (studentModel, callback) {
                add(studentModel)
                    .then(function (student) {

                        if (student) {
                            deferred.resolve(student);
                        } else {
                            deferred.resolve();
                        }
                    })
                    .catch(function (err) {
                        if (err) {
                            console.log(err);
                            callback(err, userModel);
                            deferred.reject(err);
                        }
                    });
            }
        ], function (errs, results) {
            if (errs) {
                async.each(results, rollback, function () {
                    console.log('Rollback done.');
                    //res.status(400).send(errs);
                });
            } else {
                console.log('Done.');
                deferred.resolve(results);
            }
        });

    function rollback(doc, callback) {
        if (!doc) { callback(); }
        else {
            _user.findByIdAndRemove(doc._id, function (err, doc) {
                console.log('Rolled-back document: ' + doc);
                callback();
            });
        }
    }
    return deferred.promise;
}

//function mobileRegister(userModel) {

//    var deferred = Q.defer();

//    userModel
//        .save(function (err, result) {
//            if (err) deferred.reject(err);
//            if (result) {
//                var result = _.pick(result.toObject(), ['_id', 'province', 'city', 'address', 'pinCode', 'avatarUrl', 'exams']);
//                deferred.resolve(result);
//            } else {
//                deferred.resolve();
//            }
//        });
//    return deferred.promise;
//}

function getAllStudents() {

    var deferred = Q.defer();

    _students
        .aggregate(
        { $lookup: { from: "users", localField: "userRef", foreignField: "_id", as: "user" } },
        { $unwind: "$user" },
        { $lookup: { from: "parents", localField: "parentRef", foreignField: "_id", as: "parent" } },
        {
            $unwind: {
                path: "$parent",
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $project: {
                "_id": 1,
                "currentClass": 1,
                "currentSection": 1,
                "currentRollno": 1,

                "parentRef": {
                    _id: "$parent._id",

                    fatherName: "$parent.fatherName",
                    fatherOccupation: "$parent.fatherOccupation",
                    fatherContactNumber: "$parent.fatherContactNumber",
                    fatherEmail: "$parent.fatherEmail",

                    motherName: "$parent.motherName",
                    motherOccupation: "$parent.motherOccupation",
                    motherContactNumber: "$parent.motherContactNumber",
                    motherEmail: "$parent.motherEmail"
                },

                "userRef": {
                    _id: "$user._id",

                    firstName: "$user.firstName",
                    lastName: "$user.lastName",
                    birthDate: "$user.birthDate",
                    phoneNumber: "$user.phoneNumber",
                    bloodGroup: "$user.bloodGroup",

                    email: "$user.email",
                    username: "$user.username",
                    //password: "$user.password",

                    address: "$user.address",
                    city: "$user.city",
                    province: "$user.province",
                    pinCode: "$user.pinCode"
                }
            }
        })
        .exec(function (err, result) {
            execHelper(deferred, err, result);
        });
    return deferred.promise;
}

function getAllStudentsByClassAndSection(className, section) {

    var deferred = Q.defer();

    _students
        .find({
            $and: [
                { 'currentClass': className },
                { 'currentSection': section }
            ]
        })
        .populate({
            path: 'userRef',
            select: '_id firstName phoneNumber email simCountryISO2 province city address pinCode'
        })
        .exec(function (err, result) {
            execHelper(deferred, err, result);
        });
    return deferred.promise;
}

function update(updateProfileModel, studentModel, parentModel, avatarUrl) {

    var deferred = Q.defer();

    async.waterfall(
        [
            function (callback) {

                _user.findOne({ _id: updateProfileModel._id }, function (err, user) {
                    if (err) {
                        deferred.reject(err);
                        console.log(err);
                    }
                    if (user) {

                        user.email = updateProfileModel.email ? updateProfileModel.email : user.email;
                        user.username = updateProfileModel.username ? updateProfileModel.username : user.username;
                        user.password = updateProfileModel.password ? updateProfileModel.password : user.password;

                        user.firstName = updateProfileModel.firstName ? updateProfileModel.firstName : user.firstName;
                        user.lastName = updateProfileModel.lastName ? updateProfileModel.lastName : user.lastName;
                        user.birthDate = updateProfileModel.birthDate ? updateProfileModel.birthDate : user.birthDate;
                        user.phoneNumber = updateProfileModel.phoneNumber ? updateProfileModel.phoneNumber : user.phoneNumber;
                        user.bloodGroup = updateProfileModel.bloodGroup ? updateProfileModel.bloodGroup : user.bloodGroup;

                        user.address = updateProfileModel.address ? updateProfileModel.address : user.address;
                        user.city = updateProfileModel.city ? updateProfileModel.city : user.city;
                        user.province = updateProfileModel.province ? updateProfileModel.province : user.province;
                        user.pinCode = updateProfileModel.pinCode ? updateProfileModel.pinCode : user.pinCode;
                        //user.country = updateProfileModel.country ? updateProfileModel.country : user.country;
                        //user.countryISO2 = updateProfileModel.countryISO2 ? updateProfileModel.countryISO2 : user.countryISO2;
                        //user.countryDialCode = updateProfileModel.countryDialCode ? updateProfileModel.countryDialCode : user.countryDialCode;

                        user.updatedBy = updateProfileModel._id;
                        user.updatedDate = new Date();

                        //user.status = request.status;
                        user.save(function (err, result) {
                            if (result._id) {

                                callback(null, result._id);
                            }
                        });
                    }
                });
            },
            function (userId, callback) {
                _parents.findOne({ _id: parentModel._id }, function (err, parent) {
                    if (err) {
                        console.log(err);
                        deferred.reject(err);
                    }
                    if (parent) {
                        parent.avatarUrl = avatarUrl;
                        //TODO
                        //parent.AvatarMimeType = mimeType;
                        //parent.AvatarFileName = fileName;

                        parent.fatherName = parentModel.fatherName ? parentModel.fatherName : parent.fatherName;
                        parent.fatherQualification = parentModel.fatherQualification ? parentModel.fatherQualification : parent.fatherQualification;
                        parent.fatherContactNumber = parentModel.fatherContactNumber ? parentModel.fatherContactNumber : parent.fatherContactNumber;
                        parent.fatherEmail = parentModel.fatherEmail ? parentModel.fatherEmail : parent.fatherEmail;

                        parent.motherName = parentModel.motherName ? parentModel.motherName : parent.motherName;
                        parent.motherQualification = parentModel.motherQualification ? parentModel.motherQualification : parent.motherQualification;
                        parent.motherContactNumber = parentModel.motherContactNumber ? parentModel.motherContactNumber : parent.motherContactNumber;
                        parent.motherEmail = parentModel.motherEmail ? parentModel.motherEmail : parent.motherEmail;

                        //parent.updatedBy = parentModel.UserId;
                        parent.updatedDate = new Date();

                        parent.save(function (err, result) {
                            if (err) {
                                console.log(err);
                            }
                            if (result._id) {
                                callback(null, parent._id);
                            }
                        });
                    }
                    else {
                        parentModel.userRef = userId;
                        addParent(parentModel)
                            .then(function (parent) {

                                if (parent) {
                                    callback(null, parent._id);
                                } else {
                                    deferred.resolve();
                                }
                            })
                            .catch(function (err) {
                                if (err) {
                                    console.log(err);
                                    callback(err, userModel);
                                    deferred.reject(err);
                                }
                            });
                    }
                });
            },
            function (parentId, callback) {
                _students.findOne({ _id: studentModel._id }, function (err, student) {
                    if (err) {
                        console.log(err);
                        deferred.reject(err);
                    }
                    if (student) {
                        student.avatarUrl = avatarUrl;
                        //TODO
                        //student.AvatarMimeType = mimeType;
                        //student.AvatarFileName = fileName;

                        student.designation = studentModel.designation ? studentModel.designation : student.designation;
                        student.qualification = studentModel.qualification ? studentModel.qualification : student.qualification;
                        student.experience = studentModel.experience ? studentModel.experience : student.experience;

                        student.parentRef = parentId;

                        //student.updatedBy = studentModel.UserId;
                        student.updatedDate = new Date();

                        student.save(function (err, result) {
                            if (err) {
                                console.log(err);
                            }
                            if (result._id) {

                                callback(null, updateProfileModel.studentId);
                            }
                        });
                    }

                });
            }
        ], function (errs, results) {
            if (errs) {
                async.each(results, rollback, function () {
                    console.log('Rollback done.');
                    res.status(400).send(errs);
                });
            } else {
                console.log('Done.');
                deferred.resolve(results);
            }
        });

    function rollback(doc, callback) {
        if (!doc) { callback(); }
        else {
            userModel.findByIdAndRemove(doc._id, function (err, doc) {
                console.log('Rolled-back document: ' + doc);
                callback();
            });
        }
    }
    return deferred.promise;
}

function updateProfile(updateProfileModel, studentModel, parentModel, avatarUrl) {

    var deferred = Q.defer();

    async.waterfall(
        [
            function (callback) {

                _user.findOne({ _id: updateProfileModel._id }, function (err, user) {
                    if (err) {
                        deferred.reject(err);
                        console.log(err);
                    }
                    if (user) {

                        user.firstName = updateProfileModel.firstName ? updateProfileModel.firstName : user.firstName;
                        user.lastName = updateProfileModel.lastName ? updateProfileModel.lastName : user.lastName;
                        user.birthDate = updateProfileModel.birthDate ? updateProfileModel.birthDate : user.birthDate;
                        user.phoneNumber = updateProfileModel.phoneNumber ? updateProfileModel.phoneNumber : user.phoneNumber;
                        user.bloodGroup = updateProfileModel.bloodGroup ? updateProfileModel.bloodGroup : user.bloodGroup;

                        user.address = updateProfileModel.address ? updateProfileModel.address : user.address;
                        user.city = updateProfileModel.city ? updateProfileModel.city : user.city;
                        user.province = updateProfileModel.province ? updateProfileModel.province : user.province;
                        user.pinCode = updateProfileModel.pinCode ? updateProfileModel.pinCode : user.pinCode;
                        //user.country = updateProfileModel.country ? updateProfileModel.country : user.country;
                        //user.countryISO2 = updateProfileModel.countryISO2 ? updateProfileModel.countryISO2 : user.countryISO2;
                        //user.countryDialCode = updateProfileModel.countryDialCode ? updateProfileModel.countryDialCode : user.countryDialCode;

                        user.updatedBy = updateProfileModel._id;
                        user.updatedDate = new Date();

                        //user.status = request.status;
                        user.save(function (err, result) {
                            if (result._id) {

                                callback(null, result._id);
                            }
                        });
                    }
                });
            },
            function (userId, callback) {
                _parents.findOne({ _id: parentModel._id }, function (err, parent) {
                    if (err) {
                        console.log(err);
                        deferred.reject(err);
                    }
                    if (parent) {
                        parent.avatarUrl = avatarUrl;
                        //TODO
                        //parent.AvatarMimeType = mimeType;
                        //parent.AvatarFileName = fileName;

                        parent.fatherName = parentModel.fatherName ? parentModel.fatherName : parent.fatherName;
                        parent.fatherQualification = parentModel.fatherQualification ? parentModel.fatherQualification : parent.fatherQualification;
                        parent.fatherContactNumber = parentModel.fatherContactNumber ? parentModel.fatherContactNumber : parent.fatherContactNumber;
                        parent.fatherEmail = parentModel.fatherEmail ? parentModel.fatherEmail : parent.fatherEmail;

                        parent.motherName = parentModel.motherName ? parentModel.motherName : parent.motherName;
                        parent.motherQualification = parentModel.motherQualification ? parentModel.motherQualification : parent.motherQualification;
                        parent.motherContactNumber = parentModel.motherContactNumber ? parentModel.motherContactNumber : parent.motherContactNumber;
                        parent.motherEmail = parentModel.motherEmail ? parentModel.motherEmail : parent.motherEmail;

                        //parent.updatedBy = parentModel.UserId;
                        parent.updatedDate = new Date();

                        parent.save(function (err, result) {
                            if (err) {
                                console.log(err);
                            }
                            if (result._id) {
                                callback(null, parent._id);
                            }
                        });
                    }
                    else {
                        parentModel.userRef = userId;
                        addParent(parentModel)
                            .then(function (parent) {

                                if (parent) {
                                    callback(null, parent._id);
                                } else {
                                    deferred.resolve();
                                }
                            })
                            .catch(function (err) {
                                if (err) {
                                    console.log(err);
                                    callback(err, userModel);
                                    deferred.reject(err);
                                }
                            });
                    }
                });
            },
            function (parentId, callback) {
                _students.findOne({ _id: studentModel._id }, function (err, student) {
                    if (err) {
                        console.log(err);
                        deferred.reject(err);
                    }
                    if (student) {
                        student.avatarUrl = avatarUrl;
                        //TODO
                        //student.AvatarMimeType = mimeType;
                        //student.AvatarFileName = fileName;

                        student.designation = studentModel.designation ? studentModel.designation : student.designation;
                        student.qualification = studentModel.qualification ? studentModel.qualification : student.qualification;
                        student.experience = studentModel.experience ? studentModel.experience : student.experience;

                        student.parentRef = parentId;

                        //student.updatedBy = studentModel.UserId;
                        student.updatedDate = new Date();

                        student.save(function (err, result) {
                            if (err) {
                                console.log(err);
                            }
                            if (result._id) {

                                callback(null, updateProfileModel.studentId);
                            }
                        });
                    }

                });
            }
        ], function (errs, results) {
            if (errs) {
                async.each(results, rollback, function () {
                    console.log('Rollback done.');
                    res.status(400).send(errs);
                });
            } else {
                console.log('Done.');
                deferred.resolve(results);
            }
        });

    function rollback(doc, callback) {
        if (!doc) { callback(); }
        else {
            userModel.findByIdAndRemove(doc._id, function (err, doc) {
                console.log('Rolled-back document: ' + doc);
                callback();
            });
        }
    }
    return deferred.promise;
}

function updateParent(parentUpdateCondition, parentUpdateSet) {

    var deferred = Q.defer();

    _parents.findOneAndUpdate(parentUpdateCondition, { $set: parentUpdateSet }, { new: true }, function (err, parent) {
        if (err) {
            console.log(err);
            deferred.reject(err);
        }
        if (parent) {
            parent = _.pick(parent.toObject(), ['_id', 'parentname', 'email', 'isProfileIncomplete', 'firstName', 'phoneNumber', 'province', 'city',
                'address', 'pinCode', 'countryDialCode', 'avatarUrl', 'exams']);
            deferred.resolve(parent);
        } else {
            deferred.resolve();
        }
    });
    return deferred.promise;
}

function updateStudent(studentUpdateCondition, studentUpdateSet) {

    var deferred = Q.defer();

    _students.findOneAndUpdate(studentUpdateCondition, { $set: studentUpdateSet }, { new: true }, function (err, student) {
        if (err) {
            console.log(err);
            deferred.reject(err);
        }
        if (student) {
            student = _.pick(student.toObject(), ['_id', 'studentname', 'email', 'isProfileIncomplete', 'firstName', 'phoneNumber', 'province', 'city',
                'address', 'pinCode', 'countryDialCode', 'avatarUrl', 'exams']);
            deferred.resolve(student);
        } else {
            deferred.resolve();
        }
    });
    return deferred.promise;
}

function remove(studentId, parentId, userId) {

    var userDal = require('./user.dal.js');
    var deferred = Q.defer();

    async.waterfall(
        [
            function (callback) {

                //userModel.createdDate = new Date();

                var loggedInUserId;

                var userUpdateCondition = { $and: [] };
                userUpdateCondition.$and.push({
                    _id: userId
                });

                var userUpdateSet = {
                    isDeleted: true,
                    //updatedBy: loggedInUserId,
                    updatedDate: new Date()
                };

                userDal.update(userUpdateCondition, userUpdateSet)
                    .then(function (user) {

                        if (user) {

                            callback(null, parentId);
                        } else {
                            deferred.resolve();
                        }
                    })
                    .catch(function (err) {
                        if (err) {
                            console.log(err);
                            deferred.reject(err);
                        }
                    });
            },
            function (parentId, callback) {
                if (parentId && !(typeof obj === 'undefined')) {
                    var loggedInUserId;

                    var parentUpdateCondition = { $and: [] };
                    parentUpdateCondition.$and.push({
                        _id: parentId
                    });

                    var parentUpdateSet = {
                        isDeleted: true,
                        //updatedBy: loggedInUserId,
                        updatedDate: new Date()
                    };

                    updateParent(parentUpdateCondition, parentUpdateSet)
                        .then(function (parent) {

                            if (parent) {
                                callback(null, studentId);
                            } else {
                                deferred.resolve();
                            }
                        })
                        .catch(function (err) {
                            if (err) {
                                console.log(err);
                                callback(err, userModel);
                                //deferred.reject(err);
                            }
                        });
                }
                else {
                    callback(null, studentId);
                }
            },
            function (studentId, callback) {

                var loggedInUserId;

                var studentUpdateCondition = { $and: [] };
                studentUpdateCondition.$and.push({
                    _id: studentId
                });

                var studentUpdateSet = {
                    isDeleted: true,
                    //updatedBy: loggedInUserId,
                    updatedDate: new Date()
                };

                updateStudent(studentUpdateCondition, studentUpdateSet)
                    .then(function (student) {

                        if (student) {
                            deferred.resolve(student);
                        } else {
                            deferred.resolve();
                        }
                    })
                    .catch(function (err) {
                        if (err) {
                            console.log(err);
                            callback(err, userModel);
                            //deferred.reject(err);
                        }
                    });
            }
        ], function (errs, results) {
            if (errs) {
                async.each(results, rollback, function () {
                    console.log('Rollback done.');
                    //res.status(400).send(errs);
                });
            } else {
                console.log('Done.');
                deferred.resolve(results);
            }
        });

    function rollback(doc, callback) {
        if (!doc) { callback(); }
        else {
            _user.findOne({ _id: updateProfileModel.userId }, function (err, user) {
                if (!user) {

                    user.isDeleted = false;
                    user.updatedBy = updateProfileModel._id;
                    user.updatedDate = new Date();
                }
                user.save(function (err, doc) {
                    console.log('Rolled-back document: ' + doc);
                    callback();
                });
            });
        }
    }
    return deferred.promise;
}

module.exports = studentDal;