﻿'use strict';

/**
 * Module dependencies.
 */
var passport = require('passport');

module.exports = function (app) {
    // User Routes
    var users = require('../controllers/user.server.controller');

    // Setting up the users profile api
    app.route('/users/me').get(users.me);
    app.route('/users').put(users.update);

    // Setting up the verification code api
    app.route('/account/sendCode').get(users.renderSendCode);
    app.route('/account/verifyCode').get(users.renderVerifyCode);

    // Setting up the users password api
    app.route('/users/password').post(users.changePassword);

    app.route('/account/resetPassword').get(users.renderResetPassword);
    app.route('/resetPassword').get(users.renderResetPassword);

    app.route('/account/changePassword').get(users.renderChangePassword);
    app.route('/changePassword').get(users.renderChangePassword);

    app.route('/account/resetPasswordConfirmation').get(users.renderResetPasswordConfirm);
    app.route('/resetPasswordConfirmation').get(users.renderResetPasswordConfirm);

    app.route('/account/externalLoginConfirmation').get(users.renderExternalLoginConfirm);
    app.route('/externalLoginConfirmation').get(users.renderExternalLoginConfirm);

    app.route('/account/confirmEmail').get(users.renderConfirmEmail);
    app.route('/confirmEmail').get(users.renderConfirmEmail);

    app.route('/account/forgotPassword').get(users.renderForgotPassword);
    app.route('/auth/forgot').post(users.forgot);

    app.route('/account/forgotPasswordConfirmation').get(users.renderForgotPasswordConfirm);
    app.route('/forgotPasswordConfirmation').get(users.renderForgotPasswordConfirm);

    app.route('/account/externalLoginFailure').get(users.renderExternalLoginFailure);
    app.route('/externalLoginFailure').get(users.renderExternalLoginFailure);

    app.route('/auth/reset/:token').get(users.validateResetToken);
    app.route('/auth/reset/:token').post(users.reset);

    // Setting up the users authentication api
    //app.route('/auth/signup').post(users.signup);
    //app.route('/auth/signin').post(users.signin);
    //app.route('/auth/signout').get(users.signout);

    // Finish by binding the user middleware
    app.param('userId', users.userById);
};