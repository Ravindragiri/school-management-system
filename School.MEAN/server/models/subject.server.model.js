﻿var mongoose = require('mongoose');

var subjectSchema = new mongoose.Schema({

    code: String,
    name: String,
    examtype: String,
    marks: String,
    maxMarks: String,

    createdBy: {
        type: mongoose.Schema.Types.ObjectId
    },
    createdDate: {
        type: Date
    },
    updatedBy: {
        type: mongoose.Schema.Types.ObjectId
    },
    updatedDate: Date
});

exports.subjectSchema = subjectSchema;
mongoose.model('subject', subjectSchema);