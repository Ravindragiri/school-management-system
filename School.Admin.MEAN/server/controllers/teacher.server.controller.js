﻿var config = require('../config/config');
require('../school.common/appConstants');

exports.index = function (req, res) {

    res.render('teacher/index', {
        pTitle: 'Manage Teacher | Admin'
    });
};

exports.renderRegister = function (req, res) {
    res.render('teacher/register', {
        pTitle: 'Teacher Registration | School'
    });
};

exports.renderMyProfile = function (req, res) {
    res.render('teacher/myProfile', {
        pTitle: 'Teacher My Profile | School'
    });
};
