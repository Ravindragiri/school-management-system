﻿var mongoose = require('mongoose');

var studentRequestSchema = new mongoose.Schema({

    title: String,
    desc: String,
    startDate: Date,
    endDate: Date
});

mongoose.model('studentRequest', studentRequestSchema);