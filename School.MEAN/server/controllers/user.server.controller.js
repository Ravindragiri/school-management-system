﻿'use strict';

/**
 * Module dependencies.
 */
var _ = require('lodash');

/**
 * Extend user's controller
 */
module.exports = _.extend(
    require('../controllers/users/user.authentication'),
	require('../controllers/users/user.authorization'),
	require('../controllers/users/user.password'),
	require('../controllers/users/user.profile')
);