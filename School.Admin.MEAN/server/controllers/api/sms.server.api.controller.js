﻿var twilio = require('twilio');
var config = require('../../../config/config');

exports.sendSMS = function (req, res) {
    var client = new twilio(config.twilioAccountSid, config.twilioAuthToken);

    var message = req.body.message;
    var to = req.body.to;
    var from = req.body.from;

    client.messages.create({
        body: message,
        to: to,  // Text this number
        from: from // From a valid Twilio number
    })
        //.then((message) => console.log(message.sid));
        .then(function (message) {
            if (message) {
                console.log(message.sid);
                sendSuccessResponse(res, message);
            } else {
                sendFailResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
}