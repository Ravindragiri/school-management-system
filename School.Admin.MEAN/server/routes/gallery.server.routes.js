﻿module.exports = function (app) {

    var gallery = require('../controllers/gallery.server.controller');

    app.get('/gallery/index', gallery.index);
    app.get('/gallery', gallery.index);
};