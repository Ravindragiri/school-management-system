﻿var Q = require('q');
var mongoose = require('mongoose');

require('../models/holiday.server.model');
require('../models/user.server.model');

var userDal = require('../../server/school.dal/user.dal.js');

var _holidayModel = mongoose.model('holiday');
var _user = mongoose.model('user');

require('../../server/school.common/helpers/deferredHelper');

var dal = {};

dal.add = add;
dal.update = update;
dal.deleteById = deleteById;
dal.getById = getById;
dal.getAll = getAll;

module.exports = dal;getById

function add(holidayModel) {

    var deferred = Q.defer();
    
    holidayModel
        .save(function (err, result) {
            createHelper(deferred, err, result);
        });

    return deferred.promise;
}

function update(holidayDTO, userId) {

    var deferred = Q.defer();
    _holidayModel.findById(holidayDTO._id, function (err, holiday) {
        if (err) throw err;

        if (holiday) {
            try {

                holiday.title = holidayDTO.title ? holidayDTO.title : holiday.title;
                holiday.description = holidayDTO.description ? holidayDTO.description : holiday.description;
                holiday.startDate = holidayDTO.startDate ? new Date(holidayDTO.startDate) : holiday.startDate;
                holiday.endDate = holidayDTO.endDate ? new Date(holidayDTO.endDate) : holiday.endDate;

                //holiday.updatedBy = userId;
                //holiday.updatedDate = new Date();

                holiday.save(function (err, result) {
                    if (err) {
                        console.log(err);
                        deferred.reject(err);
                    }
                });
            }
            catch (error) {
                console.log(error);
            }
        }
    });
    return deferred.promise;
}

function deleteById(holidayId) {

    var deferred = Q.defer();

    _holidayModel.findByIdAndRemove({ _id: holidayId }, function (err) {
        if (err) {
            deferred.reject(err);
        }
        else {
            console.log('holiday successfully deleted!');
            deferred.resolve(true);
        }
    });
    return deferred.promise;
}

function getById(holidayId) {

    var deferred = Q.defer();

    _holidayModel
        .findOne({ '_id': holidayId })
        .lean()
        .exec(function (err, result) {
            execHelper(deferred, err, result);
        });
    return deferred.promise;
}

function getAll() {

    var deferred = Q.defer();

    _holidayModel
        .find({})
        .lean()
        .exec(function (err, result) {
            execHelper(deferred, err, result);
        });
    return deferred.promise;
}
