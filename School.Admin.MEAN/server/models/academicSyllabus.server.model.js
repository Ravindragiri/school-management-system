﻿var mongoose = require('mongoose');

var academicSyllabusSchema = new mongoose.Schema({
    code: String,
    title: String,
    desc: String,
    uploaderType: String,
    academicYear: String,
    createdBy: {
        type: mongoose.Schema.Types.ObjectId
    },
    createdDate: {
        type: Date
    },
    fileName: String
});

mongoose.model('academicSyllabus', academicSyllabusSchema);