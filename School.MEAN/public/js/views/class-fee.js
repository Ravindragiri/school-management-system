﻿/***** angular ngClassFeeController starts *****/
var app = angular.module('schoolApp', ['ngTouch']);

app
    .directive("datepicker", function () {
        return {
            restrict: "A",
            require: "ngModel",
            link: function (scope, elem, attrs, ngModelCtrl) {
                var updateModel = function (dateText) {
                    scope.$apply(function () {
                        ngModelCtrl.$setViewValue(dateText);
                    });
                };
                var options = {
                    dateFormat: "dd/mm/yy",
                    onSelect: function (dateText) {
                        updateModel(dateText);
                    }
                };
                elem.datepicker(options);
            }
        }
    })
    .controller('ngClassFeeController', ['ClassFeeService', '$scope', '$http',
        function (ClassFeeService, $scope, $http) {

            $scope.addMode = true;

            $scope.validateRegisterForm = function () {
                $scope.registrationFormReset = false;
                $scope.showRegisterValidation = function () { return $scope.registrationForm.$invalid; };
            }

            //$scope.reset = function () {
            //    $scope.form = angular.copy(reviewFormFields());
            //    $scope.registrationFormReset = true;
            //    $scope.reviewForm.$setPristine();
            //}

            $scope.insert = function () {

                //$scope.validateRegisterForm();
                $("#registrationForm").validate();

                //if ($scope.registrationForm.$valid) {
                if ($('#registrationForm').valid()) {

                    $scope.loading = true;

                    var postData = {
                        __RequestVerificationToken: angular.element("input[name='__RequestVerificationToken']").val(),
                        classFeeDTO: $scope.classFeeDTO
                    };

                    $.ajax({
                        type: "POST",
                        url: 'http://localhost:1238/api/classFee/add',
                        data: postData,
                        dataType: "json",
                        accept: 'application/json',
                        beforeSend: function () {

                        },
                        success: function (response) {

                            if (response != null) {
                                var status = response.statusCode;

                                switch (status) {
                                    case 200:
                                        alert("Record has been saved successfully.");
                                        break;
                                    case 0:
                                        alert("Your request cannot be processed now due to some technical issue. please try again later.");
                                        break;
                                    case -1:
                                    case 400:
                                    case 401:
                                    case 404:
                                        alert(response.message);
                                        break;
                                    default:
                                }
                            }
                        },
                        error: function (xhr, status, errorThrown) {
                            alert("An error occurred while processing your request.");
                        }
                    });

                    //
                }
            };

            $scope.showServerErrorMessages = function (response, errors) {

                $.each(errors, function (index, error) {

                    $scope.$apply(function () {

                        $scope.invalidelements[error.Element] = true;
                        $('label[ng-show="invalidelements.' + error.Element + '"]').css('display', 'inline-block');
                        $('.' + error.Element).focus();
                    });
                });
            }

            $scope.update = function () {
                var data = {
                    classFeeDTO: $scope.classFeeDTO
                }
                ClassFeeService.update(data).then(function (response) {
                    
                    if (response != null) {
                        var status = response.statusCode;

                        switch (status) {
                            case 200:
                                $scope.addMode = true;
                                alert("Record has been updated successfully.");
                                break;
                            case 0:
                                alert("Your request cannot be processed now due to some technical issue. please try again later.");
                                break;
                            case -1:
                            case 400:
                            case 401:
                            case 404:
                                alert(response.message);
                                break;
                            default:
                        }
                    }
                });
            }

            $scope.cancel = function () {
                $scope.addMode = true;

                $scope.classFeeDTO = {};
            }

            $scope.search = function () {
                var data = {
                    className: $scope.className
                };

                ClassFeeService.getByClass(data).then(function (response) {
                    var data = response.data;
                    data.totalFee = data.formFee + data.libraryFee + data.labFee + data.developmentFee +
                        data.computerFee + data.sportFee;
                    $scope.classFeeDTO = data;
                });
            }

            $scope.deleteClassFee = function (row) {
                if (row.entity) {
                    var id = row.entity._id;
                    ClassFeeService.delete(id).then(function (response) {
                        if (response != null) {
                            var status = response.statusCode;

                            switch (status) {
                                case 200:
                                    alert("Record has been deleted successfully.");
                                    break;
                                case 0:
                                    alert("Your request cannot be processed now due to some technical issue. please try again later.");
                                    break;
                                case -1:
                                case 400:
                                case 401:
                                case 404:
                                    alert(response.message);
                                    break;
                                default:
                            }
                        }
                    });
                }
            };
        }]);
/***** angular ngClassFeeController ends *****/

(function () {

    angular.module('schoolApp')
        .service('ClassFeeService', ['$http', ClassFeeService]);

    function ClassFeeService($http) {

        var self = this;
        var baseUrl = 'http://localhost:1238';

        //var objectName = 'products';

        self.readAll = function () {
            return $http({
                method: 'GET',
                url: baseUrl + '/api/classFee/getAll',
                //url: baseUrl + objectName
            }).then(function (response) {
                return response.data;
            });
        };

        self.getByClass = function (data) {
            return $http({
                method: 'GET',
                url: baseUrl + '/api/classFee/getByClass',
                params: data,
                //url: baseUrl + objectName
            }).then(function (response) {
                return response.data;
            }).catch(function (response) {
                console.error('Error occurred:', response.status, response.data);
            });
        };

        self.readOne = function (id) {
            return $http({
                method: 'GET',
                url: baseUrl + objectName + '/' + id
            }).then(function (response) {
                return response.data;
            });
        };

        self.create = function (data) {
            return $http({
                method: 'POST',
                url: baseUrl + objectName,
                data: data,
                params: {
                    returnObject: true
                },
            }).then(function (response) {
                return response.data;
            });
        };

        self.update = function (data) {
            return $http({
                method: 'POST',
                //url: baseUrl + objectName + '/' + id,
                url: 'http://localhost:1238/api/classFee/update',
                data: data
            }).then(function (response) {
                return response.data;
            });
        };

        self.delete = function (id) {
            return $http({
                method: 'POST',
                //url: baseUrl + objectName + '/' + id
                url: 'http://localhost:1238/api/classFee/removeById' + '?id=' + id
            }).then(function (response) {
                return response.data;
            });
        };

    }
} ());