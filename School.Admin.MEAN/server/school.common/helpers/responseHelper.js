﻿require('../../school.common/appConstants');

sendSuccessResponse = function (res, result, message) {
    res.send({
        status: statusCode.Success.text,
        data : result,
        message : message,
        statusCode: statusCode.Success.value
    });
}

sendFailResponse = function (res, message) {
    res.send({
        status: statusCode.Failed.text,
        data: "",
        message: message,
        statusCode: statusCode.Failed.value
    });
}

sendErrorResponse = function (res) {
    res.send({
        status: statusCode.Error.text,
        message: messages.ErrorProcessing,
        statusCode: statusCode.Error.value
    });
}

sendBadRequestResponse = function (res, data) {
    res.status(400).send({
        status: statusCode.Error.text,
        data: data,
        message: messages.BadRequest,
        statusCode: statusCode.Error.value
    });
}

sendUnauthorizedResponse = function (res, data) {
    res.status(401).send({
        status: statusCode.Error.text,
        data: data,
        message: messages.UnauthorizedAccess,
        statusCode: statusCode.Error.value
    });
}

sendNoContentResponse = function (res) {
    res.status(204).send({
        status: statusCode.NotFound.text,
        data: "",
        message: messages.NotFound,
        statusCode: statusCode.NotFound.value
    });
}

exports.sendSuccessResponse = sendSuccessResponse;
exports.sendFailResponse = sendFailResponse;
exports.sendErrorResponse = sendErrorResponse;
exports.sendBadRequestResponse = sendBadRequestResponse;
exports.sendNoContentResponse = sendNoContentResponse;
exports.sendUnauthorizedResponse = sendUnauthorizedResponse;

