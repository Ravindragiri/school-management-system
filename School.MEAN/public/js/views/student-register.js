﻿/***** angular ngStudentIndexController starts *****/
var app = angular.module('schoolApp', ['ngTouch']);

app
    .directive("datepicker", function () {
        return {
            restrict: "A",
            require: "ngModel",
            link: function (scope, elem, attrs, ngModelCtrl) {
                var updateModel = function (dateText) {
                    scope.$apply(function () {
                        ngModelCtrl.$setViewValue(dateText);
                    });
                };
                var options = {
                    dateFormat: "dd/mm/yy",
                    onSelect: function (dateText) {
                        updateModel(dateText);
                    }
                };
                elem.datepicker(options);
            }
        }
    })
    .controller('ngStudentIndexController', ['StudentService', '$scope', '$http',
        function (StudentService, $scope, $http) {

            $scope.addMode = true;

            $scope.validateRegisterForm = function () {
                $scope.registrationFormReset = false;
                $scope.showRegisterValidation = function () { return $scope.registrationForm.$invalid; };
            }

            //$scope.reset = function () {
            //    $scope.form = angular.copy(reviewFormFields());
            //    $scope.registrationFormReset = true;
            //    $scope.reviewForm.$setPristine();
            //}

            $scope.register = function () {

                //$scope.validateRegisterForm();
                $("#registrationForm").validate();

                //if ($scope.registrationForm.$valid) {
                if ($('#registrationForm').valid()) {

                    $scope.loading = true;

                    var postData = {
                        __RequestVerificationToken: angular.element("input[name='__RequestVerificationToken']").val(),
                        studentDTO: $scope.studentDTO
                    };

                    $.ajax({
                        type: "POST",
                        url: 'http://localhost:1238/api/student/insert',
                        data: postData,
                        dataType: "json",
                        accept: 'application/json',
                        beforeSend: function () {

                        },
                        success: function (response) {

                            if (response != null) {
                                var status = response.statusCode;

                                switch (status) {
                                    case 1:
                                        //var objTravel = JSON.parse(response.data);
                                        break;
                                    case 0:
                                        alert(response.message);
                                        break;
                                    case -10:
                                        $scope.showServerErrorMessages(response, response.serverErrors);
                                        break;
                                    case -500:
                                        alert(response.message)
                                        //window.location.href = errorPage;
                                        break;
                                    default:
                                }
                            }
                        },
                        error: function (xhr, status, errorThrown) {
                            alert("An error occurred while processing your request.");
                        }
                    });

                    //
                }
            };

            $scope.showServerErrorMessages = function (response, errors) {

                $.each(errors, function (index, error) {

                    $scope.$apply(function () {

                        $scope.invalidelements[error.Element] = true;
                        $('label[ng-show="invalidelements.' + error.Element + '"]').css('display', 'inline-block');
                        $('.' + error.Element).focus();
                    });
                });
            }

            $scope.subjects = [];

            $scope.update = function () {

                var data = {
                    studentDTO: $scope.studentDTO
                }
                StudentService.update(data).then(function (response) {
                    
                    if (response != null) {
                        var status = response.statusCode;

                        switch (status) {
                            case 200:
                                $scope.addMode = true;
                                alert("Record has been updated successfully.");
                                break;
                            case 0:
                                alert("Your request cannot be processed now due to some technical issue. please try again later.");
                                break;
                            case -1:
                            case 400:
                            case 401:
                            case 404:
                                alert(response.message);
                                break;
                            default:
                        }
                    }
                });
            }

            $scope.cancel = function () {
                $scope.studentDTO = {};
                $scope.addMode = true;
            }
        }]);
/***** angular ngStudentIndexController ends *****/

(function () {

    angular.module('schoolApp')
        .service('StudentService', ['$http', StudentService]);

    function StudentService($http) {

        var self = this;
        var baseUrl = 'http://localhost:1238';

        //var objectName = 'products';

        self.readAll = function () {
            return $http({
                method: 'GET',
                url: baseUrl + '/api/student/getAllStudentInfo',
                //url: baseUrl + objectName
            }).then(function (response) {
                return response.data;
            });
        };

        self.readOne = function (id) {
            return $http({
                method: 'GET',
                url: baseUrl + objectName + '/' + id
            }).then(function (response) {
                return response.data;
            });
        };

        self.create = function (data) {
            return $http({
                method: 'POST',
                url: baseUrl + objectName,
                data: data,
                params: {
                    returnObject: true
                },
            }).then(function (response) {
                return response.data;
            });
        };

        self.update = function (data) {
            return $http({
                method: 'POST',
                //url: baseUrl + objectName + '/' + id,
                url: baseUrl + '/api/student/update',
                data: data
            }).then(function (response) {
                return response.data;
            });
        };

        self.delete = function (id, userId, parentId) {
            return $http({
                method: 'POST',
                //url: baseUrl + objectName + '/' + id
                url: 'http://localhost:1238/api/student/remove' + '?id=' + id + '&userId=' + userId + '&parentId=' + parentId
            }).then(function (response) {
                return response.data;
            });
        };

    }
} ());