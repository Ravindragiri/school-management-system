﻿module.exports = function (app) {

    var dashboard = require('../controllers/dashboard.server.controller');
    //app.get('/dashboard/index', isAuthenticated, dashboard.index);
    app.get('/dashboard/index', dashboard.index);
    app.get('/dashboard', dashboard.index);
};

// As with any middleware it is quintessential to call next()
// if the user is authenticated
var isAuthenticated = function (req, res, next) {
    if (req.isAuthenticated())
        return next();
    //This means that if some user tries to access http://localhost:3000/dashboard without authenticating in the application, 
    //he will be redirected to home page by doing
    res.redirect('/');
}