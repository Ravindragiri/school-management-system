﻿exports.result = function (req, res) {
    res.render('search/result', {
        pTitle: 'Search Result | Organ Donor'
    });
};

exports.advanced = function (req, res) {
    res.render('search/advanced', {
    });
};