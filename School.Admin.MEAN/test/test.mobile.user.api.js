﻿var mongoose = require("mongoose");
var userModel = require('../server/models/user.server.model');
var server = require('../server');
require('../test/common/test.result.handler');

//Require the dev-dependencies
var chai = require('chai');
var chaiHttp = require('chai-http');
var should = chai.should();

chai.use(chaiHttp);

describe('Users', function () {

    //authenticateMobileUser starts
    /*
    it('authenticateMobileUser #1', function (done) {
        chai.request(server)
      .post('/api/user/authenticateMobileUser')
      .send({
            "loginDTO": {
                "username": "recipient1",
                "password": "goswami",
                "loginType": 0,
                "userType": 1
            }
        })
        .end(function (err, res) {

            singleResultWithData(res);
            done();
        });
    });
    
    it('authenticateMobileUser #2', function (done) {
        chai.request(server)
      .post('/api/user/authenticateMobileUser')
      .send({
            "loginDTO": {
                "username": "recipient1",
                "password": "goswami123",
                "loginType": 0,
                "userType": 1
            }
        })
        .end(function (err, res) {
            
            resultFailedWithMessage(res, messages.InvalidLoginCredentials);
            done();
        });
    });

    it('authenticateMobileUser #3', function (done) {
        chai.request(server)
      .post('/api/user/authenticateMobileUser')
      .send({
            "loginDTO": {
                "username": "ravindragiri123",
                "password": "goswami",
                "loginType": 0,
                "userType": 1
            }
        })
        .end(function (err, res) {
            
            resultFailedWithMessage(res, messages.InvalidLoginCredentials);
            done();
        });
    });

    it('authenticateMobileUser #4', function (done) {
        chai.request(server)
      .post('/api/user/authenticateMobileUser')
      .end(function (err, res) {
            
            resultWithBadRequest(res);
            done();
        });
    });
    
    it('authenticateMobileUser #5', function (done) {
        chai.request(server)
      .post('/api/user/authenticateMobileUser')
      .send({
            "loginDTO": {
                "username": "recipient1",
                "password": "goswami",
                "loginType": 0,
                "userType": 100
            }
        })
        .end(function (err, res) {
            
            singleResultWithData(res);
            done();
        });
    });
    */
    //authenticateMobileUser ends

    //changePassword starts
    /*
    it('changePassword #1', function (done) {
        chai.request(server)
        .post('/api/user/changePassword')
        .end(function (err, res) {
            resultWithBadRequest(res);
            done();
        });
    });
    
    it('changePassword #2', function (done) {
        chai.request(server)
        .post('/api/user/changePassword')
        .send({
            "userID": "57b3dceb005225f21cedb963"
        })
        .end(function (err, res) {
            resultWithBadRequest(res);
            done();
        });
    });
    
    it('changePassword #3', function (done) {
        chai.request(server)
        .post('/api/user/changePassword')
        .send({
            "userID": "57b3dceb005225f21cedb963",
            "oldPassword" : ""
        })
        .end(function (err, res) {
            resultWithBadRequest(res);
            done();
        });
    });
    
    it('changePassword #3', function (done) {
        chai.request(server)
        .post('/api/user/changePassword')
        .send({
            "userID": "57b3dceb005225f21cedb963",
            "oldPassword" : "",
            "newPassword": null
        })
        .end(function (err, res) {
            resultWithBadRequest(res);
            done();
        });
    });

    it('changePassword #4', function (done) {
        chai.request(server)
        .post('/api/user/changePassword')
        .send({
            "userID": null,
            "oldPassword" : null,
            "newPassword": null
        })
        .end(function (err, res) {
            resultWithBadRequest(res);
            done();
        });
    });

    it('changePassword #5', function (done) {
        chai.request(server)
        .post('/api/user/changePassword')
        .send({
            "userID": "",
            "oldPassword" : "",
            "newPassword": ""
        })
        .end(function (err, res) {
            resultWithBadRequest(res);
            done();
        });
    });
    
    it('changePassword #6', function (done) {
        chai.request(server)
        .post('/api/user/changePassword')
        .send({
            "userID": "57b3dceb005225f21cedb963",
            "oldPassword" : "goswami",
            "newPassword": "goswami123"
        })
        .end(function (err, res) {
            singleResultWithData(res);
            done();
        });
    });

    it('changePassword #7', function (done) {
        chai.request(server)
        .post('/api/user/changePassword')
        .send({
            "userID": "57b3dceb005225f21cedb963",
            "oldPassword" : "goswami123",
            "newPassword": "goswami"
        })
        .end(function (err, res) {
            singleResultWithData(res);
            done();
        });
    });

    it('changePassword #8', function (done) {
        chai.request(server)
        .post('/api/user/changePassword')
        .send({
            "userID": "57b3dceb005225f21cedb963",
            "oldPassword" : "goswami",
            "newPassword": "goswami123"
        })
        .end(function (err, res) {
            singleResultWithData(res);
            done();
        });
    });
    */
    //changePassword ends

    //my messages starts
    /*
    it('getMessagesByUserID #1', function (done) {
        chai.request(server)
      .get('/api/message/getMessagesByUserID?userID=57b3dceb005225f21cedb963')
      .end(function (err, res) {
            resultMyMessage(res);   //TODO predefined recordset
            done();
        });
    });

    it('getMessagesByUserID #2', function (done) {
        chai.request(server)
      .get('/api/message/getMessagesByUserID?userID=')
      .end(function (err, res) {
            resultWithBadRequest(res);
            done();
        });
    });

    it('getMessagesByUserID #3', function (done) {
        chai.request(server)
      .get('/api/message/getMessagesByUserID?')
      .end(function (err, res) {
            resultWithBadRequest(res);
            done();
        });
    });

    it('getMessagesByUserID #4', function (done) {
        chai.request(server)
      .get('/api/message/getMessagesByUserID')
      .end(function (err, res) {
            resultWithBadRequest(res);
            done();
        });
    });

    it('getMessagesByUserID #5', function (done) {
        chai.request(server)
      .get('/api/message/getMessagesByUserID?userID=577e0fa774fd15ec5db7d6cc')
      .end(function (err, res) {
            resultSuccess(res);
            done();
        });
    });
    */
    //my messages ends

    //sendMessage starts
    /*
    it('sendMessage #1', function (done) {
        chai.request(server)
        .post('/api/message/sendMessage')
        .send({
            "subject" : "Hi",
            "message" : "Hi message"
        })
        .end(function (err, res) {
            resultWithBadRequest(res);
            done();
        });
    });

    it('sendMessage #2', function (done) {
        chai.request(server)
        .post('/api/message/sendMessage')
        .send({
            "fromName": "",
            "fromEmail": "",
            "fromPhoneNumber": "",
            "toUserID" : "",
            "subject" : "",
            "message" : ""
        })
        .end(function (err, res) {
            resultWithBadRequest(res);
            done();
        });
    });

    it('sendMessage #3', function (done) {
        chai.request(server)
        .post('/api/message/sendMessage')
        .send({
            "fromName": null,
            "fromEmail": null,
            "fromPhoneNumber": null,
            "toUserID" : null,
            "subject" : null,
            "message" : null
        })
        .end(function (err, res) {
            resultWithBadRequest(res);
            done();
        });
    });

    it('sendMessage #4', function (done) {
        chai.request(server)
        .post('/api/message/sendMessage')
        .end(function (err, res) {
            resultWithBadRequest(res);
            done();
        });
    });

    it('sendMessage #5', function (done) {
        chai.request(server)
        .post('/api/message/sendMessage')
        .send({
            "fromName": "vaidehi",
            "fromEmail": "vaidehi.goswami@gmail.com",
            "fromPhoneNumber": "+65 1234 4321",
            "toUserID" : "57b3dceb005225f21cedb963",
            "subject" : "test subject",
            "message" : "test message"
        })
        .end(function (err, res) {
            resultMyMessage(res);
            done();
        });
    });
    */
    //sendMessage ends

    //forgotPassword password starts
    /*
    it('forgotPassword #1', function (done) {
        chai.request(server)
        .post('/api/user/forgetPassword')
        .send({
            "email": ""
        })
        .end(function (err, res) {
            resultWithBadRequest(res);
            done();
        });
    });

    it('forgotPassword #2', function (done) {
        chai.request(server)
        .post('/api/user/forgetPassword')
        .send({
            "email": null
        })
        .end(function (err, res) {
            resultWithBadRequest(res);
            done();
        });
    });
    
    it('forgotPassword #3', function (done) {
        chai.request(server)
        .post('/api/user/forgetPassword')
        .end(function (err, res) {
            resultWithBadRequest(res);
            done();
        });
    });
    
    it('forgotPassword #4', function (done) {
        chai.request(server)
        .post('/api/user/forgetPassword')
        .send({
            "email": "wrongemail@gmail.com"
        })
        .end(function (err, res) {
            resultFailedWithMessage(res, messages.EmailNotRegistered);
            done();
        });
    });
          
    it('forgotPassword #5', function (done) {
        chai.request(server)
        .post('/api/user/forgetPassword')
        .send({
            "email": "email10@gmail.com"
        })
        .end(function (err, res) {
            resultSuccess(res);
            done();
        });
    });
    */
    //forgotPassword password ends

    //mobileUpdateProfile starts
    //TODO can not test multipart
    /*
    it('mobileUpdateProfile #1', function (done) {
        
        chai.request(server)
        .post('/api/recipient/mobileUpdateProfile')
        .send({
            "_id": "57b3dceb005225f21cedb963",
            "firstName": "Ravindragiri"
        })
        .end(function (err, res) {
            
            singleResultWithData(res);
            done();
        });
    });
    
    it('mobileUpdateProfile #2', function (done) {
        
        chai.request(server)
        .post('/api/recipient/mobileUpdateProfile')
        .send({
            "_id": "",
            "firstName": ""
        })
        .end(function (err, res) {
            
            resultWithBadRequest(res);
            done();
        });
    });

    it('mobileUpdateProfile #3', function (done) {
        
        chai.request(server)
        .post('/api/recipient/mobileUpdateProfile')
        .send({
            "_id": null,
            "firstName": null
        })
        .end(function (err, res) {
            
            resultWithBadRequest(res);
            done();
        });
    });
    */
    //mobileUpdateProfile ends

    //getUserIdByUsername starts
    /*
    it('getUserIdByUsername #1', function (done) {
        chai.request(server)
            .get('/api/user/getUserIdByUsername?username=hetvi')
            .end(function (err, res) {
                singleResultWithData(res);
                done();
            });
    });
    
    it('getUserIdByUsername #2', function (done) {
        chai.request(server)
            .get('/api/user/getUserIdByUsername')
            .end(function (err, res) {
                resultWithBadRequest(res);
                done();
            });
    });

    it('getUserIdByUsername #3', function (done) {
        chai.request(server)
            .get('/api/user/getUserIdByUsername?')
            .end(function (err, res) {
                resultWithBadRequest(res);
                done();
            });
    });

    it('getUserIdByUsername #4', function (done) {
        chai.request(server)
            .get('/api/user/getUserIdByUsername?username=hetvi')
            .end(function (err, res) {
                singleResultWithData(res);
                done();
            });
    });

    it('getUserIdByUsername #5', function (done) {
        chai.request(server)
            .get('/api/user/getUserIdByUsername?username=baburav')
            .end(function (err, res) {
                resultNotFound(res);
                done();
            });
    });
    */
    //getUserIdByUsername ends

    //getUsernameById starts
    /*
    it('getUsernameById #1', function (done) {
        chai.request(server)
            .get('/api/user/getUsernameById?userID=5a6a0dc5a06beb21006f9969')
            .end(function (err, res) {
                singleResultWithData(res);
                done();
            });
    });
    
    it('getUsernameById #2', function (done) {
        chai.request(server)
            .get('/api/user/getUsernameById')
            .end(function (err, res) {
                resultWithBadRequest(res);
                done();
            });
    });

    it('getUsernameById #3', function (done) {
        chai.request(server)
            .get('/api/user/getUsernameById?')
            .end(function (err, res) {
                resultWithBadRequest(res);
                done();
            });
    });

    it('getUsernameById #4', function (done) {
        chai.request(server)
            .get('/api/user/getUsernameById?userID=5a6a0dc5a06beb21006f9969')
            .end(function (err, res) {
                singleResultWithData(res);
                done();
            });
    });

    it('getUsernameById #5', function (done) {
        chai.request(server)
            .get('/api/user/getUsernameById?userID=5a6a0dc5a06beb21006f9968')
            .end(function (err, res) {
                resultNotFound(res);
                done();
            });
    });
    */
    //getUsernameById ends

    //getUserById starts
    /*
    it('getUserById #1', function (done) {
        chai.request(server)
            .get('/api/user/getUserById?userID=5a6a0dc5a06beb21006f9969')
            .end(function (err, res) {
                singleResultWithData(res);
                done();
            });
    });
    
    it('getUserById #2', function (done) {
        chai.request(server)
            .get('/api/user/getUserById')
            .end(function (err, res) {
                resultWithBadRequest(res);
                done();
            });
    });

    it('getUserById #3', function (done) {
        chai.request(server)
            .get('/api/user/getUserById?')
            .end(function (err, res) {
                resultWithBadRequest(res);
                done();
            });
    });

    it('getUserById #4', function (done) {
        chai.request(server)
            .get('/api/user/getUserById?userID=5a6a0dc5a06beb21006f9969')
            .end(function (err, res) {
                singleResultWithData(res);
                done();
            });
    });

    it('getUserById #5', function (done) {
        chai.request(server)
            .get('/api/user/getUserById?userID=5a6a0dc5a06beb21006f9968')
            .end(function (err, res) {
                resultNotFound(res);
                done();
            });
    });
    */
    //getUserById ends

    //getUserByEmailPassword starts
    /*
    it('getUserByEmailPassword #1', function (done) {
        chai.request(server)
      .post('/api/user/getUserByEmailPassword')
      .send({
            "loginDTO": {
                "email": "hetvi.goswami@yahoo.com",
                "password": "goswami",
                "loginType": 0,
                "userType": 1
            }
        })
        .end(function (err, res) {

            singleResultWithData(res);
            done();
        });
    });
    
    it('getUserByEmailPassword #2', function (done) {
        chai.request(server)
      .post('/api/user/getUserByEmailPassword')
      .send({
            "loginDTO": {
                "email": "hetvi.goswami@yahoo.com",
                "password": "goswami123",
                "loginType": 0,
                "userType": 1
            }
        })
        .end(function (err, res) {
            
            resultNotFound(res);
            done();
        });
    });

    it('getUserByEmailPassword #3', function (done) {
        chai.request(server)
      .post('/api/user/getUserByEmailPassword')
      .send({
            "loginDTO": {
                "email": "ravindragiri123",
                "password": "goswami",
                "loginType": 0,
                "userType": 1
            }
        })
        .end(function (err, res) {
            
            resultNotFound(res);
            done();
        });
    });

    it('getUserByEmailPassword #4', function (done) {
        chai.request(server)
      .post('/api/user/getUserByEmailPassword')
      .end(function (err, res) {
            
            resultWithBadRequest(res);
            done();
        });
    });
    
    it('getUserByEmailPassword #5', function (done) {
        chai.request(server)
      .post('/api/user/getUserByEmailPassword')
      .send({
            "loginDTO": {
                "email": "hetvi.goswami@yahoo.com",
                "password": "goswami",
                "loginType": 0,
                "userType": 100
            }
        })
        .end(function (err, res) {
            
            singleResultWithData(res);
            done();
        });
    });
    */
    //getUserByEmailPassword ends

    //getUserByEmail starts
    
    it('getUserByEmail #1', function (done) {
        chai.request(server)
            .get('/api/user/getUserByEmail?email=hetvi.goswami@yahoo.com')
            .end(function (err, res) {
                singleResultWithData(res);
                done();
            });
    });
    
    it('getUserByEmail #2', function (done) {
        chai.request(server)
            .get('/api/user/getUserByEmail')
            .end(function (err, res) {
                resultWithBadRequest(res);
                done();
            });
    });

    it('getUserByEmail #3', function (done) {
        chai.request(server)
            .get('/api/user/getUserByEmail?')
            .end(function (err, res) {
                resultWithBadRequest(res);
                done();
            });
    });

    it('getUserByEmail #4', function (done) {
        chai.request(server)
            .get('/api/user/getUserByEmail?email=hetvi.goswami@yahoo.com')
            .end(function (err, res) {
                singleResultWithData(res);
                done();
            });
    });

    it('getUserByEmail #5', function (done) {
        chai.request(server)
            .get('/api/user/getUserByEmail?email=noemail@gmail.com')
            .end(function (err, res) {
                resultNotFound(res);
                done();
            });
    });
    
    //getUserByEmail ends
});