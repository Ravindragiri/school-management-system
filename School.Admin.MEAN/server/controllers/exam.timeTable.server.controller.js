﻿var config = require('../config/config');
require('../school.common/appConstants');

exports.index = function (req, res) {

    res.render('exam/timeTable/index', {
        pTitle: 'Exam TimeTable List | School'
    });
};