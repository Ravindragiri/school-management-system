﻿var mongoose = require('mongoose');

var transportSchema = new mongoose.Schema({
    routeName: String,
    routeFare: String,
    numberOfvehicle: Number,
    driverName: String,
    driverContactNo: String,
    desc: String
});

exports.transportSchema = transportSchema;
mongoose.model('transport', transportSchema);