﻿var mongoose = require("mongoose");
var userModel = require('../server/models/user.server.model');
var server = require('../app');
require('../test/common/test.result.handler');

//Require the dev-dependencies
var chai = require('chai');
var chaiHttp = require('chai-http');
var should = chai.should();

chai.use(chaiHttp);

describe('Users', function () {
    
    //userModel.collection.drop();
    
    //searchSubjects starts
    /*       
    it('searchSubjects #1', function (done) {
        chai.request(server)
      .get('/api/teacher/subject/searchSubjects?userType=1')
      //.get('/api/teacher/subject/searchSubjects')
      //.query({ userType: 1 })
      .end(function (err, res) {
            resultWithBadRequest(res);
            done();
        });
    });
    
    it('searchSubjects #2', function (done) {
        chai.request(server)
      .get('/api/teacher/subject/searchSubjects?userType=2')
      .end(function (err, res) {
            resultWithBadRequest(res);
            done();
        });
    });
    
    it('searchSubjects #3', function (done) {
        chai.request(server)
      .get('/api/teacher/subject/searchSubjects?userType=')
      .end(function (err, res) {
            resultWithBadRequest(res);
            done();
        });
    });
    
    it('searchSubjects #4', function (done) {
        chai.request(server)
      .get('/api/teacher/subject/searchSubjects?userType=100')
      .end(function (err, res) {
            resultWithError(res);            
            done();
        });
    });
    
    it('searchSubjects #5', function (done) {
        chai.request(server)
      .get('/api/teacher/subject/searchSubjects')
      .end(function (err, res) {
            resultWithBadRequest(res);
            done();
        });
    });
    
    it('searchSubjects #6', function (done) {
        chai.request(server)
      .get('/api/teacher/subject/searchSubjects?userType=1')
      .end(function (err, res) {
            resultWithBadRequest(res);
            done();
        });
    });

    //create case resultEmptyArray

    it('searchSubjects #7', function (done) {
        chai.request(server)
      .get('/api/teacher/subject/searchSubjects?userType=2&title=Liver')
      .end(function (err, res) {
            resultReceiverArray(res);
            done();
        });
    });
      
    it('searchSubjects #8', function (done) {
        chai.request(server)
      .get('/api/teacher/subject/searchSubjects?userType=1&title=Liver')
      .end(function (err, res) {
            resultEmptyArray(res);
            done();
        });
    });
    
    it('searchSubjects #9', function (done) {
        chai.request(server)
      .get('/api/teacher/subject/searchSubjects?userType=1&title=Hair')
      .end(function (err, res) {
            resultEmptyArray(res);
            done();
        });
    });
    */
    //searchSubjects ends

    
    //getSubjectDetail starts
    /*
    it('getSubjectDetail #1', function (done) {
        chai.request(server)
      .get('/api/teacher/subject/getSubjectDetail?userID=57b3dceb005225f21cedb963')
      //.get('/api/teacher/subject/getSubjectDetail')
      //.query({ userType: 1 })
      .end(function (err, res) {
            getSubjectDetailResultWithData(res);
            done();
        });
    });

    it('getSubjectDetail #2', function (done) {
        chai.request(server)
      .get('/api/teacher/subject/getSubjectDetail')
      //.get('/api/teacher/subject/getSubjectDetail')
      //.query({ userType: 1 })
      .end(function (err, res) {
            resultWithBadRequest(res);
            done();
        });
    });

    it('getSubjectDetail #3', function (done) {
        chai.request(server)
      .get('/api/teacher/subject/getSubjectDetail?')
      //.get('/api/teacher/subject/getSubjectDetail')
      //.query({ userType: 1 })
      .end(function (err, res) {
            resultWithBadRequest(res);
            done();
        });
    });

    it('getSubjectDetail #4', function (done) {
        chai.request(server)
      .get('/api/teacher/subject/getSubjectDetail?userID=57b3dceb005225f21cedb963')
      //.get('/api/teacher/subject/getSubjectDetail')
      //.query({ userType: 1 })
      .end(function (err, res) {
            getSubjectDetailResultWithData(res);
            done();
        });
    });

    it('getSubjectDetail #5', function (done) {
        chai.request(server)
      .get('/api/teacher/subject/getSubjectDetail?userID=577e0fa784fd15ec5db7d6c1')
      //.get('/api/teacher/subject/getSubjectDetail')
      //.query({ userType: 1 })
      .end(function (err, res) {
            resultNotFound(res);
            done();
        });
    });
    */
    //getSubjectDetail ends

    //getTeacherContactDetailById starts
    /*
    it('getTeacherContactDetailById #1', function (done) {
        chai.request(server)
      .get('/api/teacher/getTeacherContactDetailById?userID=57b3dceb005225f21cedb963')
      .end(function (err, res) {
            getTeacherContactDetailByIdResultWithData(res);
            done();
        });
    });
    
    it('getTeacherContactDetailById #2', function (done) {
        chai.request(server)
      .get('/api/teacher/getTeacherContactDetailById')
      .end(function (err, res) {
            resultWithBadRequest(res);
            done();
        });
    });
    
    it('getTeacherContactDetailById #3', function (done) {
        chai.request(server)
      .get('/api/teacher/getTeacherContactDetailById?')
      .end(function (err, res) {
            resultWithBadRequest(res);
            done();
        });
    });
    
    it('getTeacherContactDetailById #4', function (done) {
        chai.request(server)
      .get('/api/teacher/getTeacherContactDetailById?userID=57b3dceb005225f21cedb963')
      .end(function (err, res) {
            getTeacherContactDetailByIdResultWithData(res);
            done();
        });
    });
    
    it('getTeacherContactDetailById #5', function (done) {
        chai.request(server)
      .get('/api/teacher/getTeacherContactDetailById?userID=577e0fa784fd15ec5db7d6c1')
      .end(function (err, res) {
            resultNotFoundWithoutMessage(res);
            done();
        });
    });
    */
    //getTeacherContactDetailById ends

    //add subjects starts
    /*
    it('add subjects #1', function (done) {
        chai.request(server)
      .post('/api/teacher/subject/add')
      .send({
            "subjects" : [
                {
                    "title": "Intestines",
                    "description": null
                }],
            "userID": "57b3dceb005225f21cedb963"
        })
        .end(function (err, res) {
            
            singleResultWithData(res);
            done();
        });
    });
    
    it('add subjects #2', function (done) {
        chai.request(server)
      .post('/api/teacher/subject/add')
      .send({
            "subjects" : [
                {
                    "title": "Intestines",
                    "description": null
                }],
            "userID": ""
        })
        .end(function (err, res) {
            resultWithBadRequest(res);
            done();
        });
    });
    
    it('add subjects #3', function (done) {
        chai.request(server)
      .post('/api/teacher/subject/add')
      .send({
            "subjects" : [
                {
                    "title": "Intestines",
                    "description": null
                }],
            "userID": null
        })
        .end(function (err, res) {
            resultWithBadRequest(res);
            done();
        });
    });
    
    it('add subjects #4', function (done) {
        chai.request(server)
      .post('/api/teacher/subject/add')
      .send({
            "subjects" : [],
            "userID": "57b3dceb005225f21cedb963"
        })
        .end(function (err, res) {
            resultWithBadRequest(res);
            done();
        });
    });
    
    it('add subjects #5', function (done) {
        chai.request(server)
      .post('/api/teacher/subject/add')
      .send({
            "subjects" : null,
            "userID": "57b3dceb005225f21cedb963"
        })
        .end(function (err, res) {
            resultWithBadRequest(res);
            done();
        });
    });
    
    it('add subjects #6', function (done) {
        chai.request(server)
      .post('/api/teacher/subject/add')
      .send({
            "subjects" : "",
            "userID": "57b3dceb005225f21cedb963"
        })
        .end(function (err, res) {
            resultWithBadRequest(res);
            done();
        });
    });
    
    it('add subjects #7', function (done) {
        chai.request(server)
      .post('/api/teacher/subject/add')
      .send({
            "userID": "57b3dceb005225f21cedb963"
        })
        .end(function (err, res) {
            resultWithBadRequest(res);
            done();
        });
    });
    
    it('add subjects #8', function (done) {
        chai.request(server)
      .post('/api/teacher/subject/add')
      .send({
            "subjects" : ""
        })
        .end(function (err, res) {
            resultWithBadRequest(res);
            done();
        });
    });
    
    it('add subjects #9', function (done) {
        chai.request(server)
        .post('/api/teacher/subject/add')
        .end(function (err, res) {
            resultWithBadRequest(res);
            done();
        });
    });
    */
    //add subjects ends

    //subject remove starts
    /*
    it('subject remove #1', function (done) {
        chai.request(server)
        .post('/api/teacher/subject/remove')
        .send({
            "userID": "",
            "subjectTitle" : ""
        })
        .end(function (err, res) {
            resultWithBadRequest(res);
            done();
        });
    });
    
    it('subject remove #2', function (done) {
        chai.request(server)
        .post('/api/teacher/subject/remove')
        .send({
            "userID": "57b3dceb005225f21cedb963",
            "subjectTitle" : ""
        })
        .end(function (err, res) {
            resultWithBadRequest(res);
            done();
        });
    });
    
    it('subject remove #2', function (done) {
        chai.request(server)
        .post('/api/teacher/subject/remove')
        .send({
            "userID": "",
            "subjectTitle" : "Liver"
        })
        .end(function (err, res) {
            resultWithBadRequest(res);
            done();
        });
    });

    it('subject remove #4', function (done) {
        chai.request(server)
        .post('/api/teacher/subject/remove')
        .send({
            "userID": null,
            "subjectTitle" : null
        })
        .end(function (err, res) {
            resultWithBadRequest(res);
            done();
        });
    });

    it('subject remove #5', function (done) {
        chai.request(server)
        .post('/api/teacher/subject/remove')
        .send({
            "userID": "57b3dceb005225f21cedb963",
            "subjectTitle" : null
        })
        .end(function (err, res) {
            resultWithBadRequest(res);
            done();
        });
    });

    it('subject remove #6', function (done) {
        chai.request(server)
        .post('/api/teacher/subject/remove')
        .send({
            "userID": null,
            "subjectTitle" : "Liver"
        })
        .end(function (err, res) {
            resultWithBadRequest(res);
            done();
        });
    });

    it('subject remove #6', function (done) {
        chai.request(server)
        .post('/api/teacher/subject/remove')
        .send({
            "userID": "57b3dceb005225f21cedb963",
            "subjectTitle" : "Intestines"
        })
        .end(function (err, res) {
            resultSuccess(res);
            done();
        });
    });

    it('subject remove #7', function (done) {
        chai.request(server)
        .post('/api/teacher/subject/remove')
        .send({
            "userID": "57b3dceb005225f21cedb963",
            "subjectTitle" : "Liver"
        })
        .end(function (err, res) {
            resultSuccess(res);
            done();
        });
    });
    */
    //subject remove ends

    //subject remove by ID starts
    /*
    it('subject removeByID #1', function (done) {
        chai.request(server)
        .post('/api/teacher/subject/removeByID')
        .send({
            "userID": "57b3dceb005225f21cedb963",
            "subjectID" : "579cd7040261fe9427ee9b58"
        })
        .end(function (err, res) {
            resultEmpty(res);
            done();
        });
    });
    */
    //subject remove by ID ends
});