﻿var dormitoryDal = require('../../../server/school.dal/dormitory.dal.js');
require('../../school.common/helpers/responseHelper');

var mongoose = require('mongoose'),
    //fs = require('fs-extra'),
    //util = require("util"),
//path = require("path");

var _dormitory = mongoose.model('dormitory');
var _user = mongoose.model('user');

exports.addDormitory = function (req, res) {

    var dormitoryDTO = req.body.dormitoryDTO;

    dormitoryDal.add(dormitoryDTO)
        .then(function (result) {
            if (result) {
                sendSuccessResponse(res, result);
            } else {
                sendErrorResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
}

exports.updateDormitory = function (req, res) {
    var dormitoryDTO = req.body.dormitoryDTO;

    dormitoryDal.getUserById(dormitoryDTO.Id)
        .then(function (dormitoryModel) {

            if (dormitoryModel) {
                if (dormitoryModel.Id > 0) {
                    if (dormitoryModel.password == dormitoryDTO.password) {

                        //update profile photo TODO
                        dormitoryModel.firstName = dormitoryDTO.firstName;
                        dormitoryModel.lastName = dormitoryDTO.lastName;
                        dormitoryModel.phoneNumber = dormitoryDTO.phoneNumber;
                        //address TODO

                        dormitoryDal.update(dormitoryModel, userId)
                            .then(function (user) {

                                if (user) {
                                    sendSuccessResponse(res, result, messages.UpdateSuccess);
                                }
                                else {
                                    sendFailResponse(res, messages.PasswordIncorrect);
                                }
                            })
                            .catch(function (err) {
                                console.log(err);
                                sendErrorResponse(res);
                            });
                    }
                }
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
};

exports.deleteDormitory = function (req, res) {
    var dormitoryModel = req.body.dormitoryModel;
    var dormitoryId = req.body.dormitoryId;

    dormitoryDal.deleteDormitory(dormitoryModel, dormitoryId)
        .then(function (result) {
            if (result) {
                sendSuccessResponse(res, result, messages.DeleteSuccess);
            } else {
                sendFailResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
};

exports.getDormitoryById = function (req, res) {

    if (req.query.dormitoryId) {
        var dormitoryId = req.query.dormitoryId;

        dormitoryDal.getDormitoryById(dormitoryId)
            .then(function (result) {
                if (result) {
                    sendSuccessResponse(res, result);
                } else {
                    sendErrorResponse(res);
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
    else {
        sendBadRequestResponse(res);
    }
}

exports.getAllDormitory = function (req, res) {

    dormitoryDal.getAllDormitory()
        .then(function (result) {
            if (result) {
                sendSuccessResponse(res, result);
            } else {
                sendErrorResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
}