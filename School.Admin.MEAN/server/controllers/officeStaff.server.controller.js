﻿var config = require('../config/config');
require('../school.common/appConstants');

exports.index = function (req, res) {

    res.render('officeStaff/index', {
        pTitle: 'Office Staff List | School'
    });
};

exports.renderRegister = function (req, res) {
    res.render('officeStaff/register', {
        pTitle: 'OfficeStaff Registration | School'
    });
};

exports.renderMyProfile = function (req, res) {
    res.render('officeStaff/myProfile', {
        pTitle: 'My Profile OfficeStaff | School'
    });
};