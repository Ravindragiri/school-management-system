﻿var transportDal = require('../../../server/school.dal/transport.dal.js');
require('../../school.common/helpers/responseHelper');

var mongoose = require('mongoose'),
    //fs = require('fs-extra'),
    //util = require("util"),
//path = require("path");

var _transport = mongoose.model('transport');
var _user = mongoose.model('user');

exports.addTransport = function (req, res) {

    var classTimeTableDTO = req.body.classTimeTableDTO;

    transportDal.add(classTimeTableDTO)
        .then(function (result) {
            if (result) {
                sendSuccessResponse(res, result);
            } else {
                sendErrorResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
}

exports.updateTransport = function (req, res) {
    var classTimeTableDTO = req.body.classTimeTableDTO;

    transportDal.getUserById(classTimeTableDTO.Id)
        .then(function (classTimeTableModel) {

            if (classTimeTableModel) {
                if (classTimeTableModel.Id > 0) {
                    if (classTimeTableModel.password == classTimeTableDTO.password) {

                        //update profile photo TODO
                        classTimeTableModel.firstName = classTimeTableDTO.firstName;
                        classTimeTableModel.lastName = classTimeTableDTO.lastName;
                        classTimeTableModel.phoneNumber = classTimeTableDTO.phoneNumber;
                        //address TODO

                        transportDal.update(classTimeTableModel, userId)
                            .then(function (user) {

                                if (user) {
                                    sendSuccessResponse(res, result, messages.UpdateSuccess);
                                }
                                else {
                                    sendFailResponse(res, messages.PasswordIncorrect);
                                }
                            })
                            .catch(function (err) {
                                console.log(err);
                                sendErrorResponse(res);
                            });
                    }
                }
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
};

exports.deleteTransport = function (req, res) {
    var classTimeTableModel = req.body.classTimeTableModel;
    var classTimeTableId = req.body.classTimeTableId;

    classTimeTableDal.deleteTransport(classTimeTableModel, classTimeTableId)
        .then(function (result) {
            if (result) {
                sendSuccessResponse(res, result, messages.DeleteSuccess);
            } else {
                sendFailResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
};