﻿var noticeDal = require('../../../server/school.dal/notice.dal.js');
require('../../school.common/helpers/responseHelper');

var mongoose = require('mongoose'),
    //fs = require('fs-extra'),
    //util = require("util"),
//path = require("path");

var _notice = mongoose.model('notice');
var _user = mongoose.model('user');

exports.addNotice = function (req, res) {

    var noticeDTO = req.body.noticeDTO;

    noticeDal.add(noticeDTO)
        .then(function (result) {
            if (result) {
                if (noticeDTO.sendSMSIsChecked) {
                    //send SMS to Parent
                }
                sendSuccessResponse(res, result);
            } else {
                sendErrorResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
}

exports.updateNotice = function (req, res) {
    var noticeDTO = req.body.noticeDTO;

    noticeDal.getUserById(noticeDTO.Id)
        .then(function (noticeModel) {

            if (noticeModel) {
                if (noticeModel.Id > 0) {
                    if (noticeModel.password == noticeDTO.password) {

                        //update profile photo TODO
                        noticeModel.title = noticeDTO.title;
                        noticeModel.notice = noticeDTO.notice;
                        noticeModel.phoneNumber = noticeDTO.phoneNumber;
                        //address TODO

                        noticeDal.update(noticeModel, userId)
                            .then(function (user) {

                                if (user) {
                                    if (noticeDTO.isSendSmsChecked) {
                                        //send SMS to Parent
                                    }
                                    sendSuccessResponse(res, result, messages.UpdateSuccess);
                                }
                                else {
                                    sendFailResponse(res, messages.PasswordIncorrect);
                                }
                            })
                            .catch(function (err) {
                                console.log(err);
                                sendErrorResponse(res);
                            });
                    }
                }
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
};

exports.deleteNotice = function (req, res) {
    var noticeModel = req.body.noticeModel;
    var noticeId = req.body.noticeId;

    noticeDal.deleteNotice(noticeModel, noticeId)
        .then(function (result) {
            if (result) {
                sendSuccessResponse(res, result, messages.DeleteSuccess);
            } else {
                sendFailResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
};