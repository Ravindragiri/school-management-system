﻿var _user = require('../controllers/users/user.authorization.js');

module.exports = function (app) {

    var teacherController = require('../controllers/teacher.server.controller');
    app.get('/teacher/index', teacherController.index);
    app.get('/teacher', teacherController.index);
    app.get('/teacher/register', teacherController.renderRegister);
    app.get('/teacher/myProfile', teacherController.renderMyProfile);

    var teacherApiController = require('../controllers/api/teacher.server.api.controller');
    //post
    app.post('/api/teacher/insert', teacherApiController.insert);
    app.post('/api/teacher/mobileRegister', teacherApiController.mobileRegister);
    app.post('/api/teacher/update', teacherApiController.update);
    app.post('/api/teacher/updateProfile', teacherApiController.updateProfile);
    app.post('/api/teacher/remove', teacherApiController.remove);
    //get
    app.get('/api/teacher/getTeacherContactDetailById', teacherApiController.getTeacherContactDetailById);
    app.get('/api/teacher/getTeachersByAnyCriteria', teacherApiController.getTeachersByAnyCriteria);
    app.get('/api/teacher/getTeacherById', teacherApiController.getTeacherById);
    app.get('/api/teacher/getAllTeacherInfo', teacherApiController.getAllTeacherInfo);
    app.get('/api/teacher/getTeacherByUserId', teacherApiController.getTeacherByUserId);
    //app.get('/api/teacher/searchTeachers', teacherApiControleler.searchTeachers);
    app.get('/api/teacher/getTeacherAvatar', teacherApiController.getTeacherAvatar);
    app.get('/api/teacher/count', teacherApiController.count);
};