﻿/***** angular ngHolidayIndexController starts *****/
var app = angular.module('schoolApp', ['ngTouch', 'ui.grid']);

app
    .directive("datepicker", function () {
        return {
            restrict: "A",
            require: "ngModel",
            link: function (scope, elem, attrs, ngModelCtrl) {
                var updateModel = function (dateText) {
                    scope.$apply(function () {
                        ngModelCtrl.$setViewValue(dateText);
                    });
                };
                var options = {
                    dateFormat: "dd/mm/yy",
                    onSelect: function (dateText) {
                        updateModel(dateText);
                    }
                };
                elem.datepicker(options);
            }
        }
    })
    .controller('ngHolidayIndexController', ['HolidayService', '$scope', '$http',
        function (HolidayService, $scope, $http) {

            $scope.addMode = true;

            $scope.gridOptions = {
                columnDefs: [
                    { name: 'title', cellTemplate: '<div class="ui-grid-cell-contents">{{ COL_FIELD }}</div>' },
                    { name: 'description', cellTemplate: '<div class="ui-grid-cell-contents">{{ COL_FIELD }}</div>' },
                    { name: 'startDate', cellTemplate: '<div class="ui-grid-cell-contents">{{ COL_FIELD }}</div>' },
                    { name: 'endDate', cellTemplate: '<div class="ui-grid-cell-contents">{{ COL_FIELD }}</div>' },
                    { name: 'edit', cellTemplate: '<button type="button" class="btn btn-xs" ng-click="grid.appScope.editHoliday(row)">Edit</button>' },
                    { name: 'delete', field: '_id', cellTemplate: '<button type="button" class="btn btn-xs" ng-click="grid.appScope.deleteHoliday(row)">Delete</button>' }
                ],
                excludeProperties: '__metadata',
            };

            $scope.validateRegisterForm = function () {
                $scope.registrationFormReset = false;
                $scope.showRegisterValidation = function () { return $scope.registrationForm.$invalid; };
            }

            //$scope.reset = function () {
            //    $scope.form = angular.copy(reviewFormFields());
            //    $scope.registrationFormReset = true;
            //    $scope.reviewForm.$setPristine();
            //}

            $scope.insert = function () {

                //$scope.validateRegisterForm();
                $("#registrationForm").validate();

                //if ($scope.registrationForm.$valid) {
                if ($('#registrationForm').valid()) {

                    $scope.loading = true;

                    var postData = {
                        __RequestVerificationToken: angular.element("input[name='__RequestVerificationToken']").val(),
                        holidayDTO: $scope.holidayDTO
                    };

                    $.ajax({
                        type: "POST",
                        url: 'http://localhost:1238/api/holiday/add',
                        data: postData,
                        dataType: "json",
                        accept: 'application/json',
                        beforeSend: function () {

                        },
                        success: function (response) {

                            if (response != null) {
                                var status = response.statusCode;

                                switch (status) {
                                    case 200:
                                        alert("Record has been saved successfully.");
                                        break;
                                    case 0:
                                        alert("Your request cannot be processed now due to some technical issue. please try again later.");
                                        break;
                                    case -1:
                                    case 400:
                                    case 401:
                                    case 204:
                                        alert(response.message);
                                        break;
                                    default:
                                }
                            }
                        },
                        error: function (xhr, status, errorThrown) {
                            alert("An error occurred while processing your request.");
                        }
                    });

                    //
                }
            };

            $scope.showServerErrorMessages = function (response, errors) {

                $.each(errors, function (index, error) {

                    $scope.$apply(function () {

                        $scope.invalidelements[error.Element] = true;
                        $('label[ng-show="invalidelements.' + error.Element + '"]').css('display', 'inline-block');
                        $('.' + error.Element).focus();
                    });
                });
            }

            $scope.load = function () {
                HolidayService.readAll().then(function (response) {
                    $scope.gridOptions.data = response.data;
                    //$scope.gridOptions.data = JSON.parse(response.data);
                });
            }

            $scope.load();

            $scope.update = function () {
                var data = {
                    holidayDTO: $scope.holidayDTO
                }
                HolidayService.update(data).then(function (response) {
                    
                    if (response != null) {
                        var status = response.statusCode;

                        switch (status) {
                            case 200:
                                $scope.addMode = true;
                                alert("Record has been updated successfully.");
                                break;
                            case 0:
                                alert("Your request cannot be processed now due to some technical issue. please try again later.");
                                break;
                            case -1:
                            case 400:
                            case 401:
                            case 204:
                                alert(response.message);
                                break;
                            default:
                        }
                    }
                });
                
            }

            $scope.cancel = function () {
                $scope.holidayDTO = {};
                $scope.addMode = true;
            }

            $scope.editHoliday = function (row) {
                if (row.entity) {
                    $scope.holidayDTO = row.entity;
                    //var index = $scope.gridOptions.data.indexOf(row.entity);
                    $scope.addMode = false;
                }
            };

            $scope.deleteHoliday = function (row) {
                if (row.entity) {
                    var id = row.entity._id;
                    HolidayService.delete(id).then(function (response) {
                        if (response != null) {
                            var status = response.statusCode;

                            switch (status) {
                                case 200:
                                    alert("Record has been deleted successfully.");
                                    break;
                                case 0:
                                    alert("Your request cannot be processed now due to some technical issue. please try again later.");
                                    break;
                                case -1:
                                case 400:
                                case 401:
                                case 204:
                                    alert(response.message);
                                    break;
                                default:
                            }
                        }
                    });
                }
            };
        }]);
/***** angular ngHolidayIndexController ends *****/

(function () {

    angular.module('schoolApp')
        .service('HolidayService', ['$http', HolidayService]);

    function HolidayService($http) {

        var self = this;
        var baseUrl = 'http://localhost:1238/api/holiday/getAll';

        //var objectName = 'products';

        self.readAll = function () {
            return $http({
                method: 'GET',
                url: baseUrl,
                //url: baseUrl + objectName
            }).then(function (response) {
                return response.data;
            });
        };

        self.readOne = function (id) {
            return $http({
                method: 'GET',
                url: baseUrl + objectName + '/' + id
            }).then(function (response) {
                return response.data;
            });
        };

        self.create = function (data) {
            return $http({
                method: 'POST',
                //url: baseUrl + objectName,
                url: 'http://localhost:1238/api/holiday/add',
                data: data,
                params: {
                    returnObject: true
                },
            }).then(function (response) {
                return response.data;
            });
        };

        self.update = function ( data) {
            return $http({
                method: 'POST',
                //url: baseUrl + objectName + '/' + id,
                url: 'http://localhost:1238/api/holiday/update',
                data: data
            }).then(function (response) {
                return response.data;
            });
        };

        self.delete = function (id) {
            return $http({
                method: 'POST',
                //url: baseUrl + objectName + '/' + id
                url: 'http://localhost:1238/api/holiday/removeById' + '?id=' + id
            }).then(function (response) {
                return response.data;
            });
        };

    }
} ());