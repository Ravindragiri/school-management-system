﻿var config = require('../config/config');

exports.index = function (req, res) {

    res.render('holiday/index', {
        pTitle: 'Holiday List | School'
    });
};