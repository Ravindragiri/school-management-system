﻿module.exports = function (app) {

    var studentApplyController = require('../controllers/student.apply.server.controller');
    app.get('/student/apply', studentApplyController.renderApply);

    var studentApplyApiController = require('../controllers/api/student.apply.server.api.controller');
    //get
    app.get('/api/student/getTotalClassAndSectionInfo', studentApplyApiController.getTotalClassAndSectionInfo);
    app.get('/api/student/getStudentListSortByName', studentApplyApiController.getStudentListSortByName);
    //post
    app.post('/api/student/applyRollNo', studentApplyApiController.applyRollNo);
    app.post('/api/student/applyRollNoInBulk', studentApplyApiController.applyRollNoInBulk);
    app.post('/api/student/applyRegistrationNo', studentApplyApiController.applyRegistrationNo);
};