﻿var mongoose = require("mongoose");
var userModel = require('../server/models/user.server.model');
var server = require('../server');
require('../test/common/test.result.handler');

//Require the dev-dependencies
var chai = require('chai');
var chaiHttp = require('chai-http');
var should = chai.should();

chai.use(chaiHttp);

describe('Users', function () {

    //userModel.collection.drop();

    //getAttendanceById starts
    /*
    it('getAttendanceById #1', function (done) {
        chai.request(server)
            .get('/api/attendance/getAttendanceById?attendanceID=5a6a0dc5a06beb21006f9968')
            .end(function (err, res) {
                getAttendanceByIdResultWithData(res);
                done();
            });
    });

    it('getAttendanceById #2', function (done) {
        chai.request(server)
            .get('/api/attendance/getAttendanceById')
            .end(function (err, res) {
                resultWithBadRequest(res);
                done();
            });
    });

    it('getAttendanceById #3', function (done) {
        chai.request(server)
            .get('/api/attendance/getAttendanceById?')
            .end(function (err, res) {
                resultWithBadRequest(res);
                done();
            });
    });

    it('getAttendanceById #4', function (done) {
        chai.request(server)
            .get('/api/attendance/getAttendanceById?attendanceID=577e0fa784fd15ec5db7d6c1')
            .end(function (err, res) {
                resultNotFoundWithoutMessage(res);
                done();
            });
    });
    */
    //getAttendanceById ends

    //getAttendanceContactDetailById starts
    /*
    it('getAttendanceContactDetailById #1', function (done) {
        chai.request(server)
            .get('/api/attendance/getAttendanceContactDetailById?attendanceID=5a6a0dc5a06beb21006f9968')
      .end(function (err, res) {
            getAttendanceContactDetailByIdResultWithData(res);
            done();
        });
    });
    
    it('getAttendanceContactDetailById #2', function (done) {
        chai.request(server)
      .get('/api/attendance/getAttendanceContactDetailById')
      .end(function (err, res) {
            resultWithBadRequest(res);
            done();
        });
    });
    
    it('getAttendanceContactDetailById #3', function (done) {
        chai.request(server)
      .get('/api/attendance/getAttendanceContactDetailById?')
      .end(function (err, res) {
            resultWithBadRequest(res);
            done();
        });
    });
    
    it('getAttendanceContactDetailById #4', function (done) {
        chai.request(server)
            .get('/api/attendance/getAttendanceContactDetailById?attendanceID=5a6a0dc5a06beb21006f9968')
      .end(function (err, res) {
            getAttendanceContactDetailByIdResultWithData(res);
            done();
        });
    });
    
    it('getAttendanceContactDetailById #5', function (done) {
        chai.request(server)
            .get('/api/attendance/getAttendanceContactDetailById?attendanceID=577e0fa784fd15ec5db7d6c1')
      .end(function (err, res) {
            resultNotFoundWithoutMessage(res);
            done();
        });
    });
    */
    //getAttendanceContactDetailById ends

    //count starts
    /*
    it('count #1', function (done) {
        chai.request(server)
            .get('/api/attendance/count')
            .end(function (err, res) {
                resultSuccess(res);
                done();
            });
    });
    */
    //count ends

    //getAttendanceProfileInfo starts

    it('getAttendanceProfileInfo #1', function (done) {
        chai.request(server)
            .get('/api/attendance/getAttendanceProfileInfo?userID=5a6a0dc5a06beb21006f9969')
            .end(function (err, res) {
                getAttendanceByIdResultWithData(res);
                done();
            });
    });

    it('getAttendanceById #2', function (done) {
        chai.request(server)
            .get('/api/attendance/getAttendanceProfileInfo')
            .end(function (err, res) {
                resultWithBadRequest(res);
                done();
            });
    });

    it('getAttendanceById #3', function (done) {
        chai.request(server)
            .get('/api/attendance/getAttendanceProfileInfo?')
            .end(function (err, res) {
                resultWithBadRequest(res);
                done();
            });
    });

    it('getAttendanceById #4', function (done) {
        chai.request(server)
            .get('/api/attendance/getAttendanceProfileInfo?userID=5a6a0dc5a06beb21006f9969')
            .end(function (err, res) {
                resultNotFoundWithoutMessage(res);
                done();
            });
    });

    //getAttendanceProfileInfo ends
});