﻿module.exports = function (app) {

    var classFeeController = require('../controllers/class.fee.server.controller');
    app.get('/class/fees', classFeeController.index);

    //var classFeeApiController = require('../controllers/api/class.fee.server.api.controller');
    //get
    //app.get('/api/classFee/getClassFeeById', classFeeApiController.getClassFeeById);
    //app.get('/api/classFee/count', classFeeApiController.count);
    //app.get('/api/classFee/getAllClassFee', classFeeApiController.getAllClassFee);
};