﻿var Q = require('q');
var _ = require('lodash');
var mongoose = require('mongoose'),
    async = require('async');
require('../school.common/appConstants');
require('../models/student.server.model');
require('../models/user.server.model');
var _students = mongoose.model('student');
var _user = mongoose.model('user');

var studentDal = {};

studentDal.add = add;
studentDal.getStudentById = getStudentById;
studentDal.searchStudents = searchStudents;
studentDal.getStudentContactDetailById = getStudentContactDetailById;

studentDal.getStudentsByAnyCriteria = getStudentsByAnyCriteria;
studentDal.getStudentProfileInfo = getStudentProfileInfo;
//studentDal.getStudentByUserID = getStudentByUserID;
studentDal.count = count;
//studentDal.getStudentById = getStudentById;
studentDal.mobileUpdateProfile = mobileUpdateProfile;
//studentDal.getStudentIDByUserID = getStudentIDByUserID;
studentDal.register = register;
studentDal.mobileRegister = mobileRegister;

//studentDal.getAdmissionNumber = getAdmissionNumber;
//studentDal.getRollno = getRollno;
//studentDal.getAllStudents = getAllStudents;
//studentDal.getAllStudentsByClassAndSection = getAllStudentsByClassAndSection;


function add(studentModel) {

    var deferred = Q.defer();

    studentModel
        .save(function (err, result) {
            if (err) deferred.reject(err);

            if (result) {
                deferred.resolve(result);
            } else {
                deferred.resolve();
            }
        });

    return deferred.promise;
}

function searchStudents(pagingSortingParams, searchCriteria) {

    var deferred = Q.defer();

    _user
        .find({})
        //.populate({ path: 'user' })
        .populate({ path: 'exams' })
        //.populate({ path: 'followers' })
        //.populate({ path: 'favorites' })
        //.populate({ path: 'messages' })
        //.populate({ path: 'profileVisitors' })
        //.populate({ path: 'sharedProfiles' })
        .find({ 'userType': 1 })
        .select('_id province city address pinCode avatarUrl exams')
        .exec(function (err, students) {

            if (err) {
                deferred.reject(err);
            }
            if (students) {
                deferred.resolve(students);
            } else {
                deferred.resolve();
            }
        });
    return deferred.promise;
}

function getStudentById(_id) {

    var deferred = Q.defer();

    _user
        .findOne({ '_id': _id })
        .populate({ path: 'user' })
        .populate({ path: 'exams' })
        //.populate({ path: 'followers' })
        //.populate({ path: 'favorites' })
        //.populate({ path: 'messages' })
        //.populate({ path: 'profileVisitors' })
        //.populate({ path: 'sharedProfiles' })
        .select('_id province city address pinCode avatarUrl exams')
        .exec(function (err, student) {

            if (err) {
                deferred.reject(err);
            }
            if (student) {
                deferred.resolve(student);
            } else {
                deferred.resolve();
            }
        });
    return deferred.promise;
}

function getStudentContactDetailById(userID) {

    var deferred = Q.defer();

    _user
        .findOne({ _id: userID })
        .select('_id firstName address city pinCode province phoneNumber email avatarUrl')
        .exec(function (err, student) {

            if (err) {
                deferred.reject(err);
            }
            if (student) {
                deferred.resolve(student);
            } else {
                deferred.resolve();
            }
        });
    return deferred.promise;
}

//function getStudentContactDetailById(studentID) {

//    var deferred = Q.defer();

//    _students
//    .findOne({ _id : studentID })
//    .select('_id address user')
//    .populate({
//        path: 'user',
//        select: 'firstName PhoneNumber email'
//    })
//    .exec(function (err, student) {

//        if (err) {
//            deferred.reject(err);
//        }
//        if (student) {
//            deferred.resolve(student);
//        } else {
//            deferred.resolve();
//        }
//    });
//    return deferred.promise;
//}

function mobileRegister(studentModel, userModel) {

    var userDal = require('./user.dal.js');
    var deferred = Q.defer();

    async.waterfall(
        [
            function (callback) {

                userModel.createdDate = new Date();

                userDal.add(userModel)
            .then(function (user) {

                    if (user) {

                        studentModel.user = user._id;
                        studentModel.createdBy = user._id;
                        studentModel.createdDate = new Date();
                        callback(null, studentModel);
                    } else {
                        deferred.resolve();
                    }
                })
            .catch(function (err) {
                    if (err) deferred.reject(err);
                });
            },
            function (studentModel, callback) {
                add(studentModel)
            .then(function (student) {

                    if (student) {
                        var student = _.pick(student.toObject(), ['_id', 'province', 'city', 'address', 'pinCode', 'avatarUrl', 'exams']);
                        deferred.resolve(student);
                    } else {
                        deferred.resolve();
                    }
                })
            .catch(function (err) {
                    if (err) deferred.reject(err);
                });
            }
        ], function (errs, results) {
            if (errs) {
                async.each(results, rollback, function () {
                    console.log('Rollback done.');
                    res.status(400).send(errs);
                });
            } else {
                console.log('Done.');
                deferred.resolve(results);
            }
        });

    function rollback(doc, callback) {
        if (!doc) { callback(); }
        else {
            userModel.findByIdAndRemove(doc._id, function (err, doc) {
                console.log('Rolled-back document: ' + doc);
                callback();
            });
        }
    }
    return deferred.promise;
}

function getStudentsByAnyCriteria(searchCriteria) {

    var deferred = Q.defer();

    var str = searchCriteria.orderBy.split("_");
    var sortObject = {};
    //var sortType = req.params.sortType;
    //var sortDir = req.params.sortDirection;
    var sortType = str[0];
    var sortDir = str[1] == "DESC" ? -1 : 1;
    sortObject[sortType] = sortDir;

    _user
        .aggregate(
        [
            //{
            //    '$geoNear': {
            //        'near': [searchCriteria.coords[0], searchCriteria.coords[1]],
            //        'spherical': true,
            //        'distanceField': 'dist',
            //        'maxDistance': searchCriteria.distance,
            //        'distanceMultiplier': 6371
            //    }
            //},
            {
                $match: {
                    $and: [
                        { 'userType': searchCriteria.userType },
                        {
                            $or: [
                                { 'country': new RegExp(searchCriteria.location, 'i') },
                                { 'province': new RegExp(searchCriteria.location, 'i') },
                                { 'city': new RegExp(searchCriteria.location, 'i') },
                                { 'address': new RegExp(searchCriteria.location, 'i') }
                                //{ 'geocoderAddress': new RegExp(searchCriteria.location, 'i') },
                                //{ 'geocoderCountry': new RegExp(searchCriteria.location, 'i') }
                            ]
                        },
                        {
                            $or: [{ 'exams.title': new RegExp(searchCriteria.searchText, 'i') },
                                { 'exams.description': new RegExp(searchCriteria.searchText, 'i') }]
                        }
                    ]
                }
            },
            //{
            //    $match: {
            //        $or: [
            //            { 'exams.title': new RegExp(searchCriteria.searchText, 'i') }, 
            //            { 'exams.description': new RegExp(searchCriteria.searchText, 'i') },
            //            { 'country': new RegExp(searchCriteria.searchText, 'i') },
            //            { 'province': new RegExp(searchCriteria.searchText, 'i') },
            //            { 'city': new RegExp(searchCriteria.searchText, 'i') },
            //            { 'address': new RegExp(searchCriteria.searchText, 'i') },
            //            { 'geocoderAddress': new RegExp(searchCriteria.location, 'i') },
            //            { 'geocoderCountry': new RegExp(searchCriteria.location, 'i') }
            //            { 'lastName': new RegExp(searchCriteria.searchText, 'i') },
            //            { 'firstName': new RegExp(searchCriteria.searchText, 'i') }
            //        ]
            //    }
            //},
            {
                $project: {
                    _id: 1,
                    province: 1,
                    city: 1,
                    address: 1,
                    pinCode: 1,
                    firstName: 1,
                    //address: 1,
                    //simCountryISO2: 1,
                    loc: 1,
                    dist: 1,
                    email: 1,
                    isActive: 1,
                    exams: 1
                }
            },
            { "$sort": sortObject },
            { "$limit": searchCriteria.pageSize },
            { "$skip": (searchCriteria.page - 1) * searchCriteria.pageSize }
        ])
        .exec(function (err, student) {

            if (err) {
                deferred.reject(err);
            }
            if (student) {
                deferred.resolve(student);
            } else {
                deferred.resolve();
            }
        });

    /*
    _user
    .find({})
    //.select('_id province city address pinCode firstName phoneNumber simCountryISO2 loc email isActive exams')
    .populate({
        path: 'exams',
        select: '_id title description'
    })
    //.populate({
    //    path: 'user',
    //    select: '_id firstName phoneNumber simCountryISO2 loc email isActive'
    //})
    //.populate({ path: 'user' })
    .populate({ path: 'followers' })
    .populate({ path: 'favorites' })
    .populate({ path: 'messages' })
    .populate({ path: 'profileVisitors' })
    //.populate({ path: 'sharedProfiles' })
    .find({
        $and: [
            {
                $or: [
                    { 'exams.title': new RegExp(searchText, 'i') }, 
                    { 'exams.description': new RegExp(searchText, 'i') },
                    { 'country': new RegExp(searchText, 'i') },
                    { 'province': new RegExp(searchText, 'i') },
                    { 'city': new RegExp(searchText, 'i') },
                    { 'address': new RegExp(searchText, 'i') },
                    { 'geocoderAddress': new RegExp(searchText, 'i') },
                    { 'geocoderCountry': new RegExp(searchText, 'i') },
                    { 'lastName': new RegExp(searchText, 'i') },
                    { 'firstName': new RegExp(searchText, 'i') },
                ]
            }
        ]
    })
    .find({ 'userType': searchCriteria.userType })
    //.find({
    //    'loc' : {
    //        '$geoNear': {
    //            'near': {
    //                'type': 'Point',
    //                'coordinates': [searchCriteria.coords[1] , searchCriteria.coords[0]]
    //            },
    //            'spherical': true, 
    //            'distanceField': 'dist',
    //            'maxDistance': searchCriteria.distance
    //        }
    //    }
    //})

    //.find({
    //    'loc' : {
    //        "$nearSphere": {
    //            "$geometry": {
    //                type: "Point", 
    //                'coordinates': [searchCriteria.coords[1], searchCriteria.coords[0]],
    //                    //near: [searchCriteria.coords[1], searchCriteria.coords[0]],
    //            },
    //            'distanceField': "dist",
    //            'distanceMultiplier': 6371,
    //            'maxDistance': searchCriteria.distance,
    //            //query: { type: "public" },
    //            //includeLocs: "location",
    //            //uniqueDocs: true, 
    //            //num: 5,
    //            'spherical': true
    //        }
    //    }
    //})

    .select('_id province city address pinCode firstName phoneNumber simCountryISO2 loc email isActive exams')
    .populate('dist')
    .exec(function (err, student) {
        
        if (err) {
            deferred.reject(err);
        }
        if (student) {
            deferred.resolve(student);
        } else {
            deferred.resolve();
        }
    });
    */
    return deferred.promise;
}

function getStudentProfileInfo(userID) {

    var deferred = Q.defer();

    _user
        .findOne({ _id: userID })
        .populate({ path: 'exams' })
        //.populate({ path: 'followers' })
        //.populate({ path: 'favorites' })
        //.populate({ path: 'messages' })
        //.populate({ path: 'profileVisitors' })
        //.populate({ path: 'sharedProfiles' })
        .exec(function (err, student) {

            if (err) {
                deferred.reject(err);
            }
            if (student) {
                deferred.resolve(student);
            } else {
                deferred.resolve();
            }
        });
    return deferred.promise;
}

//function getStudentProfileInfo(userID) {

//    var deferred = Q.defer();

//    _students
//    .findOne({})
//    .populate({
//        path: 'user',
//        match : { _id : userID },
//    })
//    .populate({ path: 'user' })
//    .populate({ path: 'exams' })
//    .populate({ path: 'followers' })
//    .populate({ path: 'favorites' })
//    .populate({ path: 'messages' })
//    .populate({ path: 'profileVisitors' })
//    .populate({ path: 'sharedProfiles' })
//    .exec(function (err, student) {

//        if (err) {
//            deferred.reject(err);
//        }
//        if (student) {
//            deferred.resolve(student);
//        } else {
//            deferred.resolve();
//        }
//    });
//    return deferred.promise;
//}

//function getStudentByUserID(userID) {

//    var deferred = Q.defer();

//    _students
//    .findOne({})
//    .select('_id user')
//    .populate({
//        path: 'user',
//        match : { _id : userID },
//        select: 'firstName phoneNumber userType'
//    })
//    .exec(function (err, student) {

//        if (err) {
//            deferred.reject(err);
//        }
//        if (student) {
//            deferred.resolve(student);
//        } else {
//            deferred.resolve();
//        }
//    });
//    return deferred.promise;
//}

//function getStudentIDByUserID(userID) {

//    var deferred = Q.defer();

//    _students
//    .findOne({ 'user._id' : userID })
//    .select('_id')
//    .exec(function (err, student) {

//        if (err) {
//            deferred.reject(err);
//        }
//        if (student) {
//            deferred.resolve(student);
//        } else {
//            deferred.resolve();
//        }
//    });
//    return deferred.promise;
//}

function count() {

    var deferred = Q.defer();

    _students
        .count({})
        .exec(function (err, count) {

            if (err) {
                deferred.reject(err);
            }
            if (count) {
                deferred.resolve(count);
            } else {
                deferred.resolve();
            }
        });
    return deferred.promise;
}

function getStudentById(studentID) {

    var deferred = Q.defer();

    _students
        .findOne({ '_id': studentID })
        .populate({ path: 'user' })
        .populate({ path: 'exams' })
        //.populate({ path: 'followers' })
        //.populate({ path: 'favorites' })
        //.populate({ path: 'messages' })
        //.populate({ path: 'profileVisitors' })
        //.populate({ path: 'sharedProfiles' })
        .exec(function (err, student) {

            if (err) {
                deferred.reject(err);
            }
            if (student) {
                deferred.resolve(student);
            } else {
                deferred.resolve();
            }
        });
    return deferred.promise;
}

function mobileUpdateProfile(updateProfileModel) {

    var deferred = Q.defer();
    //TODO password check add future
    _user.findOne({ _id: updateProfileModel._id }, function (err, user) {
        if (err)
            deferred.reject(err);

        if (user) {
            //if (updateProfileModel.loginType == loginTypeEnum.SocialAccount) {
            user.email = updateProfileModel.email ? updateProfileModel.email : user.email;
            user.phoneNumber = updateProfileModel.phoneNumber ? updateProfileModel.phoneNumber : user.phoneNumber;
            //}

            user.firstName = updateProfileModel.firstName ? updateProfileModel.firstName : user.firstName;
            user.lastName = updateProfileModel.lastName ? updateProfileModel.lastName : user.lastName;

            user.address = updateProfileModel.address ? updateProfileModel.address : user.address;
            user.city = updateProfileModel.city ? updateProfileModel.city : user.city;
            user.province = updateProfileModel.province ? updateProfileModel.province : user.province;
            user.pinCode = updateProfileModel.pinCode ? updateProfileModel.pinCode : user.pinCode;
            //user.country = updateProfileModel.country ? updateProfileModel.country : user.country;
            //user.countryISO2 = updateProfileModel.countryISO2 ? updateProfileModel.countryISO2 : user.countryISO2;
            //user.countryDialCode = updateProfileModel.countryDialCode ? updateProfileModel.countryDialCode : user.countryDialCode;

            //simCountry: updateProfileModel.simCountry ? updateProfileModel.simCountry : user.simCountry;
            //simCountryISO3: updateProfileModel.simCountryISO3 ? updateProfileModel.simCountryISO3 : user.simCountryISO3;
            //simCountryISO2: updateProfileModel.simCountryISO2 ? updateProfileModel.simCountryISO2 : user.simCountryISO2;

            //geocoderCountry: updateProfileModel.geocoderCountry ? updateProfileModel.geocoderCountry : user.geocoderCountry;
            //geocoderCountryISO2: updateProfileModel.geocoderCountryISO2 ? updateProfileModel.geocoderCountryISO2 : user.geocoderCountryISO2;
            //geocoderCountryISO3: updateProfileModel.geocoderCountryISO3 ? updateProfileModel.geocoderCountryISO3 : user.geocoderCountryISO3;
            //geocoderAddress: updateProfileModel.geocoderAddress ? updateProfileModel.geocoderAddress : user.geocoderAddress;

            //gpsCountry: updateProfileModel.gpsCountry ? updateProfileModel.gpsCountry : user.gpsCountry;
            //gpsCountryISO3: updateProfileModel.gpsCountryISO3 ? updateProfileModel.gpsCountryISO3 : user.gpsCountryISO3;
            //gpsCountryISO2: updateProfileModel.gpsCountryISO2 ? updateProfileModel.gpsCountryISO2 : user.gpsCountryISO2;

            user.avatarUrl = updateProfileModel.avatarUrl ? updateProfileModel.avatarUrl : user.avatarUrl;
            //user.AvatarMimeType = mimeType;
            //user.AvatarFileName = fileName;

            var ObjectId = mongoose.Types.ObjectId;
            for (var i = 0; i < updateProfileModel.exams.length; i++) {
                updateProfileModel.exams[i]._id = new ObjectId;
                updateProfileModel.exams[i].createdBy = updateProfileModel._id;
                updateProfileModel.exams[i].createdDate = new Date();
            }
            user.exams = updateProfileModel.exams ? updateProfileModel.exams : user.exams;
            user.isProfileIncomplete = true;

            user.updatedBy = updateProfileModel._id;
            user.updatedDate = new Date();

            user.save(function (err, result) {
                if (err) deferred.reject(err);

                if (result) {
                    result = _.pick(result.toObject(), ['_id', 'username', 'email', 'password', 'isProfileIncomplete', 'firstName', 'phoneNumber', 'province',
                        'city', 'address', 'pinCode', 'countryDialCode', 'avatarUrl', 'exams']);
                    deferred.resolve(result);
                } else {
                    deferred.resolve();
                }
            });
        }
    });
    return deferred.promise;
}

function register(userModel) {

    var deferred = Q.defer();

    userModel.createdDate = new Date();

    this.add(userModel)
        .then(function (user) {

            if (user) {
                var user = _.pick(user.toObject(), ['_id', 'province', 'city', 'address', 'pinCode', 'avatarUrl', 'exams']);
                deferred.resolve(user);
            } else {
                deferred.resolve();
            }
        })
        .catch(function (err) {
            if (err) deferred.reject(err);
        });

    return deferred.promise;
}

//function mobileRegister(userModel) {

//    var deferred = Q.defer();

//    userModel
//        .save(function (err, result) {
//            if (err) deferred.reject(err);
//            if (result) {
//                var result = _.pick(result.toObject(), ['_id', 'province', 'city', 'address', 'pinCode', 'avatarUrl', 'exams']);
//                deferred.resolve(result);
//            } else {
//                deferred.resolve();
//            }
//        });
//    return deferred.promise;
//}

module.exports = studentDal;