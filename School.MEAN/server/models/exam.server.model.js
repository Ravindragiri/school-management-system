﻿var mongoose = require('mongoose');

var examSchema = new mongoose.Schema({

    code: String,
    name: String,
    description: String,
    class: String,
    section: String,

    subjects: [require('../models/subject.server.model').subjectSchema],

    createdBy: {
        type: mongoose.Schema.Types.ObjectId
    },
    createdDate: {
        type: Date
    },
    updatedBy: {
        type: mongoose.Schema.Types.ObjectId
    },
    updatedDate: Date
});

exports.examSchema = examSchema;
mongoose.model('exam', examSchema);