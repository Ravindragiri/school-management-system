﻿var promotionDal = require('../../../server/school.dal/promotion.dal.js');
require('../../school.common/helpers/responseHelper');

var mongoose = require('mongoose'),
    //fs = require('fs-extra'),
    //util = require("util"),
//path = require("path");

var _promotion = mongoose.model('promotion');
var _user = mongoose.model('user');

exports.promote = function (req, res) {

    if (req.query.studentId && req.query.currentClass && req.query.currentSection) {

        var studentId = req.query.studentId;
        var currentClass = req.query.currentClass;
        var currentSection = req.query.currentSection;
        var loggedInUserId;

        var studentUpdateCondition = { $and: [] };
        studentUpdateCondition.$and.push({
            _id: studentId
        });

        var studentUpdateSet = {
            currentClass: currentClass,
            currentSection: currentSection,
            //updatedBy: loggedInUserId,
            updatedDate: new Date()
        };

        promotionDal.update(studentUpdateCondition, studentUpdateSet)
            .then(function (result) {
                if (result) {
                    sendSuccessResponse(res, result, messages.DeleteSuccess);
                } else {
                    sendSuccessResponse(res, result, messages.DeleteFailed);
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
    else {
        sendBadRequestResponse(res);
    }
}

exports.promoteAll = function (req, res) {

}

exports.getAllPassedStudents = function (req, res) {

    if (req.query.classId && req.query.examType) {
        var classId = req.query.classId;
        var examType = req.query.examType;

        promotionDal.getAllPassedStudents(examType)
            .then(function (result) {
                if (result) {

                    sendSuccessResponse(res, result);
                } else {
                    sendNoContentResponse(res);
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
    else {
        sendBadRequestResponse(res);
    }
}

exports.getAllFailedStudents = function (req, res) {

    if (req.query.classId && req.query.examType) {
        var classId = req.query.classId;
        var examType = req.query.examType;

        promotionDal.getAllFailedStudents(examType)
            .then(function (result) {
                if (result) {

                    sendSuccessResponse(res, result);
                } else {
                    sendNoContentResponse(res);
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
    else {
        sendBadRequestResponse(res);
    }
}

