﻿var Q = require('q');
var mongoose = require('mongoose');

require('../models/payment.server.model');
require('../models/user.server.model');

var userDal = require('../../server/school.dal/user.dal.js');

var _paymentModel = mongoose.model('payment');
var _user = mongoose.model('user');

require('../../server/school.common/helpers/deferredHelper');

var dal = {};

dal.add = add;
dal.update = update;
dal.deleteById = deleteById;
dal.getById = getById;
dal.getAll = getAll;

module.exports = dal;

function add(paymentModel) {

    var deferred = Q.defer();

    paymentModel
        .save(function (err, result) {
            createHelper(deferred, err, result);
        });

    return deferred.promise;
}

function update(paymentDTO, userId) {

    var deferred = Q.defer();
    _paymentModel.findById(paymentDTO._id, function (err, payment) {
        if (err) throw err;

        if (payment) {
            try {

                payment.title = paymentDTO.title ? paymentDTO.title : payment.title;
                payment.description = paymentDTO.description ? paymentDTO.description : payment.description;
                payment.startDate = paymentDTO.startDate ? new Date(paymentDTO.startDate) : payment.startDate;
                payment.endDate = paymentDTO.endDate ? new Date(paymentDTO.endDate) : payment.endDate;

                //payment.updatedBy = userId;
                //payment.updatedDate = new Date();

                payment.save(function (err, result) {
                    if (err) {
                        console.log(err);
                        deferred.reject(err);
                    }
                });
            }
            catch (error) {
                console.log(error);
            }
        }
    });
    return deferred.promise;
}

function deleteById(paymentId) {

    var deferred = Q.defer();

    _paymentModel.findByIdAndRemove({ _id: paymentId }, function (err) {
        if (err) {
            deferred.reject(err);
        }
        else {
            console.log('payment successfully deleted!');
            deferred.resolve(true);
        }
    });
    return deferred.promise;
}

function getById(paymentId) {

    var deferred = Q.defer();

    _paymentModel
        .findOne({ '_id': paymentId })
        .exec(function (err, result) {
            execHelper(deferred, err, result);
        });
    return deferred.promise;
}

function getAll() {

    var deferred = Q.defer();

    _paymentModel
        .find({})
        .exec(function (err, result) {
            execHelper(deferred, err, result);
        });
    return deferred.promise;
}
