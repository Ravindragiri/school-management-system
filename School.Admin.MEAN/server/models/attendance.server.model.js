﻿var mongoose = require('mongoose');

var attendanceSchema = new mongoose.Schema({

    status: Boolean,
    remarks: String,
    attendanceDate: Date,
    academicYear: String,

    createdBy: {
        type: mongoose.Schema.Types.ObjectId
    },
    createdDate: {
        type: Date
    },
    updatedBy: {
        type: mongoose.Schema.Types.ObjectId
    },
    updatedDate: Date
});

exports.attendanceSchema = attendanceSchema;
mongoose.model('attendance', attendanceSchema);