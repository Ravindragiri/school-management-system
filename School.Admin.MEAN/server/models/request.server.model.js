﻿var mongoose = require('mongoose');

var requestSchema = new mongoose.Schema({

    title: String,
    desc: String,
    startDate: Date,
    endDate: Date
});

mongoose.model('request', requestSchema);