﻿var mongoose = require('mongoose');

var paymentSchema = new mongoose.Schema({

    expenseCategory: Number,

    title: String,
    purpose: String,
    paymentType: String,
    paymentMethod: String,
    invoiceRef: { type: mongoose.Schema.Types.ObjectId, ref: 'invoice', required: true },
    studentRef: { type: mongoose.Schema.Types.ObjectId, ref: 'student', required: true },
    status: String,

    buyer_name: {
        type: String
    },
    email: {
        type: String
    },
    phone: {
        type: String
    },
    amount: String,
    redirect_url: String,
    webhook: String,

    allow_repeated_payments: String,
    send_email: String,
    send_sms: String,
    expires_at: Date,

    acedamicYear: String,

    createdBy: {
        type: mongoose.Schema.Types.ObjectId
    },
    createdDate: {
        type: Date
    },
    updatedBy: {
        type: mongoose.Schema.Types.ObjectId
    },
    updatedDate: Date
});

exports.paymentSchema = paymentSchema;
mongoose.model('payment', paymentSchema);