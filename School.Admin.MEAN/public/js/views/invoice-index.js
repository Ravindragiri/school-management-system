﻿// When the browser is ready...
/***** form validation starts *****/
$.validator.addMethod("passwordRegex", function (value, element) {
    return this.optional(element) || /^[a-z0-9\s]+$/i.test(value);
}, "Password must contain only letters or numbers.");

jQuery.validator.addMethod("lettersonly", function (value, element) {
    return this.optional(element) || /^[a-z\s]+$/i.test(value);
}, "Only alphabetical characters.");

$("#registrationForm").validate({
    // Specify the validation rules
    rules: {
        FirstName: {
            lettersonly: true
        },
        Password: {
            passwordRegex: true,
            minlength: 7
        },
        confirmPassword: {
            passwordRegex: true,
            minlength: 7,
            equalTo: "#Password",
        },
        Email: {
            email: true
        },
        Address: {
            required: true
        },
        City: {
            required: true
        },
        Province: {
            required: true
        },
        PinCode: {
            required: true
        },
        Country: {
            required: true
        },
        ExpectedWages: {
            currencyonly: true
        },
        chkTermsConditions: "required"
    },
    // Specify the validation error messages
    messages: {
        "Password": {
            passwordRegex: "Password must contain only letters or numbers.",
            minlength: "Your password is required to be at least 7 characters."
        },
        "confirmPassword": {
            passwordRegex: "Confirm Password must contain only letters or numbers.",
            minlength: "Your confirm Password is required to be at least 7 characters.",
            equalTo: "Enter Confirm Password Same as Password."
        },
        Email: "Please enter a valid email address.",
        chkTermsConditions: "Please accept our policy."
    },
    errorPlacement: function (error, element) {
        if (element.attr("name") == "chkTermsConditions") {
            error.insertAfter(element.parent("label").parent("div"));
        } else {
            error.insertAfter(element);
        }
    }
});



/***** form validation ends *****/

$(function () {

    $("#profileImage").change(function () {
        var data = new FormData();
        var files = $("#profileImage").get(0).files;
        if (files.length > 0) {
            data.append("HelpSectionImages", files[0]);
        }
        $.ajax({
            url: resolveUrl(uploadProfileImageUrl),
            type: "POST",
            processData: false,
            contentType: false,
            data: data,
            success: function (response) {
                //code after success

            },
            error: function (er) {
                alert(er);
            }

        });
    });

});

$(function () {

    $("#birthDate").datepicker();

    $('.list-group.checked-list-box .list-group-item').each(function () {

        // Settings
        var $widget = $(this),
            $checkbox = $('<input type="checkbox" class="hidden" />'),
            color = ($widget.data('color') ? $widget.data('color') : "primary"),
            style = ($widget.data('style') == "button" ? "btn-" : "list-group-item-"),
            settings = {
                on: {
                    icon: 'glyphicon glyphicon-check'
                },
                off: {
                    icon: 'glyphicon glyphicon-unchecked'
                }
            };

        $widget.css('cursor', 'pointer')
        $widget.append($checkbox);

        // Event Handlers
        $widget.on('click', function () {
            $checkbox.prop('checked', !$checkbox.is(':checked'));
            $checkbox.triggerHandler('change');
            updateDisplay();
        });
        $checkbox.on('change', function () {
            updateDisplay();
        });


        // Actions
        function updateDisplay() {
            var isChecked = $checkbox.is(':checked');

            // Set the button's state
            $widget.data('state', (isChecked) ? "on" : "off");

            // Set the button's icon
            $widget.find('.state-icon')
                .removeClass()
                .addClass('state-icon ' + settings[$widget.data('state')].icon);

            // Update the button's color
            if (isChecked) {
                $widget.addClass(style + color + ' active');
            } else {
                $widget.removeClass(style + color + ' active');
            }
        }

        // Initialization
        function init() {

            if ($widget.data('checked') == true) {
                $checkbox.prop('checked', !$checkbox.is(':checked'));
            }

            updateDisplay();

            // Inject the icon if applicable
            if ($widget.find('.state-icon').length == 0) {
                $widget.prepend('<span class="state-icon ' + settings[$widget.data('state')].icon + '"></span>');
            }
        }
        init();
    });

    $('#get-checked-data').on('click', function (event) {
        event.preventDefault();
        var checkedItems = {}, counter = 0;
        $("#check-list-box li.active").each(function (idx, li) {
            checkedItems[counter] = $(li).text();
            counter++;
        });
        $('#display-json').html(JSON.stringify(checkedItems, null, '\t'));
    });
});

/***** angular ngInvoiceIndexController starts *****/
var app = angular.module('schoolApp', ['ngTouch', 'ui.grid', 'ui.multiselect']);

app.controller('ngInvoiceIndexController', ['InvoiceService', '$scope', '$http',
    function (InvoiceService, $scope, $http) {

        $scope.addMode = true;

        $scope.gridOptions = {
            columnDefs: [
                { name: 'firstName', field: 'studentRef.firstName', cellTemplate: '<div class="ui-grid-cell-contents">{{ COL_FIELD }}</div>' },
                { name: 'purpose', field: 'purpose', cellTemplate: '<div class="ui-grid-cell-contents">{{ COL_FIELD }}</div>' },
                { name: 'total', field: 'total', cellTemplate: '<div class="ui-grid-cell-contents">{{ COL_FIELD }}</div>' },
                { name: 'paid', field: 'paid', cellTemplate: '<div class="ui-grid-cell-contents">{{ COL_FIELD }}</div>' },
                { name: 'status', cellTemplate: '<button type="button" class="btn btn-xs" ng-click="grid.appScope.editInvoice(row)">{{ COL_FIELD }}</button>' },
                { name: 'date', field: 'createdDate', cellTemplate: '<div class="ui-grid-cell-contents">{{ COL_FIELD }}</div>' },
            ],
            excludeProperties: '__metadata',
        };

        $scope.validateForm = function () {
            $scope.registrationFormReset = false;
            $scope.showValidation = function () { return $scope.registrationForm.$invalid; };
        }

        //$scope.reset = function () {
        //    $scope.form = angular.copy(reviewFormFields());
        //    $scope.registrationFormReset = true;
        //    $scope.reviewForm.$setPristine();
        //}

        $scope.showServerErrorMessages = function (response, errors) {

            $.each(errors, function (index, error) {

                $scope.$apply(function () {

                    $scope.invalidelements[error.Element] = true;
                    $('label[ng-show="invalidelements.' + error.Element + '"]').css('display', 'inline-block');
                    $('.' + error.Element).focus();
                });
            });
        }

        $scope.subjects = [];

        $scope.load = function () {
            InvoiceService.readAll().then(function (response) {
                $scope.gridOptions.data = response.data;
                //$scope.gridOptions.data = JSON.parse(response.data);
            });

            InvoiceService.getAllSubjects().then(function (response) {
                $scope.subjects = JSON.parse(response.data);
            });
        }

        $scope.load();

        $scope.insert = function () {

            //$scope.validateForm();
            $("#registrationForm").validate();

            //if ($scope.registrationForm.$valid) {
            if ($('#registrationForm').valid()) {

                $scope.loading = true;

                var postData = {
                    __RequestVerificationToken: angular.element("input[name='__RequestVerificationToken']").val(),
                    invoiceDTO: $scope.invoiceDTO
                };

                $.ajax({
                    type: "POST",
                    url: 'http://localhost:1238/api/invoice/insert',
                    data: postData,
                    dataType: "json",
                    accept: 'application/json',
                    beforeSend: function () {

                    },
                    success: function (response) {

                        if (response != null) {
                            var status = response.statusCode;

                            switch (status) {
                                case 200:
                                    alert("Record has been saved successfully.");
                                    break;
                                case 0:
                                    alert("Your request cannot be processed now due to some technical issue. please try again later.");
                                    break;
                                case -1:
                                case 400:
                                case 401:
                                case 204:
                                    alert(response.message);
                                    break;
                                default:
                            }
                        }
                    },
                    error: function (xhr, status, errorThrown) {
                        alert("An error occurred while processing your request.");
                    }
                });

                //
            }
        };

        $scope.update = function () {

            var data = {
                invoiceDTO: $scope.invoiceDTO
            }
            InvoiceService.update(data).then(function (response) {
                
                if (response != null) {
                    var status = response.statusCode;

                    switch (status) {
                        case 200:
                            $scope.addMode = true;
                            alert("Record has been updated successfully.");
                            break;
                        case 0:
                            alert("Your request cannot be processed now due to some technical issue. please try again later.");
                            break;
                        case -1:
                        case 400:
                        case 401:
                        case 204:
                            alert(response.message);
                            break;
                        default:
                    }
                }
            });
        }

        $scope.cancel = function () {
            $scope.invoiceDTO = {};
            $scope.addMode = true;
        }

        $scope.editInvoice = function (row) {

            if (row.entity) {
                $scope.invoiceDTO = row.entity;
                //var index = $scope.gridOptions.data.indexOf(row.entity);
                $scope.addMode = false;
            }
        };

        $scope.onchange = function () {
            if ($scope.standard && $scope.section) {
                var data = {
                    class: $scope.standard,
                    section: $scope.section
                };

                InvoiceService.getAllStudentsByClassAndSection(data).then(function (response) {
                    $scope.students = response.data;
                });
            }
        };

        $scope.deleteInvoice = function (row) {

            if (row.entity) {

                var id = row.entity._id;
                var userId = row.entity.userRef._id;
                InvoiceService.delete(id, userId).then(function (response) {
                    
                    if (response != null) {
                        var status = response.statusCode;

                        switch (status) {
                            case 200:
                                var index = $scope.gridOptions.data.indexOf(row.entity);
                                $scope.gridOptions.data.splice(index, 1);
                                alert("Record has been deleted successfully.");
                                break;
                            case 0:
                                alert("Your request cannot be processed now due to some technical issue. please try again later.");
                                break;
                            case -1:
                            case 400:
                            case 401:
                            case 204:
                                alert(response.message);
                                break;
                            default:
                        }
                    }
                });
            }
        };
    }]);
/***** angular ngInvoiceIndexController ends *****/

(function () {

    angular.module('schoolApp')
        .service('InvoiceService', ['$http', InvoiceService]);

    function InvoiceService($http) {

        var self = this;
        var baseUrl = 'http://localhost:1238';

        //var objectName = 'products';

        self.readAll = function () {
            return $http({
                method: 'GET',
                url: baseUrl + '/api/invoice/getAllInvoiceInfo',
                //url: baseUrl + objectName
            }).then(function (response) {
                return response.data;
            });
        };

        self.getAllSubjects = function () {
            return $http({
                method: 'GET',
                url: baseUrl + '/api/subject/getAllSubjects',
                //url: baseUrl + objectName
            }).then(function (response) {
                return response.data;
            });
        };

        self.getAllStudentsByClassAndSection = function (data) {
            return $http({
                method: 'GET',
                url: 'http://localhost:1238/api/student/getAllStudentsByClassAndSection',
                params: data
            }).then(function (response) {
                return response.data;
            });
        };

        self.readOne = function (id) {
            return $http({
                method: 'GET',
                url: baseUrl + objectName + '/' + id
            }).then(function (response) {
                return response.data;
            });
        };

        self.update = function (data) {
            return $http({
                method: 'POST',
                url: baseUrl + '/api/invoice/update',
                data: data
            }).then(function (response) {
                return response.data;
            });
        };

        self.delete = function (id, userId) {
            return $http({
                method: 'POST',
                //url: baseUrl + objectName + '/' + id
                url: 'http://localhost:1238/api/invoice/remove' + '?id=' + id + '&userId=' + userId
            }).then(function (response) {
                return response.data;
            });
        };

    }
} ());