﻿'use strict';

var _ = require('lodash');

module.exports = _.extend(

require('../models/contactUs.server.model'),
require('../models/holiday.server.model'),
require('../models/fee.server.model'),
//require('../models/share.server.model'),
//require('../models/rating.server.model'),
//require('../models/follower.server.model'),
//require('../models/favorite.server.model'),
require('../models/subject.server.model'),
require('../models/exam.server.model'),
require('../models/exam.timeTable.server.model'),
require('../models/classRoutine.server.model'),
//require('../models/blockUser.server.model'),
require('../models/user.server.model'),
require('../models/student.server.model'),
require('../models/parent.server.model'),
require('../models/officeStaff.server.model'),
require('../models/payment.server.model'),
require('../models/invoice.server.model')

);