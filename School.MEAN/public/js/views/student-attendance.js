﻿var app = angular.module('schoolApp', ['ngTouch']);

app
    .directive("datepicker", function () {
        return {
            restrict: "A",
            require: "ngModel",
            link: function (scope, elem, attrs, ngModelCtrl) {
                var updateModel = function (dateText) {
                    scope.$apply(function () {
                        ngModelCtrl.$setViewValue(dateText);
                    });
                };
                var options = {
                    dateFormat: "dd/mm/yy",
                    onSelect: function (dateText) {
                        updateModel(dateText);
                    }
                };
                elem.datepicker(options);
            }
        }
    })
    .controller('ngStudentAttendanceController', ['StudentAttendanceService', '$scope', function (StudentAttendanceService, $scope) {

        $scope.addMode = true;

        $scope.students = [];
        $scope.search = function () {
            var data = {
                class: $scope.studentDTO.currentClass,
                section: $scope.studentDTO.currentSection
            };

            StudentAttendanceService.getAllStudentsByClassAndSection(data).then(function (response) {
                $scope.students = response.data;
                $scope.studentDTO.currentRollNo = response.data.userRef.currentRollNo;
            });
        }

        $scope.insert = function () {

            var data = {
                studentId: $scope.studentDTO._id,
                attendances: $scope.studentDTO.attendances
            };

            StudentAttendanceService.create(data).then(function (response) {
                if (response != null) {
                    var status = response.statusCode;

                    switch (status) {
                        case 200:
                            alert("Record has been saved successfully.");
                            break;
                        case 0:
                            alert("Your request cannot be processed now due to some technical issue. please try again later.");
                            break;
                        case -1:
                        case 400:
                        case 401:
                        case 404:
                            alert(response.message);
                            break;
                        default:
                    }
                }
            });
        }

        $scope.studentDTO = {
            attendances: []
        };

        $scope.update = function (row) {

            var selectedDates = selected_dates;
            var updatedAttendances = [];

            var status = $scope.studentDTO.attendance.status;
            selectedDates.forEach(function (selectedDate) {
                updatedAttendances.push({
                    status: status,
                    attendanceDate: new Date(selectedDate)
                })
            });

            var data = {
                studentDTO: {
                    _id: $scope.studentDTO._id,
                    attendances: updatedAttendances
                }
            };

            StudentAttendanceService.update(data).then(function (response) {
                
                if (response != null) {
                    var status = response.statusCode;

                    switch (status) {
                        case 200:
                            $scope.addMode = true;
                            alert("Record has been updated successfully.");
                            break;
                        case 0:
                            alert("Your request cannot be processed now due to some technical issue. please try again later.");
                            break;
                        case -1:
                        case 400:
                        case 401:
                        case 404:
                            alert(response.message);
                            break;
                        default:
                    }
                }
            });
        }

        $scope.cancel = function (row) {
            $scope.studentDTO = {};
            $scope.addMode = true;
        }

        $scope.load = function () {
            $scope.addMode = false;

            var data = {
                studentId: "5a7cadf559e31f08448a3c1d",
            };

            StudentAttendanceService.readOne(data).then(function (response) {
                if (response.data.attendances) {
                    processDates(response.data.attendances);
                }
            });
        };

        $scope.load();
    }]);


(function () {

    angular.module('schoolApp')
        .service('StudentAttendanceService', ['$http', StudentAttendanceService]);

    function StudentAttendanceService($http) {

        var self = this;
        var baseUrl = 'http://localhost:1238';

        //var objectName = 'products';

        self.readAll = function () {
            return $http({
                method: 'GET',
                url: baseUrl + '/api/attendance/getAllAttendances',
                //url: baseUrl + objectName
            }).then(function (response) {
                return response.data;
            });
        };

        self.getAllStudentsByClassAndSection = function (data) {
            return $http({
                method: 'GET',
                url: baseUrl + '/api/student/getAllStudentsByClassAndSection',
                params: data,
                //url: baseUrl + objectName
            }).then(function (response) {
                return response.data;
            }).catch(function (response) {
                console.error('Error occurred:', response.status, response.data);
            });
        };

        self.readOne = function (id) {
            return $http({
                method: 'GET',
                params: id,
                //url: baseUrl + objectName + '/' + id
                url: baseUrl + '/api/attendance/getAttendanceByStudentId',
            }).then(function (response) {
                return response.data;
            });
        };

        self.create = function (data) {
            return $http({
                method: 'POST',
                url: baseUrl + '/api/attendance/addAttendance',
                data: data,
                params: {
                    returnObject: true
                },
            }).then(function (response) {
                return response.data;
            });
        };

        self.update = function (data) {
            return $http({
                method: 'POST',
                //url: baseUrl + objectName + '/' + id,
                url: baseUrl + '/api/attendance/update',
                data: data
            }).then(function (response) {
                return response.data;
            });
        };
    }
} ());

$(function () {

    //processDates();

    //$("#datepicker").datepicker({
    //    beforeShowDay: disableSpecificDates,
    //    selectMultiple: true,
    //    dateFormat: "yy-mm-dd",
    //    onSelect: function () {
    //        var selected_date = $(this).val();
    //        selected_dates.push(selected_date);
    //        alert(selected_dates);
    //    }
    //});

});

// An array of highlighting dates ( 'dd/mm/yy' )
//var present_dates = ['1/2/2018', '11/2/2018', '18/2/2018', '28/2/2018'];
//var absent_dates = ['2/2/2018', '12/2/2018', '19/2/2018'];

var present_dates = [];
var absent_dates = [];


//var myAttendances = [
//    {
//        "status": false,
//        "remarks": null,
//        "attendanceDate": "2018-02-01T02:14:56.000Z",
//        "academicYear": "2017"
//    },
//    {
//        "status": true,
//        "remarks": null,
//        "attendanceDate": "2018-02-02T02:14:56.000Z",
//        "academicYear": "2017"
//    }
//];

var selected_dates = [];

function getFormatDate(date) {

    var month = date.getMonth() + 1;
    var year = date.getFullYear();
    var day = date.getDate();

    //Change format of date
    var newdate = day + "/" + month + '/' + year;

    return newdate;
}

function processDates(attendances) {

    selected_dates = [];
    present_dates = [];
    absent_dates = [];

    var i;
    for (i = 0; i < attendances.length; i++) {
        if (attendances[i].attendanceDate) {
            if (!attendances[i].status) {
                absent_dates.push(getFormatDate(new Date(attendances[i].attendanceDate)));
            }
            else if (attendances[i].status) {
                present_dates.push(getFormatDate(new Date(attendances[i].attendanceDate)));
            }
        }
    }

    $("#datepicker").datepicker('refresh');
    $("#datepicker").datepicker({
        beforeShowDay: disableSpecificDates,
        selectMultiple: true,
        dateFormat: "yy-mm-dd",
        onSelect: function () {
            var selected_date = $(this).val();
            selected_dates.push(selected_date);
        }
    });
}

function disableSpecificDates(date) {

    var newdate = getFormatDate(date);

    if ($.datepicker.noWeekends(date)[0]) {

        if (jQuery.inArray(newdate, absent_dates) != -1) {
            return [true, 'absent', ''];
        }
        else if (jQuery.inArray(newdate, present_dates) != -1) {
            return [true, 'present', ''];
        }
        else {
            return [true];
        }
    }
    else {
        return true;
    }
}