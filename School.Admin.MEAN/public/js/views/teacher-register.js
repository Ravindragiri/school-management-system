﻿// When the browser is ready...
/***** form validation starts *****/
$.validator.addMethod("passwordRegex", function (value, element) {
    return this.optional(element) || /^[a-z0-9\s]+$/i.test(value);
}, "Password must contain only letters or numbers.");

jQuery.validator.addMethod("lettersonly", function (value, element) {
    return this.optional(element) || /^[a-z\s]+$/i.test(value);
}, "Only alphabetical characters.");

$("#registrationForm").validate({
    // Specify the validation rules
    rules: {
        firstName: {
            lettersonly: true
        },
        password: {
            passwordRegex: true,
            minlength: 7
        },
        confirmPassword: {
            passwordRegex: true,
            minlength: 7,
            equalTo: "#password",
        },
        email: {
            email: true
        },
        address: {
            required: true
        },
        city: {
            required: true
        },
        province: {
            required: true
        },
        pinCode: {
            required: true
        }
    },
    // Specify the validation error messages
    messages: {
        "password": {
            passwordRegex: "Password must contain only letters or numbers.",
            minlength: "Your password is required to be at least 7 characters."
        },
        "confirmPassword": {
            passwordRegex: "Confirm Password must contain only letters or numbers.",
            minlength: "Your confirm Password is required to be at least 7 characters.",
            equalTo: "Enter Confirm Password Same as Password."
        },
        email: "Please enter a valid email address."
    },
    errorPlacement: function (error, element) {
        error.insertAfter(element);
    }
});

/***** form validation ends *****/

$(function () {

    $("#profileImage").change(function () {
        var data = new FormData();
        var files = $("#profileImage").get(0).files;
        if (files.length > 0) {
            data.append("HelpSectionImages", files[0]);
        }
        $.ajax({
            url: resolveUrl(uploadProfileImageUrl),
            type: "POST",
            processData: false,
            contentType: false,
            data: data,
            success: function (response) {
                //code after success

            },
            error: function (er) {
                alert(er);
            }

        });
    });

});

$(function () {

    $("#birthDate").datepicker();
});

/***** angular ngTeacherIndexController starts *****/
schoolApp.controller('ngTeacherIndexController', ['$scope', '$http', function ($scope, $http) {

    $scope.validateRegisterForm = function () {
        $scope.registrationFormReset = false;
        $scope.showRegisterValidation = function () { return $scope.registrationForm.$invalid; };
    }

    //$scope.reset = function () {
    //    $scope.form = angular.copy(reviewFormFields());
    //    $scope.registrationFormReset = true;
    //    $scope.reviewForm.$setPristine();
    //}

    $scope.register = function () {

        //$scope.validateRegisterForm();
        $("#registrationForm").validate();

        //if ($scope.registrationForm.$valid) {
        if ($('#registrationForm').valid()) {

            $scope.loading = true;

            var postData = {
                teacherDTO: $scope.teacherDTO
            };

            $.ajax({
                type: "POST",
                url: registerUrl,
                data: postData,
                dataType: "json",
                accept: 'application/json',
                beforeSend: function () {

                },
                success: function (response) {

                    if (response != null) {
                        var status = response.statusCode;

                        switch (status) {
                            case 1:
                                //var objTravel = JSON.parse(response.data);
                                window.location.href = '/account/login/';
                                break;
                            case 0:
                                alert(response.message);
                                break;
                            case -10:
                                $scope.showServerErrorMessages(response, response.serverErrors);
                                break;
                            case -500:
                                alert(response.message)
                                //window.location.href = errorPage;
                                break;
                            default:
                        }
                    }
                },
                error: function (xhr, status, errorThrown) {
                    alert("An error occurred while processing your request.");
                }
            });

            //
        }
    };

    $scope.showServerErrorMessages = function (response, errors) {

        $.each(errors, function (index, error) {

            $scope.$apply(function () {

                $scope.invalidelements[error.Element] = true;
                $('label[ng-show="invalidelements.' + error.Element + '"]').css('display', 'inline-block');
                $('.' + error.Element).focus();
            });
        });
    }
}]);
/***** angular ngTeacherIndexController ends *****/
