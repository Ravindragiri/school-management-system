﻿var mongoose = require('mongoose');

var userSchema = new mongoose.Schema({

    registrationVerificationMethodType: Number,

    firstName: String,
    lastName: String,
    sex: String,
    caste: String,
    religion: String,
    birthDate: String,
    bloodGroup: String,

    phoneNumber: String,

    username: String,
    email: String,
    emailConfirmed: Boolean,
    password: String,
    passwordHash: String,
    passwordSalt: String,

    address: String,
    city: String,
    //district: String,
    province: String,
    pinCode: String,

    avatarUrl: String,
    avatarMimeType: String,
    avatarFileName: String,

    isActive: Boolean,
    isDeleted: Boolean,
    isProfileIncomplete: Boolean,

    securityStamp: String,
    phoneNumberConfirmed: String,
    twoFactorEnabled: String,
    lockoutEndDate: String,
    accessFailedCount: Number,
    userType: String,

    loginAttempts: { type: Number, required: true, default: 0 },
    lockUntil: { type: Number },

    roles: {
        type: [{
            type: String
        }]
    },
    createdBy: {
        type: mongoose.Schema.Types.ObjectId,
    },
    createdDate: {
        type: Date,
    },
    updatedBy: {
        type: mongoose.Schema.Types.ObjectId
    },
    updatedDate: Date
});

// expose enum on the model, and provide an internal convenience reference 
userSchema.statics.failedLogin = {
    NOT_FOUND: 0,
    PASSWORD_INCORRECT: 1,
    MAX_ATTEMPTS: 2
};

userSchema.methods.comparePassword = function (candidatePassword, cb) {

    var isMatch = candidatePassword == this.password;
    cb(null, isMatch);
};


exports.userSchema = userSchema;
mongoose.model('user', userSchema);
