﻿module.exports = function (app) {

    var contactUs = require('../controllers/contactUs.server.controller');
    app.get('/contactUs/contact', contactUs.index);
    app.get('/contactUs', contactUs.index);
    app.get('/contact', contactUs.index);

    var contactUsApiController = require('../controllers/api/contactUs.server.api.controller');
    app.post('/api/contactUs/sendMessage', contactUsApiController.sendMessage);
};