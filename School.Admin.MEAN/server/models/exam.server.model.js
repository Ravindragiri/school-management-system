﻿var mongoose = require('mongoose');

var examSchema = new mongoose.Schema({

    code: String,
    name: String,
    description: String,

    class: String,
    section: String,
    rollNo: String,

    examType: String,
    subjects: [require('../models/subject.server.model').subjectSchema],

    result: String,
    percentage: String,
    letterGrade: String,
    rank: String,

    academicYear: String,
    comments: String,

    examDate: String,

    createdBy: {
        type: mongoose.Schema.Types.ObjectId
    },
    createdDate: {
        type: Date
    },
    updatedBy: {
        type: mongoose.Schema.Types.ObjectId
    },
    updatedDate: Date
});

exports.examSchema = examSchema;
mongoose.model('exam', examSchema);