﻿var studentApplyDal = require('../../../server/school.dal/student.apply.dal.js');
require('../../school.common/helpers/responseHelper');
require('../../school.common/appConstants');

var mongoose = require('mongoose');
//fs = require('fs-extra'),
//util = require("util"),
//path = require("path");

var _students = mongoose.model('student');
var _user = mongoose.model('user');

exports.getTotalClassAndSectionInfo = function (req, res) {

    var totalClassAndSections = [];
    var standards = classes;
    var divisions = sections;

    if (standards && divisions) {
        try {
            var i, j;
            for (i = 0; i < standards.length; i++) {
                for (j = 0; j < divisions.length; j++) {
                    totalClassAndSections.push({ currentClass: standards[i].value, currentSection: divisions[j].value });
                }
            }
            sendSuccessResponse(res, totalClassAndSections);
        }
        catch (err) {
            console.log(err);
            sendErrorResponse(res);
        }
    }
    else {
        sendFailResponse(res);
    }
}

exports.applyRollNoInBulk = function (req, res) {

    if (req.query.classId && req.query.sectionId) {
        var classId = req.query.classId;
        var sectionId = req.query.sectionId;

        studentApplyDal.getStudentListSortByName(classId, sectionId)
            .then(function (students) {
                if (students) {

                    students.forEach(function (student, index) {
                        studentApplyDal.applyRollNo(student._id, ++index);
                        //.then(function (result) {
                        //    if (result) {
                        //        sendSuccessResponse(res, result);
                        //    } else {
                        //        sendFailResponse(res);
                        //    }
                        //})
                        //.catch(function (err) {
                        //    console.log(err);
                        //    sendErrorResponse(res);
                        //});
                    });
                    sendSuccessResponse(res);

                } else {
                    sendFailResponse(res);
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
    else {
        sendBadRequestResponse(res);
    }
}

exports.getStudentListSortByName = function (req, res) {

    if (req.query.classId && req.query.sectionId) {
        var classId = req.query.classId;
        var sectionId = req.query.sectionId;

        studentApplyDal.getStudentListSortByName(classId, sectionId)
            .then(function (students) {
                if (students) {
                    sendSuccessResponse(res, students);
                } else {
                    sendFailResponse(res);
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
    else {
        sendBadRequestResponse(res);
    }
}

exports.applyRollNo = function (req, res) {

    var studentId = req.body.studentId;
    var classId = req.body.classId;

    studentApplyDal.applyRollNo(studentId, classId)
        .then(function (result) {
            if (result) {
                sendSuccessResponse(res, result);
            } else {
                sendFailResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
}

exports.applyRegistrationNoInBulk = function (req, res) {

    if (req.query.classId && req.query.sectionId) {
        var classId = req.query.classId;
        var sectionId = req.query.sectionId;

        studentApplyDal.getStudentListSortByName(classId, sectionId)
            .then(function (students) {
                if (students) {

                    students.forEach(function (student, index) {
                        studentApplyDal.applyRegistrationNo(student._id, ++index);
                        //.then(function (result) {
                        //    if (result) {
                        //        sendSuccessResponse(res, result);
                        //    } else {
                        //        sendFailResponse(res);
                        //    }
                        //})
                        //.catch(function (err) {
                        //    console.log(err);
                        //    sendErrorResponse(res);
                        //});
                    });
                    sendSuccessResponse(res);

                } else {
                    sendFailResponse(res);
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
    else {
        sendBadRequestResponse(res);
    }
}

exports.applyRegistrationNo = function (req, res) {

    var studentId = req.body.studentId;
    var classId = req.body.classId;

    studentApplyDal.applyRegistrationNo(studentId, classId)
        .then(function (result) {
            if (result) {
                sendSuccessResponse(res, result);
            } else {
                sendFailResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
}