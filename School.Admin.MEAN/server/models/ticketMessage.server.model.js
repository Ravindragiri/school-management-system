﻿var mongoose = require('mongoose');

var ticketMessageSchema = new mongoose.Schema({

    code: String,
    message: String,
    file: String,
    senderType: String,
    createdBy: {
        type: mongoose.Schema.Types.ObjectId
    },
    createdDate: {
        type: Date
    }
});

mongoose.model('ticketMessage', ticketMessageSchema);