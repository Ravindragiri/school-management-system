﻿var Q = require('q');
var mongoose = require('mongoose')
require('../models/student.server.model');
require('../models/exam.server.model');
require('../models/user.server.model');
var _student = mongoose.model('student');
var _exam = mongoose.model('exam');
var _user = mongoose.model('user');
var ObjectId = mongoose.Types.ObjectId;

var dal = {};

dal.getExamDetails = getExamDetails;
dal.getAllStudentMarks = getAllStudentMarks;
dal.getExamDetailById = getExamDetailById;
dal.getStudentMarksDetails = getStudentMarksDetails;
dal.getExamBySubject = getExamBySubject;
dal.getStudentRollNo = getStudentRollNo;

dal.getStudentsByClassAndSection = getStudentsByClassAndSection;
dal.getStudentRollNo = getStudentRollNo;
dal.getStudents = getStudents;
dal.loadClass = loadClass;
dal.loadSection = loadSection;
dal.searchStudent = searchStudent;
dal.searchMarks = searchMarks;
dal.insertMarks = insertMarks;
dal.updateMarks = updateMarks;
dal.remove = remove;
dal.update = update;


function getExamDetails(userId) {

    var deferred = Q.defer();

    _user
        .findOne({ _id: userId })
        .select('_id firstName phoneNumber province city address pinCode email exams')
        .populate({
            path: 'organs',
            select: '_id title description'
        })
        .lean()
        .exec(function (err, result) {

            if (err) {
                deferred.reject(err);
            }
            if (result) {
                deferred.resolve(result);
            } else {
                deferred.resolve();
            }
        });
    return deferred.promise;
}

function getAllStudentMarks(classId, examType) {

    var deferred = Q.defer();

    _student
        .aggregate(
        {
            $match: {
                $or: [
                    { "exams.examType": { $eq: examType } },
                    { 'exams.class': classId }
                ]
            }
        },
        {
            $unwind: {
                path: '$exams',
                preserveNullAndEmptyArrays: true 
            }
        },
        //{ $match: { '_id': ObjectId(studentId) } },
        { $lookup: { from: "users", localField: "userRef", foreignField: "_id", as: "user" } },
        { $unwind: "$user" },
        {
            $project: {
                "_id": 1,

                userId: "$user._id",
                firstName: "$user.firstName",
                lastName: "$user.lastName",
                birthDate: "$user.birthDate",

                currentSubjects: 1,

                "exams._id": 1,
                "exams.examType": 1,

                "exams.class": 1,
                "exams.section": 1,
                "exams.rollNo": 1,

                "exams.subjects": 1,
                "exams.result": 1,
                "exams.division": 1,
                "exams.percentage": 1,
                "exams.rank": 1
            }
        })
        .exec(function (err, user) {

            if (err) deferred.reject(err);
            if (user) {

                deferred.resolve(user);
            } else {
                deferred.resolve();
            }
        });
    return deferred.promise;
}

function getExamDetailById() {
}



function getExamBySubject(studentId, examType, subjectCode) {

    var deferred = Q.defer();

    _student
        .aggregate(
        { $unwind: '$exams' },
        { $unwind: '$exams.subjects' },
        {
            $match: {
                $and: [
                    { "exams.examType": { $eq: examType } },
                    { 'exams.subjects.code': subjectCode }
                ]
            }
        },
        { $match: { '_id': ObjectId(studentId) } },
        { $lookup: { from: "users", localField: "userRef", foreignField: "_id", as: "user" } },
        { $unwind: "$user" },
        {
            "$group": {
                "_id": "$_id",
                "exams": { "$push": "$exams" }
            }
        },
        {
            $project: {
                "_id": 1,

                userId: "$user._id",
                firstName: "$user.firstName",
                lastName: "$user.lastName",
                birthDate: "$user.birthDate",

                "exams._id": 1,
                "exams.examType": 1,

                "exams.class": 1,
                "exams.section": 1,
                "exams.rollNo": 1,

                "exams.subjects": 1,
                "exams.result": 1,
                "exams.division": 1,
                "exams.percentage": 1,
                "exams.rank": 1
            }
        })
        .exec(function (err, user) {

            if (err) deferred.reject(err);
            if (user) {

                deferred.resolve(user);
            } else {
                deferred.resolve();
            }
        });
    return deferred.promise;
}

function getStudentMarksDetails(studentId, classId, examType) {

    var deferred = Q.defer();

    _student
        .aggregate(
        {
            $match: {
                $or: [
                    { "exams.examType": { $eq: examType } },
                    { 'exams.class': classId }
                ]
            }
        },
        { $unwind: '$exams' },
        { $match: { '_id': ObjectId(studentId) } },
        { $lookup: { from: "users", localField: "userRef", foreignField: "_id", as: "user" } },
        { $unwind: "$user" },
        //{
        //    $match: { "user._id": { $eq: ObjectId(userId) } }
        //},
        {
            $project: {
                "_id": 1,

                "userRef": {
                    _id: "$user._id",
                    firstName: "$user.firstName",
                    lastName: "$user.lastName",
                    birthDate: "$user.birthDate",
                },

                "examRef": {
                    _id: "$exams._id",
                    examType: "$exams.examType",

                    class:"$exams.class",
                    section:"$exams.section",
                    rollNo:"$exams.rollNo",

                    subjects:"$exams.subjects",
                    result:"$exams.result",
                    division:"$exams.division",
                    percentage:"$exams.percentage",
                    rank:"$exams.rank"
                }
            }
        })
        .exec(function (err, users) {

            if (err) deferred.reject(err);
            if (users && users.length > 0) {
                users[0].examRef = calculateResult(users[0].examRef);
                deferred.resolve(users[0]);
            } else {
                deferred.resolve();
            }
        });
    return deferred.promise;
}

function getStudentMarksByClassAndSection(classId, sectionId, examType) {

    var deferred = Q.defer();

    _student
        .aggregate(
        //{ $match: { _id: ObjectId(studentId) } },
        { $unwind: '$exams' },
        { $match: { "exams._id": { $eq: ObjectId(examId) } } },
        {
            $project: {
                "_id": 1,
                "exams._id": 1,
                "exams.examType": 1,
                "exams.class": 1,
                "exams.section": 1,
                "exams.rollNo": 1,
                "exams.subjects": 1,
                "exams.result": 1,
                "exams.division": 1,
                "exams.percentage": 1,
                "exams.rank": 1
            }
        })
        .exec(function (err, user) {

            if (err) deferred.reject(err);
            if (user) {
                calculateResult(user);
                deferred.resolve(user);
            } else {
                deferred.resolve();
            }
        });
    return deferred.promise;
}

function getStudentsByClassAndSection() {
}

function getStudentRollNo(studentClass, section, studentName) {
}

function getStudents(studentClass, section) {
}

function loadClass() {
}

function loadSection() {
}

function searchStudent(studentClass, section, studentName, examType, subjectName, rollNo) {
}

function searchMarks(studentClass, section, studentName, rollNo) {
}

function calculateGrade(perc) {

    var letterGrade = "F";
    if (perc) {
        switch (true) {
            case (perc >= 95):
                letterGrade = "A+"
                break;
            case (perc >= 93 && perc < 94):
                letterGrade = "A"
                break;
            case (perc >= 90 && perc < 92):
                letterGrade = "A-"
                break;
            case (perc >= 87 && perc < 89):
                letterGrade = "B+"
                break;
            case (perc >= 83 && perc < 86):
                letterGrade = "B"
                break;
            case (perc >= 80 && perc < 82):
                letterGrade = "B-"
                break;
            case (perc >= 77 && perc < 79):
                letterGrade = "C+"
                break;
            case (perc >= 73 && perc < 76):
                letterGrade = "C"
                break;
            case (perc >= 70 && perc < 72):
                letterGrade = "C-"
                break;
            case (perc >= 67 && perc < 69):
                letterGrade = "D+"
                break;
            case (perc >= 63 && perc < 66):
                letterGrade = "D"
                break;
            case (perc >= 60 && perc < 62):
                letterGrade = "D-"
                break;
            case (perc >= 0 && perc < 59):
                letterGrade = "F"
                break;
            default:
                letterGrade = "F"
                break;
        }
    }

    return letterGrade;
}

function calculateResult(examModel) {

    var subjects = examModel.subjects;

    var boolResult = true;
    var grandTotalMarksObtained = 0;
    var grandTotalMarks = 0;
    subjects.forEach(function (subject) {
        if (boolResult && ((subject.marksObtainedTheory + subject.marksObtainedPractical) / (subject.maxMarksTheory + subject.maxMarksPractical)< .35)) {
            boolResult = false;
        }
        grandTotalMarksObtained += parseInt(subject.marksObtainedTheory) + parseInt(subject.marksObtainedPractical);
        subject.totalMarksObtained = parseInt(subject.marksObtainedTheory) + parseInt(subject.marksObtainedPractical);

        grandTotalMarks += parseInt(subject.maxMarksTheory) + parseInt(subject.maxMarksPractical);
        subject.totalMarks = parseInt(subject.maxMarksTheory) + parseInt(subject.maxMarksPractical);

        var subjectPerc = (subject.totalMarksObtained / subject.totalMarks) * 100;
        subject.letterGrade = calculateGrade(subjectPerc);
    });

    examModel.result = boolResult ? "Pass" : "Fail";
    var perc = (grandTotalMarksObtained / grandTotalMarks) * 100;
    examModel.grandTotalMarksObtained = grandTotalMarksObtained;
    examModel.grandTotalMarks = grandTotalMarks;
    examModel.percentage = perc;

    examModel.letterGrade = calculateGrade(perc);

    return examModel;
}

function insertMarks(examModel, studentId, userId) {

    var deferred = Q.defer();

    examModel._id = new ObjectId;
    examModel.createdBy = userId;
    examModel.createdDate = new Date();

    calculateResult(examModel);

    _student
        .findByIdAndUpdate({ _id: studentId }, { $push: { "exams": examModel } }, { safe: true, upsert: true }, function (err, result) {

            if (err) deferred.reject(err);

            if (result) {
                deferred.resolve(result);
            } else {
                deferred.resolve();
            }
        });
    return deferred.promise;
}

function updateMarks(examModel, studentId, examId, userId) {

    var deferred = Q.defer();
    //TODO
    _student.findById(studentId, function (err, data) {
        if (err) throw err;

        var exam = data.exams.id(examId);

        if (exam) {
            try {

                var examModel = examModel.exams[0];

                exam.status = examModel.status ? examModel.status : exam.status;
                exam.remarks = examModel.remarks ? examModel.remarks : exam.remarks;
                exam.examDate = examModel.examDate ? examModel.examDate : exam.examDate;
                exam.academicYear = examModel.academicYear ? examModel.academicYear : exam.academicYear;

                //exam.updatedBy = userId;
                //exam.updatedDate = new Date();

                data.save();
            }
            catch (error) {
                console.log(error);
            }
        }
    });
    return deferred.promise;
}

function remove(studentId, examId) {

    var deferred = Q.defer();

    _student.findOne({ _id: studentId }, function (err, student) {

        if (err) {
            console.log(err);
            deferred.reject(err);
        }
        if (student) {

            student.exams.id(examId).subjects.remove();

            student.save(function (err, result) {
                if (err) {
                    deferred.reject(err);
                }
                if (result) {
                    console.log('exam successfully deleted!');
                    deferred.resolve(result);
                } else {
                    deferred.resolve();
                }
            });
        }
    });
    return deferred.promise;
}

function update(studentDTO, loggedInUserId) {

    var deferred = Q.defer();

    _student.findById({ _id: studentDTO._id }, function (err, student) {
        if (err) {
            console.log(err);
            deferred.reject(err);
        }
        if (student) {

            student.exams.id(studentDTO.exams._id).subjects = [];
            student.save(function (err, result) {
                if (err) {
                    console.log(err);
                    deferred.reject(err);
                }
                if (result) {
                    var studentMarksModel = new _student(studentDTO);
                    var subjects = studentMarksModel.exams[0].subjects;

                    subjects.forEach(function (subject) {

                        if (!subject._id) {
                            subject._id = new ObjectId;
                        }
                        //subject.updatedBy = loggedInUserId;
                        //subject.updatedDate = new Date();

                        result.exams.id(studentDTO.exams._id).subjects.push(subject);
                    });

                    result.save(function (err, result) {
                        if (err) {
                            console.log(err);
                            deferred.reject(err);
                        }
                    });
                }
            });
        }
    });
    return deferred.promise;
}

module.exports = dal;

