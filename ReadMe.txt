This web app is developed using
Node.js version 3.10.10
MongoDB version 3.4.2

IDE 
you can use Visual Studio, Visual Studio Code, Sublime Text

MongoDB IDE
Robomongo

Note: This website is built using w3layouts.com, but you have to provide a back link to w3layouts.com which is already included 
in footer design by w3layouts.com don�t edit or remove it.

Here,
We have two sites, 
	#. Admin Site for teacher and OfficeStaff.
	#. public Site for student and parents.
	
Before starting Installation process, we believe you have successfully installed node.js and mongodb.

Installation

#1.Unzip it.
#2.No need to create database or to run db scripts, mongoose internally takes care of it.
#3.Below are the steps to run project.
#4.Before running project make sure mongodb is Up and Running.
	(4.1)Help to run mongodb can be found here.
	https://stackoverflow.com/questions/20796714/how-do-i-start-mongo-db-from-windows
#5.Open command prompt
	(5.1)Go to School.Admin.MEAN folder
		run command npm install, to install missing packages
	(5.2)To run Admin Site		
		Run server.js using nodemon or node command
		nodemon bin/www.js or
		node bin/www.js
		we have used port 1238 for Admin Site, so http://localhost:1238 will open Admin Site
#6.Open another command prompt
	(6.1)Go to School.Public.MEAN folder
		run command npm install, to install missing packages
	(6.2)To run Public Site
		Run server.js using nodemon or node command
		nodemon bin/www.js or
		node bin/www.js
		we have used port 1237 for Public Site, so http://localhost:1237 will open Public Site
#7for both Admin & Public Site
	Database connectionUri is inside server/config/config.js file.
	Use the same connectionUri in Robomongo IDE
	
Enjoy!!!.