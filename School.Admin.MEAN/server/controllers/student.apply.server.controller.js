﻿var config = require('../config/config.js');

require('../school.common/appConstants');

exports.renderApply = function (req, res) {
    res.render('student/apply', {
        pTitle: 'Student Apply | School'
    });
};