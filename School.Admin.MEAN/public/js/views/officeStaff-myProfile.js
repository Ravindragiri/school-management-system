﻿//$(document).ready(function () {
//    $('#myTab').on('click', '.nav-tabs a', function () {
//        // set a special class on the '.dropdown' element
//        $(this).closest('.dropdown').addClass('dontClose');
//    })

//    $('#menu1').on('hide.bs.dropdown', function (e) {
//        if ($(this).hasClass('dontClose')) {
//            e.preventDefault();
//        }
//        $(this).removeClass('dontClose');
//    });
//});

// create angular controller

var app = angular.module('schoolApp', ['ngTouch']);

app.controller('ngOfficeStaffMyProfileController', ['OfficeStaffService', '$scope', '$http',
    function (OfficeStaffService, $scope, $http) {

    $scope.getContent = function (obj) {
        return obj.value + " " + obj.text;
    };

    var baseUrl = 'http://localhost:1238'

    $scope.officeStaffDTO = {};
    $scope.readOne = function () {

        var userId = $("#userId").val();
        //var userId = "5a9452690017732714660d49";

        OfficeStaffService.readOne(userId).then(function (response) {
            $scope.officeStaffDTO = response.data;
            $scope.addMode = false;
        });
    };
    $scope.readOne();

    $scope.updateProfile = function () {

        var data = {
            officeStaffDTO: $scope.officeStaffDTO
        }
        return $http({
            method: 'POST',
            //url: baseUrl + objectName + '/' + id,
            url: baseUrl + '/api/officeStaff/updateProfile',
            data: data
        }).then(function (response) {

        });
    };

    $scope.updatePassword = function () {

        var data = {
            updatePasswordDTO: {
                officeStaffId: $scope.officeStaffDTO._id,
                userId: $scope.officeStaffDTO.userRef._id,
                password: $scope.officeStaffDTO.password,
                newPassword: $scope.officeStaffDTO.newPassword,
            }
        }
        $http({
            method: 'POST',
            //url: baseUrl + objectName + '/' + id,
            url: baseUrl + '/api/user/updatePassword',
            data: data
        }).then(function (response) {

        });
    };

    $scope.updateEmail = function () {

        var data = {
            updateEmailDTO: {
                officeStaffId: $scope.officeStaffDTO._id,
                userId: $scope.officeStaffDTO.userRef._id,
                password: $scope.officeStaffDTO.password,
                email: $scope.officeStaffDTO.email,
            }
        }
        $http({
            method: 'POST',
            //url: baseUrl + objectName + '/' + id,
            url: baseUrl + '/api/user/updateEmail',
            data: data
        }).then(function (response) {

        });
    };

}]);

(function () {

    angular.module('schoolApp')
        .service('OfficeStaffService', ['$http', OfficeStaffService]);

    function OfficeStaffService($http) {

        var self = this;
        var baseUrl = 'http://localhost:1238';

        self.readOne = function (id) {
            return $http({
                method: 'GET',
                url: baseUrl + '/api/officeStaff/getOfficeStaffByUserId?userId=' + id
            }).then(function (response) {
                return response.data;
            });
        };
    }
} ());

$(function () {

    $("#btnChangePersonalInfo").on('click', function () {
        $('[href=#changePersonalInfo]').tab('show');
    });

    $("#btnChangeEmail").on('click', function () {
        $('[href=#changeEmail]').tab('show');
    });

    $("#btnChangePassword").on('click', function () {
        $('[href=#changePassword]').tab('show');
    });
});
