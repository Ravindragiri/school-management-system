﻿var Q = require('q');
var mongoose = require('mongoose'),
    async = require('async');

require('../models/exam.timeTable.server.model');
require('../models/user.server.model');

var userDal = require('../../server/school.dal/user.dal.js');

var _examTimeTable = mongoose.model('examTimeTable');
var _user = mongoose.model('user');

require('../../server/school.common/helpers/deferredHelper');

var dal = {};

dal.add = add;
dal.getExamTimeTableById = getExamTimeTableById;
dal.getExamTimeTableContactDetailById = getExamTimeTableContactDetailById;
dal.register = register;
dal.mobileRegister = mobileRegister;
dal.getExamTimeTablesByAnyCriteria = getExamTimeTablesByAnyCriteria;
dal.getAllExamTimeTableInfo = getAllExamTimeTableInfo;
dal.getExamTimeTableProfileInfo = getExamTimeTableProfileInfo;
dal.getExamTimeTableByUserId = getExamTimeTableByUserId;
dal.count = count;
dal.update = update;
//dal.insertTimeTable = insertTimeTable;
//dal.displayTimeTable = displayTimeTable;
//dal.searchTimeTable = searchTimeTable;
dal.getExamTimeTableByExamTypeAndClass = getExamTimeTableByExamTypeAndClass;
dal.removeById = removeById;

module.exports = dal;

function add(examTimeTableModel) {

    var deferred = Q.defer();

    examTimeTableModel
        .save(function (err, result) {
            createHelper(deferred, err, result);
        });

    return deferred.promise;
}

function getExamTimeTableById(_id) {

    var deferred = Q.defer();

    _examTimeTable
        .findOne({ '_id': _id })
        .populate({ path: 'userRef' })
        //.populate({ path: 'exams' })
        //.populate({ path: 'followers' })
        //.populate({ path: 'favorites' })
        //.populate({ path: 'messages' })
        //.populate({ path: 'profileVisitors' })
        //.populate({ path: 'sharedProfiles' })
        .exec(function (err, result) {
            execHelper(deferred, err, result);
        });
    return deferred.promise;
}

function getExamTimeTableByUserId(userId) {

    var deferred = Q.defer();

    _examTimeTable
        .findOne({})
        .populate({
            path: 'userRef',
            match: { _id: userId },
        })
        .exec(function (err, result) {
            execHelper(deferred, err, result);
        });
    return deferred.promise;
}

function getExamTimeTableContactDetailById(examTimeTableId) {

    var deferred = Q.defer();

    _examTimeTable
        .find({ _id: examTimeTableId })
        .populate({ path: 'userRef' })
        .exec(function (err, result) {
            execHelper(deferred, err, result);
        });
    return deferred.promise;
}

function register(examTimeTableModel, userModel) {

    //var userDal = require('./user.dal.js');
    var deferred = Q.defer();

    async.waterfall(
        [
            function (callback) {

                userModel.createdDate = new Date();

                userDal.add(userModel)
                    .then(function (user) {

                        if (user) {

                            //examTimeTableModel.user._id = user._id;
                            examTimeTableModel.userRef = user._id;
                            examTimeTableModel.createdBy = user._id;
                            examTimeTableModel.createdDate = new Date();
                            callback(null, examTimeTableModel);
                        } else {
                            deferred.resolve();
                        }
                    })
                    .catch(function (err) {
                        if (err) deferred.reject(err);
                    });
            },
            function (examTimeTableModel, callback) {
                add(examTimeTableModel)
                    .then(function (examTimeTable) {

                        if (examTimeTable) {
                            deferred.resolve(examTimeTable);
                        } else {
                            deferred.resolve();
                        }
                    })
                    .catch(function (err) {
                        if (err) deferred.reject(err);
                    });
            }
        ], function (errs, results) {
            if (errs) {
                async.each(results, rollback, function () {
                    console.log('Rollback done.');
                    res.status(400).send(errs);
                });
            } else {
                console.log('Done.');
                deferred.resolve(results);
            }
        });

    function rollback(doc, callback) {
        if (!doc) { callback(); }
        else {
            userModel.findByIdAndRemove(doc._id, function (err, doc) {
                console.log('Rolled-back document: ' + doc);
                callback();
            });
        }
    }
    return deferred.promise;
}

function mobileRegister(examTimeTableModel, userModel) {

    //var userDal = require('./user.dal.js');
    var deferred = Q.defer();

    async.waterfall(
        [
            function (callback) {

                userModel.createdDate = new Date();

                userDal.add(userModel)
                    .then(function (user) {

                        if (user) {

                            examTimeTableModel.userRef = user._id;
                            examTimeTableModel.createdBy = user._id;
                            examTimeTableModel.createdDate = new Date();
                            callback(null, examTimeTableModel);
                        } else {
                            deferred.resolve();
                        }
                    })
                    .catch(function (err) {
                        if (err) deferred.reject(err);
                    });
            },
            function (examTimeTableModel, callback) {
                add(examTimeTableModel)
                    .then(function (examTimeTable) {

                        if (examTimeTable) {
                            deferred.resolve(examTimeTable);
                        } else {
                            deferred.resolve();
                        }
                    })
                    .catch(function (err) {
                        if (err) deferred.reject(err);
                    });
            }
        ], function (errs, results) {
            if (errs) {
                async.each(results, rollback, function () {
                    console.log('Rollback done.');
                    res.status(400).send(errs);
                });
            } else {
                console.log('Done.');
                deferred.resolve(results);
            }
        });

    function rollback(doc, callback) {
        if (!doc) { callback(); }
        else {
            userModel.findByIdAndRemove(doc._id, function (err, doc) {
                console.log('Rolled-back document: ' + doc);
                callback();
            });
        }
    }
    return deferred.promise;
}

function getExamTimeTablesByAnyCriteria(searchCriteria, searchText) {

    var deferred = Q.defer();

    _examTimeTable
        //.find({
        //    loc: {
        //        $near: searchCriteria.coords,
        //        $maxDistance: searchCriteria.maxDistance
        //    }
        //})
        .find({
            '$or': [
                { designation: new RegExp(searchText, 'i') },
                { qualification: new RegExp(searchText, 'i') }
            ]
        })
        .populate({ path: 'userRef' })
        //.populate({ path: 'exams' })
        //.populate({ path: 'followers' })
        //.populate({ path: 'favorites' })
        //.populate({ path: 'messages' })
        //.populate({ path: 'profileVisitors' })
        //.populate({ path: 'sharedProfiles' })
        .exec(function (err, result) {
            execHelper(deferred, err, result);
        });
    return deferred.promise;
}

function getAllExamTimeTableInfo() {

    var deferred = Q.defer();

    _examTimeTable
        .find({})
        //.populate({ path: 'userRef' })
        //.populate({ path: 'subjects' })
        //.populate({ path: 'followers' })
        //.populate({ path: 'favorites' })
        //.populate({ path: 'messages' })
        //.populate({ path: 'profileVisitors' })
        //.populate({ path: 'sharedProfiles' })
        .lean()
        .exec(function (err, result) {
            execHelper(deferred, err, result);
        });
    return deferred.promise;
}

function getExamTimeTableProfileInfo(userId) {

    var deferred = Q.defer();

    _examTimeTable
        .findOne({})
        .populate({
            path: 'user',
            match: { _id: userId },
        })
        .exec(function (err, result) {
            execHelper(deferred, err, result);
        });
    return deferred.promise;
}

function count() {

    var deferred = Q.defer();

    _examTimeTable
        .count({})
        .exec(function (err, result) {
            execHelper(deferred, err, result);
        });
    return deferred.promise;
}

function update(examTimeTableDTO, loggedInUserId) {

    var deferred = Q.defer();

    _examTimeTable.findById({ _id: examTimeTableDTO._id }, function (err, exam) {
        if (err) {
            console.log(err);
            deferred.reject(err);
        }
        if (exam) {

            exam.timeTableList = [];
            //exam.updatedBy = loggedInUserId;
            exam.updatedDate = new Date();

            exam.save(function (err, result) {
                if (err) {
                    console.log(err);
                    deferred.reject(err);
                }

                var examTimeTableModel = new _examTimeTable(examTimeTableDTO);
                var timeTableList = examTimeTableModel.timeTableList;

                timeTableList.forEach(function (timeTable) {

                    if (!timeTable._id) {
                        timeTable._id = new ObjectId;
                    }
                    //timeTable.updatedBy = loggedInUserId;
                    //timeTable.updatedDate = new Date();

                    _examTimeTable
                        .findByIdAndUpdate({ _id: examTimeTableModel._id }, { $push: { "timeTableList": timeTable } }, { safe: true, upsert: true }, function (err, result) {
                            updateHelper(deferred, err, result);
                        });
                });
            });
        }
    });
    return deferred.promise;
}

function insertTimeTable() {
}

function displayTimeTable() {
}

function searchTimeTable(examType, studentClass, section) {
}

function getExamTimeTableByExamTypeAndClass(examType, className) {

    var deferred = Q.defer();

    _examTimeTable
        .findOne({
            $and: [
                { 'class': className },
                { 'examType': examType }
            ]
        })
        .lean()
        .exec(function (err, result) {
            execHelper(deferred, err, result);
        });
    return deferred.promise;
}

function removeById(timeTableId) {

    var deferred = Q.defer();

    _examTimeTable.findById({ _id: timeTableId }, function (err, data) {

        if (err) {
            console.log(err);
            deferred.reject(err);
        }
        if (data) {

            data.timeTableList = [];
            //data.updatedBy = loggedInUserId;
            data.updatedDate = new Date();

            data.save(function (err, timeTable) {
                if (err) {
                    console.log(err);
                    deferred.reject(err);
                }
                if (timeTable) {
                    console.log('timeTable successfully deleted!');
                    deferred.resolve(timeTable);
                } else {
                    deferred.resolve();
                }
            });
        }
    });
    return deferred.promise;
}