﻿var Q = require('q');
var mongoose = require('mongoose'),
    async = require('async');

require('../models/officeStaff.server.model');
require('../models/user.server.model');

var userDal = require('../../server/school.dal/user.dal.js');

var _officeStaffs = mongoose.model('officeStaff');
var _user = mongoose.model('user');

var dal = {};

dal.add = add;
dal.getOfficeStaffById = getOfficeStaffById;
dal.getOfficeStaffContactDetailById = getOfficeStaffContactDetailById;
dal.register = register;
dal.mobileRegister = mobileRegister;
dal.getOfficeStaffsByAnyCriteria = getOfficeStaffsByAnyCriteria;
dal.getOfficeStaffProfileInfo = getOfficeStaffProfileInfo;
dal.getOfficeStaffByUserId = getOfficeStaffByUserId;
dal.count = count;
dal.mobileUpdateProfile = mobileUpdateProfile;

module.exports = dal;

function add(officeStaffModel) {

    var deferred = Q.defer();

    officeStaffModel
        .save(function (err, result) {
            if (err) deferred.reject(err);

            if (result) {
                deferred.resolve(result);
            } else {
                deferred.resolve();
            }
        });

    return deferred.promise;
}

function getOfficeStaffById(_id) {

    var deferred = Q.defer();

    _officeStaffs
        .findOne({ '_id': _id })
        .populate({ path: 'userRef' })
        //.populate({ path: 'exams' })
        //.populate({ path: 'followers' })
        //.populate({ path: 'favorites' })
        //.populate({ path: 'messages' })
        //.populate({ path: 'profileVisitors' })
        //.populate({ path: 'sharedProfiles' })
        .exec(function (err, officeStaff) {

            if (err) {
                deferred.reject(err);
            }
            if (officeStaff) {
                deferred.resolve(officeStaff);
            } else {
                deferred.resolve();
            }
        });
    return deferred.promise;
}

function getOfficeStaffByUserId(userID) {

    var deferred = Q.defer();

    _officeStaffs
        .findOne({})
        .populate({
            path: 'userRef',
            match: { _id: userID },
        })
        .exec(function (err, officeStaff) {

            if (err) {
                deferred.reject(err);
            }
            if (officeStaff) {
                deferred.resolve(officeStaff);
            } else {
                deferred.resolve();
            }
        });
    return deferred.promise;
}

function getOfficeStaffContactDetailById(officeStaffID) {

    var deferred = Q.defer();

    _officeStaffs
        .find({ _id: officeStaffID })
        .populate({ path: 'userRef' })
        .exec(function (err, officeStaff) {

            if (err) {
                deferred.reject(err);
            }
            if (officeStaff) {
                deferred.resolve(officeStaff);
            } else {
                deferred.resolve();
            }
        });
    return deferred.promise;
}

function register(officeStaffModel, userModel) {

    //var userDal = require('./user.dal.js');
    var deferred = Q.defer();

    async.waterfall(
        [
            function (callback) {

                userModel.createdDate = new Date();

                userDal.add(userModel)
                    .then(function (user) {

                        if (user) {

                            //officeStaffModel.user._id = user._id;
                            officeStaffModel.userRef = user._id;
                            officeStaffModel.createdBy = user._id;
                            officeStaffModel.createdDate = new Date();
                            callback(null, officeStaffModel);
                        } else {
                            deferred.resolve();
                        }
                    })
                    .catch(function (err) {
                        if (err) deferred.reject(err);
                    });
            },
            function (officeStaffModel, callback) {
                add(officeStaffModel)
                    .then(function (officeStaff) {

                        if (officeStaff) {
                            deferred.resolve(officeStaff);
                        } else {
                            deferred.resolve();
                        }
                    })
                    .catch(function (err) {
                        if (err) deferred.reject(err);
                    });
            }
        ], function (errs, results) {
            if (errs) {
                async.each(results, rollback, function () {
                    console.log('Rollback done.');
                    res.status(400).send(errs);
                });
            } else {
                console.log('Done.');
                deferred.resolve(results);
            }
        });

    function rollback(doc, callback) {
        if (!doc) { callback(); }
        else {
            userModel.findByIdAndRemove(doc._id, function (err, doc) {
                console.log('Rolled-back document: ' + doc);
                callback();
            });
        }
    }
    return deferred.promise;
}

function mobileRegister(officeStaffModel, userModel) {

    //var userDal = require('./user.dal.js');
    var deferred = Q.defer();

    async.waterfall(
        [
            function (callback) {

                userModel.createdDate = new Date();

                userDal.add(userModel)
                    .then(function (user) {

                        if (user) {

                            officeStaffModel.userRef = user._id;
                            officeStaffModel.createdBy = user._id;
                            officeStaffModel.createdDate = new Date();
                            callback(null, officeStaffModel);
                        } else {
                            deferred.resolve();
                        }
                    })
                    .catch(function (err) {
                        if (err) deferred.reject(err);
                    });
            },
            function (officeStaffModel, callback) {
                add(officeStaffModel)
                    .then(function (officeStaff) {

                        if (officeStaff) {
                            deferred.resolve(officeStaff);
                        } else {
                            deferred.resolve();
                        }
                    })
                    .catch(function (err) {
                        if (err) deferred.reject(err);
                    });
            }
        ], function (errs, results) {
            if (errs) {
                async.each(results, rollback, function () {
                    console.log('Rollback done.');
                    res.status(400).send(errs);
                });
            } else {
                console.log('Done.');
                deferred.resolve(results);
            }
        });

    function rollback(doc, callback) {
        if (!doc) { callback(); }
        else {
            userModel.findByIdAndRemove(doc._id, function (err, doc) {
                console.log('Rolled-back document: ' + doc);
                callback();
            });
        }
    }
    return deferred.promise;
}

function getOfficeStaffsByAnyCriteria(searchCriteria, searchText) {

    var deferred = Q.defer();

    _officeStaffs
        //.find({
        //    loc: {
        //        $near: searchCriteria.coords,
        //        $maxDistance: searchCriteria.maxDistance
        //    }
        //})
        .find({
            '$or': [
                { designation: new RegExp(searchText, 'i') },
                { qualification: new RegExp(searchText, 'i') }
            ]
        })
        .populate({ path: 'userRef' })
        //.populate({ path: 'exams' })
        //.populate({ path: 'followers' })
        //.populate({ path: 'favorites' })
        //.populate({ path: 'messages' })
        //.populate({ path: 'profileVisitors' })
        //.populate({ path: 'sharedProfiles' })
        .exec(function (err, officeStaff) {

            if (err) {
                deferred.reject(err);
            }
            if (officeStaff) {
                deferred.resolve(officeStaff);
            } else {
                deferred.resolve();
            }
        });
    return deferred.promise;
}

function getOfficeStaffProfileInfo(userID) {

    var deferred = Q.defer();

    _officeStaffs
        .findOne({})
        .populate({
            path: 'user',
            match: { _id: userID },
        })
        .exec(function (err, officeStaff) {

            if (err) {
                deferred.reject(err);
            }
            if (officeStaff) {
                deferred.resolve(officeStaff);
            } else {
                deferred.resolve();
            }
        });
    return deferred.promise;
}

function count() {

    var deferred = Q.defer();

    _officeStaffs
        .count({})
        .exec(function (err, count) {

            if (err) {
                deferred.reject(err);
            }
            if (count) {
                deferred.resolve(count);
            } else {
                deferred.resolve();
            }
        });
    return deferred.promise;
}

function mobileUpdateProfile(updateProfileModel, officeStaffModel, avatarUrl) {

    var deferred = Q.defer();

    var userModel = updateProfileModel.User;

    async.waterfall(
        [
            function (callback) {

                userModel.findOne({ _id: updateProfileModel.userID }, function (err, user) {
                    if (!err) {
                        if (!user) {

                            user.email = updateProfileModel.email ? updateProfileModel.email : user.email;
                            user.phoneNumber = updateProfileModel.phoneNumber ? updateProfileModel.phoneNumber : user.phoneNumber;

                            user.firstName = updateProfileModel.firstName ? updateProfileModel.firstName : user.firstName;
                            user.lastName = updateProfileModel.lastName ? updateProfileModel.lastName : user.lastName;

                            user.address = updateProfileModel.address ? updateProfileModel.address : user.address;
                            user.city = updateProfileModel.city ? updateProfileModel.city : user.city;
                            user.province = updateProfileModel.province ? updateProfileModel.province : user.province;
                            user.pinCode = updateProfileModel.pinCode ? updateProfileModel.pinCode : user.pinCode;
                            user.country = updateProfileModel.country ? updateProfileModel.country : user.country;
                            //user.countryISO2 = updateProfileModel.countryISO2 ? updateProfileModel.countryISO2 : user.countryISO2;
                            user.countryDialCode = updateProfileModel.countryDialCode ? updateProfileModel.countryDialCode : user.countryDialCode;

                            user.updatedBy = updateProfileModel._id;
                            user.updatedDate = new Date();
                        }
                        user.status = request.status;
                        user.save(function (err) {
                            if (result.user._id) {

                                callback(null, updateProfileModel);
                            }
                        });
                    }
                });
            },
            function (officeStaffID, callback) {
                officeStaffModel.findOne({ _id: officeStaffModel.officeStaffID }, function (err, officeStaff) {
                    if (!err) {
                        if (!officeStaff) {

                            officeStaff.designation = officeStaffModel.designation ? officeStaffModel.designation : officeStaff.designation;
                            officeStaff.qualification = officeStaffModel.qualification ? officeStaffModel.qualification : officeStaff.qualification;
                            officeStaff.experience = officeStaffModel.experience ? officeStaffModel.experience : officeStaff.experience;

                            officeStaff.updatedBy = officeStaffModel.UserID;
                            officeStaff.updatedDate = DateTime.Now;
                        }
                        officeStaff.save(function (err) {
                            if (result.officeStaff._id) {

                                callback(null, officeStaffModel.officeStaffID);
                            }
                        });
                    }
                });
            }
        ], function (errs, results) {
            if (errs) {
                async.each(results, rollback, function () {
                    console.log('Rollback done.');
                    res.status(400).send(errs);
                });
            } else {
                console.log('Done.');
                deferred.resolve(results);
            }
        });

    function rollback(doc, callback) {
        if (!doc) { callback(); }
        else {
            userModel.findByIdAndRemove(doc._id, function (err, doc) {
                console.log('Rolled-back document: ' + doc);
                callback();
            });
        }
    }
    return deferred.promise;
}