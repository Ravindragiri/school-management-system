﻿var Q = require('q');

const MongoClient = require('mongodb').MongoClient;
var db;

MongoClient.connect('mongodb://127.0.0.1', (err, client) => {
    if (err) return console.log(err)
    db = client.db('School') // whatever your database name is
})

var dal = {};
dal.sendMessage = sendMessage;
module.exports = dal;

function sendMessage(contactUsDTO) {

    var deferred = Q.defer();

    db.collection('contactus')
        .save(contactUsDTO, function (err, result) {
            if (err) {
                console.log(err);
                deferred.reject(err);
            }
            if (result) {
                deferred.resolve(result);
            } else {
                deferred.resolve();
            }
        });

    return deferred.promise;
}