﻿var Q = require('q');
var mongoose = require('mongoose');

require('../models/dormitory.server.model');
require('../models/user.server.model');

var userDal = require('../../server/school.dal/user.dal.js');

var _dormitoryModel = mongoose.model('dormitory');
var _user = mongoose.model('user');

require('../../server/school.common/helpers/deferredHelper');

var dal = {};

dal.addDormitory = addDormitory;
dal.updateDormitory = updateDormitory;
dal.deleteDormitoryById = deleteDormitoryById;
dal.getDormitoryById = getDormitoryById;
dal.getAllDormitory = getAllDormitory;

module.exports = dal;

function addDormitory(dormitoryModel) {

    var deferred = Q.defer();

    dormitoryModel
        .save(function (err, result) {
            createHelper(deferred, err, result);
        });

    return deferred.promise;
}

function updateDormitory(dormitoryDTO, dormitoryId) {

    var dormitory = new _dormitory(dormitoryDTO);
    dormitory.createdBy = userId;
    dormitory.createdDate = new Date();

    var deferred = Q.defer();

    _dormitory
        .findByIdAndUpdate({ _id: userId }, { $push: { "dormitorys": dormitory } }, { safe: true, upsert: true }, function (err, result) {
            updateHelper(deferred, err, result);
        });
    return deferred.promise;
}

function deleteDormitoryById(dormitoryId) {

    var deferred = Q.defer();

    _dormitoryModel.findByIdAndRemove({ _id: dormitoryId }, function (err) {
        if (err) {
            deferred.reject(err);
        }
        else {
            console.log('dormitory successfully deleted!');
            deferred.resolve(true);
        }
    });
    return deferred.promise;
}

function getDormitoryById(dormitoryId) {

    var deferred = Q.defer();

    _dormitoryModel
        .findOne({ '_id': dormitoryId })
        .exec(function (err, result) {
            execHelper(deferred, err, result);
        });
    return deferred.promise;
}

function getAllDormitory() {

    var deferred = Q.defer();

    _dormitoryModel
        .find({})
        .exec(function (err, result) {
            execHelper(deferred, err, result);
        });
    return deferred.promise;
}
