﻿module.exports = function (app) {

    var promotionApiController = require('../controllers/api/promotion.server.api.controller');
    app.get('/api/promotion/promote', promotionApiController.promote);
};