﻿var userDal = require('../../school.dal/user.dal.js');
require('../../school.common/helpers/responseHelper');
var Random = require('../../utils/random');

var mongoose = require('mongoose');
var user = mongoose.model('user');
require('../../school.common/appConstants');

exports.add = function (req, res) {
    var userModel = new user(req.body.userDTO);

    userDal.add(userModel)
        .then(function (result) {
            if (result) {
                sendSuccessResponse(res, result);
            } else {
                sendErrorResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
};

exports.createNewUser = function (req, res) {
    var userModel = req.body.userModel;

    userDal.createNewUser(userModel)
        .then(function (result) {
            if (result) {
                sendSuccessResponse(res, result);
            } else {
                sendFailResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
};

exports.removeById = function (req, res) {
    var userID = req.body.userID;

    userDal.removeById(userID)
        .then(function (result) {
            if (result) {
                sendSuccessResponse(res, result, messages.DeleteSuccess);
            } else {
                sendFailResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
};

exports.deleteUser = function (req, res) {
    var userModel = req.body.userModel;
    var userID = req.body.userID;

    userDal.deleteUser(userModel, userID)
        .then(function (result) {
            if (result) {
                sendSuccessResponse(res, result, messages.DeleteSuccess);
            } else {
                sendFailResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
};

exports.getUserByEmail = function (req, res) {
    var email = req.query.email;

    userDal.getUserByEmail(email)
        .then(function (result) {
            if (result) {
                sendSuccessResponse(res, result);
            } else {
                sendFailResponse(res, messages.NotFound);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
}

exports.getUserByEmailPassword = function (req, res) {
    var email = req.param.email;
    var password = req.param.password;

    userDal.getUserByEmailPassword(email, password)
        .then(function (result) {
            if (result) {
                sendSuccessResponse(res, result);
            } else {
                sendFailResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
}

exports.getUserById = function (req, res) {
    var userID = req.query.userID;

    userDal.getUserById(userID)
        .then(function (result) {
            if (result) {
                //var avatarUrl = getSocialAccountAvatarUrl(result.socialAccountType, result.socialAccountID);
                //if (avatarUrl) {
                //    result.avatarUrl = avatarUrl;
                //}

                sendSuccessResponse(res, result);
            } else {
                sendFailResponse(res, messages.NotFound);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
}

exports.getUsernameById = function (req, res) {
    var userID = req.query.userID;

    userDal.getUsernameById(userID)
        .then(function (result) {
            if (result) {
                sendSuccessResponse(res, result);
            } else {
                sendFailResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
}

exports.GetUserIdByUsername = function (req, res) {
    var username = req.query.username;

    userDal.GetUserIdByUsername(username)
        .then(function (result) {
            if (result) {
                sendSuccessResponse(res, result);
            } else {
                sendFailResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
}

exports.authenticateUser = function (req, res) {
    var username = req.body.username;
    var password = req.body.password;

    userDal.authenticateUser(username, password, reason)
        .then(function (result) {

            if (err) throw err;

            // login was successful if we have a user
            if (result) {
                // handle login success
                console.log('login success');
                return;
            }

            // otherwise we can determine why we failed
            var reasons = User.failedLogin;
            switch (reason) {
                case reasons.NOT_FOUND:
                case reasons.PASSWORD_INCORRECT:
                    // note: these cases are usually treated the same - don't tell
                    // the user *why* the login failed, only that it did
                    break;
                case reasons.MAX_ATTEMPTS:
                    // send email or otherwise notify user that account is
                    // temporarily locked
                    break;
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
}

exports.authenticateMobileUser = function (req, res) {

    if (!req.body.loginDTO)
        return sendBadRequestResponse(res);

    var loginDTO = req.body.loginDTO;

    if (loginDTO.loginType == loginTypeEnum.LocalAccount) {

        if (loginDTO.username && loginDTO.password) {
            userDal.authenticateMobileUser(loginDTO.username, loginDTO.password)
                .then(function (result) {
                    res.send(result);
                    //if (user) {
                    //    sendSuccessResponse(res, user, messages.LoggedInSuccess);
                    //} else {
                    //    sendFailResponse(res, messages.InvalidLoginCredentials);
                    //}
                })
                .catch(function (err) {
                    console.log(err);
                    sendErrorResponse(res);
                });
        }
        else {
            sendBadRequestResponse(res);
        }
    }
    //else if (loginDTO.loginType == loginTypeEnum.SocialAccount) {

    //    userDal.getSocialAccountUserByUsername(loginDTO.socialAccountID, loginDTO.socialAccountType)
    //        .then(function (user) {
    //            if (user) {

    //                var userUpdateSet = {
    //                    firstName: loginDTO.firstName ? loginDTO.firstName : user.firstName,
    //                    lastName: loginDTO.lastName ? loginDTO.lastName : user.lastName,
    //                    email: loginDTO.email ? loginDTO.email : user.email,

    //                    avatarUrl: loginDTO.avatarUrl ? loginDTO.avatarUrl : user.avatarUrl,

    //                    phoneNumber: loginDTO.phoneNumber ? loginDTO.phoneNumber : user.phoneNumber,

    //                    simCountry: loginDTO.simCountry ? loginDTO.simCountry : user.simCountry,
    //                    simCountryISO3: loginDTO.simCountryISO3 ? loginDTO.simCountryISO3 : user.simCountryISO3,
    //                    simCountryISO2: loginDTO.simCountryISO2 ? loginDTO.simCountryISO2 : user.simCountryISO2,

    //                    geocoderCountry: loginDTO.geocoderCountry ? loginDTO.geocoderCountry : user.geocoderCountry,
    //                    geocoderCountryISO2: loginDTO.geocoderCountryISO2 ? loginDTO.geocoderCountryISO2 : user.geocoderCountryISO2,
    //                    geocoderCountryISO3: loginDTO.geocoderCountryISO3 ? loginDTO.geocoderCountryISO3 : user.geocoderCountryISO3,
    //                    geocoderAddress: loginDTO.geocoderAddress ? loginDTO.geocoderAddress : user.geocoderAddress,

    //                    gpsCountry: loginDTO.gpsCountry ? loginDTO.gpsCountry : user.gpsCountry,
    //                    gpsCountryISO3: loginDTO.gpsCountryISO3 ? loginDTO.gpsCountryISO3 : user.gpsCountryISO3,
    //                    gpsCountryISO2: loginDTO.gpsCountryISO2 ? loginDTO.gpsCountryISO2 : user.gpsCountryISO2,

    //                    deviceIdentityNumber: loginDTO.deviceIdentityNumber ? loginDTO.deviceIdentityNumber : user.deviceIdentityNumber,
    //                    languageG: loginDTO.languageG ? loginDTO.languageG : user.languageG,
    //                    languageC: loginDTO.languageC ? loginDTO.languageC : user.languageC,
    //                    simMnc: loginDTO.simMnc ? loginDTO.simMnc : user.simMnc,
    //                    simMcc: loginDTO.simMcc ? loginDTO.simMcc : user.simMcc,
    //                    latitude: loginDTO.latitude ? loginDTO.latitude : user.latitude,
    //                    longitude: loginDTO.longitude ? loginDTO.longitude : user.longitude,

    //                    address: loginDTO.address ? loginDTO.address : user.address,
    //                    city: loginDTO.city ? loginDTO.city : user.city,
    //                    province: loginDTO.province ? loginDTO.province : user.province,
    //                    pinCode: loginDTO.pinCode ? loginDTO.pinCode : user.pinCode,
    //                    country: loginDTO.country ? loginDTO.country : user.country,

    //                    updatedBy: user._id.toString(),
    //                    updatedDate: new Date()
    //                };

    //                var userUpdateCondition = { _id: user._id.toString() };

    //                userDal.update(userUpdateCondition, userUpdateSet)
    //                    .then(function (user) {
    //                        sendSuccessResponse(res, user, messages.LoggedInSuccess);
    //                    })
    //                    .catch(function (err) {
    //                        console.log(err);
    //                        sendErrorResponse(res);
    //                    });
    //            }
    //            else {
    //                var userDTO = req.body.loginDTO;
    //                if (userDTO) {

    //                    //userDTO.firstName = loginDTO.firstName;
    //                    //userDTO.lastName = loginDTO.lastName;
    //                    //userDTO.email = loginDTO.email;

    //                    //userDTO.socialAccountType = loginDTO.socialAccountType;
    //                    //userDTO.socialAccountID = loginDTO.socialAccountID;
    //                    //userDTO.socialAccountAvatarUrl = loginDTO.socialAccountAvatarUrl;

    //                    //userDTO.phoneNumber = loginDTO.phoneNumber;
    //                    //userDTO.password = loginDTO.password;
    //                    //userDTO.simCountryISO2 = loginDTO.country;
    //                    //userDTO.deviceIdentityNumber = loginDTO.deviceIdentityNumber;
    //                    //userDTO.languageG = loginDTO.languageG;
    //                    //userDTO.languageC = loginDTO.languageC;
    //                    //userDTO.simMnc = loginDTO.simMnc;
    //                    //userDTO.simMcc = loginDTO.simMcc;
    //                    //userDTO.latitude = loginDTO.latitude;
    //                    //userDTO.longitude = loginDTO.longitude;
    //                    //userDTO.userType = loginDTO.userType;

    //                    var userModel = new user(userDTO);
    //                    userModel.loc.type = "Point";
    //                    userModel.loc.coordinates = [parseFloat(loginDTO.longitude), parseFloat(loginDTO.latitude)];
    //                    userModel.isProfileIncomplete = false;

    //                    userDal.mobileRegisterSocialProfile(userModel)
    //                        .then(function (userDTO) {

    //                            //TODO
    //                            //populate SocialAccountUser
    //                            if (userDTO) {
    //                                sendSuccessResponse(res, loginDTO, messages.LoggedInSuccess);
    //                            }
    //                        })
    //                        .catch(function (err) {
    //                            console.log(err);
    //                            sendErrorResponse(res);
    //                        });
    //                }
    //            }
    //        })
    //        .catch(function (err) {
    //            sendErrorResponse(res);
    //        });
    //}
    else {
        return sendBadRequestResponse(res);
    }
};

exports.authenticateWebUser = function (req, res) {
    var loginDTO = req.body.loginDTO;

    userDal.authenticateWebUser(loginDTO)
        .then(function (result) {

            if (result) {
                sendSuccessResponse(res, result, messages.LoggedInSuccess);
            } else {
                sendFailResponse(res, messages.InvalidLoginCredentials);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
};

exports.requestMobileVerificationCode = function (req, res) {
    var mobileVerification = req.body.mobileVerification;

    userDal.requestMobileVerificationCode(mobileVerification)
        .then(function (result) {
            if (result) {
                sendSuccessResponse(res, result);
            } else {
                sendFailResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
}

exports.changePassword = function (req, res) {

    var userID = req.body.userID;
    var oldPassword = req.body.oldPassword;
    var newPassword = req.body.newPassword;

    if (userID && oldPassword && newPassword) {

        var userUpdateCondition = { $and: [] };
        userUpdateCondition.$and.push({
            _id: userID,
            password: oldPassword
        });

        var userUpdateSet = {
            password: newPassword,
            updatedBy: userID,
            updatedDate: new Date()
        };

        userDal.update(userUpdateCondition, userUpdateSet)
            .then(function (result) {
                if (result) {
                    sendSuccessResponse(res, result, messages.UpdateSuccess);
                } else {
                    sendSuccessResponse(res, result, messages.CurrentPasswordIncorrect);
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
    else {
        sendBadRequestResponse(res);
    }
};

exports.forgetPassword = function (req, res) {
    if (!req.body.email)
        return sendBadRequestResponse(res);

    var email = req.body.email;
    userDal.getUserByEmail(email)
        .then(function (user) {
            if (user) {

                var userUpdateCondition = { _id: user._id.toString() };
                var userUpdateSet = {
                    password: Random.generateRandomString(8)
                };

                userDal.update(userUpdateCondition, userUpdateSet)
                    .then(function (user) {

                        if (user) {
                            //TODO
                            //send resetPassword email
                            sendSuccessResponse(res, '', messages.ResetPasswordEmailSent);
                        }
                        else {
                            sendFailResponse(res);
                        }
                    })
                    .catch(function (err) {
                        console.log(err);
                        sendErrorResponse(res);
                    });
            } else {
                sendFailResponse(res, messages.EmailNotRegistered);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
};

exports.updateProfile = function (req, res) {
    var mobileUserDTO = req.body.mobileUserDTO;

    userDal.getUserById(mobileUserDTO.ID)
        .then(function (userDTO) {

            if (userDTO) {
                if (userDTO.ID > 0) {
                    if (userDTO.password == mobileUserDTO.password) {

                        //update profile photo TODO
                        userDTO.firstName = mobileUserDTO.firstName;
                        userDTO.lastName = mobileUserDTO.lastName;
                        userDTO.phoneNumber = mobileUserDTO.phoneNumber;
                        //address TODO

                        userDal.update(userDTO, userID)
                            .then(function (user) {

                                if (user) {
                                    sendSuccessResponse(res, result, messages.UpdateSuccess);
                                }
                                else {
                                    sendFailResponse(res, messages.PasswordIncorrect);
                                }
                            })
                            .catch(function (err) {
                                console.log(err);
                                sendErrorResponse(res);
                            });
                    }
                }
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
};


