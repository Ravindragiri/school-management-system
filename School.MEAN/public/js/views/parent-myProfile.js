﻿//$(document).ready(function () {
//    $('#myTab').on('click', '.nav-tabs a', function () {
//        // set a special class on the '.dropdown' element
//        $(this).closest('.dropdown').addClass('dontClose');
//    })

//    $('#menu1').on('hide.bs.dropdown', function (e) {
//        if ($(this).hasClass('dontClose')) {
//            e.preventDefault();
//        }
//        $(this).removeClass('dontClose');
//    });
//});

// create angular controller

var app = angular.module('schoolApp', ['ngTouch']);

app.controller('ngParentMyProfileController', function ($scope, $http) {

    $scope.getContent = function (obj) {
        return obj.value + " " + obj.text;
    };

    var baseUrl = 'http://localhost:1238'

    $scope.parentDTO = {};
    $scope.readOne = function () {

        var userId = $("#userId").val();
        //var userId = "5a89c945480d720aa0a4337d";

        $http({
            method: 'GET',
            url: baseUrl + '/api/parent/getParentByUserId' + '?userId=' + userId
        }).then(function (response) {
            $scope.parentDTO = response.data.data[0];
            $scope.addMode = false;
        });
    };
    $scope.readOne();

    $scope.updateProfile = function () {

        var data = {
            parentDTO: $scope.parentDTO
        }
        return $http({
            method: 'POST',
            //url: baseUrl + objectName + '/' + id,
            url: baseUrl + '/api/parent/updateProfile',
            data: data
        }).then(function (response) {

        });
    };

    $scope.updatePassword = function () {

        var data = {
            updatePasswordDTO: {
                parentId: $scope.parentDTO._id,
                userId: $scope.parentDTO.userRef._id,
                password: $scope.parentDTO.password,
                newPassword: $scope.parentDTO.newPassword,
            }
        }
        $http({
            method: 'POST',
            //url: baseUrl + objectName + '/' + id,
            url: baseUrl + '/api/user/updatePassword',
            data: data
        }).then(function (response) {

        });
    };

    $scope.updateEmail = function () {

        var data = {
            updateEmailDTO: {
                parentId: $scope.parentDTO._id,
                userId: $scope.parentDTO.userRef._id,
                password: $scope.parentDTO.password,
                email: $scope.parentDTO.email,
            }
        }
        $http({
            method: 'POST',
            //url: baseUrl + objectName + '/' + id,
            url: baseUrl + '/api/user/updateEmail',
            data: data
        }).then(function (response) {

        });
    };

});

$(function () {

    $("#btnChangePersonalInfo").on('click', function () {
        $('[href=#changePersonalInfo]').tab('show');
    });

    $("#btnChangeEmail").on('click', function () {
        $('[href=#changeEmail]').tab('show');
    });

    $("#btnChangePassword").on('click', function () {
        $('[href=#changePassword]').tab('show');
    });
});
