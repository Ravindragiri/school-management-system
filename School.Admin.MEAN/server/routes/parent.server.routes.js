﻿module.exports = function (app) {

    var parentController = require('../controllers/parent.server.controller');
    app.get('/parent/index', parentController.index);
    app.get('/parent', parentController.index);

    var parentApiController = require('../controllers/api/parent.server.api.controller');
    app.get('/api/parent/getAllParentInfo', parentApiController.getAllParentInfo);
    app.get('/api/parent/getParentByUserId', parentApiController.getParentByUserId);
    app.post('/api/parent/update', parentApiController.update);
    app.post('/api/parent/updateProfile', parentApiController.updateProfile);
    app.post('/api/parent/add', parentApiController.add);
    app.post('/api/parent/remove', parentApiController.remove);
};