﻿var mongoose = require('mongoose');

var addmissionRegisterSchema = new mongoose.Schema({

    registrationNo: Number,
    addmissionDate: Date,

    createdBy: {
        type: mongoose.Schema.Types.ObjectId
    },
    createdDate: {
        type: Date
    },
    updatedBy: {
        type: mongoose.Schema.Types.ObjectId
    },
    updatedDate: Date
});

mongoose.model('addmissionRegister', addmissionRegisterSchema);