﻿(function ($) {

    //re-set all client validation given a jQuery selected form or child
    $.fn.resetValidation = function () {

        var $form = this.closest('form');

        //reset jQuery Validate's internals
        $form.validate().resetForm();

        //reset unobtrusive validation summary, if it exists
        $form.find("[data-valmsg-summary=true]")
            .removeClass("validation-summary-errors")
            .addClass("validation-summary-valid")
            .find("ul").empty();

        //reset unobtrusive field level, if it exists
        $form.find("[data-valmsg-replace]")
            .removeClass("field-validation-error")
            .addClass("field-validation-valid")
            .empty();

        $form.find('.input-validation-error')
            .removeClass('input-validation-error')
            .addClass('valid');

        return $form;
    };

    //reset a form given a jQuery selected form or a child
    //by default validation is also reset
    $.fn.formReset = function (resetValidation) {
        var $form = this.closest('form');

        $form[0].reset();

        if (typeof (resetValidation) === "undefined" || resetValidation) {
            $form.resetValidation();
        }

        return $form;
    };
})(jQuery);

function generateUUID() {
    var d = new Date().getTime();
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = (d + Math.random() * 16) % 16 | 0;
        d = Math.floor(d / 16);
        return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
    return uuid;
};

$(document).ready(function () {

    $("#btnSignIn").click(function (e) {
        e.preventDefault();

        var postData = {
            username: $("#exampleInputEmail2", "#login-dp").val(),
            password: $("#exampleInputPassword2", "#login-dp").val()
        };

        $.ajax({
            type: "POST",
            url: '/account/login/',
            data: postData,
            dataType: "json",
            accept: 'application/json',
            beforeSend: function () {

            },
            success: function (response) {

                if (response != null) {
                    var status = response.statusCode;

                    switch (status) {
                        case 200:
                            var userInfo = response.data;
                            if (userInfo) {
                                if (userInfo.userType == 1) {
                                    window.location.href = '/student/myProfile';
                                }
                                else if (userInfo.userType == 4) {
                                    window.location.href = '/parent/myProfile';
                                }
                            }
                            break;
                        case 0:
                            alert(response.message);
                            break;
                        case -10:
                            //$scope.showServerErrorMessages(response, response.serverErrors);
                            alert(response.serverErrors);
                            break;
                        case -500:
                            alert(response.message)
                            //window.location.href = errorPage;
                            break;
                        default:
                    }
                }
            },
            error: function (xhr, status, errorThrown) {
                alert("An error occurred while processing your request.");
            }
        });
    });
});