﻿var mongoose = require('mongoose');

var dormitorySchema = new mongoose.Schema({
    name: String,
    numberOfRooms: Number,
    desc: String
});

exports.dormitorySchema = dormitorySchema;
mongoose.model('dormitory', dormitorySchema);