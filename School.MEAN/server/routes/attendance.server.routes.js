﻿module.exports = function (app) {

    var attendanceController = require('../controllers/attendance.server.controller');
    app.get('/student/attendance/index', attendanceController.index);
    app.get('/student/attendance', attendanceController.index);

    //var attendanceApiController = require('../controllers/api/attendance.server.api.controller');
    //get
    //app.get('/api/attendance/getAttendanceDetail', attendanceApiController.getAttendanceDetail);
    //app.get('/api/attendance/getAttendanceById', attendanceApiController.getAttendanceById);
    //app.get('/api/attendance/count', attendanceApiController.count);
    //app.get('/api/attendance/getAllAttendances', attendanceApiController.getAllAttendances);
    //app.get('/api/attendance/getAttendanceByStudentId', attendanceApiController.getAttendanceByStudentId);
    //post
    //app.post('/api/attendance/removeByDate', attendanceApiController.removeByDate);
    //app.post('/api/attendance/removeById', attendanceApiController.removeById);
    //app.post('/api/attendance/addAttendance', attendanceApiController.addAttendance);
    //app.post('/api/attendance/update', attendanceApiController.update);
};