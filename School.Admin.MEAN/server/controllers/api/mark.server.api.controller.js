﻿var markDal = require('../../../server/school.dal/mark.dal.js');
require('../../school.common/helpers/responseHelper');

var mongoose = require('mongoose'),
    //fs = require('fs-extra'),
    //util = require("util"),
//path = require("path");

var _mark = mongoose.model('mark');
var _user = mongoose.model('user');

exports.uploadMarks = function (req, res) {

}

exports.addMark  = function (req, res) {

    var markDTO = req.body.markDTO;

    markDal.add(markDTO)
        .then(function (result) {
            if (result) {
                sendSuccessResponse(res, result);
            } else {
                sendErrorResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
}

exports.updateMark = function (req, res) {
    var markDTO = req.body.markDTO;

    markDal.getUserById(markDTO.Id)
        .then(function (markModel) {

            if (markModel) {
                if (markModel.Id > 0) {
                    if (markModel.password == markDTO.password) {

                        //update profile photo TODO
                        markModel.markObtained = markDTO.markObtained;
                        markModel.markCategoryId = markDTO.markCategoryId;
                        markModel.description = markDTO.description;
                        markModel.method = markDTO.method;
                        markModel.amount = markDTO.amount;
                        //address TODO

                        markDal.update(markModel, userId)
                            .then(function (user) {

                                if (user) {
                                    sendSuccessResponse(res, result, messages.UpdateSuccess);
                                }
                                else {
                                    sendFailResponse(res, messages.PasswordIncorrect);
                                }
                            })
                            .catch(function (err) {
                                console.log(err);
                                sendErrorResponse(res);
                            });
                    }
                }
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
};

exports.deleteMark = function (req, res) {
    var markModel = req.body.markModel;
    var markId = req.body.markId;

    markDal.deleteMark(markModel, markId)
        .then(function (result) {
            if (result) {
                sendSuccessResponse(res, result, messages.DeleteSuccess);
            } else {
                sendFailResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
};

exports.getMark = function (req, res) {

    var searchCriteria = {
        userType: (req.query.userType) ? parseInt(req.query.userType) : appDefaults.UserType,
        searchText: req.query.title,

        page: (req.query.page) ? parseInt(req.query.page) : appDefaults.Page,
        pageSize: (req.query.pageSize) ? parseInt(req.query.pageSize) : appDefaults.PageSize,
        orderBy: req.query.orderBy ? req.query.orderBy : appDefaults.OrderBy
    };

    markDal.getMarksByAnyCriteria(searchCriteria)
        .then(function (result) {
            if (result) {
                sendSuccessResponse(res, result);
            } else {
                sendFailResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            return sendErrorResponse(res);
        });
}
