﻿module.exports = function (app) {

    var classFeeController = require('../controllers/class.fee.server.controller');
    app.get('/class/fees', classFeeController.index);

    var classFeeApiController = require('../controllers/api/class.fee.server.api.controller');
    //get
    app.get('/api/classFee/getById', classFeeApiController.getById);
    app.get('/api/classFee/getByClass', classFeeApiController.getByClass);
    //app.get('/api/classFee/count', classFeeApiController.count);
    app.get('/api/classFee/getAll', classFeeApiController.getAll);
    //post
    app.post('/api/classFee/add', classFeeApiController.add);
    app.post('/api/classFee/update', classFeeApiController.update);
    app.post('/api/classFee/removeById', classFeeApiController.removeById);
};