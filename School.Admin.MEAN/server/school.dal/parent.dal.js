﻿var Q = require('q'),
    mongoose = require('mongoose'),
    async = require('async'),
    _ = require('lodash');

require('../models/parent.server.model');
require('../models/user.server.model');

var _parent = mongoose.model('parent');
var _student = mongoose.model('student');
var _user = mongoose.model('user');

var ObjectId = mongoose.Types.ObjectId;

require('../../server/school.common/helpers/deferredHelper');

var dal = {};

dal.add = add;
dal.getAllParentDetails = getAllParentDetails;
dal.getParentDetailsById = getParentDetailsById;
dal.loadEmail = loadEmail;
//dal.searchParent = searchParent;
dal.getUserName = getUserName;
dal.update = update;
dal.updateProfile = updateProfile;
dal.remove = remove;
dal.getParentByUserId = getParentByUserId;

function insert(parentModel, studentId) {

    var loggedInUserId;
    var deferred = Q.defer();

    parentModel
        .save(function (err, parent) {
            if (err) deferred.reject(err);

            if (parent) {
                _student.findOne({ _id: studentId }, function (err, student) {
                    if (err)
                        deferred.reject(err);

                    if (student) {

                        student.parentRef = parent._id;
                        student.updatedBy = loggedInUserId;
                        student.updatedDate = new Date();

                        student.save(function (err, result) {
                            if (err) deferred.reject(err);

                            if (result) {
                                result = _.pick(result.toObject(), ['_id', 'parentname', 'fatherEmail', 'isProfileIncomplete', 'firstName', 'fatherContactNumber', 'province',
                                    'city', 'address', 'pinCode', 'countryDialCode', 'avatarUrl', 'exams']);
                                deferred.resolve(result);
                            } else {
                                deferred.resolve();
                            }
                        });
                    }
                });
            } else {
                deferred.resolve();
            }
        });

    return deferred.promise;
}

function add(parentModel, userModel, studentId) {

    var userDal = require('./user.dal.js');
    var deferred = Q.defer();

    async.waterfall(
        [
            function (callback) {

                userModel.createdDate = new Date();

                userDal.add(userModel)
                    .then(function (user) {

                        if (user) {

                            parentModel.userRef = user._id;
                            parentModel.studentRef = studentId;

                            parentModel.createdBy = user._id;
                            parentModel.createdDate = new Date();
                            callback(null, parentModel, studentId);
                        } else {
                            deferred.resolve();
                        }
                    })
                    .catch(function (err) {
                        if (err) deferred.reject(err);
                    });
            },
            function (parentModel, studentId, callback) {
                insert(parentModel, studentId)
                    .then(function (student) {

                        if (student) {
                            var student = _.pick(student.toObject(), ['_id', 'province', 'city', 'address', 'pinCode', 'avatarUrl', 'exams']);
                            deferred.resolve(student);
                        } else {
                            deferred.resolve();
                        }
                    })
                    .catch(function (err) {
                        if (err) deferred.reject(err);
                    });
            }
        ], function (errs, results) {
            if (errs) {
                async.each(results, rollback, function () {
                    console.log('Rollback done.');
                    res.status(400).send(errs);
                });
            } else {
                console.log('Done.');
                deferred.resolve(results);
            }
        });

    function rollback(doc, callback) {
        if (!doc) { callback(); }
        else {
            userModel.findByIdAndRemove(doc._id, function (err, doc) {
                console.log('Rolled-back document: ' + doc);
                callback();
            });
        }
    }
    return deferred.promise;
}

function update(updateParentModel) {

    var deferred = Q.defer();
    parentModel.findOne({ _id: updateParentModel._id }, function (err, parent) {
        if (err)
            deferred.reject(err);

        if (parent) {

            parent.fatherEmail = updateParentModel.fatherEmail ? updateParentModel.fatherEmail : parent.fatherEmail;
            parent.fatherContactNumber = updateParentModel.fatherContactNumber ? updateParentModel.fatherContactNumber : parent.fatherContactNumber;
            parent.fatherName = updateParentModel.fatherName ? updateParentModel.fatherName : parent.fatherName;
            parent.fatherOccupation = updateParentModel.fatherOccupation ? updateParentModel.fatherOccupation : parent.fatherOccupation;

            parent.motherEmail = updateParentModel.motherEmail ? updateParentModel.motherEmail : parent.motherEmail;
            parent.motherContactNumber = updateParentModel.motherContactNumber ? updateParentModel.motherContactNumber : parent.motherContactNumber;
            parent.motherName = updateParentModel.motherName ? updateParentModel.motherName : parent.motherName;
            parent.motherOccupation = updateParentModel.motherOccupation ? updateParentModel.motherOccupation : parent.motherOccupation;

            //parent.country = updateParentModel.country ? updateParentModel.country : parent.country;
            //parent.countryISO2 = updateParentModel.countryISO2 ? updateParentModel.countryISO2 : parent.countryISO2;

            parent.avatarUrl = updateParentModel.avatarUrl ? updateParentModel.avatarUrl : parent.avatarUrl;
            //parent.AvatarMimeType = mimeType;
            //parent.AvatarFileName = fileName;

            parent.updatedBy = updateParentModel._id;
            parent.updatedDate = new Date();

            parent.save(function (err, result) {
                if (err) deferred.reject(err);

                if (result) {
                    result = _.pick(result.toObject(), ['_id', 'parentname', 'fatherEmail', 'isProfileIncomplete', 'firstName', 'fatherContactNumber', 'province',
                        'city', 'address', 'pinCode', 'countryDialCode', 'avatarUrl', 'exams']);
                    deferred.resolve(result);
                } else {
                    deferred.resolve();
                }
            });
        }
    });
    return deferred.promise;
}

function updateParent(parentDTO, parentId) {

    var parent = new _parent(parentDTO);
    parent.createdBy = userId;
    parent.createdDate = new Date();

    var deferred = Q.defer();

    _parent
        .findByIdAndUpdate({ _id: userId }, { $push: { "parents": parent } }, { safe: true, upsert: true }, function (err, result) {
            updateHelper(deferred, err, result);
        });
    return deferred.promise;
}

function getAllParentDetails() {

    var deferred = Q.defer();

    _parent
        .find({})
        .populate({
            path: 'userRef',
            select: '_id firstName lastName birthDate phoneNumber bloodGroup email username address city province pinCode'
        })
        .exec(function (err, result) {
            execHelper(deferred, err, result);
        });
    return deferred.promise;
}

function getParentDetailsById() {

    var deferred = Q.defer();

    _parentModel
        .findOne({ '_id': parentId })
        .exec(function (err, result) {
            execHelper(deferred, err, result);
        });
    return deferred.promise;
}

function loadEmail() {

}

function getAllEmail() {

    _parent
        .aggregate(

        { $lookup: { from: "users", localField: "userRef", foreignField: "_id", as: "user" } },
        { $unwind: "$user" },
        {
            $project: {
                "_id": 1,
                "fatherName": 1,
                "fatherContactNumber": 1,
                "motherName": 1,
                "motherContactNumber": 1,

                userId: "$user._id",
                email: "$user.email",
            }
        })
        .exec(function (err, result) {
            execHelper(deferred, err, result);
        });
    return deferred.promise;
}

function getEmailById(parentId) {

    var deferred = Q.defer();

    _parent
        .aggregate(

        { $match: { '_id': ObjectId(parentId) } },
        { $lookup: { from: "users", localField: "userRef", foreignField: "_id", as: "user" } },
        { $unwind: "$user" },
        {
            $project: {
                "_id": 1,
                "fatherName": 1,
                "fatherContactNumber": 1,
                "motherName": 1,
                "motherContactNumber": 1,

                userId: "$user._id",
                email: "$user.email",
            }
        })
        .exec(function (err, result) {
            execHelper(deferred, err, result);
        });
    return deferred.promise;
}

function getParentByUserId(userId) {

    var deferred = Q.defer();

    _parent
        .aggregate(
        { $lookup: { from: "users", localField: "userRef", foreignField: "_id", as: "user" } },
        { $unwind: "$user" },
        {
            $match: { "user._id": { $eq: ObjectId(userId) } }
        },
        { $lookup: { from: "students", localField: "studentRef", foreignField: "_id", as: "student" } },
        {
            $unwind: {
                path: '$student',
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $project: {
                "_id": 1,
                "fatherName": 1,
                "fatherContactNumber": 1,
                "fatherOccupation": 1,
                "fatherEmail": 1,

                "motherName": 1,
                "motherContactNumber": 1,
                "motherOccupation": 1,
                "motherEmail": 1,

                "userRef": {
                    _id: "$user._id",
                    address: "$user.address",
                    city: "$user.city",
                    province: "$user.province",
                    pinCode: "$user.pinCode"
                }
            }
        })
        .exec(function (err, result) {
            execHelper(deferred, err, result);
        });
    return deferred.promise;
}

function getEmailByStudentId(studentId) {

    var deferred = Q.defer();

    _parent
        .aggregate(
        {
            $match: { "student._id": { $eq: studentId } }
        },
        { $lookup: { from: "users", localField: "userRef", foreignField: "_id", as: "user" } },
        { $unwind: "$user" },
        { $lookup: { from: "students", localField: "studentRef", foreignField: "_id", as: "student" } },
        { $unwind: "$student" },
        {
            $project: {
                "_id": 1,
                "fatherName": 1,
                "fatherContactNumber": 1,
                "motherName": 1,
                "motherContactNumber": 1,

                userId: "$user._id",
                email: "$user.email",
            }
        })
        .exec(function (err, result) {
            execHelper(deferred, err, result);
        });
    return deferred.promise;
}

function getParentContactDetailsById(parentId) {

    var deferred = Q.defer();

    _parent
        .aggregate(

        { $match: { '_id': ObjectId(parentId) } },
        { $lookup: { from: "users", localField: "userRef", foreignField: "_id", as: "user" } },
        { $unwind: "$user" },
        {
            $project: {
                "_id": 1,
                "fatherName": 1,
                "fatherContactNumber": 1,
                "motherName": 1,
                "motherContactNumber": 1,

                userId: "$user._id",
                email: "$user.email",
                phoneNumber: "$user.phoneNumber",

                address: "$user.address",
                city: "$user.city",
                province: "$user.province",
                pinCode: "$user.pinCode"
            }
        })
        .exec(function (err, result) {
            execHelper(deferred, err, result);
        });
    return deferred.promise;
}

function getParentContactDetailsByStudentId() {

    var deferred = Q.defer();

    _parent
        .aggregate(
        {
            $match: { "student._id": { $eq: studentId } }
        },
        { $lookup: { from: "users", localField: "userRef", foreignField: "_id", as: "user" } },
        { $unwind: "$user" },
        { $lookup: { from: "students", localField: "studentRef", foreignField: "_id", as: "student" } },
        { $unwind: "$student" },
        {
            $project: {
                "_id": 1,
                "fatherName": 1,
                "fatherContactNumber": 1,
                "motherName": 1,
                "motherContactNumber": 1,

                userId: "$user._id",
                email: "$user.email",
                phoneNumber: "$user.phoneNumber",

                address: "$user.address",
                city: "$user.city",
                province: "$user.province",
                pinCode: "$user.pinCode"
            }
        })
        .exec(function (err, result) {
            execHelper(deferred, err, result);
        });
    return deferred.promise;
}

function getParentsByAnyCriteria(searchCriteria) {

    var deferred = Q.defer();

    var str = searchCriteria.orderBy.split("_");
    var sortObject = {};
    var sortType = str[0];
    var sortDir = str[1] == "DESC" ? -1 : 1;
    sortObject[sortType] = sortDir;

    _user
        .aggregate(
        [
            {
                $match: {
                    $and: [
                        { 'userType': searchCriteria.userType },
                        {
                            $or: [
                                { 'country': new RegExp(searchCriteria.location, 'i') },
                                { 'province': new RegExp(searchCriteria.location, 'i') },
                                { 'city': new RegExp(searchCriteria.location, 'i') },
                                { 'address': new RegExp(searchCriteria.location, 'i') }
                            ]
                        },
                        {
                            $or: [{ 'exams.title': new RegExp(searchCriteria.searchText, 'i') },
                                { 'exams.description': new RegExp(searchCriteria.searchText, 'i') }]
                        }
                    ]
                }
            },
            {
                $project: {
                    _id: 1,
                    province: 1,
                    city: 1,
                    address: 1,
                    pinCode: 1,
                    firstName: 1,
                    loc: 1,
                    dist: 1,
                    email: 1,
                    exams: 1
                }
            },
            { "$sort": sortObject },
            { "$limit": searchCriteria.pageSize },
            { "$skip": (searchCriteria.page - 1) * searchCriteria.pageSize }
        ])
        .exec(function (err, result) {
            execHelper(deferred, err, result);
        });

    return deferred.promise;
}

function getUserName(parentId) {

    var deferred = Q.defer();

    _parent
        .aggregate(

        { $match: { '_id': ObjectId(parentId) } },
        { $lookup: { from: "users", localField: "userRef", foreignField: "_id", as: "user" } },
        { $unwind: "$user" },
        {
            $project: {
                "_id": 1,
                "fatherName": 1,
                "fatherContactNumber": 1,
                "motherName": 1,
                "motherContactNumber": 1,

                userId: "$user._id",
                username: "$user.username",
                email: "$user.email",
                phoneNumber: "$user.phoneNumber"
            }
        })
        .exec(function (err, result) {
            execHelper(deferred, err, result);
        });
    return deferred.promise;
}

function update(updateProfileModel, parentModel, avatarUrl) {

    var deferred = Q.defer();

    var userModel = updateProfileModel.userRef;

    async.waterfall(
        [
            function (callback) {

                _user.findOne({ _id: updateProfileModel._id }, function (err, user) {
                    if (err) {
                        console.log(err);
                        deferred.reject(err);
                    }
                    if (user) {

                        user.email = updateProfileModel.email ? updateProfileModel.email : user.email;
                        user.username = updateProfileModel.username ? updateProfileModel.username : user.username;
                        user.password = updateProfileModel.password ? updateProfileModel.password : user.password;

                        user.firstName = updateProfileModel.firstName ? updateProfileModel.firstName : user.firstName;
                        user.lastName = updateProfileModel.lastName ? updateProfileModel.lastName : user.lastName;
                        user.birthName = updateProfileModel.birthName ? updateProfileModel.birthName : user.birthName;
                        user.phoneNumber = updateProfileModel.phoneNumber ? updateProfileModel.phoneNumber : user.phoneNumber;
                        user.bloodGroup = updateProfileModel.bloodGroup ? updateProfileModel.bloodGroup : user.bloodGroup;

                        user.address = updateProfileModel.address ? updateProfileModel.address : user.address;
                        user.city = updateProfileModel.city ? updateProfileModel.city : user.city;
                        user.province = updateProfileModel.province ? updateProfileModel.province : user.province;
                        user.pinCode = updateProfileModel.pinCode ? updateProfileModel.pinCode : user.pinCode;

                        user.qualification = updateProfileModel.qualification ? updateProfileModel.qualification : user.qualification;
                        user.designation = updateProfileModel.designation ? updateProfileModel.designation : user.designation;
                        user.experience = updateProfileModel.experience ? updateProfileModel.experience : user.experience;

                        //user.country = updateProfileModel.country ? updateProfileModel.country : user.country;
                        //user.countryISO2 = updateProfileModel.countryISO2 ? updateProfileModel.countryISO2 : user.countryISO2;
                        //user.countryDialCode = updateProfileModel.countryDialCode ? updateProfileModel.countryDialCode : user.countryDialCode;

                        user.updatedBy = updateProfileModel._id;
                        user.updatedDate = new Date();

                        //user.status = request.status;
                        user.save(function (err, result) {
                            if (err) {
                                console.log(err);
                                deferred.reject(err);
                            }
                            if (result._id) {
                                callback(null, updateProfileModel);
                            }
                        });
                    }
                });
            },
            function (parentId, callback) {
                _parent.findOne({ _id: parentModel._id }, function (err, parent) {
                    if (err) {
                        console.log(err);
                        deferred.reject(err);
                    }
                    if (parent) {

                        parent.fatherName = parentModel.fatherName ? parentModel.fatherName : parent.fatherName;
                        parent.fatherOccupation = parentModel.fatherOccupation ? parentModel.fatherOccupation : parent.fatherOccupation;
                        parent.fatherContactNumber = parentModel.fatherContactNumber ? parentModel.fatherContactNumber : parent.fatherContactNumber;
                        parent.fatherEmail = parentModel.fatherEmail ? parentModel.fatherEmail : parent.fatherEmail;

                        parent.motherName = parentModel.motherName ? parentModel.motherName : parent.motherName;
                        parent.motherOccupation = parentModel.motherOccupation ? parentModel.motherOccupation : parent.motherOccupation;
                        parent.motherContactNumber = parentModel.motherContactNumber ? parentModel.motherContactNumber : parent.motherContactNumber;
                        parent.motherEmail = parentModel.motherEmail ? parentModel.motherEmail : parent.motherEmail;

                        //parent.updatedBy = parentModel.userRef._id;
                        parent.updatedDate = new Date();;

                        parent.save(function (err, result) {
                            if (err) {
                                console.log(err);
                                deferred.reject(err);
                            }
                            if (result._id) {
                                callback(null, parentModel.parentId);
                            }
                        });
                    }
                    else {
                        console.log(err);
                        deferred.reject(err);
                    }
                });
            }
        ], function (errs, results) {
            if (errs) {
                async.each(results, rollback, function () {
                    console.log('Rollback done.');
                    res.status(400).send(errs);
                });
            } else {
                console.log('Done.');
                deferred.resolve(results);
            }
        });

    function rollback(doc, callback) {
        if (!doc) { callback(); }
        else {
            //userModel.findByIdAndRemove(doc._id, function (err, doc) {
            //    console.log('Rolled-back document: ' + doc);
            //    callback();
            //});
        }
    }
    return deferred.promise;
}

function remove(parentId, userId) {

    var userDal = require('./user.dal.js');
    var deferred = Q.defer();

    async.waterfall(
        [
            function (callback) {

                //userModel.createdDate = new Date();

                var loggedInUserId;

                var userUpdateCondition = { $and: [] };
                userUpdateCondition.$and.push({
                    _id: userId
                });

                var userUpdateSet = {
                    isDeleted: true,
                    //updatedBy: loggedInUserId,
                    updatedDate: new Date()
                };

                userDal.update(userUpdateCondition, userUpdateSet)
                    .then(function (user) {

                        if (user) {

                            callback(null, parentId);
                        } else {
                            deferred.resolve();
                        }
                    })
                    .catch(function (err) {
                        if (err) deferred.reject(err);
                    });
            },
            function (parentId, callback) {

                var loggedInUserId;

                var parentUpdateCondition = { $and: [] };
                parentUpdateCondition.$and.push({
                    _id: parentId
                });

                var parentUpdateSet = {
                    isDeleted: true,
                    //updatedBy: loggedInUserId,
                    updatedDate: new Date()
                };

                update(parentUpdateCondition, parentUpdateSet)
                    .then(function (parent) {

                        if (parent) {
                            deferred.resolve(parent);
                        } else {
                            deferred.resolve();
                        }
                    })
                    .catch(function (err) {
                        if (err) {
                            callback(err, userModel);
                            //deferred.reject(err);
                        }
                    });
            }
        ], function (errs, results) {
            if (errs) {
                async.each(results, rollback, function () {
                    console.log('Rollback done.');
                    //res.status(400).send(errs);
                });
            } else {
                console.log('Done.');
                deferred.resolve(results);
            }
        });

    function rollback(doc, callback) {
        if (!doc) { callback(); }
        else {
            _user.findOne({ _id: updateProfileModel.userId }, function (err, user) {
                if (!user) {

                    user.isDeleted = false;
                    user.updatedBy = updateProfileModel._id;
                    user.updatedDate = new Date();
                }
                user.save(function (err, doc) {
                    console.log('Rolled-back document: ' + doc);
                    callback();
                });
            });
        }
    }
    return deferred.promise;
}

function updateProfile(updateProfileModel, parentModel, avatarUrl) {

    var deferred = Q.defer();

    var userModel = updateProfileModel.userRef;

    async.waterfall(
        [
            function (callback) {

                _user.findOne({ _id: updateProfileModel._id }, function (err, user) {
                    if (err) {
                        console.log(err);
                        deferred.reject(err);
                    }
                    if (user) {

                        //user.email = updateProfileModel.email ? updateProfileModel.email : user.email;
                        //user.username = updateProfileModel.username ? updateProfileModel.username : user.username;
                        //user.password = updateProfileModel.password ? updateProfileModel.password : user.password;

                        user.firstName = updateProfileModel.firstName ? updateProfileModel.firstName : user.firstName;
                        user.lastName = updateProfileModel.lastName ? updateProfileModel.lastName : user.lastName;
                        user.birthName = updateProfileModel.birthName ? updateProfileModel.birthName : user.birthName;
                        user.phoneNumber = updateProfileModel.phoneNumber ? updateProfileModel.phoneNumber : user.phoneNumber;
                        user.bloodGroup = updateProfileModel.bloodGroup ? updateProfileModel.bloodGroup : user.bloodGroup;

                        user.address = updateProfileModel.address ? updateProfileModel.address : user.address;
                        user.city = updateProfileModel.city ? updateProfileModel.city : user.city;
                        user.province = updateProfileModel.province ? updateProfileModel.province : user.province;
                        user.pinCode = updateProfileModel.pinCode ? updateProfileModel.pinCode : user.pinCode;

                        user.qualification = updateProfileModel.qualification ? updateProfileModel.qualification : user.qualification;
                        user.designation = updateProfileModel.designation ? updateProfileModel.designation : user.designation;
                        user.experience = updateProfileModel.experience ? updateProfileModel.experience : user.experience;

                        //user.country = updateProfileModel.country ? updateProfileModel.country : user.country;
                        //user.countryISO2 = updateProfileModel.countryISO2 ? updateProfileModel.countryISO2 : user.countryISO2;
                        //user.countryDialCode = updateProfileModel.countryDialCode ? updateProfileModel.countryDialCode : user.countryDialCode;

                        user.updatedBy = updateProfileModel._id;
                        user.updatedDate = new Date();

                        //user.status = request.status;
                        user.save(function (err, result) {
                            if (err) {
                                console.log(err);
                                deferred.reject(err);
                            }
                            if (result._id) {
                                callback(null, updateProfileModel);
                            }
                        });
                    }
                });
            },
            function (parentId, callback) {
                _parent.findOne({ _id: parentModel._id }, function (err, parent) {
                    if (err) {
                        console.log(err);
                        deferred.reject(err);
                    }
                    if (parent) {

                        parent.fatherName = parentModel.fatherName ? parentModel.fatherName : parent.fatherName;
                        parent.fatherOccupation = parentModel.fatherOccupation ? parentModel.fatherOccupation : parent.fatherOccupation;
                        parent.fatherContactNumber = parentModel.fatherContactNumber ? parentModel.fatherContactNumber : parent.fatherContactNumber;
                        parent.fatherEmail = parentModel.fatherEmail ? parentModel.fatherEmail : parent.fatherEmail;

                        parent.motherName = parentModel.motherName ? parentModel.motherName : parent.motherName;
                        parent.motherOccupation = parentModel.motherOccupation ? parentModel.motherOccupation : parent.motherOccupation;
                        parent.motherContactNumber = parentModel.motherContactNumber ? parentModel.motherContactNumber : parent.motherContactNumber;
                        parent.motherEmail = parentModel.motherEmail ? parentModel.motherEmail : parent.motherEmail;

                        //parent.updatedBy = parentModel.userRef._id;
                        parent.updatedDate = new Date();;

                        parent.save(function (err, result) {
                            if (err) {
                                console.log(err);
                                deferred.reject(err);
                            }
                            if (result._id) {
                                callback(null, parentModel.parentId);
                            }
                        });
                    }
                    else {
                        console.log(err);
                        deferred.reject(err);
                    }
                });
            }
        ], function (errs, results) {
            if (errs) {
                async.each(results, rollback, function () {
                    console.log('Rollback done.');
                    res.status(400).send(errs);
                });
            } else {
                console.log('Done.');
                deferred.resolve(results);
            }
        });

    function rollback(doc, callback) {
        if (!doc) { callback(); }
        else {
            //userModel.findByIdAndRemove(doc._id, function (err, doc) {
            //    console.log('Rolled-back document: ' + doc);
            //    callback();
            //});
        }
    }
    return deferred.promise;
}

module.exports = dal;