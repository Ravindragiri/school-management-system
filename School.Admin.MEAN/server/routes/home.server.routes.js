﻿module.exports = function (app) {

    var home = require('../controllers/home.server.controller');
    app.get('/', home.index);
    app.get('/home/index', home.index);
    app.get('/index', home.index);
};