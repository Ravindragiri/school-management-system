﻿module.exports = function (app) {

    var examTimeTableController = require('../controllers/exam.timeTable.server.controller');
    app.get('/exam/timeTable/index', examTimeTableController.index);
    app.get('/exam/timeTable', examTimeTableController.index);
    //app.get('/exam/timeTable/register', examTimeTableController.renderRegister);
    //app.get('/exam/timeTable/profile', examTimeTableController.renderProfile);
    //app.get('/exam/timeTable/myProfile', examTimeTableController.renderMyProfile);

    var examTimeTableApiController = require('../controllers/api/exam.timeTable.server.api.controller');
    //post
    app.post('/api/exam/timeTable/add', examTimeTableApiController.add);
    //app.post('/api/exam/timeTable/register', examTimeTableApiController.register);
    //app.post('/api/exam/timeTable/mobileRegister', examTimeTableApiController.mobileRegister);
    app.post('/api/exam/timeTable/update', examTimeTableApiController.update);
    app.post('/api/exam/timeTable/removeById', examTimeTableApiController.removeById);
    //get
    //app.get('/api/exam/timeTable/getStudentContactDetailById', examTimeTableApiController.getStudentContactDetailById);
    //app.get('/api/exam/timeTable/getStudentsByAnyCriteria', examTimeTableApiController.getStudentsByAnyCriteria);
    //app.get('/api/exam/timeTable/getStudentById', examTimeTableApiController.getStudentById);
    app.get('/api/exam/timeTable/getAllExamTimeTableInfo', examTimeTableApiController.getAllExamTimeTableInfo);
    app.get('/api/exam/timeTable/getExamTimeTableByExamTypeAndClass', examTimeTableApiController.getExamTimeTableByExamTypeAndClass);
    //app.get('/api/exam/timeTable/searchStudents', examTimeTableApiController.searchStudents);
    //app.get('/api/exam/timeTable/getStudentAvatar', examTimeTableApiController.getStudentAvatar);
};