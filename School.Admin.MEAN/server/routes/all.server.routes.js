﻿module.exports = function (app) {

    require('../routes/home.server.routes.js')(app);
    require('../routes/student.server.routes.js')(app);
    require('../routes/student.exam.server.routes.js')(app);
    require('../routes/exam.timeTable.server.routes.js')(app);
    require('../routes/class.timeTable.server.routes.js')(app);
    require('../routes/teacher.server.routes.js')(app);
    require('../routes/officeStaff.server.routes.js')(app);
    require('../routes/attendance.server.routes.js')(app);
    require('../routes/parent.server.routes.js')(app);
    require('../routes/subject.server.routes.js')(app);
    //require('../routes/message.server.routes.js')(app);
    //require('../routes/recipient.server.routes.js')(app);
    require('../routes/error.server.routes.js')(app);
    require('../routes/gallery.server.routes.js')(app);
    //require('../routes/student.exam.server.routes.js')(app);
    require('../routes/search.server.routes.js')(app);
    require('../routes/holiday.server.routes.js')(app);
    require('../routes/student.apply.server.routes.js')(app);
    require('../routes/class.fee.server.routes.js')(app);
    //authenticate with $.ajax
    require('../routes/account.server.routes.js')(app);
    require('../routes/user.server.routes.js')(app);
    require('../routes/contactUs.server.routes.js')(app);
    require('../routes/invoice.server.routes.js')(app);

    //authenticate with form post
    //require('../routes/authenticate.server.routes.js')(app);
};