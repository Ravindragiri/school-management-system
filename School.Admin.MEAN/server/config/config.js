﻿//-- config.js
module.exports = {
    //mongoose connection failed with localhost while offline
    //just replace localhost with 127.0.0.1 in the connection string
    connectionUrl: 'mongodb://127.0.0.1/School',
    footerText: '© 2017 School. All rights reserved',
    schoolName: '',
    schoolAddress: 'Avenue park, 4th Block, Near statue',
    schoolContactNo: '+456 123 7890',
    schoolFaxNo: '',
    schoolEmail: 'info@example.com',
    projectName: 'School',
    projectTag: 'Education',
    twilioAccountSid: 'ACXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX',
    twilioAuthToken: 'your_auth_token',

    hostUrl: 'https://www.g1fresh.com',

};