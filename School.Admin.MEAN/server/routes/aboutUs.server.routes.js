﻿module.exports = function (app) {

    var aboutUs = require('../controllers/aboutUs.server.controller');
    app.get('/aboutUs/index', aboutUs.index);
};