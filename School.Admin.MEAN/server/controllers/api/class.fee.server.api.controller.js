﻿var classFeeDal = require('../../../server/school.dal/class.fee.dal.js');
require('../../school.common/helpers/responseHelper');

var mongoose = require('mongoose');
    //fs = require('fs-extra'),
    //util = require("util"),
//path = require("path");

var _classFee = mongoose.model('fee');
var _user = mongoose.model('user');

exports.add = function (req, res) {

    if (req.body.classFeeDTO) {
        var classFeeDTO = req.body.classFeeDTO;

        classFeeDal.add(classFeeDTO)
            .then(function (result) {
                if (result) {
                    sendSuccessResponse(res, result);
                } else {
                    sendErrorResponse(res);
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
    else {
        sendBadRequestResponse(res);
    }
}

exports.update = function (req, res) {

    if (req.body.classFeeDTO) {

        var classFeeDTO = req.body.classFeeDTO;
        var loggedInUserId;
        classFeeDal.update(classFeeDTO, loggedInUserId)
            .then(function (result) {

                if (result) {
                    sendSuccessResponse(res, result, messages.UpdateSuccess);
                }
                else {
                    sendFailResponse(res, messages.PasswordIncorrect);
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
    else {
        sendBadRequestResponse(res);
    }
};

exports.removeById = function (req, res) {
    if (req.query.id) {
        var classFeeId = req.query.id;

        classFeeDal.deleteById(classFeeId)
            .then(function (result) {
                if (result) {
                    sendSuccessResponse(res, result, messages.DeleteSuccess);
                } else {
                    sendFailResponse(res);
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
    else {
        sendBadRequestResponse(res);
    }
};

exports.getById = function (req, res) {

    if (req.query.classFeeId) {
        var classFeeId = req.query.classFeeId;

        classFeeDal.getById(classFeeId)
            .then(function (result) {
                if (result) {
                    sendSuccessResponse(res, result);
                } else {
                    sendErrorResponse(res);
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
    else {
        sendBadRequestResponse(res);
    }
}

exports.getByClass = function (req, res) {

    if (req.query.className) {
        var className = req.query.className;

        classFeeDal.getByClass(className)
            .then(function (result) {
                if (result) {
                    sendSuccessResponse(res, result);
                } else {
                    sendErrorResponse(res);
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
    else {
        sendBadRequestResponse(res);
    }
}

exports.getAll = function (req, res) {

    classFeeDal.getAll()
        .then(function (result) {
            if (result) {
                sendSuccessResponse(res, result);
            } else {
                sendErrorResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
}