﻿//var register = require('../../server/controllers/register'),
//    login = require('../../server/controllers/login'),
var user = require('../../server/controllers/user.server.controller'),
    passport = require('passport');

module.exports = function (app) {

    //app.route('/account').post(account.create).get(account.list);

    //app.route('/account/:userId').get(account.read).put(account.update).delete(account.delete);

    //app.param('userId', account.userByID);

    app.route('/account/register')
        .get(user.renderRegister)
        .post(user.signup);

    app.route('/account/login')
        .get(user.renderLogin)
        .post(user.signin);

    app.route('/login')
        .get(user.renderLogin)
        .post(user.signin);

    app.get('/logout', user.signout);

    app.route('/account/registrationSuccess')
        .get(user.renderRegistrationSuccess)
};