﻿/***** angular ngParentIndexController starts *****/
var app = angular.module('schoolApp', ['ngTouch', 'ui.grid']);

app.controller('ngParentIndexController', ['ParentService', '$scope', '$http',
    function (ParentService, $scope, $http) {

        $scope.addMode = true;

        $scope.gridOptions = {
            columnDefs: [
                { name: 'Student Name', field: "userRef.firstName", cellTemplate: '<div class="ui-grid-cell-contents">{{ COL_FIELD }}</div>' },
                { name: 'Father Name', field: "fatherName", cellTemplate: '<div class="ui-grid-cell-contents">{{ COL_FIELD }}</div>' },
                { name: 'Father Contact No', field: "fatherOccupation", cellTemplate: '<div class="ui-grid-cell-contents">{{ COL_FIELD }}</div>' },
                { name: 'Mother Name', field: "motherOccupation", cellTemplate: '<div class="ui-grid-cell-contents">{{ COL_FIELD }}</div>' },
                { name: 'Mother Contact No', field: "motherOccupation",cellTemplate: '<div class="ui-grid-cell-contents">{{ COL_FIELD }}</div>' },
                { name: 'edit', cellTemplate: '<button type="button" class="btn btn-xs" ng-click="grid.appScope.editParent(row)">Edit</button>' },
                { name: 'delete', cellTemplate: '<button type="button" class="btn btn-xs" ng-click="grid.appScope.deleteParent(row)">Delete</button>' }
            ],
            excludeProperties: '__metadata',
        };

        $scope.validateRegisterForm = function () {
            $scope.registrationFormReset = false;
            $scope.showRegisterValidation = function () { return $scope.registrationForm.$invalid; };
        }

        //$scope.reset = function () {
        //    $scope.form = angular.copy(reviewFormFields());
        //    $scope.registrationFormReset = true;
        //    $scope.reviewForm.$setPristine();
        //}

        $scope.register = function () {

            //$scope.validateRegisterForm();
            $("#registrationForm").validate();

            //if ($scope.registrationForm.$valid) {
            if ($('#registrationForm').valid()) {

                $scope.loading = true;

                var postData = {
                    __RequestVerificationToken: angular.element("input[name='__RequestVerificationToken']").val(),
                    UserModel: $scope.UserModel,
                    ServiceProviderModel: $scope.ServiceProviderModel
                };

                $.ajax({
                    type: "POST",
                    url: 'http://localhost:1238/api/parent/add',
                    data: postData,
                    dataType: "json",
                    accept: 'application/json',
                    beforeSend: function () {

                    },
                    success: function (response) {

                        if (response != null) {
                            var status = response.statusCode;

                            switch (status) {
                                case 1:
                                    //var objTravel = JSON.parse(response.data);
                                    break;
                                case 0:
                                    alert(response.message);
                                    break;
                                case -10:
                                    $scope.showServerErrorMessages(response, response.serverErrors);
                                    break;
                                case -500:
                                    alert(response.message)
                                    //window.location.href = errorPage;
                                    break;
                                default:
                            }
                        }
                    },
                    error: function (xhr, status, errorThrown) {
                        alert("An error occurred while processing your request.");
                    }
                });

                //
            }
        };

        $scope.showServerErrorMessages = function (response, errors) {

            $.each(errors, function (index, error) {

                $scope.$apply(function () {

                    $scope.invalidelements[error.Element] = true;
                    $('label[ng-show="invalidelements.' + error.Element + '"]').css('display', 'inline-block');
                    $('.' + error.Element).focus();
                });
            });
        }

        $scope.load = function () {
            ParentService.readAll().then(function (response) {
                $scope.gridOptions.data = response.data;
                //$scope.gridOptions.data = JSON.parse(response.data);
            });
        }

        $scope.load();

        $scope.editParent = function (row) {
            if (row.entity) {
                $scope.parentDTO = row.entity;
                //var index = $scope.gridOptions.data.indexOf(row.entity);
                $scope.addMode = false;
            }
        };

        $scope.deleteParent = function (field) {
            if (row.entity._id) {

                var data = {
                    parentId: row.entity._id,
                    userId: row.entity.userRef._id
                }

                ParentService.delete(data).then(function (response) {
                    
                    if (response != null) {
                        var status = response.statusCode;

                        switch (status) {
                            case 200:
                                $scope.gridOptions.data = response.data;
                                var index = $scope.gridOptions.data.indexOf(row.entity);
                                $scope.gridOptions.data.splice(index, 1);
                                alert("Record has been deleted successfully.");
                                break;
                            case 0:
                                alert("Your request cannot be processed now due to some technical issue. please try again later.");
                                break;
                            case -1:
                            case 400:
                            case 401:
                            case 204:
                                alert(response.message);
                                break;
                            default:
                        }
                    }
                });
            }
        };

        $scope.search = function () {
            if ($scope.standard && $scope.section) {
                var data = {
                    class: $scope.standard,
                    section: $scope.section
                };

                ParentService.getStudentsByClassAndSection(data).then(function (response) {
                    $scope.students = response.data;
                    //$scope.gridOptions.data = JSON.parse(response.data);
                });
            }
        };

        $scope.insert = function () {

            var data = {
                studentId: $scope.studentId,
                parentDTO: $scope.parentDTO
            };

            ParentService.create(data).then(function (response) {
                
                if (response != null) {
                    var status = response.statusCode;

                    switch (status) {
                        case 200:
                            $scope.parentDTO = {};
                            $scope.load();
                            alert("Record has been saved successfully.");
                            break;
                        case 0:
                            alert("Your request cannot be processed now due to some technical issue. please try again later.");
                            break;
                        case -1:
                        case 400:
                        case 401:
                        case 204:
                            alert(response.message);
                            break;
                        default:
                    }
                }
            });
        };

        $scope.update = function () {
            var data = {
                parentDTO: $scope.parentDTO
            }
            ParentService.update(data).then(function (response) {
                
                if (response != null) {
                    var status = response.statusCode;

                    switch (status) {
                        case 200:
                            $scope.addMode = true;
                            alert("Record has been updated successfully.");
                            break;
                        case 0:
                            alert("Your request cannot be processed now due to some technical issue. please try again later.");
                            break;
                        case -1:
                        case 400:
                        case 401:
                        case 204:
                            alert(response.message);
                            break;
                        default:
                    }
                }
            });
        };

        $scope.cancel = function () {
            $scope.parentDTO = {};
            $scope.addMode = true;
        }
    }]);
/***** angular ngParentIndexController ends *****/

(function () {

    angular.module('schoolApp')
        .service('ParentService', ['$http', ParentService]);

    function ParentService($http) {

        var self = this;
        var baseUrl = 'http://localhost:1238';

        //var objectName = 'products';

        self.readAll = function () {
            return $http({
                method: 'GET',
                url: baseUrl + '/api/parent/getAllParentInfo',
                //url: baseUrl + objectName
            }).then(function (response) {
                return response.data;
            });
        };

        self.getStudentsByClassAndSection = function (data) {
            return $http({
                method: 'GET',
                url: 'http://localhost:1238/api/student/getAllStudentsByClassAndSection',
                params: data
            }).then(function (response) {
                return response.data;
            });
        };

        self.readOne = function (id) {
            return $http({
                method: 'GET',
                url: baseUrl + objectName + '/' + id
            }).then(function (response) {
                return response.data;
            });
        };

        self.create = function (data) {
            return $http({
                method: 'POST',
                url: 'http://localhost:1238/api/parent/add',
                data: data,
                params: {
                    returnObject: true
                },
            }).then(function (response) {
                return response.data;
            });
        };

        self.update = function (data) {
            return $http({
                method: 'POST',
                //url: baseUrl + objectName + '/' + id,
                url: baseUrl + '/api/parent/update',
                data: data
            }).then(function (response) {
                return response.data;
            });
        };

        self.delete = function (data) {
            return $http({
                method: 'POST',
                url: 'http://localhost:1238/api/parent/remove',
                params: data
            }).then(function (response) {
                return response.data;
            });
        };
    }
} ());