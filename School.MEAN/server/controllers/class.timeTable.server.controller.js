﻿var config = require('../config/config');
require('../school.common/appConstants');

exports.index = function (req, res) {

    res.render('class/timeTable/index', {
        pTitle: 'Class TimeTable List | School'
    });
};