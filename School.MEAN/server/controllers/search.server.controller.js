﻿exports.result = function (req, res) {
    res.render('search/result', {
        pTitle: 'Search Result | School'
    });
};

exports.advanced = function (req, res) {
    res.render('search/advanced', {
    });
};