﻿'use strict';
var config = require('../../config/config');
require('../../school.common/appConstants');
/**
 * Module dependencies.
 */
var _ = require('lodash'),
    errorHandler = require('../../controllers/error.server.controller'),
    mongoose = require('mongoose'),
    passport = require('passport'),
    User = mongoose.model('user');

exports.renderRegister = function (req, res) {
    res.render('account/register', {
        pTitle: 'Register'
    });
};

/**
 * Signup
 */
exports.signup = function (req, res) {
    // For security measurement we remove the roles from the req.body object
    delete req.body.roles;

    // Init Variables
    var user = new User(req.body);
    var message = null;

    // Add missing user fields
    user.provider = 'local';
    //user.displayName = user.firstName + ' ' + user.lastName;

    // Then save the user 
    user.save(function (err) {
        if (err) {
            //return res.status(400).send({
            //    message: errorHandler.getErrorMessage(err)
            //});
            res.status(400).send({
                state: 'failure',
                user: null,
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            // Remove sensitive data before login
            user.password = undefined;
            user.salt = undefined;

            req.login(user, function (err) {
                if (err) {
                    //res.status(400).send(err);
                    res.status(400).send({
                        state: 'failure',
                        user: null,
                        message: errorHandler.getErrorMessage(err)
                    });
                } else {
                    //res.json(user);
                    res.send({
                        state: 'success',
                        user: user ? user : null
                    });
                }
            });
        }
    });
};

exports.renderLogin = function (req, res, next) {
    if (!req.user) {
        res.render('account/login', {
            pTitle: 'Login | Organ Donor',
            heading: 'Login',
            messages: req.flash('error') || req.flash('info')
        });
    } else {
        return res.redirect('/');
    }
};

/**
 * Signin after passport authentication
 */
exports.signin = function (req, res, next) {
    passport.authenticate('local', function (err, user, info) {
        if (err) {
            console.log(err);
            res.status(400).send(info);
        }
        // Redirect if it fails
        if (!user) {
            return res.json({ info: info });
        }
        if (user) {
            // Remove sensitive data before login
            user.password = undefined;
            user.salt = undefined;

            req.logIn(user, function (err) {
                if (err) {
                    console.log(err);
                    sendErrorResponse(res);
                } else {
                    //res.json(user);
                    sendSuccessResponse(res, user);
                }
            });
        }
    })(req, res, next);
};

/**
 * Signout
 */
exports.signout = function (req, res) {
    req.logout();
    res.redirect('/');
};