﻿var parentDal = require('../../../server/school.dal/parent.dal.js');
require('../../school.common/helpers/responseHelper');

var mongoose = require('mongoose'),
    //fs = require('fs-extra'),
    //util = require("util"),
    async = require('async');
//path = require("path");

var _parent = mongoose.model('parent');
var _user = mongoose.model('user');

exports.add = function (req, res) {

    if (req.body.parentDTO && req.body.studentId) {

        var userDal = require('../../school.dal/user.dal.js');

        var parentModel = new _parent(req.body.parentDTO);
        var userModel = new _user(req.body.parentDTO.userRef);
        var studentId = req.body.studentId;

        async.waterfall(
            [
                function (callback) {
                    userDal.checkUserNameExist(userModel.username)
                        .then(function (result) {
                            if (result) {
                                sendFailResponse(res, messages.UsernameNotAvailable);
                            }
                            else {
                                callback(null);
                            }
                        });
                },
                function (callback) {
                    userDal.checkEmailExist(userModel.email)
                        .then(function (result) {
                            if (result) {
                                sendFailResponse(res, messages.EmailNotAvailable);
                            }
                            else {
                                callback(null);
                            }
                        });
                },
                function (callback) {

                    //userModel.createdDate = new Date();

                    parentDal.add(parentModel, userModel, studentId)
                        .then(function (user) {
                            if (user) {
                                sendSuccessResponse(res, user, messages.UserRegisteredSuccessfully);
                                //uploadAvatar(files, user._id.toString());
                            } else {
                                sendFailResponse(res, messages.UserRegistrationFailed);
                            }
                        })
                        .catch(function (err) {
                            console.log(err);
                            sendErrorResponse(res);
                        });
                }
            ], function (errs, results) {
                if (errs) {
                    console.log(err);
                    sendErrorResponse(res);
                } else {
                    console.log('Done.');
                }
            });
    }
};

exports.mobileAdd = function (req, res) {

    var userDal = require('../../school.dal/user.dal.js');

    var parentModel = new _parent(req.body.data.parentDTO);
    var userModel = new _user(req.body.data.userDTO);

    async.waterfall(
        [
            function (callback) {
                userDal.checkUserNameExist(userModel.username)
                    .then(function (result) {
                        if (result) {
                            sendFailResponse(res, messages.UsernameNotAvailable);
                        }
                        else {
                            callback(null);
                        }
                    });
            },
            function (callback) {
                userDal.checkEmailExist(userModel.email)
                    .then(function (result) {
                        if (result) {
                            sendFailResponse(res, messages.EmailNotAvailable);
                        }
                        else {
                            callback(null);
                        }
                    });
            },
            function (callback) {

                //userModel.createdDate = new Date();

                parentDal.mobileRegister(userModel)
                    .then(function (user) {
                        if (user) {
                            sendSuccessResponse(res, user, messages.UserRegisteredSuccessfully);
                            //uploadAvatar(files, user._id.toString());
                        } else {
                            sendFailResponse(res, messages.UserRegistrationFailed);
                        }
                    })
                    .catch(function (err) {
                        console.log(err);
                        sendErrorResponse(res);
                    });
            }
        ], function (errs, results) {
            if (errs) {
                console.log(err);
                sendErrorResponse(res);
            } else {
                console.log('Done.');
            }
        });

};

uploadAvatar = function (files, userId) {
    if (userId) {
        if (files.image) {
            if (files.image.name && files.image.name.length > 0) {
                var newFileName = userId + path.extname(files.image.name);
                if (!fs.existsSync(path.join(__dirname, util.format('../../../public/uploads/avatar/%s', userId)))) {
                    fs.mkdirSync(path.join(__dirname, util.format('../../../public/uploads/avatar/%s', userId)));
                }
                var tempPath = files.image.path,
                    targetPath = path.join(__dirname, util.format('../../../public/uploads/avatar/%s/%s', userId, newFileName));

                fs.copy(tempPath, targetPath, function (err) {
                    if (err) { console.log(err); }
                    else { console.log("Upload completed!"); }
                });
            }
        }
    }
}

var formidable = require('formidable');

exports.update = function (req, res) {

    console.log(req.body);
    if (req.body.parentDTO) {

        var userDal = require('../../school.dal/user.dal.js');
        var parentModel = new _parent(req.body.parentDTO);
        var updateProfileModel = new _user(req.body.parentDTO.userRef);
        var avatarUrl;

        parentDal.update(updateProfileModel, parentModel, avatarUrl)
            .then(function (result) {
                if (result) {
                    sendSuccessResponse(res, result);
                } else {
                    sendFailResponse(res);
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
    else {
        sendBadRequestResponse(res);
    }
};

exports.updateProfile = function (req, res) {

    console.log(req.body);
    if (req.body.parentDTO) {

        var userDal = require('../../school.dal/user.dal.js');
        var parentModel = new _parent(req.body.parentDTO);
        var updateProfileModel = new _user(req.body.parentDTO.userRef);
        var avatarUrl;

        parentDal.update(updateProfileModel, parentModel, avatarUrl)
            .then(function (result) {
                if (result) {
                    sendSuccessResponse(res, result);
                } else {
                    sendFailResponse(res);
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
    else {
        sendBadRequestResponse(res);
    }
};

exports.mobileUpdateProfile = function (req, res) {

    var userDal = require('../../school.dal/user.dal.js');

    var userModel = new _user(req.body.data.userDTO);
    var parentModel = new _parent(req.body.data.parentDTO);


    parentDal.mobileUpdateProfile(updateProfileModel, parentModel, avatarUrl)
        .then(function (result) {
            if (result) {
                sendSuccessResponse(res, result);
            } else {
                sendFailResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
};

exports.getUserName = function (req, res) {

    if (req.query.parentId) {
        var parentId = req.query.parentId;

        parentDal.getUserName(parentId)
            .then(function (result) {
                if (result) {
                    sendSuccessResponse(res, result);
                } else {
                    sendFailResponse(res);
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
    else {
        sendBadRequestResponse(res);
    }
}

exports.getParentByUserId = function (req, res) {

    if (req.query.userId) {
        var userId = req.query.userId;

        parentDal.getParentByUserId(userId)
            .then(function (result) {
                if (result) {
                    sendSuccessResponse(res, result);
                } else {
                    sendFailResponse(res);
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
    else {
        sendBadRequestResponse(res);
    }
}

exports.getAllParentInfo = function (req, res) {

    //sendSuccessResponse(res, JSON.stringify(getSubjectList()));

    parentDal.getAllParentDetails()
        .then(function (result) {
            if (result) {
                sendSuccessResponse(res, result);
            } else {
                sendFailResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
}

exports.getParentById = function (req, res) {

    if (req.query.parentId) {
        var parentId = req.query.parentId;

        parentDal.getParentById(parentId)
            .then(function (result) {
                if (result) {
                    sendSuccessResponse(res, result);
                } else {
                    sendFailResponse(res);
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
    else {
        sendBadRequestResponse(res);
    }
}

exports.getEmailByParentId = function (req, res) {

    if (req.query.parentId) {
        var parentId = req.query.parentId;

        parentDal.getEmailByParentId(parentId)
            .then(function (result) {
                if (result) {
                    sendSuccessResponse(res, result);
                } else {
                    sendFailResponse(res);
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
    else {
        sendBadRequestResponse(res);
    }
}

exports.getEmailByStudentId = function (req, res) {

    if (req.query.studentId) {
        var studentId = req.query.studentId;

        parentDal.getEmailByStudentId(studentId)
            .then(function (result) {
                if (result) {
                    sendSuccessResponse(res, result);
                } else {
                    sendFailResponse(res);
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
    else {
        sendBadRequestResponse(res);
    }
}

exports.getParentContactDetailById = function (req, res) {

    var parentId = req.query.parentId;

    parentDal.getParentContactDetailById(parentId)
        .then(function (result) {
            if (result) {
                sendSuccessResponse(res, result);
            } else {
                sendFailResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
}

exports.getParentContactDetailsByStudentId = function (req, res) {

    if (req.query.studentId) {
        var studentId = req.query.studentId;

        parentDal.getParentContactDetailsByStudentId(studentId)
            .then(function (result) {
                if (result) {
                    sendSuccessResponse(res, result);
                } else {
                    sendFailResponse(res);
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
    else {
        sendBadRequestResponse(res);
    }
}

exports.getParentsByAnyCriteria = function (req, res) {

    var searchCriteria = {
        userType: (req.query.userType) ? parseInt(req.query.userType) : appDefaults.UserType,
        searchText: req.query.title,

        page: (req.query.page) ? parseInt(req.query.page) : appDefaults.Page,
        pageSize: (req.query.pageSize) ? parseInt(req.query.pageSize) : appDefaults.PageSize,
        orderBy: req.query.orderBy ? req.query.orderBy : appDefaults.OrderBy
    };

    parentDal.getParentsByAnyCriteria(searchCriteria)
        .then(function (result) {
            if (result) {
                sendSuccessResponse(res, result);
            } else {
                sendFailResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            return sendErrorResponse(res);
        });
}

exports.count = function (req, res) {

    parentDal.count()
        .then(function (result) {
            if (result) {
                sendSuccessResponse(res, result);
            } else {
                sendFailResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
};

exports.getParentProfileInfo = function (req, res) {

    var userId = req.query.userId;

    parentDal.getParentProfileInfo(userId)
        .then(function (result) {
            if (result) {
                sendSuccessResponse(res, result);
            } else {
                sendFailResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
}

exports.getParentAvatar = function (req, res) {

    var parentId = req.query.parentId;

    parentDal.getParentAvatar(parentId)
        .then(function (result) {
            if (result) {
                sendSuccessResponse(res, result);
            } else {
                sendFailResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
}

exports.remove = function (req, res) {

    if (req.body.parentId && req.body.userId) {

        var parentId = req.body.parentId;
        var userId = req.body.userId;

        parentDal.remove(parentId, userId)
            .then(function (result) {
                if (result) {
                    sendSuccessResponse(res, result);
                } else {
                    sendNoContentResponse(res);
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
    else {
        sendBadRequestResponse(res);
    }
}
