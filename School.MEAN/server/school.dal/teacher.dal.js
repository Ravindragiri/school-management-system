﻿var Q = require('q');
var mongoose = require('mongoose')
require('../models/teacher.server.model');
require('../models/user.server.model');
var _teachers = mongoose.model('teacher');
var _user = mongoose.model('user');

var dal = {};

dal.getTeacherById = getTeacherById;
dal.getTeacherContactDetailById = getTeacherContactDetailById;
dal.register = register;
dal.mobileRegistration = mobileRegistration;
dal.getTeachersByAnyCriteria = getTeachersByAnyCriteria;
dal.getTeacherProfileInfo = getTeacherProfileInfo;
dal.getTeacherByUserID = getTeacherByUserID;
dal.add = add;
dal.count = count;
dal.getTeacherById = getTeacherById;
dal.mobileUpdateProfile = mobileUpdateProfile;
dal.getSubjectNames = getSubjectNames;

module.exports = dal;

function getSubjectNames(userID) {

    var deferred = Q.defer();

    _teachers
        .findOne({})
        .populate({
            path: 'userRef',
            match: { _id: userID },
        })
        .populate({ path: 'subjects' })
        .exec(function (err, teacher) {

            if (err) {
                deferred.reject(err);
            }
            if (teacher) {
                deferred.resolve(teacher);
            } else {
                deferred.resolve();
            }
        });
    return deferred.promise;
}

function add(teacherModel) {

    var deferred = Q.defer();

    teacherModel
        .save(function (err, result) {
            if (err) deferred.reject(err);

            if (result) {
                deferred.resolve(result);
            } else {
                deferred.resolve();
            }
        });

    return deferred.promise;
}

function getTeacherById(_id) {

    var deferred = Q.defer();

    _teachers
        .findOne({ '_id': _id })
        .populate({ path: 'userRef' })
        .populate({ path: 'subjects' })
        //.populate({ path: 'followers' })
        //.populate({ path: 'favorites' })
        //.populate({ path: 'messages' })
        //.populate({ path: 'profileVisitors' })
        //.populate({ path: 'sharedProfiles' })
        .exec(function (err, teacher) {

            if (err) {
                deferred.reject(err);
            }
            if (teacher) {
                deferred.resolve(teacher);
            } else {
                deferred.resolve();
            }
        });
    return deferred.promise;
}

function getTeacherContactDetailById(teacherID) {

    var deferred = Q.defer();

    _teachers
        .find({ _iD: teacherID })
        .populate({ path: 'userRef' })
        .exec(function (err, teacher) {

            if (err) {
                deferred.reject(err);
            }
            if (teacher) {
                deferred.resolve(teacher);
            } else {
                deferred.resolve();
            }
        });
    return deferred.promise;
}

var userDal = require('../../server/school.dal/user.dal.js');

function register(teacherModel, userModel) {

    //var userDal = require('./user.dal.js');
    var deferred = Q.defer();

    async.waterfall(
        [
            function (callback) {

                userModel.createdDate = new Date();

                userDal.add(userModel)
                    .then(function (user) {

                        if (user) {

                            //teacherModel.user._id = user._id;
                            teacherModel.userRef = user._id;
                            teacherModel.createdBy = user._id;
                            teacherModel.createdDate = new Date();
                            callback(null, teacherModel);
                        } else {
                            deferred.resolve();
                        }
                    })
                    .catch(function (err) {
                        if (err) deferred.reject(err);
                    });
            },
            function (teacherModel, callback) {
                add(teacherModel)
                    .then(function (teacher) {

                        if (teacher) {
                            deferred.resolve(teacher);
                        } else {
                            deferred.resolve();
                        }
                    })
                    .catch(function (err) {
                        if (err) deferred.reject(err);
                    });
            }
        ], function (errs, results) {
            if (errs) {
                async.each(results, rollback, function () {
                    console.log('Rollback done.');
                    res.status(400).send(errs);
                });
            } else {
                console.log('Done.');
                deferred.resolve(results);
            }
        });

    function rollback(doc, callback) {
        if (!doc) { callback(); }
        else {
            userModel.findByIdAndRemove(doc._id, function (err, doc) {
                console.log('Rolled-back document: ' + doc);
                callback();
            });
        }
    }
    return deferred.promise;
}

function mobileRegistration(mobileteacherRegistrationModel) {

    var deferred = Q.defer();

    var userModel = mobileteacherRegistrationModel.User;

    async.waterfall(
        [
            function (callback) {

                userDal.add(userModel)
                    .then(function (result) {
                        if (result.user._id) {

                            var teacherModel = mobileteacherRegistrationModel.teacher;
                            teacherModel.user._id = result.user._id;

                            callback(null, teacherModel);
                        }
                    })
            },
            function (teacherModel, callback) {
                this.add(teacherModel)
                    .then(function (result) {

                    });
            }
        ], function (errs, results) {
            if (errs) {
                async.each(results, rollback, function () {
                    console.log('Rollback done.');
                    res.status(400).send(errs);
                });
            } else {
                console.log('Done.');
                deferred.resolve(results);
            }
        });

    function rollback(doc, callback) {
        if (!doc) { callback(); }
        else {
            userModel.findByIdAndRemove(doc._id, function (err, doc) {
                console.log('Rolled-back document: ' + doc);
                callback();
            });
        }
    }
    return deferred.promise;
}

function getTeachersByAnyCriteria(searchCriteria, searchText) {

    var deferred = Q.defer();

    _teachers
        //.find({
        //    loc: {
        //        $near: searchCriteria.coords,
        //        $maxDistance: searchCriteria.maxDistance
        //    }
        //})
        .find({
            '$or': [
                { designation: new RegExp(searchText, 'i') },
                { qualification: new RegExp(searchText, 'i') }
            ]
        })
        .populate({ path: 'userRef' })
        .populate({ path: 'subjects' })
        //.populate({ path: 'followers' })
        //.populate({ path: 'favorites' })
        //.populate({ path: 'messages' })
        //.populate({ path: 'profileVisitors' })
        //.populate({ path: 'sharedProfiles' })
        .exec(function (err, teacher) {

            if (err) {
                deferred.reject(err);
            }
            if (teacher) {
                deferred.resolve(teacher);
            } else {
                deferred.resolve();
            }
        });
    return deferred.promise;
}

function getTeacherProfileInfo(userID) {

    var deferred = Q.defer();

    _teachers
        .findOne({})
        .populate({
            path: 'userRef',
            match: { _id: userID },
        })
        .populate({ path: 'subjects' })
        .exec(function (err, teacher) {

            if (err) {
                deferred.reject(err);
            }
            if (teacher) {
                deferred.resolve(teacher);
            } else {
                deferred.resolve();
            }
        });
    return deferred.promise;
}

function getTeacherByUserID(userID) {

    var deferred = Q.defer();

    _teachers
        .findOne({})
        .populate({
            path: 'userRef',
            match: { _id: userID },
        })
        .populate({ path: 'subjects' })
        .exec(function (err, teacher) {

            if (err) {
                deferred.reject(err);
            }
            if (teacher) {
                deferred.resolve(teacher);
            } else {
                deferred.resolve();
            }
        });
    return deferred.promise;
}

function count() {

    var deferred = Q.defer();

    _teachers
        .count({})
        .exec(function (err, count) {

            if (err) {
                deferred.reject(err);
            }
            if (count) {
                deferred.resolve(count);
            } else {
                deferred.resolve();
            }
        });
    return deferred.promise;
}

function getTeacherById(userID) {

    var deferred = Q.defer();

    _teachers
        .findOne({ 'user._id': userID })
        .populate({ path: 'user' })
        .populate({ path: 'subjects' })
        //.populate({ path: 'followers' })
        //.populate({ path: 'favorites' })
        //.populate({ path: 'messages' })
        //.populate({ path: 'profileVisitors' })
        //.populate({ path: 'sharedProfiles' })
        .exec(function (err, teacher) {

            if (err) {
                deferred.reject(err);
            }
            if (teacher) {
                deferred.resolve(teacher);
            } else {
                deferred.resolve();
            }
        });
    return deferred.promise;
}

function mobileUpdateProfile(updateProfileModel, avatarUrl) {

    var deferred = Q.defer();

    var userModel = updateProfileModel.User;

    async.waterfall(
        [
            function (callback) {

                userModel.findOne({ _id: updateProfileModel.userID }, function (err, user) {
                    if (!err) {
                        if (!user) {

                            user.email = updateProfileModel.email ? updateProfileModel.email : user.email;
                            user.phoneNumber = updateProfileModel.phoneNumber ? updateProfileModel.phoneNumber : user.phoneNumber;

                            user.firstName = updateProfileModel.firstName ? updateProfileModel.firstName : user.firstName;
                            user.lastName = updateProfileModel.lastName ? updateProfileModel.lastName : user.lastName;

                            user.address = updateProfileModel.address ? updateProfileModel.address : user.address;
                            user.city = updateProfileModel.city ? updateProfileModel.city : user.city;
                            user.province = updateProfileModel.province ? updateProfileModel.province : user.province;
                            user.pinCode = updateProfileModel.pinCode ? updateProfileModel.pinCode : user.pinCode;
                            user.country = updateProfileModel.country ? updateProfileModel.country : user.country;
                            //user.countryISO2 = updateProfileModel.countryISO2 ? updateProfileModel.countryISO2 : user.countryISO2;
                            user.countryDialCode = updateProfileModel.countryDialCode ? updateProfileModel.countryDialCode : user.countryDialCode;

                            user.updatedBy = updateProfileModel._id;
                            user.updatedDate = new Date();
                        }
                        user.status = request.status;
                        user.save(function (err) {
                            if (result.user._id) {

                                callback(null, updateProfileModel);
                            }
                        });
                    }
                });
            },
            function (teacherID, callback) {
                teacherModel.findOne({ _id: updateProfileModel.teacherID }, function (err, teacher) {
                    if (!err) {
                        if (!teacher) {
                            teacher.avatarUrl = avatarUrl;
                            //TODO
                            //teacher.AvatarMimeType = mimeType;
                            //teacher.AvatarFileName = fileName;

                            teacher.designation = teacherModel.designation ? teacherModel.designation : teacher.designation;
                            teacher.qualification = teacherModel.qualification ? teacherModel.qualification : teacher.qualification;
                            teacher.experience = teacherModel.experience ? teacherModel.experience : teacher.experience;

                            teacher.updatedBy = teacherModel.UserID;
                            teacher.updatedDate = DateTime.Now;
                        }
                        teacher.save(function (err) {
                            if (result.teacher._id) {

                                callback(null, updateProfileModel.teacherID);
                            }
                        });
                    }
                });
            }
        ], function (errs, results) {
            if (errs) {
                async.each(results, rollback, function () {
                    console.log('Rollback done.');
                    res.status(400).send(errs);
                });
            } else {
                console.log('Done.');
                deferred.resolve(results);
            }
        });

    function rollback(doc, callback) {
        if (!doc) { callback(); }
        else {
            userModel.findByIdAndRemove(doc._id, function (err, doc) {
                console.log('Rolled-back document: ' + doc);
                callback();
            });
        }
    }
    return deferred.promise;
}