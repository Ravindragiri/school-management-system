﻿var mongoose = require('mongoose');

var holidaySchema = new mongoose.Schema({
    title: String,
    description:String,
    startDate: Date,
    endDate: Date
});

exports.holidaySchema = holidaySchema;
mongoose.model('holiday', holidaySchema);