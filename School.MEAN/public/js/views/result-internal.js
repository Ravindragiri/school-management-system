﻿var app = angular.module('schoolApp', ['ngTouch']);

app.controller('ngResultInternalController', function ($scope, $http) {

    //$scope.resultDTO = {

    //    class: "Seventh",
    //    section "A",
    //    currentRollNo: "21",

    //    examDate: "JUN-JUL 2018",

    //    firstName: "Ravindragiri",
    //    lastName: "Goswami",

    //    externalTotal: "700",
    //    internalTotal: "200",
    //    grandTotal: "900",
    //    creditTotal: "100",
    //    sgpa: "7.1",
    //    result: "FIRST",
    //    lastDateForVerification: "10/03/2018",

    //    subjects: [
    //        {
    //            text: "Hindi",
    //                marksObtainedTheory: 69,
    //                marksObtainedPractical: 19,
    //                GP: "5",
    //                CR: "20"
    //        },
    //        {
    //            text: "Maths",
    //                marksObtainedTheory: 61,
    //                marksObtainedPractical: 21,
    //                GP: "15",
    //                CR: "21"
    //        }

    //    ]
    //};

    var baseUrl = 'http://localhost:1238';

    $scope.readOne = function () {

        var data = {
            examType: 'First',
            classId: 'First',
            studentId: '5a7cadf559e31f08448a3c1d'
        };
        //  ?examType=First&classId=First&studentId=5a7cadf559e31f08448a3c1d
        $http({
            method: 'GET',
            url: baseUrl + '/api/student/exam/getMarksheet',
            params: data
        }).then(function (response) {
            if (response.data)
                $scope.resultDTO = response.data.data;
        });
    };

    $scope.readOne();
});