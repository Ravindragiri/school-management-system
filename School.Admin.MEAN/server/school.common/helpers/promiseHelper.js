﻿require('../../school.common/helpers/responseHelper');

onPromiseResolve = function (result) {
    if (result) {
        sendSuccessResponse(res, result);
    } else {
        sendNoContentResponse(res);
    }
}

onPromiseReject = function (err) {
    console.log(err);
    sendErrorResponse(res);
}

exports.onPromiseReject = onPromiseReject;
exports.onPromiseReject = onPromiseReject;
