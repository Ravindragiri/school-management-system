﻿var timeTableDal = require('../../school.dal/timeTable.dal.js');
require('../../school.common/helpers/responseHelper');

var mongoose = require('mongoose');
var timeTable = mongoose.model('timeTable');
require('../../school.common/appConstants');

exports.add = function (req, res) {
    var timeTableModel = new timeTable(req.body.timeTableDTO);

    timeTableDal.add(timeTableModel)
        .then(function (result) {
            if (result) {
                sendSuccessResponse(res, result);
            } else {
                sendErrorResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
};

exports.removeById = function (req, res) {
    var timeTableID = req.body.timeTableID;

    timeTableDal.removeById(timeTableID)
        .then(function (result) {
            if (result) {
                sendSuccessResponse(res, result, messages.DeleteSuccess);
            } else {
                sendFailResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
};

exports.getTimeTableById = function (req, res) {
    var timeTableID = req.query.timeTableID;

    timeTableDal.getTimeTableById(timeTableID)
        .then(function (result) {
            if (result) {

                sendSuccessResponse(res, result);
            } else {
                sendFailResponse(res, messages.NotFound);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
}

exports.getAllTimeTables = function (req, res) {
    var timeTableID = req.query.timeTableID;

    timeTableDal.getAllTimeTables(timeTableID)
        .then(function (result) {
            if (result) {

                sendSuccessResponse(res, result);
            } else {
                sendFailResponse(res, messages.NotFound);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
}

exports.updateProfile = function (req, res) {
    var mobileTimeTableDTO = req.body.mobileTimeTableDTO;

    timeTableDal.getTimeTableById(mobileTimeTableDTO.ID)
        .then(function (timeTableDTO) {

            if (timeTableDTO) {
                if (timeTableDTO.ID > 0) {
                    if (timeTableDTO.password == mobileTimeTableDTO.password) {

                        //update profile photo TODO
                        timeTableDTO.firstName = mobileTimeTableDTO.firstName;
                        timeTableDTO.lastName = mobileTimeTableDTO.lastName;
                        timeTableDTO.phoneNumber = mobileTimeTableDTO.phoneNumber;
                        //address TODO

                        timeTableDal.update(timeTableDTO, timeTableID)
                            .then(function (timeTable) {

                                if (timeTable) {
                                    sendSuccessResponse(res, result, messages.UpdateSuccess);
                                }
                                else {
                                    sendFailResponse(res, messages.PasswordIncorrect);
                                }
                            })
                            .catch(function (err) {
                                console.log(err);
                                sendErrorResponse(res);
                            });
                    }
                }
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
};