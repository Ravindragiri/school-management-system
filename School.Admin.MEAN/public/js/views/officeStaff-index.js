﻿/***** angular ngOfficeStaffIndexController starts *****/
var app = angular.module('schoolApp', ['ngTouch', 'ui.grid']);

app
    .directive("datepicker", function () {
        return {
            restrict: "A",
            require: "ngModel",
            link: function (scope, elem, attrs, ngModelCtrl) {
                var updateModel = function (dateText) {
                    scope.$apply(function () {
                        ngModelCtrl.$setViewValue(dateText);
                    });
                };
                var options = {
                    dateFormat: "dd/mm/yy",
                    onSelect: function (dateText) {
                        updateModel(dateText);
                    }
                };
                elem.datepicker(options);
            }
        }
    })
    .controller('ngOfficeStaffIndexController', ['OfficeStaffService', '$scope', '$http',
    function (OfficeStaffService, $scope, $http) {

        $scope.addMode = true;

        $scope.gridOptions = {
            columnDefs: [
                { name: 'firstName', field: 'userRef.firstName', cellTemplate: '<div class="ui-grid-cell-contents">{{ COL_FIELD }}</div>' },
                { name: 'lastName', field: 'userRef.lastName', cellTemplate: '<div class="ui-grid-cell-contents">{{ COL_FIELD }}</div>' },
                { name: 'birthDate', field: 'userRef.birthDate', cellTemplate: '<div class="ui-grid-cell-contents">{{ COL_FIELD }}</div>' },
                { name: 'email', field: 'userRef.email', cellTemplate: '<div class="ui-grid-cell-contents">{{ COL_FIELD }}</div>' },
                { name: 'phoneNumber', field: 'userRef.phoneNumber', cellTemplate: '<div class="ui-grid-cell-contents">{{ COL_FIELD }}</div>' },
                { name: 'edit', cellTemplate: '<button type="button" class="btn btn-xs" ng-click="grid.appScope.editOfficeStaff(row)">Edit</button>' },
                { name: 'delete', cellTemplate: '<button type="button" class="btn btn-xs" ng-click="grid.appScope.deleteOfficeStaff(row)">Delete</button>' }
            ],
            excludeProperties: '__metadata',
        };

        $scope.validateRegisterForm = function () {
            $scope.registrationFormReset = false;
            $scope.showRegisterValidation = function () { return $scope.registrationForm.$invalid; };
        }

        //$scope.reset = function () {
        //    $scope.form = angular.copy(reviewFormFields());
        //    $scope.registrationFormReset = true;
        //    $scope.reviewForm.$setPristine();
        //}

        $scope.insert = function () {

            //$scope.validateRegisterForm();
            $("#registrationForm").validate();

            //if ($scope.registrationForm.$valid) {
            if ($('#registrationForm').valid()) {

                $scope.loading = true;

                var postData = {
                    //__RequestVerificationToken: angular.element("input[name='__RequestVerificationToken']").val(),
                    officeStaffDTO: $scope.officeStaffDTO
                };

                $.ajax({
                    type: "POST",
                    url: 'http://localhost:1238/api/officeStaff/insert',
                    data: postData,
                    dataType: "json",
                    accept: 'application/json',
                    beforeSend: function () {

                    },
                    success: function (response) {

                        if (response != null) {
                            var status = response.statusCode;

                            switch (status) {
                                case 200:
                                    alert("Record has been saved successfully.");
                                    break;
                                case 0:
                                    alert("Your request cannot be processed now due to some technical issue. please try again later.");
                                    break;
                                case -1:
                                case 400:
                                case 401:
                                case 204:
                                    alert(response.message);
                                    break;
                                default:
                            }
                        }
                    },
                    error: function (xhr, status, errorThrown) {
                        alert("An error occurred while processing your request.");
                    }
                });

                //
            }
        };

        $scope.showServerErrorMessages = function (response, errors) {

            $.each(errors, function (index, error) {

                $scope.$apply(function () {

                    $scope.invalidelements[error.Element] = true;
                    $('label[ng-show="invalidelements.' + error.Element + '"]').css('display', 'inline-block');
                    $('.' + error.Element).focus();
                });
            });
        }

        $scope.load = function () {
            OfficeStaffService.readAll().then(function (response) {
                $scope.gridOptions.data = response.data;
                //$scope.gridOptions.data = JSON.parse(response.data);
            });
        }

        $scope.load();

        $scope.editOfficeStaff = function (row) {
            if (row.entity) {
                $scope.officeStaffDTO = row.entity;
                //var index = $scope.gridOptions.data.indexOf(row.entity);
                $scope.addMode = false;
            }
        };

        $scope.deleteOfficeStaff = function (row) {
            if (row.entity) {
                var id = row.entity._id;
                var userId = row.entity.userRef._id;
                OfficeStaffService.delete(id, userId).then(function (response) {
                    if (response != null) {
                        var status = response.statusCode;

                        switch (status) {
                            case 200:
                                alert("Record has been deleted successfully.");
                                break;
                            case 0:
                                alert("Your request cannot be processed now due to some technical issue. please try again later.");
                                break;
                            case -1:
                            case 400:
                            case 401:
                            case 204:
                                alert(response.message);
                                break;
                            default:
                        }
                    }
                });
            }
        };

        $scope.update = function (row) {
            var data = {
                officeStaffDTO: $scope.officeStaffDTO
            }
            OfficeStaffService.update(data).then(function (response) {
                
                if (response != null) {
                    var status = response.statusCode;

                    switch (status) {
                        case 200:
                            $scope.addMode = true;
                            alert("Record has been updated successfully.");
                            break;
                        case 0:
                            alert("Your request cannot be processed now due to some technical issue. please try again later.");
                            break;
                        case -1:
                        case 400:
                        case 401:
                        case 204:
                            alert(response.message);
                            break;
                        default:
                    }
                }
            });
        }

        $scope.cancel = function () {
            $scope.officeStaffDTO = {};
            $scope.addMode = true;
        }

    }]);
/***** angular ngOfficeStaffIndexController ends *****/

(function () {

    angular.module('schoolApp')
        .service('OfficeStaffService', ['$http', OfficeStaffService]);

    function OfficeStaffService($http) {

        var self = this;
        var baseUrl = 'http://localhost:1238/api/officeStaff/getAllOfficeStaffInfo';

        //var objectName = 'products';

        self.readAll = function () {
            return $http({
                method: 'GET',
                url: baseUrl,
                //url: baseUrl + objectName
            }).then(function (response) {
                return response.data;
            });
        };

        self.readOne = function (id) {
            return $http({
                method: 'GET',
                url: baseUrl + objectName + '/' + id
            }).then(function (response) {
                return response.data;
            });
        };

        self.create = function (data) {
            return $http({
                method: 'POST',
                url: baseUrl + objectName,
                data: data,
                params: {
                    returnObject: true
                },
            }).then(function (response) {
                return response.data;
            });
        };

        self.update = function (data) {
            return $http({
                method: 'POST',
                //url: baseUrl + objectName + '/' + id,
                url: 'http://localhost:1238/api/officeStaff/update',
                data: data
            }).then(function (response) {
                return response.data;
            });
        };

        self.delete = function (id, userId) {
            return $http({
                method: 'POST',
                //url: baseUrl + objectName + '/' + id
                url: 'http://localhost:1238/api/officeStaff/remove' + '?id=' + id + '&userId=' + userId
            }).then(function (response) {
                return response.data;
            });
        };

    }
} ());