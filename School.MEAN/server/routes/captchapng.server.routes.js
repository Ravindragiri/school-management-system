﻿module.exports = function (app) {

    var captchapng = require('../controllers/captchapng.server.controller');
    app.get('/public/captcha.png', captchapng.generateCaptcha);
};