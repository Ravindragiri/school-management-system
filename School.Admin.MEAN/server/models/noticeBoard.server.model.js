﻿var mongoose = require('mongoose');

var noticeBoardSchema = new mongoose.Schema({
    title: String,
    notice: String,
    status: Boolean,
    createdBy: {
        type: mongoose.Schema.Types.ObjectId,
    },
    createdDate: {
        type: Date,
    }
});

exports.noticeBoardSchema = noticeBoardSchema;
mongoose.model('noticeBoard', noticeBoardSchema);