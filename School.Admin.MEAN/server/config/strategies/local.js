'use strict';

/**
 * Module dependencies.
 */
var passport = require('passport'),
    LocalStrategy = require('passport-local').Strategy,
    User = require('mongoose').model('user');

var userDal = require('../../school.dal/user.dal.js');

module.exports = function () {
    // Use local strategy
    //Passport recognizes that each application has unique authentication requirements. 
    //Authentication mechanisms, known as strategies, are packaged as individual modules.Applications can choose which strategies to employ, 
    //without creating unnecessary dependencies.
    passport.use(new LocalStrategy({
        usernameField: 'username',
        passwordField: 'password'
    },
        function (username, password, done) {
            userDal.authenticateUser(username, password, function (err, user, reason) {

                if (err) {
                    console.log(err);
                    return done(err);
                }

                // login was successful if we have a user
                if (user) {
                    // handle login success
                    return done(null, user);
                }

                // otherwise we can determine why we failed
                //var reasons = User.failedLogin;
                switch (reason) {
                    case 0:
                    case 1:
                        // note: these cases are usually treated the same - don't tell
                        // the user *why* the login failed, only that it did
                        return done(null, false, {
                            message: 'Unknown user or invalid password'
                        });
                }
            });

            //User.findOne({
            //	username: username
            //}, function(err, user) {
            //	if (err) {
            //		return done(err);
            //	}
            //	if (!user) {
            //		return done(null, false, {
            //			message: 'Unknown user or invalid password'
            //		});
            //	}
            //    user.comparePassword(password, function (err, isMatch) {
            //        if (err) return done(err);

            //        if (!isMatch) {

            //            return done(null, false, {
            //                message: 'Unknown user or invalid password'
            //            });
            //        }

            //        return done(null, user);
            //    }); 
            //});
        }
    ));
};