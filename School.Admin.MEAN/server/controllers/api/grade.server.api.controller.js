﻿var gradeDal = require('../../../server/school.dal/grade.dal.js');
require('../../school.common/helpers/responseHelper');

var mongoose = require('mongoose'),
    //fs = require('fs-extra'),
    //util = require("util"),
//path = require("path");

var _grade = mongoose.model('grade');
var _user = mongoose.model('user');

exports.addGrade = function (req, res) {

    var gradeModel = req.body.GradeModel;

    gradeDal.add(gradeModel)
        .then(function (result) {
            if (result) {
                sendSuccessResponse(res, result);
            } else {
                sendErrorResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
}