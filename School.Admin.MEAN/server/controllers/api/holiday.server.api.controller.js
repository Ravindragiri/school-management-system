﻿var holidayDal = require('../../../server/school.dal/holiday.dal.js');
require('../../school.common/helpers/responseHelper');

var mongoose = require('mongoose'),
    moment = require("moment");
//fs = require('fs-extra'),
//util = require("util"),
//path = require("path");

var _holiday = mongoose.model('holiday');
var _user = mongoose.model('user');

function parseClientDate(candidateDTO) {
    if (candidateDTO.startDate && candidateDTO.endDate) {
        candidateDTO.startDate = moment(candidateDTO.startDate, 'DD/MM/YYYY').toDate();
        candidateDTO.endDate = moment(candidateDTO.endDate, 'DD/MM/YYYY').toDate();
    }
    return candidateDTO;
}

function formatServerDate(candidateDTO) {
    if (candidateDTO.startDate && candidateDTO.endDate) {
        candidateDTO.startDate = moment(candidateDTO.startDate).format("DD/MM/YYYY");
        candidateDTO.endDate = moment(candidateDTO.endDate).format("DD/MM/YYYY");
    }
    return candidateDTO;
}

exports.add = function (req, res) {

    if (req.body.holidayDTO) {

        var holidayDTO = req.body.holidayDTO;
        holidayDTO = parseClientDate(holidayDTO);
        var holidayModel = new _holiday(holidayDTO);
        holidayDal.add(holidayModel)
            .then(function (result) {
                if (result) {
                    sendSuccessResponse(res, result);
                } else {
                    sendErrorResponse(res);
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
    else {
        sendBadRequestResponse(res);
    }
}

exports.update = function (req, res) {

    if (req.body.holidayDTO) {

        var holidayDTO = req.body.holidayDTO;
        holidayDTO = parseClientDate(holidayDTO);
        var loggedInUserId;
        holidayDal.update(holidayDTO, loggedInUserId)
            .then(function (result) {

                if (result) {
                    sendSuccessResponse(res, result, messages.UpdateSuccess);
                }
                else {
                    sendFailResponse(res, messages.PasswordIncorrect);
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
    else {
        sendBadRequestResponse(res);
    }
};

exports.removeById = function (req, res) {
    if (req.query.id) {
        var holidayId = req.query.id;

        holidayDal.deleteById(holidayId)
            .then(function (result) {
                if (result) {
                    sendSuccessResponse(res, result, messages.DeleteSuccess);
                } else {
                    sendFailResponse(res);
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
    else {
        sendBadRequestResponse(res);
    }
};

exports.getById = function (req, res) {

    if (req.query.holidayId) {
        var holidayId = req.query.holidayId;

        holidayDal.getById(holidayId)
            .then(function (result) {
                if (result) {
                    result = formatServerDate(result);
                    sendSuccessResponse(res, result);
                } else {
                    sendErrorResponse(res);
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
    else {
        sendBadRequestResponse(res);
    }
}

exports.getAll = function (req, res) {

    holidayDal.getAll()
        .then(function (result) {
            if (result) {

                for (i = 0; i < result.length; i++) {
                    result[i] = formatServerDate(result[i]);
                }
                sendSuccessResponse(res, result);
            } else {
                sendErrorResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
}