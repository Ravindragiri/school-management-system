﻿var officeStaffDal = require('../../../server/school.dal/officeStaff.dal.js');
require('../../school.common/helpers/responseHelper');

var mongoose = require('mongoose'),
    //fs = require('fs-extra'),
    //util = require("util"),
    async = require('async');
//path = require("path");

var _officeStaff = mongoose.model('officeStaff');
var _user = mongoose.model('user');

exports.register = function (req, res) {

    var userDal = require('../../school.dal/user.dal.js');

    var officeStaffModel = new _officeStaff(req.body.data.officeStaffDTO);
    var userModel = new _user(req.body.data.userDTO);

    async.waterfall(
        [
            function (callback) {
                userDal.checkUserNameExist(userModel.username)
                    .then(function (result) {
                        if (result) {
                            sendFailResponse(res, messages.UsernameNotAvailable);
                        }
                        else {
                            callback(null);
                        }
                    });
            },
            function (callback) {
                userDal.checkEmailExist(userModel.email)
                    .then(function (result) {
                        if (result) {
                            sendFailResponse(res, messages.EmailNotAvailable);
                        }
                        else {
                            callback(null);
                        }
                    });
            },
            function (callback) {

                //userModel.loc.type = "Point";
                //userModel.loc.coordinates = [parseFloat(officeStaffRegistration.data.userDTO.longitude),
                //    parseFloat(officeStaffRegistration.data.userDTO.latitude)];
                //if (officeStaffModel.vehicles) {
                //    userModel.vehicles = JSON.parse(officeStaffModel.vehicles);
                //    var ObjectId = mongoose.Types.ObjectId;
                //    for (var i = 0; i < userModel.vehicles.length; i++) {
                //        userModel.vehicles[i]._id = new ObjectId;
                //        userModel.vehicles[i].createdDate = new Date();
                //    }
                //}
                //userModel.createdDate = new Date();

                officeStaffDal.register(officeStaffModel, userModel)
                    .then(function (user) {
                        if (user) {
                            sendSuccessResponse(res, user, messages.UserRegisteredSuccessfully);
                            //uploadAvatar(files, user._id.toString());
                        } else {
                            sendFailResponse(res, messages.UserRegistrationFailed);
                        }
                    })
                    .catch(function (err) {
                        console.log(err);
                        sendErrorResponse(res);
                    });
            }
        ], function (errs, results) {
            if (errs) {
                console.log(err);
                sendErrorResponse(res);
            } else {
                console.log('Done.');
            }
        });

};

exports.mobileRegister = function (req, res) {

    var userDal = require('../../school.dal/user.dal.js');

    var officeStaffModel = new _officeStaff(req.body.data.officeStaffDTO);
    var userModel = new _user(req.body.data.userDTO);

    async.waterfall(
        [
            function (callback) {
                userDal.checkUserNameExist(userModel.username)
                    .then(function (result) {
                        if (result) {
                            sendFailResponse(res, messages.UsernameNotAvailable);
                        }
                        else {
                            callback(null);
                        }
                    });
            },
            function (callback) {
                userDal.checkEmailExist(userModel.email)
                    .then(function (result) {
                        if (result) {
                            sendFailResponse(res, messages.EmailNotAvailable);
                        }
                        else {
                            callback(null);
                        }
                    });
            },
            function (callback) {

                //userModel.loc.type = "Point";
                //userModel.loc.coordinates = [parseFloat(officeStaffRegistration.data.userDTO.longitude),
                //    parseFloat(officeStaffRegistration.data.userDTO.latitude)];
                //if (officeStaffRegistration.data.userDTO.vehicleList) {
                //    userModel.vehicles = JSON.parse(officeStaffRegistration.data.userDTO.vehicleList);
                //    var ObjectId = mongoose.Types.ObjectId;
                //    for (var i = 0; i < userModel.vehicles.length; i++) {
                //        userModel.vehicles[i]._id = new ObjectId;
                //        userModel.vehicles[i].createdDate = new Date();
                //    }
                //}
                //userModel.createdDate = new Date();

                officeStaffDal.mobileRegister(userModel)
                    .then(function (user) {
                        if (user) {
                            sendSuccessResponse(res, user, messages.UserRegisteredSuccessfully);
                            //uploadAvatar(files, user._id.toString());
                        } else {
                            sendFailResponse(res, messages.UserRegistrationFailed);
                        }
                    })
                    .catch(function (err) {
                        console.log(err);
                        sendErrorResponse(res);
                    });
            }
        ], function (errs, results) {
            if (errs) {
                console.log(err);
                sendErrorResponse(res);
            } else {
                console.log('Done.');
            }
        });

};

uploadAvatar = function (files, userID) {
    if (userID) {
        if (files.image) {
            if (files.image.name && files.image.name.length > 0) {
                var newFileName = userID + path.extname(files.image.name);
                if (!fs.existsSync(path.join(__dirname, util.format('../../../public/uploads/avatar/%s', userID)))) {
                    fs.mkdirSync(path.join(__dirname, util.format('../../../public/uploads/avatar/%s', userID)));
                }
                var tempPath = files.image.path,
                    targetPath = path.join(__dirname, util.format('../../../public/uploads/avatar/%s/%s', userID, newFileName));

                fs.copy(tempPath, targetPath, function (err) {
                    if (err) { console.log(err); }
                    else { console.log("Upload completed!"); }
                });
            }
        }
    }
}

var formidable = require('formidable');

exports.mobileUpdateProfile = function (req, res) {

    var userDal = require('../../school.dal/user.dal.js');

    var userModel = new _user(req.body.data.userDTO);
    var officeStaffModel = new _officeStaff(req.body.data.officeStaffDTO);
    

    officeStaffDal.mobileUpdateProfile(updateProfileModel, officeStaffModel, avatarUrl)
        .then(function (result) {
            if (result) {
                sendSuccessResponse(res, result);
            } else {
                sendFailResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
};

exports.getOfficeStaffByUserId = function (req, res) {

    var userId = req.query.userId;

    officeStaffDal.getOfficeStaffByUserId(userId)
        .then(function (result) {
            if (result) {
                sendSuccessResponse(res, result);
            } else {
                sendFailResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
}

exports.getOfficeStaffById = function (req, res) {

    var officeStaffID = req.query.officeStaffID;

    officeStaffDal.getOfficeStaffById(officeStaffID)
        .then(function (result) {
            if (result) {
                sendSuccessResponse(res, result);
            } else {
                sendFailResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
}

exports.getOfficeStaffContactDetailById = function (req, res) {

    var officeStaffID = req.query.officeStaffID;

    officeStaffDal.getOfficeStaffContactDetailById(officeStaffID)
        .then(function (result) {
            if (result) {
                sendSuccessResponse(res, result);
            } else {
                sendFailResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
}

exports.getOfficeStaffsByAnyCriteria = function (req, res) {

    var searchCriteria = {
        //coords: [parseFloat(req.query.longitude), parseFloat(req.query.latitude)],
        //distance: (req.query.distance) ? parseFloat(req.query.distance) : appDefaults.Distance,
        userType: (req.query.userType) ? parseInt(req.query.userType) : appDefaults.UserType,
        searchText: req.query.title,

        page: (req.query.page) ? parseInt(req.query.page) : appDefaults.Page,
        pageSize: (req.query.pageSize) ? parseInt(req.query.pageSize) : appDefaults.PageSize,
        orderBy: req.query.orderBy ? req.query.orderBy : appDefaults.OrderBy
    };

    officeStaffDal.getOfficeStaffsByAnyCriteria(searchCriteria)
        .then(function (result) {
            if (result) {
                sendSuccessResponse(res, result);
            } else {
                sendFailResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            return sendErrorResponse(res);
        });
}

exports.count = function (req, res) {

    officeStaffDal.count()
        .then(function (result) {
            if (result) {
                sendSuccessResponse(res, result);
            } else {
                sendFailResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
};

exports.getOfficeStaffProfileInfo = function (req, res) {

    var userID = req.query.userID;

    officeStaffDal.getOfficeStaffProfileInfo(userID)
        .then(function (result) {
            if (result) {
                sendSuccessResponse(res, result);
            } else {
                sendFailResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
}

exports.getOfficeStaffAvatar = function (req, res) {

    var officeStaffID = req.query.officeStaffID;

    officeStaffDal.getOfficeStaffAvatar(officeStaffID)
        .then(function (result) {
            if (result) {
                sendSuccessResponse(res, result);
            } else {
                sendFailResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
}
