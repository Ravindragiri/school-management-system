﻿/***** angular ngExamMarksController starts *****/
var app = angular.module('schoolApp', ['ngTouch', 'ui.grid', 'ui.grid.expandable', 'ui.grid.selection', 'ui.grid.pinning']);

app.controller('ngExamMarksController', ['ExamMarksService', '$scope', '$http',
    function (ExamMarksService, $scope, $http) {

        $scope.addMode = true;
        $scope.showSearchResult = false;

        $scope.gridOptions = {
            excludeProperties: '__metadata',
            enableRowSelection: true,
            expandableRowTemplate: '<div ui-grid="row.entity.subGridOptions" style="height:140px;"></div>',
            expandableRowHeight: 150,
            expandableRowScope: {
                subGridVariable: 'subGridScopeVariable'
            }
        };

        $scope.gridOptions.columnDefs = [
            { name: 'Student Name', field: "firstName", cellTemplate: '<div class="ui-grid-cell-contents">{{ COL_FIELD }}</div>' },
            { name: 'Class', field: "exams.class", cellTemplate: '<div class="ui-grid-cell-contents">{{ COL_FIELD }}</div>' },
            { name: 'Section', field: "exams.section", cellTemplate: '<div class="ui-grid-cell-contents">{{ COL_FIELD }}</div>' },
            { name: 'Result', field: "exams.result", cellTemplate: '<div class="ui-grid-cell-contents">{{ COL_FIELD }}</div>' },
            { name: 'Percentage', field: "exams.percentage", cellTemplate: '<div class="ui-grid-cell-contents">{{ COL_FIELD }}</div>' },

            { name: 'edit', cellTemplate: '<button type="button" class="btn btn-xs" ng-click="grid.appScope.editExamMark(row)">Edit</button>' },
            { name: 'delete', cellTemplate: '<button type="button" class="btn btn-xs" ng-click="grid.appScope.deleteExamMark(row)">Delete</button>' }
        ];

        $scope.validateRegisterForm = function () {
            $scope.registrationFormReset = false;
            $scope.showRegisterValidation = function () { return $scope.registrationForm.$invalid; };
        }

        //$scope.reset = function () {
        //    $scope.form = angular.copy(reviewFormFields());
        //    $scope.registrationFormReset = true;
        //    $scope.reviewForm.$setPristine();
        //}

        $scope.register = function () {

            //$scope.validateRegisterForm();
            $("#registrationForm").validate();

            //if ($scope.registrationForm.$valid) {
            if ($('#registrationForm').valid()) {

                $scope.loading = true;

                var postData = {
                    __RequestVerificationToken: angular.element("input[name='__RequestVerificationToken']").val(),
                    UserModel: $scope.UserModel,
                    ServiceProviderModel: $scope.ServiceProviderModel
                };

                $.ajax({
                    type: "POST",
                    url: 'http://localhost:1238/api/student/exam/addMarksheet',
                    data: postData,
                    dataType: "json",
                    accept: 'application/json',
                    beforeSend: function () {

                    },
                    success: function (response) {

                        if (response != null) {
                            var status = response.statusCode;

                            switch (status) {
                                case 1:
                                    //var objTravel = JSON.parse(response.data);
                                    break;
                                case 0:
                                    alert(response.message);
                                    break;
                                case -10:
                                    $scope.showServerErrorMessages(response, response.serverErrors);
                                    break;
                                case -500:
                                    alert(response.message)
                                    //window.location.href = errorPage;
                                    break;
                                default:
                            }
                        }
                    },
                    error: function (xhr, status, errorThrown) {
                        alert("An error occurred while processing your request.");
                    }
                });

                //
            }
        };

        $scope.showServerErrorMessages = function (response, errors) {

            $.each(errors, function (index, error) {

                $scope.$apply(function () {

                    $scope.invalidelements[error.Element] = true;
                    $('label[ng-show="invalidelements.' + error.Element + '"]').css('display', 'inline-block');
                    $('.' + error.Element).focus();
                });
            });
        }

        $scope.load = function () {
            var data = {
                examType: 'First'
            };

            ExamMarksService.readAll(data).then(function (response) {
                var data = response.data;
                for (i = 0; i < data.length; i++) {

                    data[i].subGridOptions = {
                        columnDefs: [
                            { name: 'text', field: 'text' },
                            { name: 'marksObtainedTheory', field: 'marksObtainedTheory' },
                            { name: 'maxMarksTheory', field: 'maxMarksTheory' },
                            { name: 'marksObtainedPractical', field: 'marksObtainedPractical' },
                            { name: 'maxMarksPractical', field: 'maxMarksPractical' }
                        ],
                        data: data[i].exams.subjects
                    };
                }
                $scope.gridOptions.data = data;
            });
        }

        $scope.load();

        $scope.editExamMark = function (row) {
            if (row.entity) {
                $scope.studentDTO = row.entity;
                if ($scope.studentDTO.exams && (!$scope.studentDTO.exams.subjects || $scope.studentDTO.exams.subjects.length == 0)) {
                    $scope.studentDTO.exams.subjects = row.entity.currentSubjects;
                }
                $scope.addMode = false;
                $scope.showSearchResult = true;
            }
        };

        $scope.deleteExamMark = function (row) {
            if (row.entity) {

                var data = {
                    studentId: row.entity._id,
                    examId: row.entity.exams._id
                }

                ExamMarksService.delete(data).then(function (response) {

                    if (response != null) {
                        var status = response.statusCode;

                        switch (status) {
                            case 200:
                                $scope.gridOptions.data = response.data;
                                var index = $scope.gridOptions.data.indexOf(row.entity);
                                $scope.gridOptions.data.splice(index, 1);
                                alert("Record has been deleted successfully.");
                                break;
                            case 0:
                                alert("Your request cannot be processed now due to some technical issue. please try again later.");
                                break;
                            case -1:
                            case 400:
                            case 401:
                            case 204:
                                alert(response.message);
                                break;
                            default:
                        }
                    }
                });
            }
        };

        $scope.onchange = function () {
            if ($scope.standard && $scope.section) {
                var data = {
                    class: $scope.standard,
                    section: $scope.section
                };

                ExamMarksService.getStudentsByClassAndSection(data).then(function (response) {
                    $scope.students = response.data;

                });
            }
        };

        $scope.search = function () {

            var data = {
                studentId: $scope.studentDTO._id,
                examType: 'First'
            };

            ExamMarksService.getMarksheet(data).then(function (response) {
                var data = response.data;
                for (i = 0; i < data.length; i++) {

                    data[i].subGridOptions = {
                        columnDefs: [
                            { name: 'code', field: 'code' },
                            { name: 'marksObtainedTheory', field: 'marksObtainedTheory' },
                            { name: 'maxMarksTheory', field: 'maxMarksTheory' },
                            { name: 'marksObtainedPractical', field: 'marksObtainedPractical' },
                            { name: 'maxMarksPractical', field: 'maxMarksPractical' }
                        ],
                        data: data[i].exams.subjects
                    };
                }
                $scope.gridOptions.data = data;
                $scope.showSearchResult = true;
            });
        };

        $scope.insert = function () {

            var data = {
                studentId: $scope.studentId,
                studentDTO: $scope.studentDTO
            };

            ExamMarksService.create(data).then(function (response) {
                
                if (response != null) {
                    var status = response.statusCode;

                    switch (status) {
                        case 200:
                            $scope.studentDTO = {};
                            $scope.load();
                            alert("Record has been saved successfully.");
                            break;
                        case 0:
                            alert("Your request cannot be processed now due to some technical issue. please try again later.");
                            break;
                        case -1:
                        case 400:
                        case 401:
                        case 204:
                            alert(response.message);
                            break;
                        default:
                    }
                }
            });
        };

        $scope.update = function () {
            var data = {
                studentDTO: $scope.studentDTO
            }
            ExamMarksService.update(data).then(function (response) {
                
                if (response != null) {
                    var status = response.statusCode;

                    switch (status) {
                        case 200:
                            $scope.addMode = true;
                            alert("Record has been updated successfully.");
                            break;
                        case 0:
                            alert("Your request cannot be processed now due to some technical issue. please try again later.");
                            break;
                        case -1:
                        case 400:
                        case 401:
                        case 204:
                            alert(response.message);
                            break;
                        default:
                    }
                }
            });
        }

        $scope.cancel = function () {
            $scope.studentDTO = {};
            $scope.addMode = true;
            $scope.showSearchResult = false;
        }
    }]);
/***** angular ngExamMarksController ends *****/

(function () {

    angular.module('schoolApp')
        .service('ExamMarksService', ['$http', ExamMarksService]);

    function ExamMarksService($http) {

        var self = this;
        var baseUrl = 'http://localhost:1238/api/student/exam/getAllStudentMarks';

        //var objectName = 'products';

        self.readAll = function (data) {
            return $http({
                method: 'GET',
                url: baseUrl,
                //url: baseUrl + objectName
                params: data
            }).then(function (response) {
                return response.data;
            });
        };

        self.getMarksheet = function (data) {
            return $http({
                method: 'GET',
                url: 'http://localhost:1238/api/student/exam/getMarksheet',
                //url: baseUrl + objectName
                params: data
            }).then(function (response) {
                return response.data;
            });
        };

        self.getStudentsByClassAndSection = function (data) {
            return $http({
                method: 'GET',
                url: 'http://localhost:1238/api/student/getAllStudentsByClassAndSection',
                params: data
            }).then(function (response) {
                return response.data;
            });
        };

        self.readOne = function (id) {
            return $http({
                method: 'GET',
                url: baseUrl + objectName + '/' + id
            }).then(function (response) {
                return response.data;
            });
        };

        self.create = function (data) {
            return $http({
                method: 'POST',
                url: 'http://localhost:1238/api/student/exam/addMarksheet',
                data: data,
                params: {
                    returnObject: true
                },
            }).then(function (response) {
                return response.data;
            });
        };

        self.update = function (data) {
            return $http({
                method: 'POST',
                //url: baseUrl + objectName + '/' + id,
                url: 'http://localhost:1238/api/student/exam/update',
                data: data
            }).then(function (response) {
                return response.data;
            });
        };

        self.delete = function (data) {
            return $http({
                method: 'POST',
                url: 'http://localhost:1238/api/student/exam/remove',
                params: data
            }).then(function (response) {
                return response.data;
            });
        };
    }
} ());