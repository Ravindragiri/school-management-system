﻿var config = require('../config/config');
require('../school.common/appConstants');

exports.index = function (req, res) {
    res.render('parent/index', {
        pTitle: 'Parent | School'
    });
};

exports.renderRegister = function (req, res) {
    res.render('parent/register', {
        pTitle: 'Parent Registration | School'
    });
};

exports.renderMyProfile = function (req, res) {
    res.render('parent/myProfile', {
        pTitle: 'Parent My Profile | School'
    });
};