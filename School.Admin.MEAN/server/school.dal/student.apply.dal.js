﻿var Q = require('q');
var mongoose = require('mongoose');
var _ = require('lodash');

require('../models/student.server.model');
require('../models/user.server.model');

var userDal = require('../../server/school.dal/user.dal.js');

var _student = mongoose.model('student');
var _user = mongoose.model('user');

require('../../server/school.common/helpers/deferredHelper');

var dal = {};

dal.applyRollNo = applyRollNo;
dal.applyRegistrationNo = applyRegistrationNo;
dal.getStudentListSortByName = getStudentListSortByName;

module.exports = dal;

function getStudentListSortByName(classId, sectionId) {

    var deferred = Q.defer();

    _student
        .aggregate(
        { $match: { currentClass: classId, currentSection: sectionId } },
        { $lookup: { from: "users", localField: "userRef", foreignField: "_id", as: "user" } },
        { $unwind: "$user" },
        {
            $sort: {
                'user.lastName': 1,
                'user.firstName': 1
            }
        },
        {
            $project: {
                "_id": 1,
                "currentClass": 1,
                "currentSection": 1,
                "currentRollNo": 1,

                "userRef": {
                    _id: "$user._id",
                    firstName: "$user.firstName",
                    lastName: "$user.lastName",
                }
            }
        })
        .exec(function (err, result) {
            execHelper(deferred, err, result);
        });
    return deferred.promise;
}

function update(studentUpdateCondition, studentUpdateSet) {

    var deferred = Q.defer();

    _student.findOneAndUpdate(studentUpdateCondition, { $set: studentUpdateSet }, { new: true }, function (err, student) {
        if (err) {
            console.log(err);
            deferred.reject(err);
        }
        if (student) {
            student = _.pick(student.toObject(), ['_id', 'userRef', 'subjects']);
            deferred.resolve(student);
        } else {
            deferred.resolve();
        }
    });
    return deferred.promise;
}

function applyRollNo(studentId, rollNo) {

    //var deferred = Q.defer();

    var studentUpdateCondition = { $and: [] };
    studentUpdateCondition.$and.push({
        _id: studentId
    });

    var studentUpdateSet = {
        currentRollNo: rollNo,
        //updatedBy: loggedInUserId,
        updatedDate: new Date()
    };

    update(studentUpdateCondition, studentUpdateSet);
        //.then(function (student) {

        //    if (student) {
        //        deferred.resolve(student);
        //    } else {
        //        deferred.resolve();
        //    }
        //})
        //.catch(function (err) {
        //    if (err) {
        //        console.log(err);
        //        deferred.reject(err);
        //    }
        //});

    //return deferred.promise;
}

function applyRegistrationNo(studentId, classId) {

    var sortedStudentList = getStudentListSortByName(classId, sectionId);

    var studentUpdateCondition = { $and: [] };
    studentUpdateCondition.$and.push({
        _id: studentId
    });

    var studentUpdateSet = {
        registrationNo: registrationNo,
        //updatedBy: loggedInUserId,
        updatedDate: new Date()
    };

    update(studentUpdateCondition, studentUpdateSet);
}