﻿module.exports = function (app) {

    var classTimeTableController = require('../controllers/class.TimeTable.server.controller');
    app.get('/class/TimeTable/index', classTimeTableController.index);
    app.get('/class/TimeTable', classTimeTableController.index);

    var classTimeTableApiController = require('../controllers/api/class.timeTable.server.api.controller');
    //get
    app.get('/api/class/timeTable/getByClassAndSection', classTimeTableApiController.getClassTimeTableByClassAndSection);
    app.get('/api/class/timeTable/getAll', classTimeTableApiController.getAllTimeTables);
    app.get('/api/class/timeTable/getTimeTableById', classTimeTableApiController.getTimeTableById);
    app.get('/api/class/timeTable/getTimeTableByDay', classTimeTableApiController.getTimeTableByDay);
    //post
    app.post('/api/class/timeTable/removeById', classTimeTableApiController.removeById);
    app.post('/api/class/timeTable/add', classTimeTableApiController.add);
    app.post('/api/class/timeTable/update', classTimeTableApiController.update);
    
}