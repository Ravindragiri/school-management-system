﻿var teacherDal = require('../../../server/school.dal/teacher.dal.js');
require('../../school.common/helpers/responseHelper');

var mongoose = require('mongoose'),
    //fs = require('fs-extra'),
    //util = require("util"),
    async = require('async');
//path = require("path");

var _teacher = mongoose.model('teacher');
var _user = mongoose.model('user');

exports.register = function (req, res) {
    var userDal = require('../../school.dal/user.dal.js');

    var teacherModel = new _teacher(req.body.data.teacherDTO);
    var userModel = new _user(req.body.data.userDTO);

    async.waterfall(
        [
            function (callback) {
                userDal.checkUserNameExist(userModel.username)
                    .then(function (result) {
                        if (result) {
                            sendFailResponse(res, messages.UsernameNotAvailable);
                        }
                        else {
                            callback(null);
                        }
                    });
            },
            function (callback) {
                userDal.checkEmailExist(userModel.email)
                    .then(function (result) {
                        if (result) {
                            sendFailResponse(res, messages.EmailNotAvailable);
                        }
                        else {
                            callback(null);
                        }
                    });
            },
            function (callback) {

                //userModel.loc.type = "Point";
                //userModel.loc.coordinates = [parseFloat(teacherRegistration.data.userDTO.longitude),
                //    parseFloat(teacherRegistration.data.userDTO.latitude)];
                //if (teacherModel.vehicles) {
                //    userModel.vehicles = JSON.parse(teacherModel.vehicles);
                //    var ObjectId = mongoose.Types.ObjectId;
                //    for (var i = 0; i < userModel.vehicles.length; i++) {
                //        userModel.vehicles[i]._id = new ObjectId;
                //        userModel.vehicles[i].createdDate = new Date();
                //    }
                //}
                //userModel.createdDate = new Date();

                teacherDal.register(teacherModel, userModel)
                    .then(function (user) {
                        if (user) {
                            sendSuccessResponse(res, user, messages.UserRegisteredSuccessfully);
                            //uploadAvatar(files, user._id.toString());
                        } else {
                            sendFailResponse(res, messages.UserRegistrationFailed);
                        }
                    })
                    .catch(function (err) {
                        console.log(err);
                        sendErrorResponse(res);
                    });
            }
        ], function (errs, results) {
            if (errs) {
                console.log(err);
                sendErrorResponse(res);
            } else {
                console.log('Done.');
            }
        });

};

exports.mobileRegister = function (req, res) {
    var userDal = require('../../school.dal/user.dal.js');

    var teacherModel = new _teacher(req.body.data.teacherDTO);
    var userModel = new _user(req.body.data.userDTO);

    async.waterfall(
        [
            function (callback) {
                userDal.checkUserNameExist(userModel.username)
                    .then(function (result) {
                        if (result) {
                            sendFailResponse(res, messages.UsernameNotAvailable);
                        }
                        else {
                            callback(null);
                        }
                    });
            },
            function (callback) {
                userDal.checkEmailExist(userModel.email)
                    .then(function (result) {
                        if (result) {
                            sendFailResponse(res, messages.EmailNotAvailable);
                        }
                        else {
                            callback(null);
                        }
                    });
            },
            function (callback) {

                //userModel.loc.type = "Point";
                //userModel.loc.coordinates = [parseFloat(teacherRegistration.data.userDTO.longitude),
                //    parseFloat(teacherRegistration.data.userDTO.latitude)];
                //if (teacherRegistration.data.userDTO.vehicleList) {
                //    userModel.vehicles = JSON.parse(teacherRegistration.data.userDTO.vehicleList);
                //    var ObjectId = mongoose.Types.ObjectId;
                //    for (var i = 0; i < userModel.vehicles.length; i++) {
                //        userModel.vehicles[i]._id = new ObjectId;
                //        userModel.vehicles[i].createdDate = new Date();
                //    }
                //}
                //userModel.createdDate = new Date();

                teacherDal.mobileRegister(userModel)
                    .then(function (user) {
                        if (user) {
                            sendSuccessResponse(res, user, messages.UserRegisteredSuccessfully);
                            //uploadAvatar(files, user._id.toString());
                        } else {
                            sendFailResponse(res, messages.UserRegistrationFailed);
                        }
                    })
                    .catch(function (err) {
                        console.log(err);
                        sendErrorResponse(res);
                    });
            }
        ], function (errs, results) {
            if (errs) {
                console.log(err);
                sendErrorResponse(res);
            } else {
                console.log('Done.');
            }
        });

};

uploadAvatar = function (files, userID) {
    if (userID) {
        if (files.image) {
            if (files.image.name && files.image.name.length > 0) {
                var newFileName = userID + path.extname(files.image.name);
                if (!fs.existsSync(path.join(__dirname, util.format('../../../public/uploads/avatar/%s', userID)))) {
                    fs.mkdirSync(path.join(__dirname, util.format('../../../public/uploads/avatar/%s', userID)));
                }
                var tempPath = files.image.path,
                    targetPath = path.join(__dirname, util.format('../../../public/uploads/avatar/%s/%s', userID, newFileName));

                fs.copy(tempPath, targetPath, function (err) {
                    if (err) { console.log(err); }
                    else { console.log("Upload completed!"); }
                });
            }
        }
    }
}

var formidable = require('formidable');

exports.mobileUpdateProfile = function (req, res) {
    var userDal = require('../../school.dal/user.dal.js');

    var userModel = new _user(req.body.data.userDTO);
    var teacherModel = new _teacher(req.body.data.teacherDTO);


    teacherDal.mobileUpdateProfile(updateProfileModel, teacherModel, avatarUrl)
        .then(function (result) {
            if (result) {
                sendSuccessResponse(res, result);
            } else {
                sendFailResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
};

exports.getTeacherByUserId = function (req, res) {

    var userId = req.query.userId;

    teacherDal.getTeacherByUserId(userId)
        .then(function (result) {
            if (result) {
                sendSuccessResponse(res, result);
            } else {
                sendFailResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
}

exports.getTeacherById = function (req, res) {

    var teacherID = req.query.teacherID;

    teacherDal.getTeacherById(teacherID)
        .then(function (result) {
            if (result) {
                sendSuccessResponse(res, result);
            } else {
                sendFailResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
}

exports.getTeacherContactDetailById = function (req, res) {

    var teacherID = req.query.teacherID;

    teacherDal.getTeacherContactDetailById(teacherID)
        .then(function (result) {
            if (result) {
                sendSuccessResponse(res, result);
            } else {
                sendFailResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
}

exports.count = function (req, res) {

    teacherDal.count()
        .then(function (result) {
            if (result) {
                sendSuccessResponse(res, result);
            } else {
                sendFailResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
};

exports.getTeacherProfileInfo = function (req, res) {

    var userID = req.query.userID;

    teacherDal.getTeacherProfileInfo(userID)
        .then(function (result) {
            if (result) {
                sendSuccessResponse(res, result);
            } else {
                sendFailResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
}

exports.getTeachersByAnyCriteria = function (req, res) {

    var searchCriteria = {
        //coords: [parseFloat(req.query.longitude), parseFloat(req.query.latitude)],
        //distance: (req.query.distance) ? parseFloat(req.query.distance) : appDefaults.Distance,
        userType: (req.query.userType) ? parseInt(req.query.userType) : appDefaults.UserType,
        searchText: req.query.title,

        page: (req.query.page) ? parseInt(req.query.page) : appDefaults.Page,
        pageSize: (req.query.pageSize) ? parseInt(req.query.pageSize) : appDefaults.PageSize,
        orderBy: req.query.orderBy ? req.query.orderBy : appDefaults.OrderBy
    };

    teacherDal.getTeachersByAnyCriteria(searchCriteria)
        .then(function (result) {
            if (result) {
                sendSuccessResponse(res, result);
            } else {
                sendFailResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            return sendErrorResponse(res);
        });
}

exports.count = function (req, res) {

    teacherDal.count()
        .then(function (result) {
            if (result) {
                sendSuccessResponse(res, result);
            } else {
                sendFailResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
};

exports.getTeacherAvatar = function (req, res) {

    var teacherID = req.query.teacherID;

    teacherDal.getTeacherAvatar(teacherID)
        .then(function (result) {
            if (result) {
                sendSuccessResponse(res, result);
            } else {
                sendFailResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
}
