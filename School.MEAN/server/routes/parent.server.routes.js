﻿module.exports = function (app) {

    var parentController = require('../controllers/parent.server.controller');
    app.get('/parent/index', parentController.index);
    app.get('/parent', parentController.index);
    app.get('/parent/register', parentController.renderRegister);
    app.get('/parent/myProfile', parentController.renderMyProfile);
};