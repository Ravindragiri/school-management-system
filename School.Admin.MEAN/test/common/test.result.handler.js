﻿resultWithError = function (res) {
    
    res.should.have.status(400);
    res.should.be.json;
    
    res.body.should.have.property('status');
    res.body.status.should.equal('Error');
    
    res.body.should.have.property('statusCode');
    res.body.statusCode.should.equal(-1);
    
    res.body.should.have.property('data');
    res.body.data.should.be.equal('');
}

resultFailedWithMessage = function (res, message) {
    
    res.should.have.status(200);
    res.should.be.json;
    
    res.body.should.have.property('status');
    res.body.status.should.equal('Error');
    
    res.body.should.have.property('statusCode');
    res.body.statusCode.should.equal(-1);
    
    res.body.should.have.property('data');
    res.body.data.should.be.equal('');

    res.body.should.have.property('message');
    res.body.message.should.be.equal(message);
}

resultNotFound = function (res) {
    
    res.should.have.status(404);
    res.body.should.deep.equal(JSON.parse('{"status":"NotFound","data":"","message":"Not Found!","statusCode":404}'));
};

resultNotFoundWithoutMessage = function (res) {
    
    res.should.have.status(404);
    res.body.should.deep.equal(JSON.parse('{"data":"", "status":"NotFound", "statusCode":404}'));
};

resultSuccess = function (res) {
    
    res.should.have.status(200);
    res.should.be.json;
    
    res.body.should.have.property('status');
    res.body.status.should.equal('Success');
    
    res.body.should.have.property('statusCode');
    res.body.statusCode.should.equal(1);
};

resultEmpty = function (res) {
    
    res.should.have.status(200);
    res.should.be.json;
    
    res.body.should.have.property('status');
    res.body.status.should.equal('Success');
    
    res.body.should.have.property('statusCode');
    res.body.statusCode.should.equal(1);
    
    res.body.should.have.property('data');
    //res.body.data.should.be.a('array');
    res.body.data.should.deep.equal('');
};

resultReceiverArray = function (res) {
    
    res.should.have.status(200);
    res.should.be.json;
    
    res.body.should.have.property('status');
    res.body.status.should.equal('Success');
    
    res.body.should.have.property('statusCode');
    res.body.statusCode.should.equal(1);
    
    res.body.should.have.property('data');
    res.body.data.should.be.a('array');
};

resultEmptyArray = function (res) {
    
    res.should.have.status(200);
    res.should.be.json;
    
    res.body.should.have.property('status');
    res.body.status.should.equal('Success');
    
    res.body.should.have.property('statusCode');
    res.body.statusCode.should.equal(1);
    
    res.body.should.have.property('data');
    res.body.data.should.be.a('array');
    res.body.data.should.eql([]);
    res.body.data.should.deep.equal([]);
};

resultWithBadRequest = function (res) {
    
    res.should.have.status(400);
    res.body.should.deep.equal(JSON.parse('{"status":"Error","data":"","message":"Bad request , try again please !","statusCode":-1}'));
}

resultWithDatauserType0 = function (res) {
    
    res.should.have.status(200);
    res.should.be.json;
    
    res.body.should.have.property('status');
    res.body.status.should.equal('Success');
    
    res.body.should.have.property('statusCode');
    res.body.statusCode.should.equal(1);
    
    res.body.should.have.property('data');
    res.body.data.should.be.a('array');
    //res.body.data.should.eql([]);
    //res.body.data.should.deep.equal([]);

    /*
    res.body.data[0].should.have.property('_id');
    res.body.data[0].should.have.property('email');
    res.body.data[0].should.have.property('firstName');
    res.body.data[0].should.have.property('phoneNumber');
    res.body.data[0].should.have.property('simCountryISO2');
    res.body.data[0].should.have.property('address');
    //res.body.data[0].should.have.property('city');
    //res.body.data[0].should.have.property('province');
    //res.body.data[0].should.have.property('pinCode');
    
    res.body.data[0]._id.should.equal('578cbc7bf08730c865e9d0fd');
    res.body.data[0].email.should.equal('test@gmail.com');
    res.body.data[0].firstName.should.equal('bob');
    res.body.data[0].phoneNumber.should.equal('89562345');
    res.body.data[0].simCountryISO2.should.equal('sg');
    res.body.data[0].address.should.equal('but');
    //res.body.data[0].city.should.equal('3');
    //res.body.data[0].province.should.equal('3');
    //res.body.data[0].pinCode.should.equal('3');
    */
}

resultWithData = function (res) {
    
    res.should.have.status(200);
    res.should.be.json;
    
    res.body.should.have.property('status');
    res.body.status.should.equal('Success');
    
    res.body.should.have.property('statusCode');
    res.body.statusCode.should.equal(1);
    
    res.body.should.have.property('data');
    res.body.data.should.be.a('array');
    /*
    res.body.data[0].should.have.property('_id');
    res.body.data[0].should.have.property('email');
    res.body.data[0].should.have.property('firstName');
    res.body.data[0].should.have.property('phoneNumber');
    res.body.data[0].should.have.property('simCountryISO2');
    res.body.data[0].should.have.property('address');
    res.body.data[0].should.have.property('city');
    res.body.data[0].should.have.property('province');
    res.body.data[0].should.have.property('pinCode');
    
    res.body.data[0]._id.should.equal('57a0b3d3d18e5fc725041c18');
    res.body.data[0].email.should.equal('rg.goswami@gmail.com');
    res.body.data[0].firstName.should.equal('donor1');
    res.body.data[0].phoneNumber.should.equal('+65 8657 7196');
    res.body.data[0].simCountryISO2.should.equal('in');
    res.body.data[0].address.should.equal('2');
    res.body.data[0].city.should.equal('2');
    res.body.data[0].province.should.equal('2');
    res.body.data[0].pinCode.should.equal('2');
    */ 
}

singleResultWithData = function (res) {
    
    res.should.have.status(200);
    res.should.be.json;
    
    res.body.should.have.property('status');
    res.body.status.should.equal('Success');
    
    res.body.should.have.property('statusCode');
    res.body.statusCode.should.equal(200);
    
    res.body.should.have.property('data');
    
    //res.body.data.should.have.property('_id');
    //res.body.data.should.have.property('email');
    //res.body.data.should.have.property('firstName');
    //res.body.data.should.have.property('phoneNumber');
    //res.body.data.should.have.property('simCountryISO2');
    //res.body.data.should.have.property('address');
    //res.body.data.should.have.property('city');
    //res.body.data.should.have.property('province');
    //res.body.data.should.have.property('pinCode');
    
    //res.body.data._id.should.equal('57a0b3d3d18e5fc725041c18');
    //res.body.data.email.should.equal('rg.goswami@gmail.com');
    //res.body.data.firstName.should.equal('donor1');
    //res.body.data.phoneNumber.should.equal('+65 8657 7196');
    //res.body.data.simCountryISO2.should.equal('in');
    //res.body.data.address.should.equal('2');
    //res.body.data.city.should.equal('2');
    //res.body.data.province.should.equal('2');
    //res.body.data.pinCode.should.equal('2');
}

getOrganDetailResultWithData = function (res) {
    
    res.should.have.status(200);
    res.should.be.json;
    
    res.body.should.have.property('status');
    res.body.status.should.equal('Success');
    
    res.body.should.have.property('statusCode');
    res.body.statusCode.should.equal(1);
    
    res.body.should.have.property('data');
    //res.body.data.should.be.a('array');
    
    //res.body.data.should.have.property('_id');
    //res.body.data.should.have.property('email');
    //res.body.data.should.have.property('firstName');
    //res.body.data.should.have.property('phoneNumber');
    //res.body.data.should.have.property('simCountryISO2');
    //res.body.data.should.have.property('address');
    //res.body.data.should.have.property('city');
    //res.body.data.should.have.property('province');
    //res.body.data.should.have.property('pinCode');
    
    //res.body.data._id.should.equal('57a0b3d3d18e5fc725041c18');
    //res.body.data.email.should.equal('rg.goswami@gmail.com');
    //res.body.data.firstName.should.equal('donor1');
    //res.body.data.phoneNumber.should.equal('+65 8657 7196');
    //res.body.data.simCountryISO2.should.equal('in');
    //res.body.data.address.should.equal('2');
    //res.body.data.city.should.equal('2');
    //res.body.data.province.should.equal('2');
    //res.body.data.pinCode.should.equal('2');
}

getDonorContactDetailByIdResultWithData = function (res) {
    
    res.should.have.status(200);
    res.should.be.json;
    
    res.body.should.have.property('status');
    res.body.status.should.equal('Success');
    
    res.body.should.have.property('statusCode');
    res.body.statusCode.should.equal(1);
    
    res.body.should.have.property('data');
    //res.body.data.should.be.a('array');
    /*
    res.body.data.should.have.property('_id');
    res.body.data.should.have.property('email');
    res.body.data.should.have.property('firstName');
    res.body.data.should.have.property('phoneNumber');
    //res.body.data.should.have.property('simCountryISO2');
    res.body.data.should.have.property('address');
    res.body.data.should.have.property('city');
    res.body.data.should.have.property('province');
    res.body.data.should.have.property('pinCode');
    
    res.body.data._id.should.equal('57a0b3d3d18e5fc725041c18');
    res.body.data.email.should.equal('rg.goswami@gmail.com');
    res.body.data.firstName.should.equal('donor1');
    res.body.data.phoneNumber.should.equal('+65 8657 7196');
    //res.body.data.simCountryISO2.should.equal('in');
    res.body.data.address.should.equal('2');
    res.body.data.city.should.equal('2');
    res.body.data.province.should.equal('2');
    res.body.data.pinCode.should.equal('2');
    */ 
}

resultMyMessage = function (res) {
    
    res.should.have.status(200);
    res.should.be.json;
    
    res.body.should.have.property('status');
    res.body.status.should.equal('Success');
    
    res.body.should.have.property('statusCode');
    res.body.statusCode.should.equal(1);
    
    res.body.should.have.property('data');
    //res.body.data.should.be.a('array');
    
    //res.body.data[0].should.have.property('_id');
    //res.body.data[0].should.have.property('createdDate');
    //res.body.data[0].should.have.property('toUserID');
    //res.body.data[0].should.have.property('fromEmail');
    //res.body.data[0].should.have.property('fromName');
    //res.body.data[0].should.have.property('fromPhoneNumber');
    //res.body.data[0].should.have.property('subject');
    //res.body.data[0].should.have.property('message');
    
    //res.body.data[0]._id.should.equal('5790e958356c345432235db7');
    //res.body.data[0].createdDate.should.equal('2016-07-21T15:25:16.237Z');
    //res.body.data[0].toUserID.should.equal('57a0b3d3d18e5fc725041c18');
    //res.body.data[0].fromEmail.should.equal('rg.goswami@gmail.com');
    //res.body.data[0].fromName.should.equal('goswami');
    //res.body.data[0].fromPhoneNumber.should.equal('+65 8657 7196');
    //res.body.data[0].subject.should.equal('Hi');
    //res.body.data[0].message.should.equal('Hi message');
}

getRecipientContactDetailByIdResultWithData = function (res) {
    
    res.should.have.status(200);
    res.should.be.json;
    
    res.body.should.have.property('status');
    res.body.status.should.equal('Success');
    
    res.body.should.have.property('statusCode');
    res.body.statusCode.should.equal(1);
    
    res.body.should.have.property('data');
    //res.body.data.should.be.a('array');
    /*
    res.body.data.should.have.property('_id');
    res.body.data.should.have.property('email');
    res.body.data.should.have.property('firstName');
    res.body.data.should.have.property('phoneNumber');
    //res.body.data.should.have.property('simCountryISO2');
    res.body.data.should.have.property('address');
    res.body.data.should.have.property('city');
    res.body.data.should.have.property('province');
    res.body.data.should.have.property('pinCode');
    
    res.body.data._id.should.equal('57a0b3d3d18e5fc725041c18');
    res.body.data.email.should.equal('rg.goswami@gmail.com');
    res.body.data.firstName.should.equal('donor1');
    res.body.data.phoneNumber.should.equal('+65 8657 7196');
    //res.body.data.simCountryISO2.should.equal('in');
    res.body.data.address.should.equal('2');
    res.body.data.city.should.equal('2');
    res.body.data.province.should.equal('2');
    res.body.data.pinCode.should.equal('2');
    */ 
}

getOfficeStaffByIdResultWithData = function (res) {

    res.should.have.status(200);
    res.should.be.json;

    res.body.should.have.property('status');
    res.body.status.should.equal('Success');

    res.body.should.have.property('statusCode');
    res.body.statusCode.should.equal(1);

    res.body.should.have.property('data');
    //res.body.data.should.be.a('array');
    /*
    res.body.data.should.have.property('_id');
    res.body.data.should.have.property('email');
    res.body.data.should.have.property('firstName');
    res.body.data.should.have.property('phoneNumber');
    //res.body.data.should.have.property('simCountryISO2');
    res.body.data.should.have.property('address');
    res.body.data.should.have.property('city');
    res.body.data.should.have.property('province');
    res.body.data.should.have.property('pinCode');
    
    res.body.data._id.should.equal('57a0b3d3d18e5fc725041c18');
    res.body.data.email.should.equal('rg.goswami@gmail.com');
    res.body.data.firstName.should.equal('donor1');
    res.body.data.phoneNumber.should.equal('+65 8657 7196');
    //res.body.data.simCountryISO2.should.equal('in');
    res.body.data.address.should.equal('2');
    res.body.data.city.should.equal('2');
    res.body.data.province.should.equal('2');
    res.body.data.pinCode.should.equal('2');
    */
}

getOfficeStaffContactDetailByIdResultWithData = function (res) {

    res.should.have.status(200);
    res.should.be.json;

    res.body.should.have.property('status');
    res.body.status.should.equal('Success');

    res.body.should.have.property('statusCode');
    res.body.statusCode.should.equal(1);

    res.body.should.have.property('data');
    //res.body.data.should.be.a('array');
    /*
    res.body.data.should.have.property('_id');
    res.body.data.should.have.property('email');
    res.body.data.should.have.property('firstName');
    res.body.data.should.have.property('phoneNumber');
    //res.body.data.should.have.property('simCountryISO2');
    res.body.data.should.have.property('address');
    res.body.data.should.have.property('city');
    res.body.data.should.have.property('province');
    res.body.data.should.have.property('pinCode');
    
    res.body.data._id.should.equal('57a0b3d3d18e5fc725041c18');
    res.body.data.email.should.equal('rg.goswami@gmail.com');
    res.body.data.firstName.should.equal('donor1');
    res.body.data.phoneNumber.should.equal('+65 8657 7196');
    //res.body.data.simCountryISO2.should.equal('in');
    res.body.data.address.should.equal('2');
    res.body.data.city.should.equal('2');
    res.body.data.province.should.equal('2');
    res.body.data.pinCode.should.equal('2');
    */
}

exports.resultWithError = resultWithError;
exports.resultEmpty = resultEmpty;
exports.resultEmptyArray = resultEmptyArray;
exports.resultReceiverArray = resultReceiverArray;
exports.resultWithBadRequest = resultWithBadRequest;
exports.resultWithDatauserType0 = resultWithDatauserType0;
exports.resultWithData = resultWithData;
exports.resultMyMessage = resultMyMessage;

exports.getOrganDetailResultWithData = getOrganDetailResultWithData;
exports.getDonorContactDetailByIdResultWithData = getDonorContactDetailByIdResultWithData;
exports.getRecipientContactDetailByIdResultWithData = getRecipientContactDetailByIdResultWithData;
exports.getOfficeStaffByIdResultWithData = getOfficeStaffByIdResultWithData;
exports.getOfficeStaffContactDetailByIdResultWithData = getOfficeStaffContactDetailByIdResultWithData;
exports.resultNotFoundWithoutMessage = resultNotFoundWithoutMessage;
exports.resultFailedWithMessage = resultFailedWithMessage;
