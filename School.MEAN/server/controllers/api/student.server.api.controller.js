﻿var studentDal = require('../../school.dal/student.dal.js');
require('../../school.common/appConstants');
require('../../school.common/helpers/responseHelper');

var mongoose = require('mongoose'),
    fs = require('fs-extra'),
    util = require("util"),
    async = require('async'),
    path = require("path");

var student = mongoose.model('student');
var _user = mongoose.model('user');

exports.searchStudents = function (req, res) {

    var pagingSortingParams = req.param.pagingSortingParams;
    var searchCriteria = req.query.searchCriteria;

    studentDal.searchStudents(pagingSortingParams, searchCriteria)
        .then(function (result) {
            if (result) {
                sendSuccessResponse(res, result);
            } else {
                sendFailResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
}

exports.register = function (req, res) {
    var studentRegistration = req.body.data;
    //var studentModel = new student(studentRegistration.studentDTO);
    var userModel = new _user(studentRegistration.userDTO);

    studentDal.register(userModel)
        .then(function (result) {
            if (result) {
                sendSuccessResponse(res, result);
            } else {
                sendFailResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
};

exports.mobileRegister = function (req, res) {

    var userDal = require('../../school.dal/user.dal.js');

    // parse a file upload 
    var form = new formidable.IncomingForm();
    form.parse(req, function (err, fields, files) {

        if (fields.data) {

            var studentRegistration = JSON.parse(fields.data);
            //var studentModel = new student(studentRegistration.studentDTO);
            var userModel = new _user(studentRegistration.data.userDTO);
            async.waterfall(
                [
                    function (callback) {
                        userDal.checkUserNameExist(userModel.username)
                            .then(function (result) {
                                if (result) {
                                    sendFailResponse(res, messages.UsernameNotAvailable);
                                }
                                else {
                                    callback(null);
                                }
                            });
                    },
                    function (callback) {
                        userDal.checkEmailExist(userModel.email)
                            .then(function (result) {
                                if (result) {
                                    sendFailResponse(res, messages.EmailNotAvailable);
                                }
                                else {
                                    callback(null);
                                }
                            });
                    },
                    function (callback) {

                        //userModel.loc.type = "Point";
                        //userModel.loc.coordinates = [parseFloat(studentRegistration.data.userDTO.longitude),
                        //    parseFloat(studentRegistration.data.userDTO.latitude)];
                        //if (studentRegistration.data.userDTO.organList) {
                        //    userModel.organs = JSON.parse(studentRegistration.data.userDTO.organList);
                        //    var ObjectId = mongoose.Types.ObjectId;
                        //    for (var i = 0; i < userModel.organs.length; i++) {
                        //        userModel.organs[i]._id = new ObjectId;
                        //        userModel.organs[i].createdDate = new Date();
                        //    }
                        //}
                        userModel.createdDate = new Date();

                        studentDal.mobileRegister(userModel)
                            .then(function (user) {
                                if (user) {
                                    sendSuccessResponse(res, user, messages.UserRegisteredSuccessfully);
                                    uploadAvatar(files, user._id.toString());
                                } else {
                                    sendFailResponse(res, messages.UserRegistrationFailed);
                                }
                            })
                            .catch(function (err) {
                                console.log(err);
                                sendErrorResponse(res);
                            });
                    }
                ], function (errs, results) {
                    if (errs) {
                        console.log(err);
                        sendErrorResponse(res);
                    } else {
                        console.log('Done.');
                    }
                });
        } else {
            sendBadRequestResponse(res);
        }
    });
};

uploadAvatar = function (files, userID) {
    if (userID) {
        if (files.image) {
            if (files.image.name && files.image.name.length > 0) {
                var newFileName = userID + path.extname(files.image.name);
                if (!fs.existsSync(path.join(__dirname, util.format('../../../public/uploads/avatar/%s', userID)))) {
                    fs.mkdirSync(path.join(__dirname, util.format('../../../public/uploads/avatar/%s', userID)));
                }
                var tempPath = files.image.path,
                    targetPath = path.join(__dirname, util.format('../../../public/uploads/avatar/%s/%s', userID, newFileName));

                fs.copy(tempPath, targetPath, function (err) {
                    if (err) { console.log(err); }
                    else { console.log("Upload completed!"); }
                });
            }
        }
    }
}

var formidable = require('formidable');

exports.mobileUpdateProfile = function (req, res) {
    // parse a file upload 
    var form = new formidable.IncomingForm();

    form.parse(req, function (err, fields, files) {

        if (fields.data && JSON.parse(fields.data)) {
            var studentRegistration = JSON.parse(fields.data);
            //
            var userModel = new _user(studentRegistration.data.userDTO);
            userModel.loc.type = "Point";
            userModel.loc.coordinates = [parseFloat(studentRegistration.data.userDTO.longitude), parseFloat(studentRegistration.data.userDTO.latitude)];

            studentDal.mobileUpdateProfile(userModel)
                .then(function (user) {
                    if (user) {
                        sendSuccessResponse(res, user, messages.UserRegisteredSuccessfully);
                        uploadAvatar(files, user._id.toString());
                    } else {
                        sendFailResponse(res, messages.UserRegistrationFailed);
                    }
                })
                .catch(function (err) {
                    console.log(err);
                    sendErrorResponse(res);
                });
        }
        else {
            sendBadRequestResponse(res);
        }
    });
};

exports.getStudentById = function (req, res) {

    var studentID = req.query.studentID;

    studentDal.getStudentById(studentID)
        .then(function (result) {
            if (result) {
                sendSuccessResponse(res, result);
            } else {
                sendFailResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
}

exports.getStudentContactDetailById = function (req, res) {

    if (req.query.userID) {
        var userID = req.query.userID;

        studentDal.getStudentContactDetailById(userID)
            .then(function (result) {
                if (result) {
                    sendSuccessResponse(res, result, messages.ReadSuccess);
                } else {
                    sendFailResponse(res);
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
    else {
        sendBadRequestResponse(res);
    }
}

exports.getStudentsByAnyCriteria = function (req, res) {

    var searchCriteria = {
        //coords: [parseFloat(req.query.longitude), parseFloat(req.query.latitude)],
        //distance: (req.query.distance) ? parseFloat(req.query.distance) : appDefaults.Distance,
        userType: (req.query.userType) ? parseInt(req.query.userType) : appDefaults.UserType,
        searchText: req.query.title,

        page: (req.query.page) ? parseInt(req.query.page) : appDefaults.Page,
        pageSize: (req.query.pageSize) ? parseInt(req.query.pageSize) : appDefaults.PageSize,
        orderBy: req.query.orderBy ? req.query.orderBy : appDefaults.OrderBy
    };

    studentDal.getStudentsByAnyCriteria(searchCriteria)
        .then(function (result) {
            if (result) {
                sendSuccessResponse(res, result);
            } else {
                sendFailResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            return sendErrorResponse(res);
        });
}

exports.count = function (req, res) {

    studentDal.count()
        .then(function (result) {
            if (result) {
                sendSuccessResponse(res, result);
            } else {
                sendFailResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
};

exports.getStudentProfileInfo = function (req, res) {

    var userID = req.query.userID;

    studentDal.getStudentProfileInfo(userID)
        .then(function (result) {
            if (result) {
                sendSuccessResponse(res, result);
            } else {
                sendFailResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
}

exports.getStudentAvatar = function (req, res) {

    var studentID = req.query.studentID;

    studentDal.getStudentAvatar(studentID)
        .then(function (result) {
            if (result) {
                sendSuccessResponse(res, result);
            } else {
                sendFailResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
}
