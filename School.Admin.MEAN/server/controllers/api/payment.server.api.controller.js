﻿var studentDal = require('../../../server/school.dal/student.dal.js');
require('../../school.common/helpers/responseHelper');

var mongoose = require('mongoose'),
    //fs = require('fs-extra'),
    //util = require("util"),
    async = require('async');
//path = require("path");

var _student = mongoose.model('student');
var _user = mongoose.model('user');

exports.addPayment = function (req, res) {

    //var userDal = require('./user.dal.js');
    var deferred = Q.defer();

    var paymentModel = new _payment(req.body.data.paymentDTO);
    var paymentModel = new _paymentModel(req.body.data.paymentDTO);

    async.waterfall(
        [
            function (callback) {

                paymentModel.createdDate = new Date();

                studentDal.addPayment(paymentModel)
                    .then(function (payment) {

                        if (payment) {

                            paymentModel.paymentRef = payment._id;
                            //paymentModel.createdBy = user._id;
                            //paymentModel.createdDate = new Date();
                            callback(null, paymentModel);
                        } else {
                            deferred.resolve();
                        }
                    })
                    .catch(function (err) {
                        if (err) deferred.reject(err);
                    });
            },
            function (paymentModel, callback) {
                studentDal.addPayment(paymentModel)
                    .then(function (payment) {

                        if (payment) {
                            deferred.resolve(payment);
                        } else {
                            deferred.resolve();
                        }
                    })
                    .catch(function (err) {
                        if (err) deferred.reject(err);
                    });
            }
        ], function (errs, results) {
            if (errs) {
                async.each(results, rollback, function () {
                    console.log('Rollback done.');
                    res.status(400).send(errs);
                });
            } else {
                console.log('Done.');
                deferred.resolve(results);
            }
        });

    function rollback(doc, callback) {
        if (!doc) { callback(); }
        else {
            paymentModel.findByIdAndRemove(doc._id, function (err, doc) {
                console.log('Rolled-back payment document: ' + doc);
                callback();
            });
        }
    }
    return deferred.promise;
}

exports.updatePayment = function (req, res) {

    if (req.body.paymentDTO && req.body.paymentId) {

        var paymentDTO = req.body.paymentDTO;
        var paymentId = req.body.paymentId;
        var userId = req.body.userId;

        paymentDal.updatePayment(paymentDTO, paymentId, userId)
            .then(function (result) {
                if (result) {
                    sendSuccessResponse(res, result);
                } else {
                    sendFailResponse(res);
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
    else {
        sendBadRequestResponse(res);
    }
};

exports.removePayment = function (req, res) {

    if (req.query.paymentId) {

        var paymentId = req.body.paymentId;

        paymentDal.removePayment(paymentId)
            .then(function (result) {
                if (result) {
                    sendSuccessResponse(res, result);
                } else {
                    sendFailResponse(res);
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
    else {
        sendBadRequestResponse(res);
    }
};