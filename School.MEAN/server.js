﻿//Once all of the prerequisite software is setup, we can create our Express server:
//express School.MEAN
//cd School.MEAN
//npm install

var mongoose = require('mongoose'),
    //Mongoose is object modeling for our MongoDB database.
    debug = require('debug')('School'),
    express = require('express'),
    expressValidator = require('express-validator'),
    ejsmate = require('ejs-mate'),
    http = require('http'),
    path = require('path'),
    //Passport just provides the mechanism to handle authentication leaving the onus of implementing session-handling ourselves and 
    //for that we will be using express- session. 
    passport = require('passport'),
    session = require('express-session'),
    bodyParser = require('body-parser'),
    flash = require('connect-flash'),
    helmet = require('helmet'),
    config = require('./server/config/config');
//npm install passport --save
//Adding the --save command will automatically add each dependency to our package.json file. 

var ObjectId = mongoose.Types.ObjectId;

var app = express();

app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(expressValidator());

app.use(session({
    saveUninitialized: true,
    resave: true,
    secret: 'OurSuperSecretCookieSecret',

    cookieName: 'session',
    duration: 30 * 60 * 1000,
    activeDuration: 5 * 60 * 1000
}));

app.use(flash());   // use connect-flash for flash messages stored in session
app.use(passport.initialize());
app.use(passport.session());
//app.use(helmet())

require('./server/models/all.server.model');

// Initialize Passport
var initPassport = require('./server/config/strategies/passport-init');
initPassport(passport);

app.set('layout', 'shared/_layout.html');

app.use(bodyParser.json());

app.engine('html', ejsmate);

var views = [
    path.join(__dirname, '/server/views')
];

app.set('views', views);
app.set('view engine', 'html');

app.use(function (req, res, next) {

    res.locals.footerText = config.footerText;
    res.locals.schoolAddress = config.schoolAddress;
    res.locals.schoolContactNo = config.schoolContactNo;
    res.locals.schoolEmail = config.schoolEmail;
    res.locals.projectName = config.projectName;
    res.locals.projectTag = config.projectTag;

    res.locals.bloodGroups = bloodGroups;
    res.locals.examTypes = examTypes;
    res.locals.provinces = indiaStates;
    res.locals.designations = designations;
    res.locals.qualifications = qualifications;
    res.locals.standards = classes;

    res.locals.isAuthenticated = req.isAuthenticated();
    res.locals.user = req.user;

    next();
});

require('./server/routes/captchapng.server.routes.js')(app);
require('./server/routes/api.server.routes.js')(app);
require('./server/routes/all.server.routes.js')(app);

app.use('/public', express.static(__dirname + '/public'));


// Create HTTP Server
//mongoose.Promise = global.Promise;
//mongoose.connect(config.connectionUrl, {
//    keepAlive: true,
//    reconnectTries: Number.MnAX_VALUE,
//    useMongoClient: true
//});
mongoose.connect(config.connectionUrl);
var db = mongoose.connection;


db.on('error', console.error.bind(console, "connection error"));
db.once('open', function () {
    console.log("School database is open...");
});

module.exports = app;