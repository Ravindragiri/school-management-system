﻿var Q = require('q');
var _ = require('lodash');
var mongoose = require('mongoose'),
    async = require('async');

require('../school.common/appConstants');

require('../models/teacher.server.model');
require('../models/user.server.model');

var _teachers = mongoose.model('teacher');
var _user = mongoose.model('user');

require('../../server/school.common/helpers/deferredHelper');

var dal = {};

dal.getTeacherById = getTeacherById;
dal.getAllTeacherInfo = getAllTeacherInfo;
dal.getTeacherContactDetailById = getTeacherContactDetailById;
dal.insert = insert;
dal.mobileRegistration = mobileRegistration;
dal.getTeachersByAnyCriteria = getTeachersByAnyCriteria;
dal.getTeacherProfileInfo = getTeacherProfileInfo;
dal.getTeacherByUserId = getTeacherByUserId;
dal.add = add;
dal.update = update;
dal.count = count;
dal.getTeacherById = getTeacherById;
dal.mobileUpdateProfile = mobileUpdateProfile;
dal.getSubjectNames = getSubjectNames;
dal.remove = remove;
dal.updateProfile = updateProfile;

module.exports = dal;

function getSubjectNames(userId) {

    var deferred = Q.defer();

    _teachers
        .findOne({})
        .populate({
            path: 'userRef',
            match: { _id: userId },
        })
        .populate({ path: 'subjects' })
        .exec(function (err, result) {
            execHelper(deferred, err, result);
        });
    return deferred.promise;
}

function add(teacherModel) {

    var deferred = Q.defer();

    teacherModel
        .save(function (err, result) {
            if (err) deferred.reject(err);

            if (result) {
                deferred.resolve(result);
            } else {
                deferred.resolve();
            }
        });

    return deferred.promise;
}

function update(teacherUpdateCondition, teacherUpdateSet) {

    var deferred = Q.defer();

    _teachers.findOneAndUpdate(teacherUpdateCondition, { $set: teacherUpdateSet }, { new: true }, function (err, teacher) {
        if (err) {
            deferred.reject(err);
        }
        if (teacher) {
            teacher = _.pick(teacher.toObject(), ['_id', 'userRef', 'subjects']);
            deferred.resolve(teacher);
        } else {
            deferred.resolve();
        }
    });
    return deferred.promise;
}

function getAllTeacherInfo() {

    var deferred = Q.defer();

    _teachers
        .find({})
        .populate({ path: 'userRef' })
        .populate({ path: 'subjects' })
        //.populate({ path: 'followers' })
        //.populate({ path: 'favorites' })
        //.populate({ path: 'messages' })
        //.populate({ path: 'profileVisitors' })
        //.populate({ path: 'sharedProfiles' })
        .lean()
        .exec(function (err, result) {
            execHelper(deferred, err, result);
        });
    return deferred.promise;
}

function getTeacherById(_id) {

    var deferred = Q.defer();

    _teachers
        .findOne({ '_id': _id })
        .populate({ path: 'userRef' })
        .populate({ path: 'subjects' })
        //.populate({ path: 'followers' })
        //.populate({ path: 'favorites' })
        //.populate({ path: 'messages' })
        //.populate({ path: 'profileVisitors' })
        //.populate({ path: 'sharedProfiles' })
        .exec(function (err, result) {
            execHelper(deferred, err, result);
        });
    return deferred.promise;
}

function getTeacherContactDetailById(teacherId) {

    var deferred = Q.defer();

    _teachers
        .find({ _iD: teacherId })
        .populate({ path: 'userRef' })
        .exec(function (err, result) {
            execHelper(deferred, err, result);
        });
    return deferred.promise;
}

//var userDal = require('../../server/school.dal/user.dal.js');

function insert(teacherModel, userModel) {

    var userDal = require('./user.dal.js');
    var deferred = Q.defer();

    async.waterfall(
        [
            function (callback) {

                userModel.createdDate = new Date();

                userDal.add(userModel)
                    .then(function (user) {

                        if (user) {

                            //teacherModel.user._id = user._id;
                            teacherModel.userRef = user._id;
                            teacherModel.createdBy = user._id;
                            teacherModel.createdDate = new Date();
                            callback(null, teacherModel);
                        } else {
                            deferred.resolve();
                        }
                    })
                    .catch(function (err) {
                        if (err) deferred.reject(err);
                    });
            },
            function (teacherModel, callback) {
                add(teacherModel)
                    .then(function (teacher) {

                        if (teacher) {
                            deferred.resolve(teacher);
                        } else {
                            deferred.resolve();
                        }
                    })
                    .catch(function (err) {
                        if (err) {
                            callback(err, userModel);
                            //deferred.reject(err);
                        }
                    });
            }
        ], function (errs, results) {
            if (errs) {
                async.each(results, rollback, function () {
                    console.log('Rollback done.');
                    //res.status(400).send(errs);
                });
            } else {
                console.log('Done.');
                deferred.resolve(results);
            }
        });

    function rollback(doc, callback) {
        if (!doc) { callback(); }
        else {
            _user.findByIdAndRemove(doc._id, function (err, doc) {
                console.log('Rolled-back document: ' + doc);
                callback();
            });
        }
    }
    return deferred.promise;
}

function mobileRegistration(mobileteacherRegistrationModel) {

    var deferred = Q.defer();

    var userModel = mobileteacherRegistrationModel.User;

    async.waterfall(
        [
            function (callback) {

                userDal.add(userModel)
                    .then(function (result) {
                        if (result.user._id) {

                            var teacherModel = mobileteacherRegistrationModel.teacher;
                            teacherModel.user._id = result.user._id;

                            callback(null, teacherModel);
                        }
                    })
            },
            function (teacherModel, callback) {
                this.add(teacherModel)
                    .then(function (result) {

                    });
            }
        ], function (errs, results) {
            if (errs) {
                async.each(results, rollback, function () {
                    console.log('Rollback done.');
                    res.status(400).send(errs);
                });
            } else {
                console.log('Done.');
                deferred.resolve(results);
            }
        });

    function rollback(doc, callback) {
        if (!doc) { callback(); }
        else {
            userModel.findByIdAndRemove(doc._id, function (err, doc) {
                console.log('Rolled-back document: ' + doc);
                callback();
            });
        }
    }
    return deferred.promise;
}

function getTeachersByAnyCriteria(searchCriteria, searchText) {

    var deferred = Q.defer();

    _teachers
        //.find({
        //    loc: {
        //        $near: searchCriteria.coords,
        //        $maxDistance: searchCriteria.maxDistance
        //    }
        //})
        .find({
            '$or': [
                { designation: new RegExp(searchText, 'i') },
                { qualification: new RegExp(searchText, 'i') }
            ]
        })
        .populate({ path: 'userRef' })
        .populate({ path: 'subjects' })
        //.populate({ path: 'followers' })
        //.populate({ path: 'favorites' })
        //.populate({ path: 'messages' })
        //.populate({ path: 'profileVisitors' })
        //.populate({ path: 'sharedProfiles' })
        .exec(function (err, result) {
            execHelper(deferred, err, result);
        });
    return deferred.promise;
}

function getTeacherProfileInfo(userId) {

    var deferred = Q.defer();

    _teachers
        .findOne({})
        .populate({
            path: 'userRef',
            match: { _id: userId },
        })
        .populate({ path: 'subjects' })
        .exec(function (err, result) {
            execHelper(deferred, err, result);
        });
    return deferred.promise;
}

function getTeacherByUserId(userId) {

    var deferred = Q.defer();

    _teachers
        .findOne({})
        .populate({
            path: 'userRef',
            match: { _id: userId },
        })
        .populate({ path: 'subjects' })
        .exec(function (err, result) {
            execHelper(deferred, err, result);
        });
    return deferred.promise;
}

function count() {

    var deferred = Q.defer();

    _teachers
        .count({})
        .exec(function (err, result) {
            execHelper(deferred, err, result);
        });
    return deferred.promise;
}

function getTeacherById(userId) {

    var deferred = Q.defer();

    _teachers
        .findOne({ 'user._id': userId })
        .populate({ path: 'user' })
        .populate({ path: 'subjects' })
        //.populate({ path: 'followers' })
        //.populate({ path: 'favorites' })
        //.populate({ path: 'messages' })
        //.populate({ path: 'profileVisitors' })
        //.populate({ path: 'sharedProfiles' })
        .exec(function (err, result) {
            execHelper(deferred, err, result);
        });
    return deferred.promise;
}

function mobileUpdateProfile(updateProfileModel, teacherModel, avatarUrl) {

    var deferred = Q.defer();

    async.waterfall(
        [
            function (callback) {

                _user.findOne({ _id: updateProfileModel._id }, function (err, user) {
                    if (err) {
                        deferred.reject(err);
                        console.log(err);
                    }
                    if (user) {

                        user.email = updateProfileModel.email ? updateProfileModel.email : user.email;
                        user.username = updateProfileModel.username ? updateProfileModel.username : user.username;
                        user.password = updateProfileModel.password ? updateProfileModel.password : user.password;

                        user.firstName = updateProfileModel.firstName ? updateProfileModel.firstName : user.firstName;
                        user.lastName = updateProfileModel.lastName ? updateProfileModel.lastName : user.lastName;
                        user.birthDate = updateProfileModel.birthDate ? updateProfileModel.birthDate : user.birthDate;
                        user.phoneNumber = updateProfileModel.phoneNumber ? updateProfileModel.phoneNumber : user.phoneNumber;
                        user.bloodGroup = updateProfileModel.bloodGroup ? updateProfileModel.bloodGroup : user.bloodGroup;

                        user.address = updateProfileModel.address ? updateProfileModel.address : user.address;
                        user.city = updateProfileModel.city ? updateProfileModel.city : user.city;
                        user.province = updateProfileModel.province ? updateProfileModel.province : user.province;
                        user.pinCode = updateProfileModel.pinCode ? updateProfileModel.pinCode : user.pinCode;
                        //user.country = updateProfileModel.country ? updateProfileModel.country : user.country;
                        //user.countryISO2 = updateProfileModel.countryISO2 ? updateProfileModel.countryISO2 : user.countryISO2;
                        //user.countryDialCode = updateProfileModel.countryDialCode ? updateProfileModel.countryDialCode : user.countryDialCode;

                        user.updatedBy = updateProfileModel._id;
                        user.updatedDate = new Date();

                        //user.status = request.status;
                        user.save(function (err, result) {
                            if (err) {
                                console.log(err);
                                deferred.reject(err);
                            }
                            if (result._id) {

                                callback(null, updateProfileModel);
                            }
                        });
                    }
                });
            },
            function (teacherId, callback) {
                _teachers.findOne({ _id: teacherModel._id }, function (err, teacher) {
                    if (err) {
                        console.log(err);
                        deferred.reject(err);
                    }
                    if (teacher) {
                        teacher.avatarUrl = avatarUrl;
                        //TODO
                        //teacher.AvatarMimeType = mimeType;
                        //teacher.AvatarFileName = fileName;

                        teacher.designation = teacherModel.designation ? teacherModel.designation : teacher.designation;
                        teacher.qualification = teacherModel.qualification ? teacherModel.qualification : teacher.qualification;
                        teacher.experience = teacherModel.experience ? teacherModel.experience : teacher.experience;

                        //teacher.updatedBy = teacherModel.UserId;
                        teacher.updatedDate = new Date();

                        teacher.save(function (err, result) {
                            if (err) {
                                console.log(err);
                                deferred.reject(err);
                            }
                            if (result._id) {

                                callback(null, teacherModel._id);
                            }
                        });
                    }
                });
            }
        ], function (errs, results) {
            if (errs) {
                async.each(results, rollback, function () {
                    console.log('Rollback done.');
                    res.status(400).send(errs);
                });
            } else {
                console.log('Done.');
                deferred.resolve(results);
            }
        });

    function rollback(doc, callback) {
        if (!doc) { callback(); }
        else {
            userModel.findByIdAndRemove(doc._id, function (err, doc) {
                console.log('Rolled-back document: ' + doc);
                callback();
            });
        }
    }
    return deferred.promise;
}

function updateProfile(updateProfileModel, teacherModel, avatarUrl) {

    var deferred = Q.defer();

    async.waterfall(
        [
            function (callback) {

                _user.findOne({ _id: updateProfileModel._id }, function (err, user) {
                    if (err) {
                        deferred.reject(err);
                        console.log(err);
                    }
                    if (user) {

                        //user.email = updateProfileModel.email ? updateProfileModel.email : user.email;
                        //user.username = updateProfileModel.username ? updateProfileModel.username : user.username;
                        //user.password = updateProfileModel.password ? updateProfileModel.password : user.password;

                        user.firstName = updateProfileModel.firstName ? updateProfileModel.firstName : user.firstName;
                        user.lastName = updateProfileModel.lastName ? updateProfileModel.lastName : user.lastName;
                        user.birthDate = updateProfileModel.birthDate ? updateProfileModel.birthDate : user.birthDate;
                        user.phoneNumber = updateProfileModel.phoneNumber ? updateProfileModel.phoneNumber : user.phoneNumber;
                        user.bloodGroup = updateProfileModel.bloodGroup ? updateProfileModel.bloodGroup : user.bloodGroup;

                        user.address = updateProfileModel.address ? updateProfileModel.address : user.address;
                        user.city = updateProfileModel.city ? updateProfileModel.city : user.city;
                        user.province = updateProfileModel.province ? updateProfileModel.province : user.province;
                        user.pinCode = updateProfileModel.pinCode ? updateProfileModel.pinCode : user.pinCode;
                        //user.country = updateProfileModel.country ? updateProfileModel.country : user.country;
                        //user.countryISO2 = updateProfileModel.countryISO2 ? updateProfileModel.countryISO2 : user.countryISO2;
                        //user.countryDialCode = updateProfileModel.countryDialCode ? updateProfileModel.countryDialCode : user.countryDialCode;

                        user.updatedBy = updateProfileModel._id;
                        user.updatedDate = new Date();

                        //user.status = request.status;
                        user.save(function (err, result) {
                            if (result._id) {

                                callback(null, updateProfileModel);
                            }
                        });
                    }
                });
            },
            function (teacherId, callback) {
                _teachers.findOne({ _id: teacherModel._id }, function (err, teacher) {
                    if (err) {
                        console.log(err);
                        deferred.reject(err);
                    }
                    if (teacher) {
                        teacher.avatarUrl = avatarUrl;
                        //TODO
                        //teacher.AvatarMimeType = mimeType;
                        //teacher.AvatarFileName = fileName;

                        teacher.designation = teacherModel.designation ? teacherModel.designation : teacher.designation;
                        teacher.qualification = teacherModel.qualification ? teacherModel.qualification : teacher.qualification;
                        teacher.experience = teacherModel.experience ? teacherModel.experience : teacher.experience;

                        //teacher.updatedBy = teacherModel.UserId;
                        teacher.updatedDate = new Date();

                        teacher.save(function (err, result) {
                            if (result._id) {

                                callback(null, teacherModel._id);
                            }
                        });
                    }
                });
            }
        ], function (errs, results) {
            if (errs) {
                async.each(results, rollback, function () {
                    console.log('Rollback done.');
                    res.status(400).send(errs);
                });
            } else {
                console.log('Done.');
                deferred.resolve(results);
            }
        });

    function rollback(doc, callback) {
        if (!doc) { callback(); }
        else {
            userModel.findByIdAndRemove(doc._id, function (err, doc) {
                console.log('Rolled-back document: ' + doc);
                callback();
            });
        }
    }
    return deferred.promise;
}

function updateTeacher(teacherUpdateCondition, teacherUpdateSet) {

    var deferred = Q.defer();

    _teachers.findOneAndUpdate(teacherUpdateCondition, { $set: teacherUpdateSet }, { new: true }, function (err, teacher) {
        if (err) {
            deferred.reject(err);
        }
        if (teacher) {
            teacher = _.pick(teacher.toObject(), ['_id', 'teachername', 'email', 'isProfileIncomplete', 'firstName', 'phoneNumber', 'province', 'city',
                'address', 'pinCode', 'countryDialCode', 'avatarUrl', 'exams']);
            deferred.resolve(teacher);
        } else {
            deferred.resolve();
        }
    });
    return deferred.promise;
}

function remove(teacherId, userId) {

    var userDal = require('./user.dal.js');
    var deferred = Q.defer();

    async.waterfall(
        [
            function (callback) {

                //userModel.createdDate = new Date();

                var loggedInUserId;

                var userUpdateCondition = { $and: [] };
                userUpdateCondition.$and.push({
                    _id: userId
                });

                var userUpdateSet = {
                    isDeleted: true,
                    //updatedBy: loggedInUserId,
                    updatedDate: new Date()
                };

                userDal.update(userUpdateCondition, userUpdateSet)
                    .then(function (user) {

                        if (user) {

                            callback(null, teacherId);
                        } else {
                            deferred.resolve();
                        }
                    })
                    .catch(function (err) {
                        if (err) deferred.reject(err);
                    });
            },
            function (teacherId, callback) {

                var loggedInUserId;

                var teacherUpdateCondition = { $and: [] };
                teacherUpdateCondition.$and.push({
                    _id: teacherId
                });

                var teacherUpdateSet = {
                    isDeleted: true,
                    //updatedBy: loggedInUserId,
                    updatedDate: new Date()
                };

                updateTeacher(teacherUpdateCondition, teacherUpdateSet)
                    .then(function (teacher) {

                        if (teacher) {
                            deferred.resolve(teacher);
                        } else {
                            deferred.resolve();
                        }
                    })
                    .catch(function (err) {
                        if (err) {
                            callback(err, userModel);
                            //deferred.reject(err);
                        }
                    });
            }
        ], function (errs, results) {
            if (errs) {
                async.each(results, rollback, function () {
                    console.log('Rollback done.');
                    //res.status(400).send(errs);
                });
            } else {
                console.log('Done.');
                deferred.resolve(results);
            }
        });

    function rollback(doc, callback) {
        if (!doc) { callback(); }
        else {
            _user.findOne({ _id: updateProfileModel.userId }, function (err, user) {
                if (!user) {

                    user.isDeleted = false;
                    user.updatedBy = updateProfileModel._id;
                    user.updatedDate = new Date();
                }
                user.save(function (err, doc) {
                    console.log('Rolled-back document: ' + doc);
                    callback();
                });
            });
        }
    }
    return deferred.promise;
}