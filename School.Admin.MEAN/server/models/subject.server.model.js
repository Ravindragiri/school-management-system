﻿var mongoose = require('mongoose');

var subjectSchema = new mongoose.Schema({

    text: String,
    value: String,

    //examType: String,
    marksObtainedTheory: String,
    maxMarksTheory: {
        type: String,
        default: 100
    },

    marksObtainedPractical: String,
    maxMarksPractical: {
        type: String,
        default: 100
    },

    teacherRef: { type: mongoose.Schema.Types.ObjectId, ref: 'teacher' },

    createdBy: {
        type: mongoose.Schema.Types.ObjectId
    },
    createdDate: {
        type: Date
    },
    updatedBy: {
        type: mongoose.Schema.Types.ObjectId
    },
    updatedDate: Date
});

exports.subjectSchema = subjectSchema;
mongoose.model('subject', subjectSchema);