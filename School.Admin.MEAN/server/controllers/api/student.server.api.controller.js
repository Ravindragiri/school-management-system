﻿var studentDal = require('../../school.dal/student.dal.js');
require('../../school.common/appConstants');
require('../../school.common/helpers/responseHelper');

var mongoose = require('mongoose'),
    fs = require('fs-extra'),
    util = require("util"),
    async = require('async'),
    path = require("path"),
    moment = require("moment");

var student = mongoose.model('student');
var _parent = mongoose.model('parent');
var _user = mongoose.model('user');

function formatServerDate(candidateDTO) {
    if (candidateDTO.userRef.birthDate) {
        candidateDTO.userRef.birthDate = moment(candidateDTO.userRef.birthDate).format("DD/MM/YYYY");
    }
    return candidateDTO;
}

function parseClientDate(candidateDTO) {
    if (candidateDTO.userRef.birthDate) {
        candidateDTO.userRef.birthDate = moment(candidateDTO.userRef.birthDate, 'DD/MM/YYYY').toDate();
    }
    return candidateDTO;
}

exports.searchStudents = function (req, res) {

    var pagingSortingParams = req.param.pagingSortingParams;
    var searchCriteria = req.query.searchCriteria;

    studentDal.searchStudents(pagingSortingParams, searchCriteria)
        .then(function (result) {
            if (result) {
                sendSuccessResponse(res, result);
            } else {
                sendFailResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
}

exports.insert = function (req, res) {

    if (req.body.studentDTO) {
        var userDal = require('../../school.dal/user.dal.js');
        var studentDTO = req.body.studentDTO;
        studentDTO = parseClientDate(studentDTO);
        var studentModel = new student(studentDTO);
        var userModel = new _user(studentDTO.userRef);
        var parentModel = new _parent(studentDTO.parentRef);

        async.waterfall(
            [
                function (callback) {
                    userDal.checkUserNameExist(userModel.username)
                        .then(function (result) {
                            if (result) {
                                sendFailResponse(res, messages.UsernameNotAvailable);
                            }
                            else {
                                callback(null);
                            }
                        });
                },
                function (callback) {
                    userDal.checkEmailExist(userModel.email)
                        .then(function (result) {
                            if (result) {
                                sendFailResponse(res, messages.EmailNotAvailable);
                            }
                            else {
                                callback(null);
                            }
                        });
                },
                function (callback) {

                    //userModel.createdDate = new Date();

                    studentDal.insert(studentModel, parentModel, userModel)
                        .then(function (user) {
                            if (user) {
                                sendSuccessResponse(res, user, messages.UserRegisteredSuccessfully);
                                //uploadAvatar(files, user._id.toString());
                            } else {
                                sendFailResponse(res, messages.UserRegistrationFailed);
                            }
                        })
                        .catch(function (err) {
                            console.log(err);
                            sendErrorResponse(res);
                        });
                }
            ], function (errs, results) {
                if (errs) {
                    console.log(err);
                    sendErrorResponse(res);
                } else {
                    console.log('Done.');
                }
            });
    }
    else {
        sendBadRequestResponse();
    }
};

exports.mobileRegister = function (req, res) {

    var userDal = require('../../school.dal/user.dal.js');

    // parse a file upload 
    var form = new formidable.IncomingForm();
    form.parse(req, function (err, fields, files) {

        if (fields.data) {

            var studentRegistration = JSON.parse(fields.data);
            var studentModel = new student(studentRegistration.studentDTO);
            var userModel = new _user(studentRegistration.data.userDTO);

            async.waterfall(
                [
                    function (callback) {
                        userDal.checkUserNameExist(userModel.username)
                            .then(function (result) {
                                if (result) {
                                    sendFailResponse(res, messages.UsernameNotAvailable);
                                }
                                else {
                                    callback(null);
                                }
                            });
                    },
                    function (callback) {
                        userDal.checkEmailExist(userModel.email)
                            .then(function (result) {
                                if (result) {
                                    sendFailResponse(res, messages.EmailNotAvailable);
                                }
                                else {
                                    callback(null);
                                }
                            });
                    },
                    function (callback) {

                        if (studentRegistration.data.userDTO.attendances) {
                            studentModel.attendances = JSON.parse(studentRegistration.data.userDTO.attendances);
                            var ObjectId = mongoose.Types.ObjectId;
                            for (var i = 0; i < studentModel.attendances.length; i++) {
                                studentModel.attendances[i]._id = new ObjectId;

                                studentModel.attendances[i].createdDate = new Date();
                            }
                        }
                        //userModel.createdDate = new Date();

                        studentDal.mobileRegister(studentModel, userModel)
                            .then(function (user) {
                                if (user) {
                                    sendSuccessResponse(res, user, messages.UserRegisteredSuccessfully);
                                    //uploadAvatar(files, user._id.toString());
                                } else {
                                    sendFailResponse(res, messages.UserRegistrationFailed);
                                }
                            })
                            .catch(function (err) {
                                console.log(err);
                                sendErrorResponse(res);
                            });
                    }
                ], function (errs, results) {
                    if (errs) {
                        console.log(err);
                        sendErrorResponse(res);
                    } else {
                        console.log('Done.');
                    }
                });
        } else {
            sendBadRequestResponse(res);
        }
    });
};

exports.remove = function (req, res) {

    console.log(req.query);

    if (req.query.id && req.query.userId) {

        var studentId = req.query.id;
        var userId = req.query.userId;
        var parentId = req.query.parentId;

        studentDal.remove(studentId, parentId, userId)
            .then(function (result) {
                if (result) {
                    sendSuccessResponse(res, result);
                } else {
                    sendNoContentResponse(res);
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
    else {
        sendBadRequestResponse(res);
    }
}

uploadAvatar = function (files, userId) {
    if (userId) {
        if (files.image) {
            if (files.image.name && files.image.name.length > 0) {
                var newFileName = userId + path.extname(files.image.name);
                if (!fs.existsSync(path.join(__dirname, util.format('../../../public/uploads/avatar/%s', userId)))) {
                    fs.mkdirSync(path.join(__dirname, util.format('../../../public/uploads/avatar/%s', userId)));
                }
                var tempPath = files.image.path,
                    targetPath = path.join(__dirname, util.format('../../../public/uploads/avatar/%s/%s', userId, newFileName));

                fs.copy(tempPath, targetPath, function (err) {
                    if (err) { console.log(err); }
                    else { console.log("Upload completed!"); }
                });
            }
        }
    }
}

var formidable = require('formidable');

exports.mobileUpdateProfile = function (req, res) {
    // parse a file upload 
    var form = new formidable.IncomingForm();

    form.parse(req, function (err, fields, files) {

        if (fields.data && JSON.parse(fields.data)) {
            var studentRegistration = JSON.parse(fields.data);
            //
            var userModel = new _user(studentRegistration.data.userDTO);

            studentDal.mobileUpdateProfile(userModel)
                .then(function (user) {
                    if (user) {
                        sendSuccessResponse(res, user, messages.UserRegisteredSuccessfully);
                        uploadAvatar(files, user._id.toString());
                    } else {
                        sendFailResponse(res, messages.UserRegistrationFailed);
                    }
                })
                .catch(function (err) {
                    console.log(err);
                    sendErrorResponse(res);
                });
        }
        else {
            sendBadRequestResponse(res);
        }
    });
};

exports.update = function (req, res) {

    console.log(req.body);

    if (req.body.studentDTO) {
        var studentDTO = req.body.studentDTO;
        studentDTO = studentDTO.parseClientDate(studentDTO);
        var updateProfileModel = new student(studentDTO);
        var userModel = new _user(studentDTO.userRef);
        var parentModel = new _parent(studentDTO.parentRef);
        var avatarUrl;

        studentDal.update(userModel, updateProfileModel, parentModel, avatarUrl)
            .then(function (result) {
                if (result) {
                    sendSuccessResponse(res, result);
                } else {
                    sendFailResponse(res);
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
    else {
        sendBadRequestResponse(res);
    }
};

exports.updateProfile = function (req, res) {

    console.log(req.body);

    if (req.body.studentDTO) {

        var updateProfileModel = new student(req.body.studentDTO);
        var userModel = new _user(req.body.studentDTO.userRef);
        var parentModel = new _parent(req.body.studentDTO.parentRef);
        var avatarUrl;

        studentDal.updateProfile(userModel, updateProfileModel, parentModel, avatarUrl)
            .then(function (result) {
                if (result) {
                    sendSuccessResponse(res, result);
                } else {
                    sendFailResponse(res);
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
    else {
        sendBadRequestResponse(res);
    }
};

exports.bulkAdd = function (req, res) {

}

exports.getStudentById = function (req, res) {

    var studentId = req.query.studentId;

    studentDal.getStudentById(studentId)
        .then(function (result) {
            if (result) {
                sendSuccessResponse(res, result);
            } else {
                sendNoContentResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
}

exports.getStudentByUserId = function (req, res) {
    
    if (req.query.userId) {
        var userId = req.query.userId;

        studentDal.getStudentByUserId(userId)
            .then(function (result) {
                if (result) {
                    sendSuccessResponse(res, result);
                } else {
                    sendNoContentResponse(res);
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
    else {
        sendBadRequestResponse(res);
    }
}

exports.getAllStudentInfo = function (req, res) {

    //sendSuccessResponse(res, JSON.stringify(getSubjectList()));

    studentDal.getAllStudents()
        .then(function (result) {
            if (result) {
                for (var i = 0; i < result.length; i++) {
                    result[i] = formatServerDate(result[i]);
                }
                var data = {
                    result: result,
                    subjects: subjects
                }
                sendSuccessResponse(res, data);
            } else {
                sendFailResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
}

exports.getStudentContactDetailById = function (req, res) {

    if (req.query.userId) {
        var userId = req.query.userId;

        studentDal.getStudentContactDetailById(userId)
            .then(function (result) {
                if (result) {
                    sendSuccessResponse(res, result, messages.ReadSuccess);
                } else {
                    sendNoContentResponse(res);
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
    else {
        sendBadRequestResponse(res);
    }
}

exports.getStudentsByAnyCriteria = function (req, res) {

    var searchCriteria = {
        userType: (req.query.userType) ? parseInt(req.query.userType) : appDefaults.UserType,
        searchText: req.query.title,

        page: (req.query.page) ? parseInt(req.query.page) : appDefaults.Page,
        pageSize: (req.query.pageSize) ? parseInt(req.query.pageSize) : appDefaults.PageSize,
        orderBy: req.query.orderBy ? req.query.orderBy : appDefaults.OrderBy
    };

    studentDal.getStudentsByAnyCriteria(searchCriteria)
        .then(function (result) {
            if (result) {
                sendSuccessResponse(res, result);
            } else {
                sendFailResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            return sendErrorResponse(res);
        });
}

exports.count = function (req, res) {

    studentDal.count()
        .then(function (result) {
            if (result) {
                sendSuccessResponse(res, result);
            } else {
                sendFailResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
};

exports.getStudentProfileInfo = function (req, res) {

    var userId = req.query.userId;

    studentDal.getStudentProfileInfo(userId)
        .then(function (result) {
            if (result) {
                sendSuccessResponse(res, result);
            } else {
                sendNoContentResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
}

//exports.getApprovalWaitingStudents = function (req, res) {

//}

//exports.approveStudent = function (req, res) {

//}

exports.importStudentInfo = function (req, res) {

}

exports.exportStudentInfoAsExcel = function (req, res) {

}

exports.exportStudentInfoAsPdf = function (req, res) {

}

exports.exportStudentInfoAsCsv = function (req, res) {

}

exports.getStudentAvatar = function (req, res) {

    var studentId = req.query.studentId;

    studentDal.getStudentAvatar(studentId)
        .then(function (result) {
            if (result) {
                sendSuccessResponse(res, result);
            } else {
                sendFailResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
}

exports.getAllStudentsByClassAndSection = function (req, res) {

    if (req.query.class && req.query.section) {
        var className = req.query.class;
        var section = req.query.section;
        studentDal.getAllStudentsByClassAndSection(className, section)
            .then(function (result) {
                if (result) {

                    sendSuccessResponse(res, result);
                } else {
                    sendNoContentResponse(res);
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
}
