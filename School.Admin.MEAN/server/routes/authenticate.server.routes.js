﻿//var passport = require('../../config/strategies/passport-init');
var user = require('../../server/controllers/user.server.controller'),
    passport = require('passport');

module.exports = function (app) {

    //sends successful login state back to angular
    app.route('/success').get(function (req, res) {
        res.send({ state: 'success', user: req.user ? req.user : null });
    });

    //sends failure login state back to angular
    app.route('/failure').get(function (req, res) {
        res.send({ state: 'failure', user: null, message: messages.InvalidLoginCredentials });
    });

    //Note that it is('/login' and '/signup') not mandatory to name the strategies on the route path and it can be named anything.

    //log in
    //if log in is success, redirect to home page (/)
    //if log in is failure, redirect to login page
    app.route('/account/login')
        .get(user.renderLogin)
        .post(passport.authenticate('login', {
            successRedirect: '/organ/index',
            failureRedirect: '/'
        }));

    app.route('/login')
        .get(user.renderLogin)
        .post(passport.authenticate('login', {
            successRedirect: '/organ/index',
            failureRedirect: '/'
        }));

    //sign up
    //if log in is success, redirect to home page (/)
    //if log in is failure, redirect to register page
    app.route('/account/register')
        .get(user.renderRegister)
        .post(passport.authenticate('signup', {
            successRedirect: '/organ/index',
            failureRedirect: '/'
        }));

    //log out
    app.route('/logout').get(function (req, res) {
        req.logout();
        res.redirect('/');
    });
}