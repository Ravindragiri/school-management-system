﻿'use strict';

/**
 * Module dependencies.
 */
var _ = require('lodash'),
    errorHandler = require('../../controllers/error.server.controller'),
    mongoose = require('mongoose'),
    User = mongoose.model('user'),
    //config = require('../../../config/config'),
    //nodemailer = require('nodemailer'),
    async = require('async'),
    crypto = require('crypto');

/**
 * confirm email GET
 */
exports.renderConfirmEmail = function (req, res) {
    res.render('account/confirmEmail', {
        pTitle: 'Confirm Email | Organ Donor',
        heading: 'Confirm Email',
        loginProvider: 'Facebook'
    });
};

/**
 * external login confirmation GET
 */
exports.renderExternalLoginConfirm = function (req, res) {
    res.render('account/externalLoginConfirmation', {
        pTitle: 'External Login Confirmation | Organ Donor',
        heading: 'Externa lLogin Confirmation',
        loginProvider: 'Facebook'
    });
};


/**
 * reset password confirmation GET
 */
exports.renderResetPasswordConfirm = function (req, res) {
    res.render('account/resetPasswordConfirmation', {
        pTitle: 'Reset Password Confirmation | Organ Donor',
        heading: 'Reset Password Confirmation'
    });
};

/**
 * Reset Password GET
 */
exports.renderResetPassword = function (req, res) {
    res.render('account/resetPassword', {
        pTitle: 'Reset Password | Organ Donor',
        heading: 'Reset Password'
    });
};

/**
 * send verification code GET
 */
exports.renderSendCode = function (req, res) {
    res.render('account/sendCode', {
        pTitle: 'Send Verification Code | Organ Donor',
        heading: 'Send Verification Code'
    });
};

/**
 * send verification code GET
 */
exports.renderVerifyCode = function (req, res) {
    res.render('account/verifyCode', {
        pTitle: 'Verifing Code | Organ Donor',
        heading: 'Verifing Code'
    });
};

/**
 * forgot Password Confirm GET
 */
exports.renderExternalLoginFailure = function (req, res) {
    res.render('account/externalLoginFailure', {
        pTitle: 'External Login Failure | Organ Donor',
        heading: 'External Login Failure'
    });
};

/**
 * forgot Password Confirm GET
 */
exports.renderForgotPasswordConfirm = function (req, res) {
    res.render('account/forgotPasswordConfirmation', {
        pTitle: 'Forgot Password Confirmation | Organ Donor',
        heading: 'Forgot Password Confirmation'
    });
};

/**
 * forgot Password GET
 */
exports.renderForgotPassword = function (req, res) {
    res.render('account/forgotPassword', {
        pTitle: 'Forgot Password | Organ Donor',
        heading: 'Forgot Password'
    });
};

/**
 * Forgot for reset password (forgot POST)
 */
exports.forgot = function (req, res, next) {
    async.waterfall([
        // Generate random token
        function (done) {
            crypto.randomBytes(20, function (err, buffer) {
                var token = buffer.toString('hex');
                done(err, token);
            });
        },
        // Lookup user by username
        function (token, done) {
            if (req.body.username) {
                User.findOne({
                    username: req.body.username
                }, '-salt -password', function (err, user) {
                    if (!user) {
                        return res.status(400).send({
                            message: 'No account with that username has been found'
                        });
                    } else if (user.provider !== 'local') {
                        return res.status(400).send({
                            message: 'It seems like you signed up using your ' + user.provider + ' account'
                        });
                    } else {
                        user.resetPasswordToken = token;
                        user.resetPasswordExpires = Date.now() + 3600000; // 1 hour

                        user.save(function (err) {
                            done(err, token, user);
                        });
                    }
                });
            } else {
                return res.status(400).send({
                    message: 'Username field must not be blank'
                });
            }
        }
        //,
        //function (token, user, done) {
        //    res.render('templates/reset-password-email', {
        //        name: user.displayName,
        //        appName: config.app.title,    TODO
        //        url: 'http://' + req.headers.host + '/auth/reset/' + token
        //    }, function (err, emailHTML) {
        //        done(err, emailHTML, user);
        //    });
        //},
        // If valid email, send reset email using service
        //function (emailHTML, user, done) {
        //    var smtpTransport = nodemailer.createTransport(config.mailer.options);
        //    var mailOptions = {
        //        to: user.email,
        //        from: config.mailer.from,     TODO
        //        subject: 'Password Reset',
        //        html: emailHTML
        //    };
        //    smtpTransport.sendMail(mailOptions, function (err) {
        //        if (!err) {
        //            res.send({
        //                message: 'An email has been sent to ' + user.email + ' with further instructions.'
        //            });
        //        }

        //        done(err);
        //    });
        //}
    ], function (err) {
        if (err) return next(err);
    });
};

/**
 * Reset password GET from email token
 */
exports.validateResetToken = function (req, res) {
    User.findOne({
        resetPasswordToken: req.params.token,
        resetPasswordExpires: {
            $gt: Date.now()
        }
    }, function (err, user) {
        if (!user) {
            return res.redirect('/#!/password/reset/invalid');
        }

        res.redirect('/#!/password/reset/' + req.params.token);
    });
};

/**
 * Reset password POST from email token
 */
exports.reset = function (req, res, next) {
    // Init Variables
    var passwordDetails = req.body;

    async.waterfall([

        function (done) {
            User.findOne({
                resetPasswordToken: req.params.token,
                resetPasswordExpires: {
                    $gt: Date.now()
                }
            }, function (err, user) {
                if (!err && user) {
                    if (passwordDetails.newPassword === passwordDetails.verifyPassword) {
                        user.password = passwordDetails.newPassword;
                        user.resetPasswordToken = undefined;
                        user.resetPasswordExpires = undefined;

                        user.save(function (err) {
                            if (err) {
                                return res.status(400).send({
                                    message: errorHandler.getErrorMessage(err)
                                });
                            } else {
                                req.login(user, function (err) {
                                    if (err) {
                                        console.log(err);
                                        sendErrorResponse(res);
                                    } else {
                                        // Return authenticated user 
                                        res.json(user);

                                        done(err, user);
                                    }
                                });
                            }
                        });
                    } else {
                        return res.status(400).send({
                            message: 'Passwords do not match'
                        });
                    }
                } else {
                    return res.status(400).send({
                        message: 'Password reset token is invalid or has expired.'
                    });
                }
            });
        },
        function (user, done) {
            res.render('templates/reset-password-confirm-email', {
                name: user.displayName,
                appName: config.app.title
            }, function (err, emailHTML) {
                done(err, emailHTML, user);
            });
        },
        // If valid email, send reset email using service
        function (emailHTML, user, done) {
            var smtpTransport = nodemailer.createTransport(config.mailer.options);
            var mailOptions = {
                to: user.email,
                from: config.mailer.from,
                subject: 'Your password has been changed',
                html: emailHTML
            };

            smtpTransport.sendMail(mailOptions, function (err) {
                done(err, 'done');
            });
        }
    ], function (err) {
        if (err) return next(err);
    });
};

/**
 * change password GET
 */
exports.renderChangePassword = function (req, res) {
    res.render('account/changePassword', {
        pTitle: 'Change Password | Organ Donor',
        heading: 'Change Password'
    });
};

/**
 * Change Password
 */
exports.changePassword = function (req, res) {
    // Init Variables
    var passwordDetails = req.body;

    if (req.user) {
        if (passwordDetails.newPassword) {
            User.findById(req.user.id, function (err, user) {
                if (!err && user) {
                    if (user.authenticate(passwordDetails.currentPassword)) {
                        if (passwordDetails.newPassword === passwordDetails.verifyPassword) {
                            user.password = passwordDetails.newPassword;

                            user.save(function (err) {
                                if (err) {
                                    return res.status(400).send({
                                        message: errorHandler.getErrorMessage(err)
                                    });
                                } else {
                                    req.login(user, function (err) {
                                        if (err) {
                                            console.log(err);
                                            sendErrorResponse(res);
                                        } else {
                                            res.send({
                                                message: 'Password changed successfully'
                                            });
                                        }
                                    });
                                }
                            });
                        } else {
                            res.status(400).send({
                                message: 'Passwords do not match'
                            });
                        }
                    } else {
                        res.status(400).send({
                            message: 'Current password is incorrect'
                        });
                    }
                } else {
                    res.status(400).send({
                        message: 'User is not found'
                    });
                }
            });
        } else {
            res.status(400).send({
                message: 'Please provide a new password'
            });
        }
    } else {
        res.status(400).send({
            message: 'User is not signed in'
        });
    }
};