﻿var mongoose = require('mongoose');

var invoiceSchema = new mongoose.Schema({
    title: String,
    purpose: String,
    amount: String,
    amountPaid: String,
    amountdue: String,
    paymentMethod: String,
    paymentDetails: String,
    status: String,

    studentRef: { type: mongoose.Schema.Types.ObjectId, ref: 'student', required: true },

    academicYear: String,
    createdBy: {
        type: mongoose.Schema.Types.ObjectId,
    },
    createdDate: {
        type: Date,
    },
    paymentDate: {
        type: Date,
    }
});

exports.invoiceSchema = invoiceSchema;
mongoose.model('invoice', invoiceSchema);