﻿var mongoose = require('mongoose');

var expenseCategorySchema = new mongoose.Schema({

    name: String,
    desc: String
});

exports.expenseCategorySchema = expenseCategorySchema;
mongoose.model('expenseCategory', expenseCategorySchema);