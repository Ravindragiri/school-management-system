﻿var Q = require('q');
var mongoose = require('mongoose')
require('../models/student.server.model');
require('../models/exam.server.model');
require('../models/user.server.model');
var _student = mongoose.model('student');
var _exam = mongoose.model('exam');
var _user = mongoose.model('user');
var ObjectId = mongoose.Types.ObjectId;

var dal = {};

dal.getExamDetails = getExamDetails;
dal.getAllStudentMarks = getAllStudentMarks;
dal.getExamDetailById = getExamDetailById;
dal.getStudentMarksDetails = getStudentMarksDetails;
dal.getStudentRollNo = getStudentRollNo;
dal.insertTimeTable = insertTimeTable;
dal.displayTimeTable = displayTimeTable;
dal.searchTimeTable = searchTimeTable;
dal.getStudentMarksDetails = getStudentMarksDetails;
dal.getStudentsByClassAndSection = getStudentsByClassAndSection;
dal.getStudentRollNo = getStudentRollNo;
dal.getStudents = getStudents;
dal.loadClass = loadClass;
dal.loadSection = loadSection;
dal.searchStudent = searchStudent;
dal.searchMarks = searchMarks;
dal.insertMarks = insertMarks;
dal.updateMarks = updateMarks;

function getExamDetail(userID) {

    var deferred = Q.defer();

    _user
        .findOne({ _id: userID })
        .select('_id firstName phoneNumber province city address pinCode email isActive exams')
        .populate({
            path: 'organs',
            select: '_id title description'
        })
        .lean()
        .exec(function (err, result) {

            if (err) {
                deferred.reject(err);
            }
            if (result) {
                deferred.resolve(result);
            } else {
                deferred.resolve();
            }
        });
    return deferred.promise;
}

function getAllStudentMarks() {
}

function getExamDetailById() {
}

function insertTimeTable() {
}

function displayTimeTable() {
}

function searchTimeTable(examType, studentClass, section) {
}

function getStudentMarksDetails(studentName, studentClass, section, rollno) {
}

function getStudentsByClassAndSection() {
}

function getStudentRollNo(studentClass, section, studentName) {
}

function getStudents(studentClass, section) {
}

function loadClass() {
}

function loadSection() {
}

function searchStudent(studentClass, section, studentName, examType, subjectName, rollNo) {
}

function searchMarks(studentClass, section, studentName, rollNo) {
}

function insertMarks(rollNo, studentName, subjectName, marks, studentClass, section, examType, messsage) {
}

function updateMarks(rollNo, studentName, subjectName, marks, studentClass, section, examType, messsage) {
}

module.exports = dal;

