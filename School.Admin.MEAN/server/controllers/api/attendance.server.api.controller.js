﻿var attendanceDal = require('../../../server/school.dal/attendance.dal.js');
require('../../school.common/helpers/responseHelper');

var mongoose = require('mongoose');
    //fs = require('fs-extra'),
    //util = require("util"),
//path = require("path");

var _student = mongoose.model('student');
var _user = mongoose.model('user');

exports.getAttendanceById = function (req, res) {

    if (req.query.studentId && req.query.attendanceId) {

        var studentId = req.query.studentId;
        var attendanceId = req.query.attendanceId;

        attendanceDal.getById(studentId, attendanceId)
            .then(function (result) {
                if (result) {
                    sendSuccessResponse(res, result);
                } else {
                    sendNoContentResponse(res);
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
    else {
        sendBadRequestResponse(res);
    }
}

exports.count = function (req, res) {

    if (req.query.studentId) {

        var studentId = req.query.studentId;
        attendanceDal.count(studentId)
            .then(function (result) {
                if (result) {
                    sendSuccessResponse(res, result);
                } else {
                    sendFailResponse(res);
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
    else {
        sendBadRequestResponse(res);
    }
}

exports.getAllAttendance = function (req, res) {

}

exports.getAttendanceByStudentId = function (req, res) {

    console.log(req.query.studentId);
    if (req.query.studentId) {

        var studentId = req.query.studentId;
        attendanceDal.getAttendanceByStudentId(studentId)
            .then(function (result) {
                if (result) {
                    sendSuccessResponse(res, result);
                } else {
                    sendNoContentResponse(res);
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
    else {
        sendBadRequestResponse(res);
    }
}

exports.getAttendanceByUserId = function (req, res) {

}

exports.addAttendance = function (req, res) {

    if (req.body.studentId && req.body.attendances) {

        var studentId = req.body.studentId;
        var attendances = req.body.attendances;

        attendances.forEach(function (attendance) {

            attendanceDal.add(studentId, attendance)
                .then(function (result) {
                    if (result) {
                        sendSuccessResponse(res, result);
                        //sendSMS to parent
                    } else {
                        sendErrorResponse(res);
                    }
                })
                .catch(function (err) {
                    console.log(err);
                    sendErrorResponse(res);
                });
        });
    }
    else {
        sendBadRequestResponse(res);
    }
}

//exports.add = function (req, res) {

//    var studentId = req.body.studentId;
//    var userId = req.body.userId;
//    var attendances = req.body.attendances;

//    attendances.forEach(function (attendance) {

//        attendanceDal.add(studentId, userId, attendance)
//        .then(function (result) {
//            if (result) {
//                sendSuccessResponse(res, result);
//            } else {
//                sendFailResponse(res);
//            }
//        })
//        .catch(function (err) {
//            res.status(400).send(err);
//        });
//    });
//};

exports.update = function (req, res) {

    console.log(req.body.studentDTO);
    if (req.body.studentDTO) {

        var studentModel = new _student(req.body.studentDTO);
        var studentId = req.body.studentDTO._id;
        var attendanceId = req.body.studentDTO.attendances[0]._id;
        var loggedInuserId;

        attendanceDal.update(studentModel, studentId, attendanceId, loggedInuserId)
            .then(function (result) {
                if (result) {
                    sendSuccessResponse(res, result);
                } else {
                    sendFailResponse(res);
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
    else {
        sendBadRequestResponse(res);
    }
};

exports.updateAll = function (req, res) {

    if (req.body.data.studentDTO && req.body.data.userId) {

        var studentModel = new student(req.body.data.studentDTO);
        //var studentId = req.body.data.studentDTO._id;
        //var attendanceId = req.body.data.studentDTO.ads[0]._id;
        var userId = req.body.data.userId;

        attendance.updateAll(studentModel, userId)
            .then(function (result) {
                if (result) {
                    sendSuccessResponse(res, result);
                } else {
                    sendFailResponse(res);
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
    else {
        sendBadRequestResponse(res);
    }
};

exports.removeByDate = function (req, res) {

    if (req.body.studentId && req.body.attendanceDate) {

        var studentId = req.body.studentId;
        var attendanceDate = req.body.attendanceDate;

        attendanceDal.removeByDate(studentId, attendanceDate)
            .then(function (result) {
                if (result) {
                    sendSuccessResponse(res);
                } else {
                    sendFailResponse(res);
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
    else {
        sendBadRequestResponse(res);
    }
};

exports.removeById = function (req, res) {

    if (req.body.studentId && req.body.attendanceId) {

        var studentId = req.body.studentId;
        var attendanceId = req.body.attendanceId;

        attendanceDal.removeById(studentId, attendanceId)
            .then(function (result) {
                if (result) {
                    sendSuccessResponse(res);
                } else {
                    sendFailResponse(res);
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
    else {
        sendBadRequestResponse(res);
    }
};

exports.getAttendances = function (req, res) {

    if (req.query.userType && req.query.page && req.query.pageSize) {

        var searchCriteria = {
            userType: (req.query.userType) ? parseInt(req.query.userType) : appDefaults.UserType,
            page: (req.query.page) ? parseInt(req.query.page) : appDefaults.Page,
            pageSize: (req.query.pageSize) ? parseInt(req.query.pageSize) : appDefaults.PageSize,
            orderBy: req.query.orderBy ? req.query.orderBy : appDefaults.OrderBy
        };

        attendance.getAttendances(searchCriteria)
            .then(function (result) {
                if (result) {
                    sendSuccessResponse(res, result);
                } else {
                    sendNoContentResponse(res);
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
    else {
        sendBadRequestResponse(res);
    }
}

exports.searchAttendances = function (req, res) {

    if (req.query.title) {

        var searchCriteria = {
            userType: (req.query.userType) ? req.query.userType : appDefaults.UserType,
            searchText: req.query.title,
            page: (req.query.page) ? parseInt(req.query.page) : appDefaults.Page,
            pageSize: (req.query.pageSize) ? parseInt(req.query.pageSize) : appDefaults.PageSize,
            orderBy: req.query.orderBy ? req.query.orderBy : appDefaults.OrderBy
        };

        attendance.searchAttendances(searchCriteria)
            .then(function (result) {
                if (result) {
                    sendSuccessResponse(res, result);
                } else {
                    sendNoContentResponse(res);
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
    else {
        sendBadRequestResponse(res);
    }
}

exports.getAllAttendances = function (req, res) {

    var searchCriteria = {
        userType: (req.query.userType) ? parseInt(req.query.userType) : appDefaults.UserType,
        page: (req.query.page) ? parseInt(req.query.page) : appDefaults.Page,
        pageSize: (req.query.pageSize) ? parseInt(req.query.pageSize) : appDefaults.PageSize,
    };

    attendanceDal.getAllAttendances(searchCriteria)
        .then(function (result) {
            if (result) {
                sendSuccessResponse(res, result);
            } else {
                sendNoContentResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
}

exports.getAttendanceDetail = function (req, res) {

    if (req.query.userId) {

        var userId = req.query.userId;

        attendanceDal.getAttendanceDetail(userId)
            .then(function (result) {
                if (result) {
                    sendSuccessResponse(res, result);
                } else {
                    sendNoContentResponse(res);
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
    else {
        sendBadRequestResponse(res);
    }
}

exports.getAttendancesByUserId = function (req, res) {

    var userId = req.query.userId;
    var page = req.query.page;
    if (page)
        page = appDefaults.Page;
    page = page - 1;

    var pageSize = req.query.pageSize;
    if (!pageSize)
        pageSize = appDefaults.PageSize;

    attendanceDal.getAttendancesByUserId(userId, page, pageSize)
        .then(function (user) {
            if (user) {
                res.send({
                    status: statusCode.Success.text,
                    data: user,
                    numpage: (!user.organs) ? 0 : Math.ceil(parseFloat(user.organs.length / pageSize)) + 1,
                    statusCode: statusCode.Success.value
                });
            } else {
                sendNoContentResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
}