﻿var mongoose = require('mongoose');

var feesRegisterSchema = new mongoose.Schema({

    receiptNumber: String,
    receivedDate: Date,

    class: String,
    tutorFee: Number,
    formFee: Number,
    libraryFee: Number,
    labFee: Number,
    developmentFee: Number,
    computerFee: Number,
    sportFee: Number,
    dueDate: Date,
    lateFee: Number,

    createdBy: {
        type: mongoose.Schema.Types.ObjectId
    },
    createdDate: {
        type: Date
    },
    updatedBy: {
        type: mongoose.Schema.Types.ObjectId
    },
    updatedDate: Date
});

mongoose.model('fee', feesRegisterSchema);