﻿var expenseDal = require('../../../server/school.dal/expense.dal.js');
require('../../school.common/helpers/responseHelper');

var mongoose = require('mongoose'),
    //fs = require('fs-extra'),
    //util = require("util"),
//path = require("path");

var _expense = mongoose.model('expense');
var _user = mongoose.model('user');

exports.addExpense = function (req, res) {

    var expenseDTO = req.body.expenseDTO;

    expenseTimeTableDal.add(expenseDTO)
        .then(function (result) {
            if (result) {
                sendSuccessResponse(res, result);
            } else {
                sendErrorResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
}

exports.updateExpense = function (req, res) {
    var expenseDTO = req.body.expenseDTO;

    expenseTimeTableDal.getUserById(expenseDTO.Id)
        .then(function (expenseModel) {

            if (expenseModel) {
                if (expenseModel.Id > 0) {
                    if (expenseModel.password == expenseDTO.password) {

                        //update profile photo TODO
                        expenseModel.title = expenseDTO.title;
                        expenseModel.expenseCategoryId = expenseDTO.expenseCategoryId;
                        expenseModel.description = expenseDTO.description;
                        expenseModel.method = expenseDTO.method;
                        expenseModel.amount = expenseDTO.amount;
                        //address TODO

                        expenseTimeTableDal.update(expenseModel, userId)
                            .then(function (user) {

                                if (user) {
                                    sendSuccessResponse(res, result, messages.UpdateSuccess);
                                }
                                else {
                                    sendFailResponse(res, messages.PasswordIncorrect);
                                }
                            })
                            .catch(function (err) {
                                console.log(err);
                                sendErrorResponse(res);
                            });
                    }
                }
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
};

exports.deleteExpense = function (req, res) {
    var expenseModel = req.body.expenseModel;
    var expenseId = req.body.expenseId;

    expenseDal.deleteExpense(expenseModel, expenseId)
        .then(function (result) {
            if (result) {
                sendSuccessResponse(res, result, messages.DeleteSuccess);
            } else {
                sendFailResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
};