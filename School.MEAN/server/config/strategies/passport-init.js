var mongoose = require('mongoose'),
    bCrypt = require('bcrypt-nodejs'),
    path = require('path'),
    baseConfig = require('../../config/base.config');

var User = mongoose.model('user');
var LocalStrategy = require('passport-local').Strategy;

module.exports = function (passport) {

    // Passport needs to be able to serialize and deserialize users to support persistent login sessions
    //Passport also needs to serialize and deserialize user instance from a session store in order to support login sessions, 
    //so that every subsequent request will not contain the user credentials.
    passport.serializeUser(function (user, done) {
        console.log('serializing user:', user.username);
        done(null, user._id);
    });

    passport.deserializeUser(function (id, done) {
        User.findById(id, function (err, user) {
            console.log('deserializing user:', user.username);
            done(err, user);
        });
    });

    // Initialize strategies
    //baseConfig.getGlobbedFiles('./config/strategies/*.js').forEach(function (strategy) {
    //    require(path.resolve(strategy))();
    //});

    //while running app
    //require(path.resolve('./config/strategies/basic'))();
    require('./local');

    //while running test cases
    //require(path.resolve('../config/strategies/basic'))();
    //require(path.resolve('../config/strategies/local'))();

    var isValidPassword = function (user, password) {
        //return bCrypt.compareSync(password, user.password);
        return password == user.password;
    };

    //The strategy we�ll use is called �local�, as it uses a username and password stored locally.
    passport.use('local', new LocalStrategy({
        // First, we�re going to need a form that sends an HTTP-POST request to our login route with a username field and a password field
        //�if they�re named incorrectly, the form will not work.
        usernameField: 'username',
        passwordField: 'password',
        ////The passReqToCallback config variable allows us to access the request object in the callback, thereby enabling us to use any parameter associated with the request.
        passReqToCallback: true
    },
        function (req, username, password, done) {
            // check in mongo if a user with username exists or not
            User.findOne({ 'username': username },
                function (err, user) {
                    //To specify failure 
                    //      either the first parameter should contain the error, or 
                    //      the second parameter should evaluate to false.
                    // In case of any error, return using the done method
                    if (err)
                        return done(err);
                    // Username does not exist, log the error and redirect back
                    if (!user) {
                        console.log('User Not Found with username ' + username);
                        return done(null, false);
                    }
                    // User exists but wrong password, log the error 
                    if (!isValidPassword(user, password)) {
                        console.log('Invalid Password');
                        return done(null, false); // redirect back to login page
                    }
                    // User and password both match, return user from done method
                    // which will be treated like success
                    //To signify success the first parameter should be null and the second parameter should evaluate to a truthy value, 
                    //in which case it will be made available on the request object
                    return done(null, user);
                }
            );
        }
    ));

    /*
    // Generates hash using bCrypt
    var createHash = function (password) {
        return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
    };

    passport.use('signup', new LocalStrategy({
        passReqToCallback : true // allows us to pass back the entire request to the callback
    },
        function (req, username, password, done) {
        
        // find a user in mongo with provided username
        User.findOne({ 'username' : username }, function (err, user) {
            // In case of any error, return using the done method
            if (err) {
                console.log('Error in SignUp: ' + err);
                return done(err);
            }
            // already exists
            if (user) {
                console.log('User already exists with username: ' + username);
                return done(null, false);
            } else {
                // if there is no user, create the user
                var newUser = new User();
                
                // set the user's local credentials
                newUser.username = username;
                newUser.password = createHash(password);
                
                // save the user
                newUser.save(function (err) {
                    if (err) {
                        console.log('Error in Saving user: ' + err);
                        throw err;
                    }
                    console.log(newUser.username + ' Registration succesful');
                    return done(null, newUser);
                });
            }
        });
    })
    );
    */
};
