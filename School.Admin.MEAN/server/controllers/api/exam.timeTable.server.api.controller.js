﻿var examTimeTableDal = require('../../../server/school.dal/exam.TimeTable.dal.js');
require('../../school.common/helpers/responseHelper');

var mongoose = require('mongoose'),
    moment = require("moment");
//fs = require('fs-extra'),
//util = require("util"),
//path = require("path");

var _examTimeTable = mongoose.model('examTimeTable');
var _user = mongoose.model('user');

function formatServerDate(examDTO) {
    if (examDTO.timeTableList) {
        for (i = 0; i < examDTO.timeTableList.length; i++) {
            if (examDTO.timeTableList[i].examDate) {
                console.log(examDTO.timeTableList[i].examDate);
                examDTO.timeTableList[i].examDate = moment(examDTO.timeTableList[i].examDate).format("DD/MM/YYYY");
            }
        }
    }
    return examDTO;
}

function parseClientDate(examDTO) {
    if (examDTO.timeTableList) {
        for (i = 0; i < examDTO.timeTableList.length; i++) {
            if (examDTO.timeTableList[i].examDate) {
                console.log(examDTO.timeTableList[i].examDate);
                examDTO.timeTableList[i].examDate = moment(examDTO.timeTableList[i].examDate, 'DD/MM/YYYY').toDate();
            }
        }
    }
    return examDTO;
}

exports.add = function (req, res) {

    if (req.body.examDTO) {
        var examDTO = req.body.examDTO;
        examDTO = parseClientDate(examDTO);
        var examTimeTableModel = new _examTimeTable(examDTO);

        examTimeTableDal.add(examTimeTableModel)
            .then(function (result) {
                if (result) {
                    sendSuccessResponse(res, result);
                } else {
                    sendErrorResponse(res);
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
    else {
        sendBadRequestResponse(res);
    }
};

exports.update = function (req, res) {

    console.log(req.body);

    if (req.body.examDTO) {

        var examDTO = req.body.examDTO;
        examDTO = parseClientDate(examDTO);
        var loggedInUserId;

        examTimeTableDal.update(examDTO, loggedInUserId)
            .then(function (result) {

                if (result) {
                    sendSuccessResponse(res, result, messages.UpdateSuccess);
                }
                else {
                    sendFailResponse(res, messages.PasswordIncorrect);
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
    else {
        sendBadRequestResponse(res);
    }
};

exports.removeById = function (req, res) {

    console.log(req.query.id);

    if (req.query.id) {
        var timeTableId = req.query.id;

        examTimeTableDal.removeById(timeTableId)
            .then(function (result) {
                if (result) {
                    sendSuccessResponse(res, result, messages.DeleteSuccess);
                } else {
                    sendFailResponse(res);
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
};

exports.getExamTimeTableByUserId = function (req, res) {

    var userId = req.query.userId;

    examTimeTableDal.getExamTimeTableByUserId(userId)
        .then(function (result) {
            if (result) {
                sendSuccessResponse(res, result);
            } else {
                sendNoContentResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
}

exports.getAllExamTimeTableInfo = function (req, res) {

    //sendSuccessResponse(res, JSON.stringify(getSubjectList()));

    examTimeTableDal.getAllExamTimeTableInfo()
        .then(function (result) {
            if (result) {
                for (j = 0; j < result.length; j++) {
                    result[j] = formatServerDate(result[j]);
                }
                sendSuccessResponse(res, result);
            } else {
                sendFailResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
}

exports.getExamTimeTableById = function (req, res) {

    var examTimeTableId = req.query.examTimeTableId;

    examTimeTableDal.getExamTimeTableById(examTimeTableId)
        .then(function (result) {
            if (result) {
                sendSuccessResponse(res, result);
            } else {
                sendNoContentResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
}

exports.getExamTimeTableContactDetailById = function (req, res) {

    var examTimeTableId = req.query.examTimeTableId;

    examTimeTableDal.getExamTimeTableContactDetailById(examTimeTableId)
        .then(function (result) {
            if (result) {
                sendSuccessResponse(res, result);
            } else {
                sendNoContentResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
}

exports.getExamTimeTablesByAnyCriteria = function (req, res) {

    var searchCriteria = {
        userType: (req.query.userType) ? parseInt(req.query.userType) : appDefaults.UserType,
        searchText: req.query.title,

        page: (req.query.page) ? parseInt(req.query.page) : appDefaults.Page,
        pageSize: (req.query.pageSize) ? parseInt(req.query.pageSize) : appDefaults.PageSize,
        orderBy: req.query.orderBy ? req.query.orderBy : appDefaults.OrderBy
    };

    examTimeTableDal.getExamTimeTablesByAnyCriteria(searchCriteria)
        .then(function (result) {
            if (result) {
                sendSuccessResponse(res, result);
            } else {
                sendNoContentResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            return sendErrorResponse(res);
        });
}

exports.count = function (req, res) {

    examTimeTableDal.count()
        .then(function (result) {
            if (result) {
                sendSuccessResponse(res, result);
            } else {
                sendFailResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
};

exports.getExamTimeTableProfileInfo = function (req, res) {

    var userId = req.query.userId;

    examTimeTableDal.getExamTimeTableProfileInfo(userId)
        .then(function (result) {
            if (result) {
                sendSuccessResponse(res, result);
            } else {
                sendNoContentResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
}

exports.getExamTimeTableAvatar = function (req, res) {

    var examTimeTableId = req.query.examTimeTableId;

    examTimeTableDal.getExamTimeTableAvatar(examTimeTableId)
        .then(function (result) {
            if (result) {
                sendSuccessResponse(res, result);
            } else {
                sendFailResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
}

exports.getExamTimeTableByExamTypeAndClass = function (req, res) {

    if (req.query.class && req.query.examType) {
        var className = req.query.class;
        var examType = req.query.examType;
        examTimeTableDal.getExamTimeTableByExamTypeAndClass(examType, className)
            .then(function (result) {
                if (result) {
                    result = formatServerDate(result);
                    sendSuccessResponse(res, result);
                } else {
                    sendNoContentResponse(res);
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
}
