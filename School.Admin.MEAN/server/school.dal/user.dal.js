﻿var Q = require('q');
var mongoose = require('mongoose'),
    _ = require('lodash');
var userServerModel = require('../models/user.server.model');
var reasons = userServerModel.reasons;
var _user = mongoose.model('user');

//require('../models/blockUser.server.model');
//var blockUser = mongoose.model('blockUser');

require('../../server/school.common/helpers/deferredHelper');

var userDal = {};

userDal.add = add;
userDal.createNewUser = createNewUser;
//userDal.blockUser = blockUser;
userDal.update = update;
userDal.getUserByEmail = getUserByEmail;
userDal.getUserByEmailPassword = getUserByEmailPassword;
userDal.getUserById = getUserById;
userDal.getUsernameById = getUsernameById;
userDal.getUserIdByUsername = getUserIdByUsername;
userDal.authenticateUser = authenticateUser;
userDal.authenticateMobileUser = authenticateMobileUser;
userDal.requestMobileVerificationCode = requestMobileVerificationCode;
userDal.checkIfPhoneNumberRegistered = checkIfPhoneNumberRegistered;
userDal.checkUserNameExist = checkUserNameExist;
userDal.checkEmailExist = checkEmailExist;
userDal.removeById = removeById;
userDal.updatePassword = updatePassword;
userDal.updateEmail = updateEmail;
//userDal.mobileRegisterSocialProfile = mobileRegisterSocialProfile;

function add(userModel) {

    var deferred = Q.defer();

    userModel
        .save(function (err, result) {
            if (err) deferred.reject(err);

            if (result) {
                deferred.resolve(result);
            } else {
                deferred.resolve();
            }
        });

    return deferred.promise;
}

function createNewUser(userModel) {

    var deferred = Q.defer();

    userModel
        .save(function (err, result) {
            if (err) deferred.reject(err);

            if (result) {
                deferred.resolve(result);
            } else {
                deferred.resolve();
            }
        });

    return deferred.promise;
}

//function blockUser(blockUserModel) {

//    var deferred = Q.defer();

//    blockUserModel
//        .save(function (err, result) {
//            if (err) deferred.reject(err);

//            if (result) {
//                deferred.resolve(result);
//            } else {
//                deferred.resolve();
//            }
//        });

//    return deferred.promise;
//}

function update(userUpdateCondition, userUpdateSet) {

    var deferred = Q.defer();

    _user.findOneAndUpdate(userUpdateCondition, { $set: userUpdateSet }, { new: true }, function (err, user) {
        if (err) {
            deferred.reject(err);
        }
        if (user) {
            user = _.pick(user.toObject(), ['_id', 'username', 'email', 'isProfileIncomplete', 'firstName', 'phoneNumber', 'province', 'city',
                'address', 'pinCode', 'countryDialCode', 'avatarUrl', 'exams']);
            deferred.resolve(user);
        } else {
            deferred.resolve();
        }
    });
    return deferred.promise;
}

function removeById(userId) {

    var deferred = Q.defer();

    _user.findByIdAndRemove({ _id: userId }, function (err) {
        if (err) {
            deferred.reject(err);
        }
        else {
            console.log('user successfully deleted!');
            deferred.resolve(true);
        }
    });
    return deferred.promise;
}

function getUserByEmail(email) {

    var deferred = Q.defer();

    _user
        .findOne({ 'email': email })
        .select('-password ')
        .exec(function (err, result) {
            execHelper(deferred, err, result);
        });
    return deferred.promise;
}

function getUserByEmailPassword(email, password) {

    var deferred = Q.defer();


    _user
        .findOne({
            $and: [
                { 'email': email },
                { 'password': password }
            ]
        })
        .exec(function (err, result) {
            execHelper(deferred, err, result);
        });
    return deferred.promise;
}

function getUserById(userId) {

    var deferred = Q.defer();

    _user
        .findOne({ '_id': userId })
        .select('_id firstName phoneNumber province city address pinCode loc email userType exams')
        //.lean()
        .exec(function (err, result) {
            execHelper(deferred, err, result);
        });
    return deferred.promise;
}

function getUsernameById(userId) {

    var deferred = Q.defer();

    _user
        .findOne({ '_id': userId })
        .select('username')
        .exec(function (err, result) {
            execHelper(deferred, err, result);
        });
    return deferred.promise;
}

function getUserIdByUsername(username) {

    var deferred = Q.defer();

    _user
        .findOne({ 'username': username })
        .select('_id')
        .exec(function (err, result) {
            execHelper(deferred, err, result);
        });
    return deferred.promise;
}

function authenticateUser(username, password, cb) {
    //var deferred = Q.defer();

    _user.findOne({ username: username }, function (err, user) {
        if (err) {
            console.log(err);
            cb(err);
        }

        // make sure the user exists
        if (!user) {
            return cb(null, null, 0);
        }

        // check if the account is currently locked
        //if (user.isLocked) {
        //    // just increment login attempts if account is already locked
        //    return user.incrementLoginAttempts(function (err) {
        //        if (err) cb(err);
        //        return cb(null, null, 2);
        //    });
        //}

        // test for a matching password
        user.comparePassword(password, function (err, isMatch) {
            if (err) {
                console.log(err);
                cb(err);
            }
            // check if the password was a match
            if (isMatch) {
                return cb(null, user);

                // if there's no lock or failed attempts, just return the user
                //if (!user.loginAttempts && !user.lockUntil) return cb(null, user);
                //// reset attempts and lock info
                //var updates = {
                //    $set: { loginAttempts: 0 },
                //    $unset: { lockUntil: 1 }
                //};
                //return user.update(updates, function (err) {
                //    if (err) return cb(err);
                //    return cb(null, user);
                //});
            }
            else {
                return cb(null, null, 1);
            }

            // password is incorrect, so increment login attempts before responding
            //user.incrementLoginAttempts(function (err) {
            //    if (err) return cb(err);
            //    return cb(null, null, 1);
            //});
        });
    });

    //return deferred.promise;
}

function authenticateMobileUser(username, password) {
    var deferred = Q.defer();

    _user.findOne({ username: username }, function (err, user) {
        if (err) {
            console.log(err);
            deferred.reject(err);
        }
        // make sure the user exists
        if (!user) {
            return deferred.resolve({
                status: statusCode.Error.text,
                data: "",
                message: messages.InvalidLoginCredentials,
                statusCode: statusCode.Error.value
            });
        }

        // check if the account is currently locked
        //if (user.isLocked) {
        //    // just increment login attempts if account is already locked
        //    return user.incrementLoginAttempts(function (err) {
        //        if (err) deferred.reject(err);
        //        return deferred.resolve({
        //            status: statusCode.Error.text,
        //            data: "",
        //            message: messages.MaxLoginAttempts,
        //            statusCode: statusCode.Error.value
        //        });
        //    });
        //}

        // test for a matching password
        user.comparePassword(password, function (err, isMatch) {
            if (err) {
                console.log(err);
                deferred.reject(err);
            }

            // check if the password was a match
            if (isMatch) {
                // if there's no lock or failed attempts, just return the user
                //if (!user.loginAttempts && !user.lockUntil) {
                return deferred.resolve({
                    status: statusCode.Success.text,
                    data: user,
                    message: messages.LoggedInSuccess,
                    statusCode: statusCode.Success.value
                });
                //}
                // reset attempts and lock info
                //var updates = {
                //    $set: { loginAttempts: 0 },
                //    $unset: { lockUntil: 1 }
                //};
                //return user.update(updates, function (err) {
                //    if (err) deferred.reject(err);
                //    return deferred.resolve({
                //        status: statusCode.Success.text,
                //        data : user,
                //        message : messages.LoggedInSuccess,
                //        statusCode: statusCode.Success.value
                //    });
                //});
            }

            // password is incorrect, so increment login attempts before responding
            //user.incrementLoginAttempts(function (err) {
            //    if (err) deferred.reject(err);
            else {
                return deferred.resolve({
                    status: statusCode.Error.text,
                    data: "",
                    message: messages.InvalidLoginCredentials,
                    statusCode: statusCode.Error.value
                });
            }
            //});
        });
    });

    return deferred.promise;
}

//function getDonorByUserId(userId) {

//    var donorDal = require('../school.dal/donor.dal.js');
//    var deferred = Q.defer();

//    donorDal.getDonorByUserId(userId)
//    .then(function (result) {
//        if (result) {
//            deferred.resolve(result);
//        } else {
//            deferred.resolve();
//        }
//    })
//    .catch(function (err) {
//        deferred.reject(err);
//    });
//    return deferred.promise;
//}

function getRecipientByUserId(userId) {

    var recipientDal = require('../school.dal/recipient.dal.js');
    var deferred = Q.defer();

    recipientDal.getRecipientByUserId(userId)
        .then(function (result) {
            if (result) {
                deferred.resolve(result);
            } else {
                deferred.resolve();
            }
        })
        .catch(function (err) {
            console.log(err);
            deferred.reject(err);
        });
    return deferred.promise;
}

//TODO requestMobileVerificationCode
function requestMobileVerificationCode(mobileVerificationModel) {

}

//TODO checkIfPhoneNumberRegistered
function checkIfPhoneNumberRegistered(simCountryISO2, phoneNumber, deviceIdentityNumber, languageG, LanguageC) {

}

function checkUserNameExist(username) {

    var deferred = Q.defer();

    _user
        .findOne({ 'username': username })
        .exec(function (err, doc) {

            if (err) {
                console.log(err);
                deferred.reject(err);
            }
            if (doc) {
                deferred.resolve(true);
            } else {
                deferred.resolve();
            }
        });
    return deferred.promise;
}

function checkEmailExist(email) {

    var deferred = Q.defer();

    _user
        .findOne({ 'email': email })
        .exec(function (err, doc) {

            if (err) {
                console.log(err);
                deferred.reject(err);
            }
            if (doc) {
                deferred.resolve(true);
            } else {
                deferred.resolve();
            }
        });
    return deferred.promise;
}

//function mobileRegisterSocialProfile(userModel) {

//    var deferred = Q.defer();
//    userModel.createdDate = new Date();

//    this.add(userModel)
//        .then(function (user) {

//            if (user) {
//                var user = _.pick(user.toObject(), ['_id', 'province', 'city', 'address', 'pinCode', 'avatarUrl', 'exams']);
//                deferred.resolve(user);
//            } else {
//                deferred.resolve();
//            }
//        })
//        .catch(function (err) {
//            if (err) deferred.reject(err);
//        });

//    return deferred.promise;
//}

function updatePassword(updateProfileModel, newPassowrd) {

    var deferred = Q.defer();
    //TODO password check add future
    _user.findOne({ _id: updateProfileModel._id, password: updateProfileModel.password }, function (err, user) {
        if (err) {
            console.log(err);
            deferred.reject(err);
        }
        if (user) {
            user.password = newPassowrd;
            user.updatedBy = updateProfileModel._id;
            user.updatedDate = new Date();

            user.save(function (err, result) {
                if (err) {
                    console.log(err);
                    deferred.reject(err);
                }
                if (result) {
                    result = _.pick(result.toObject(), ['_id', 'username', 'email', 'password', 'isProfileIncomplete', 'firstName', 'phoneNumber', 'province',
                        'city', 'address', 'pinCode', 'countryDialCode', 'avatarUrl', 'exams']);
                    deferred.resolve(result);
                } else {
                    deferred.resolve();
                }
            });
        }
    });
    return deferred.promise;
}

function updateEmail(updateProfileModel) {

    var deferred = Q.defer();
    //TODO password check add future
    _user.findOne({ _id: updateProfileModel._id, password: updateProfileModel.password }, function (err, user) {
        if (err) {
            console.log(err);
            deferred.reject(err);
        }
        if (user) {
            user.email = updateProfileModel.email ? updateProfileModel.email : user.email;
            user.updatedBy = updateProfileModel._id;
            user.updatedDate = new Date();

            user.save(function (err, result) {
                if (err) {
                    console.log(err);
                    deferred.reject(err);
                }
                if (result) {
                    result = _.pick(result.toObject(), ['_id', 'username', 'email', 'password', 'isProfileIncomplete', 'firstName', 'phoneNumber', 'province',
                        'city', 'address', 'pinCode', 'countryDialCode', 'avatarUrl', 'exams']);
                    deferred.resolve(result);
                } else {
                    deferred.resolve();
                }
            });
        }
    });
    return deferred.promise;
}

module.exports = userDal;