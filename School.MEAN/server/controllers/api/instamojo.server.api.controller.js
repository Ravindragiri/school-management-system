﻿config = require('./server/config/config');
require('../../school.common/helpers/responseHelper');

var headers = { 'X-Api-Key': config.instamojoApiKey, 'X-Auth-Token': config.instamojoAuthToken }

var payload = {
    purpose: 'FIFA',
    amount: '250',
    phone: '9999999999',
    buyer_name: 'Goswami Ravindragiri',
    redirect_url: config.hostUrl + '/payment/success/',
    send_email: true,
    webhook: '',
    send_sms: true,
    email: 'foo@example.com',
    allow_repeated_payments: false
}

isSaltProvided = function (candidateSalt) {
    //return config.instamojoSalt == candidateSalt;
    return true;
}

exports.webhook = function (req, res) {

    if (req.body.payment_id && req.body.payment_request_id && isSaltProvided()) {
        if (req.body.status == 'Completed') {
            //TO DO: save to db get code from web
        }
    }
    else {
        console.log(req.body);
    }
}

exports.createRequest = function (req, res) {

    request.post(config.instamojoBaseUrl + '/api/1.1/payment-requests/', { form: payload, headers: headers }, function (error, response, body) {
        if (error) {
            console.log(err);
            sendErrorResponse(res);
        }
        else {
            //201 Created
            if (response.statusCode == 201) {
                console.log(body);
                sendSuccessResponse(res, body);
            }
        }
        console.log(body);
        sendFailResponse(res, body);
    })
}

exports.getListOfRequests = function (req, res) {

    request.get(config.instamojoBaseUrl + '/api/1.1/payment-requests/', { headers: headers }, function (error, response, body) {

        if (error) {
            console.log(err);
            sendErrorResponse(res);
        }
        else {
            //200 OK
            if (response.statusCode == 200) {
                console.log(body);
                sendSuccessResponse(res, body);
            }
        }
        console.log(body);
        sendFailResponse(res, body);
    })
}

function getPaymentRequestDetails(paymentRequestId) {

    request.get(config.instamojoBaseUrl + '/api/1.1/payment-requests/' + paymentRequestId + '/', { headers: headers }, function (error, response, body) {
        if (error) {
            console.log(err);
            sendErrorResponse(res);
        }
        else {
            //200 OK
            if (response.statusCode == 200) {
                console.log(body);
                sendSuccessResponse(res, body);
            }
        }
        console.log(body);
        sendFailResponse(res, body);
    })
}

//A payment ID is a 20 character string prefixed with MOJO
function getPaymentDetails(paymentId) {

    request.get(
        config.instamojoBaseUrl + '/api/1.1/payments/' + paymentId + '/',
        { form: payload, headers: headers }, function (error, response, body) {
            //200 OK
            if (!error && response.statusCode == 200)
            { console.log(body) }
            //401 Unauthorized
            if (!error && response.statusCode == 401)
            { console.log(body) }
        })
}


