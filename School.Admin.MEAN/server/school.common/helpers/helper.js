﻿global.getSubjectList = function () {
    return [
        {
            Id: 1,
            code: "Subject 1",
            desc: "Subject 1"
        },
        {
            Id: 2,
            code: "Subject 2",
            desc: "Subject 2"
        }
    ]
};

global.getProfileVisitors = function () {
    return [
        {
            Id: 1, 
            ProfileId: 1, 
            VisitorId: 2,
            Name: "Name1",
            PhoneNumber: "12345678"
        },
        {
            Id: 1, 
            ProfileId: 1, 
            VisitorId: 3,
            Name: "Name2",
            PhoneNumber: "11111111"
        }
    ]
};

global.getMessages = function () {
    var date = new Date();
    
    return [
        {
            Id : 1,
            From: 2,
            To: 1,
            Contents : "Message 1",
            Sent: date
        },
        {
            Id : 1,
            From: 3,
            To: 1,
            Contents : "Message 2",
            Sent: date.getDate() - 1
        }
    ]
};

global.getAllCountries = function () {
    var countries = require('country-data').countries;
    countries.all.select('alpha2 alpha3 name');
};