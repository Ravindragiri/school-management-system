﻿var Q = require('q');
var mongoose = require('mongoose');

require('../models/fee.server.model');
require('../models/user.server.model');

var userDal = require('../../server/school.dal/user.dal.js');

var _classFees = mongoose.model('fee');
var _user = mongoose.model('user');

require('../../server/school.common/helpers/deferredHelper');

var dal = {};

dal.add = add;
dal.update = update;
dal.deleteById = deleteById;
dal.getById = getById;
dal.getByClass = getByClass;
dal.getAll = getAll;

module.exports = dal;

function add(classFeeDTO) {

    var classFeeModel = new _classFees(classFeeDTO);

    var deferred = Q.defer();

    classFeeModel
        .save(function (err, result) {
            if (err) deferred.reject(err);

            if (result) {
                deferred.resolve(result);
            } else {
                deferred.resolve();
            }
        });

    return deferred.promise;
}

function update(feeDTO, userId) {

    var deferred = Q.defer();
    _classFees.findById(feeDTO._id, function (err, fee) {
        if (err) throw err;

        if (fee) {
            try {

                fee.class = feeDTO.class ? feeDTO.class : fee.class;
                fee.tutorFee = feeDTO.tutorFee ? feeDTO.tutorFee : fee.tutorFee;
                fee.formFee = feeDTO.formFee ? feeDTO.formFee : fee.formFee;
                fee.libraryFee = feeDTO.libraryFee ? feeDTO.libraryFee : fee.libraryFee;
                fee.labFee = feeDTO.labFee ? feeDTO.labFee : fee.labFee;

                fee.developmentFee = feeDTO.developmentFee ? feeDTO.developmentFee : fee.developmentFee;
                fee.computerFee = feeDTO.computerFee ? feeDTO.computerFee : fee.computerFee;
                fee.sportFee = feeDTO.sportFee ? feeDTO.sportFee : fee.sportFee;
                fee.dueDate = feeDTO.dueDate ? feeDTO.endDate : fee.dueDate;
                fee.lateFee = feeDTO.lateFee ? feeDTO.lateFee : fee.lateFee;

                //fee.updatedBy = userId;
                //fee.updatedDate = new Date();

                fee.save(function (err, result) {
                    if (err) {
                        console.log(err);
                        deferred.reject(err);
                    }
                });
            }
            catch (error) {
                console.log(error);
            }
        }
    });
    return deferred.promise;
}

function deleteById(classFeeId) {

    var deferred = Q.defer();

    _classFees.findByIdAndRemove({ _id: classFeeId }, function (err) {
        if (err) {
            deferred.reject(err);
        }
        else {
            console.log('classFee successfully deleted!');
            deferred.resolve(true);
        }
    });
    return deferred.promise;
}

function getById(classFeeId) {

    var deferred = Q.defer();

    _classFees
        .findOne({ '_id': classFeeId })
        .exec(function (err, result) {
            execHelper(deferred, err, result);
        });
    return deferred.promise;
}

function getByClass(className) {

    var deferred = Q.defer();

    _classFees
        .findOne({ 'class': className })
        .exec(function (err, result) {
            execHelper(deferred, err, result);
        });
    return deferred.promise;
}

function getAll() {

    var deferred = Q.defer();

    _classFees
        .find({})
        .exec(function (err, result) {
            execHelper(deferred, err, result);
        });
    return deferred.promise;
}
