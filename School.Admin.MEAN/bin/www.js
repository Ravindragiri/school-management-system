﻿var debug = require('debug')('School.MEAN');
var app = require('../server');

//app.set('port', process.env.PORT || 3000);
app.set('port', 1238);

var server = app.listen(app.get('port'), function() {
    debug('Express (Admin) server listening on port ' + server.address().port);
    console.log('Express (Admin) server listening on port ' + server.address().port);
});
