﻿module.exports = function (app) {

    var officeStaffController = require('../controllers/officeStaff.server.controller');
    app.get('/officeStaff/index', officeStaffController.index);
    app.get('/officeStaff', officeStaffController.index);
    app.get('/officeStaff/register', officeStaffController.renderRegister);
    //app.get('/officeStaff/profile', officeStaffController.renderProfile);
    app.get('/officeStaff/myProfile', officeStaffController.renderMyProfile);

    var officeStaffApiController = require('../controllers/api/officeStaff.server.api.controller');
    //post
    app.post('/api/officeStaff/insert', officeStaffApiController.insert);
    app.post('/api/officeStaff/mobileRegister', officeStaffApiController.mobileRegister);
    app.post('/api/officeStaff/update', officeStaffApiController.update);
    app.post('/api/officeStaff/remove', officeStaffApiController.remove);
    app.post('/api/officeStaff/updateProfile', officeStaffApiController.updateProfile);
    //get
    app.get('/api/officeStaff/getOfficeStaffContactDetailById', officeStaffApiController.getOfficeStaffContactDetailById);
    app.get('/api/officeStaff/getOfficeStaffsByAnyCriteria', officeStaffApiController.getOfficeStaffsByAnyCriteria);
    app.get('/api/officeStaff/getOfficeStaffById', officeStaffApiController.getOfficeStaffById);
    app.get('/api/officeStaff/getAllOfficeStaffInfo', officeStaffApiController.getAllOfficeStaffInfo);
    app.get('/api/officeStaff/getOfficeStaffByUserId', officeStaffApiController.getOfficeStaffByUserId);
    //app.get('/api/officeStaff/searchOfficeStaffs', officeStaffApiController.searchOfficeStaffs);
    app.get('/api/officeStaff/getOfficeStaffAvatar', officeStaffApiController.getOfficeStaffAvatar);
    app.get('/api/officeStaff/count', officeStaffApiController.count);
    app.get('/api/officeStaff/getOfficeStaffProfileInfo', officeStaffApiController.getOfficeStaffProfileInfo);
    app.get('/api/officeStaff/getAllOfficeStaffInfo', officeStaffApiController.getAllOfficeStaffInfo);
};