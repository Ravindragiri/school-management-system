﻿var officeStaffDal = require('../../../server/school.dal/officeStaff.dal.js');
require('../../school.common/helpers/responseHelper');


var mongoose = require('mongoose'),
    //fs = require('fs-extra'),
    //util = require("util"),
    formidable = require('formidable'),
    moment = require("moment"),
    async = require('async');
//path = require("path");

var _officeStaff = mongoose.model('officeStaff');
var _user = mongoose.model('user');

function formatServerDate(candidateDTO) {
    if (candidateDTO.birthDate) {
        candidateDTO.birthDate = moment(candidateDTO.birthDate).format("DD/MM/YYYY");
    }
    return candidateDTO;
}

function parseClientDate(candidateDTO) {
    if (candidateDTO.birthDate) {
        candidateDTO.birthDate = moment(candidateDTO.birthDate, 'DD/MM/YYYY').toDate();
    }
    return candidateDTO;
}

exports.insert = function (req, res) {

    if (req.body.officeStaffDTO) {
        var userDal = require('../../school.dal/user.dal.js');
        var officeStaffDTO = req.body.officeStaffDTO;
        officeStaffDTO = parseClientDate(officeStaffDTO);
        var officeStaffModel = new _officeStaff(officeStaffDTO);
        var userModel = new _user(officeStaffDTO.userRef);

        async.waterfall(
            [
                function (callback) {
                    userDal.checkUserNameExist(userModel.username)
                        .then(function (result) {
                            if (result) {
                                sendFailResponse(res, messages.UsernameNotAvailable);
                            }
                            else {
                                callback(null);
                            }
                        });
                },
                function (callback) {
                    userDal.checkEmailExist(userModel.email)
                        .then(function (result) {
                            if (result) {
                                sendFailResponse(res, messages.EmailNotAvailable);
                            }
                            else {
                                callback(null);
                            }
                        });
                },
                function (callback) {

                    //userModel.createdDate = new Date();

                    officeStaffDal.insert(officeStaffModel, userModel)
                        .then(function (user) {
                            if (user) {
                                sendSuccessResponse(res, user, messages.UserRegisteredSuccessfully);
                                //uploadAvatar(files, user._id.toString());
                            } else {
                                sendFailResponse(res, messages.UserRegistrationFailed);
                            }
                        })
                        .catch(function (err) {
                            console.log(err);
                            sendErrorResponse(res);
                        });
                }
            ], function (errs, results) {
                if (errs) {
                    console.log(err);
                    sendErrorResponse(res);
                } else {
                    console.log('Done.');
                }
            });
    }
};

exports.mobileRegister = function (req, res) {

    var userDal = require('../../school.dal/user.dal.js');

    var officeStaffModel = new _officeStaff(req.body.data.officeStaffDTO);
    var userModel = new _user(req.body.data.userDTO);

    async.waterfall(
        [
            function (callback) {
                userDal.checkUserNameExist(userModel.username)
                    .then(function (result) {
                        if (result) {
                            sendFailResponse(res, messages.UsernameNotAvailable);
                        }
                        else {
                            callback(null);
                        }
                    });
            },
            function (callback) {
                userDal.checkEmailExist(userModel.email)
                    .then(function (result) {
                        if (result) {
                            sendFailResponse(res, messages.EmailNotAvailable);
                        }
                        else {
                            callback(null);
                        }
                    });
            },
            function (callback) {

                //userModel.createdDate = new Date();

                officeStaffDal.mobileRegister(userModel)
                    .then(function (user) {
                        if (user) {
                            sendSuccessResponse(res, user, messages.UserRegisteredSuccessfully);
                            //uploadAvatar(files, user._id.toString());
                        } else {
                            sendFailResponse(res, messages.UserRegistrationFailed);
                        }
                    })
                    .catch(function (err) {
                        console.log(err);
                        sendErrorResponse(res);
                    });
            }
        ], function (errs, results) {
            if (errs) {
                console.log(err);
                sendErrorResponse(res);
            } else {
                console.log('Done.');
            }
        });

};

uploadAvatar = function (files, userId) {
    if (userId) {
        if (files.image) {
            if (files.image.name && files.image.name.length > 0) {
                var newFileName = userId + path.extname(files.image.name);
                if (!fs.existsSync(path.join(__dirname, util.format('../../../public/uploads/avatar/%s', userId)))) {
                    fs.mkdirSync(path.join(__dirname, util.format('../../../public/uploads/avatar/%s', userId)));
                }
                var tempPath = files.image.path,
                    targetPath = path.join(__dirname, util.format('../../../public/uploads/avatar/%s/%s', userId, newFileName));

                fs.copy(tempPath, targetPath, function (err) {
                    if (err) { console.log(err); }
                    else { console.log("Upload completed!"); }
                });
            }
        }
    }
}

exports.update = function (req, res) {

    if (req.body.officeStaffDTO) {

        var userDal = require('../../school.dal/user.dal.js');
        var officeStaffDTO = req.body.officeStaffDTO;
        officeStaffDTO = parseClientDate(officeStaffDTO);
        var officeStaffModel = new _officeStaff(officeStaffDTO);
        var updateProfileModel = new _user(officeStaffDTO.userRef);
        var avatarUrl;

        officeStaffDal.update(updateProfileModel, officeStaffModel, avatarUrl)
            .then(function (result) {
                if (result) {
                    sendSuccessResponse(res, result);
                } else {
                    sendFailResponse(res);
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
    else {
        sendBadRequestResponse(res);
    }
};

exports.getOfficeStaffByUserId = function (req, res) {

    if (req.query.userId) {
        var userId = req.query.userId;

        officeStaffDal.getOfficeStaffByUserId(userId)
            .then(function (result) {
                if (result) {
                    sendSuccessResponse(res, result);
                } else {
                    sendNoContentResponse(res);
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
    else {
        sendBadRequestResponse(res);
    }
}

exports.getAllOfficeStaffInfo = function (req, res) {

    sendSuccessResponse(res, JSON.stringify(getSubjectList()));

    //teacherDal.getAllOfficeStaffInfo()
    //    .then(function (result) {
    //        if (result) {
    //            sendSuccessResponse(res, result);
    //        } else {
    //            sendFailResponse(res);
    //        }
    //    })
    //    .catch(function (err) {
    //        console.log(err);
    //        sendErrorResponse(res);
    //    });
}

exports.getOfficeStaffById = function (req, res) {

    if (req.query.officeStaffId) {

        var officeStaffId = req.query.officeStaffId;

        officeStaffDal.getOfficeStaffById(officeStaffId)
            .then(function (result) {
                if (result) {
                    sendSuccessResponse(res, result);
                } else {
                    sendNoContentResponse(res);
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
    else {
        sendBadRequestResponse(res);
    }
}

exports.getOfficeStaffContactDetailById = function (req, res) {

    if (req.query.officeStaffId) {
        var officeStaffId = req.query.officeStaffId;

        officeStaffDal.getOfficeStaffContactDetailById(officeStaffId)
            .then(function (result) {
                if (result) {
                    sendSuccessResponse(res, result);
                } else {
                    sendNoContentResponse(res);
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
    else {
        sendBadRequestResponse(res);
    }
}

exports.getOfficeStaffsByAnyCriteria = function (req, res) {

    var searchCriteria = {
        userType: (req.query.userType) ? parseInt(req.query.userType) : appDefaults.UserType,
        searchText: req.query.title,

        page: (req.query.page) ? parseInt(req.query.page) : appDefaults.Page,
        pageSize: (req.query.pageSize) ? parseInt(req.query.pageSize) : appDefaults.PageSize,
        orderBy: req.query.orderBy ? req.query.orderBy : appDefaults.OrderBy
    };

    officeStaffDal.getOfficeStaffsByAnyCriteria(searchCriteria)
        .then(function (result) {
            if (result) {
                sendSuccessResponse(res, result);
            } else {
                sendNoContentResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            return sendErrorResponse(res);
        });
}

exports.count = function (req, res) {

    officeStaffDal.count()
        .then(function (result) {
            if (result) {
                sendSuccessResponse(res, result);
            } else {
                sendFailResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
};

exports.getOfficeStaffProfileInfo = function (req, res) {

    if (req.query.userId) {
        var userId = req.query.userId;

        officeStaffDal.getOfficeStaffProfileInfo(userId)
            .then(function (result) {
                if (result) {
                    sendSuccessResponse(res, result);
                } else {
                    sendNoContentResponse(res);
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
    else {
        sendBadRequestResponse(res);
    }
}

exports.getOfficeStaffAvatar = function (req, res) {

    if (req.query.officeStaffId) {

        var officeStaffId = req.query.officeStaffId;

        officeStaffDal.getOfficeStaffAvatar(officeStaffId)
            .then(function (result) {
                if (result) {
                    sendSuccessResponse(res, result);
                } else {
                    sendFailResponse(res);
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
    else {
        sendBadRequestResponse(res);
    }
}

exports.getAllOfficeStaffInfo = function (req, res) {

    //sendSuccessResponse(res, JSON.stringify(getSubjectList()));

    officeStaffDal.getAllOfficeStaffInfo()
        .then(function (result) {
            if (result) {
                sendSuccessResponse(res, result);
            } else {
                sendNoContentResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
}

exports.remove = function (req, res) {

    console.log(req.query);

    if (req.query.id && req.query.userId) {

        var officeStaffId = req.query.id;
        var userId = req.query.userId;

        officeStaffDal.remove(officeStaffId, userId)
            .then(function (result) {
                if (result) {
                    sendSuccessResponse(res, result);
                } else {
                    sendNoContentResponse(res);
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
    else {
        sendBadRequestResponse(res);
    }
}

exports.updateProfile = function (req, res) {

    if (req.body.officeStaffDTO) {

        var userDal = require('../../school.dal/user.dal.js');
        var officeStaffDTO = req.body.officeStaffDTO;
        officeStaffDTO = parseClientDate(officeStaffDTO);
        var officeStaffModel = new _officeStaff(officeStaffDTO);
        var updateProfileModel = new _user(officeStaffDTO.userRef);
        var avatarUrl;

        officeStaffDal.updateProfile(updateProfileModel, officeStaffModel, avatarUrl)
            .then(function (result) {
                if (result) {
                    sendSuccessResponse(res, result);
                } else {
                    sendFailResponse(res);
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
    else {
        sendBadRequestResponse(res);
    }
};