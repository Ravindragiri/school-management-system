﻿/***** angular ngOfficeStaffIndexController starts *****/
var app = angular.module('schoolApp', ['ngTouch']);

app
    .directive("datepicker", function () {
        return {
            restrict: "A",
            require: "ngModel",
            link: function (scope, elem, attrs, ngModelCtrl) {
                var updateModel = function (dateText) {
                    scope.$apply(function () {
                        ngModelCtrl.$setViewValue(dateText);
                    });
                };
                var options = {
                    dateFormat: "dd/mm/yy",
                    onSelect: function (dateText) {
                        updateModel(dateText);
                    }
                };
                elem.datepicker(options);
            }
        }
    })
    .controller('ngOfficeStaffIndexController', ['OfficeStaffService', '$scope', '$http',
    function (OfficeStaffService, $scope, $http) {

        $scope.addMode = true;

        $scope.validateRegisterForm = function () {
            $scope.registrationFormReset = false;
            $scope.showRegisterValidation = function () { return $scope.registrationForm.$invalid; };
        }

        //$scope.reset = function () {
        //    $scope.form = angular.copy(reviewFormFields());
        //    $scope.registrationFormReset = true;
        //    $scope.reviewForm.$setPristine();
        //}

        $scope.register = function () {

            //$scope.validateRegisterForm();
            $("#registrationForm").validate();

            //if ($scope.registrationForm.$valid) {
            if ($('#registrationForm').valid()) {

                $scope.loading = true;

                var postData = {
                    //__RequestVerificationToken: angular.element("input[name='__RequestVerificationToken']").val(),
                    officeStaffDTO: $scope.officeStaffDTO
                };

                $.ajax({
                    type: "POST",
                    url: 'http://localhost:1238/api/officeStaff/insert',
                    data: postData,
                    dataType: "json",
                    accept: 'application/json',
                    beforeSend: function () {

                    },
                    success: function (response) {

                        if (response != null) {
                            var status = response.statusCode;

                            switch (status) {
                                case 1:
                                    //var objTravel = JSON.parse(response.data);
                                    break;
                                case 0:
                                    alert(response.message);
                                    load();
                                    break;
                                case -10:
                                    $scope.showServerErrorMessages(response, response.serverErrors);
                                    break;
                                case -500:
                                    alert(response.message)
                                    //window.location.href = errorPage;
                                    break;
                                default:
                            }
                        }
                    },
                    error: function (xhr, status, errorThrown) {
                        alert("An error occurred while processing your request.");
                    }
                });

                //
            }
        };

        $scope.showServerErrorMessages = function (response, errors) {

            $.each(errors, function (index, error) {

                $scope.$apply(function () {

                    $scope.invalidelements[error.Element] = true;
                    $('label[ng-show="invalidelements.' + error.Element + '"]').css('display', 'inline-block');
                    $('.' + error.Element).focus();
                });
            });
        }

        $scope.deleteOfficeStaff = function (row) {
            if (row.entity) {
                var id = row.entity._id;
                var userId = row.entity.userRef._id;
                OfficeStaffService.delete(id, userId).then(function (response) {
                    if (response != null) {
                        var status = response.statusCode;

                        switch (status) {
                            case 200:
                                alert("Record has been deleted successfully.");
                                break;
                            case 0:
                                alert("Your request cannot be processed now due to some technical issue. please try again later.");
                                break;
                            case -1:
                            case 400:
                            case 401:
                            case 204:
                                alert(response.message);
                                break;
                            default:
                        }
                    }
                });
            }
        };

        $scope.update = function (row) {
            var data = {
                officeStaffDTO: $scope.officeStaffDTO
            }
            OfficeStaffService.update(data).then(function (response) {
                
                if (response != null) {
                    var status = response.statusCode;

                    switch (status) {
                        case 200:
                            $scope.addMode = true;
                            alert("Record has been updated successfully.");
                            break;
                        case 0:
                            alert("Your request cannot be processed now due to some technical issue. please try again later.");
                            break;
                        case -1:
                        case 400:
                        case 401:
                        case 204:
                            alert(response.message);
                            break;
                        default:
                    }
                }
            });
        }

        $scope.cancel = function () {
            $scope.officeStaffDTO = {};
            $scope.addMode = true;
        }

    }]);
/***** angular ngOfficeStaffIndexController ends *****/

(function () {

    angular.module('schoolApp')
        .service('OfficeStaffService', ['$http', OfficeStaffService]);

    function OfficeStaffService($http) {

        var self = this;
        var baseUrl = 'http://localhost:1238/api/officeStaff/getAllOfficeStaffInfo';

        //var objectName = 'products';

        self.readAll = function () {
            return $http({
                method: 'GET',
                url: baseUrl,
                //url: baseUrl + objectName
            }).then(function (response) {
                return response.data;
            });
        };

        self.readOne = function (id) {
            return $http({
                method: 'GET',
                url: baseUrl + objectName + '/' + id
            }).then(function (response) {
                return response.data;
            });
        };

        self.create = function (data) {
            return $http({
                method: 'POST',
                url: baseUrl + objectName,
                data: data,
                params: {
                    returnObject: true
                },
            }).then(function (response) {
                return response.data;
            });
        };

        self.update = function (data) {
            return $http({
                method: 'POST',
                //url: baseUrl + objectName + '/' + id,
                url: 'http://localhost:1238/api/officeStaff/update',
                data: data
            }).then(function (response) {
                return response.data;
            });
        };

        self.delete = function (id, userId) {
            return $http({
                method: 'POST',
                //url: baseUrl + objectName + '/' + id
                url: 'http://localhost:1238/api/officeStaff/remove' + '?id=' + id + '&userId=' + userId
            }).then(function (response) {
                return response.data;
            });
        };

    }
} ());