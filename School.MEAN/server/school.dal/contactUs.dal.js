﻿var Q = require('q');
var mongoose = require('mongoose')
require('../models/contactUs.server.model');
var _contactUs = mongoose.model('contactUs');

var dal = {};
dal.sendMessage = sendMessage;
module.exports = dal;

function sendMessage(contactUsModel) {

    var deferred = Q.defer();

    contactUsModel
        .save(function (err, result) {
            if (err) deferred.reject(err);

            if (result) {
                deferred.resolve(result);
            } else {
                deferred.resolve();
            }
        });

    return deferred.promise;
}