﻿'use strict';

var config = require('../config/config');

var helper = require('../school.common/helpers/helper');

exports.index = function (req, res) {
    res.render('exam/index', {
    });
};

exports.renderStudentExamMarks = function (req, res) {
    res.render('student/marks/index', {
        pTitle: "Manage Student Exam Marks"
    });
};

exports.renderTimeTable = function (req, res) {
    res.render('student/exam/timeTable', {
        pTitle: "Manage Student Exam TimeTable"
    });
};

exports.renderResult = function (req, res) {
    res.render('student/exam/result', {
    });
};



