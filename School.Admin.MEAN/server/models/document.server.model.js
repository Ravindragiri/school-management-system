﻿var mongoose = require('mongoose');

var documentSchema = new mongoose.Schema({

    title: String,
    desc: String,
    fileName: String,
    fileType: String,
    createdBy: {
        type: mongoose.Schema.Types.ObjectId
    },
    createdDate: {
        type: Date
    }
});

mongoose.model('document', documentSchema);