﻿var examDal = require('../../../server/school.dal/exam.dal.js');
require('../../school.common/helpers/responseHelper');

var mongoose = require('mongoose'),
    //fs = require('fs-extra'),
    //util = require("util"),
//path = require("path");

var _exam = mongoose.model('exam');
var _user = mongoose.model('user');

exports.getClassSection = function (req, res) {

    var classId = req.query.classId;

    _exam.getClassSection(classId)
        .then(function (result) {
            if (result) {
                sendSuccessResponse(res, result);
            } else {
                sendNoContentResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
}

exports.getClassSubject = function (req, res) {

    var classId = req.query.classId;

    _exam.getClassSubject(classId)
        .then(function (result) {
            if (result) {
                sendSuccessResponse(res, result);
            } else {
                sendNoContentResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
}

exports.getClassStudent = function (req, res) {

    var classId = req.query.classId;

    _exam.getClassSubject(classId)
        .then(function (result) {
            if (result) {
                sendSuccessResponse(res, result);
            } else {
                sendNoContentResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
}

exports.addClassTimeTable = function (req, res) {

    var classTimeTableDTO = req.body.classTimeTableDTO;

    examTimeTableDal.add(classTimeTableDTO)
        .then(function (result) {
            if (result) {
                sendSuccessResponse(res, result);
            } else {
                sendErrorResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
}

exports.updateClassTimeTable = function (req, res) {
    var classTimeTableDTO = req.body.classTimeTableDTO;

    examTimeTableDal.getUserById(classTimeTableDTO.Id)
        .then(function (classTimeTableModel) {

            if (classTimeTableModel) {
                if (classTimeTableModel.Id > 0) {
                    if (classTimeTableModel.password == classTimeTableDTO.password) {

                        //update profile photo TODO
                        classTimeTableModel.firstName = classTimeTableDTO.firstName;
                        classTimeTableModel.lastName = classTimeTableDTO.lastName;
                        classTimeTableModel.phoneNumber = classTimeTableDTO.phoneNumber;
                        //address TODO

                        examTimeTableDal.update(classTimeTableModel, userId)
                            .then(function (user) {

                                if (user) {
                                    sendSuccessResponse(res, result, messages.UpdateSuccess);
                                }
                                else {
                                    sendFailResponse(res, messages.PasswordIncorrect);
                                }
                            })
                            .catch(function (err) {
                                console.log(err);
                                sendErrorResponse(res);
                            });
                    }
                }
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
};

exports.deleteClassTimeTable = function (req, res) {
    var classTimeTableModel = req.body.classTimeTableModel;
    var classTimeTableId = req.body.classTimeTableId;

    classTimeTableDal.deleteClassTimeTable(classTimeTableModel, classTimeTableId)
        .then(function (result) {
            if (result) {
                sendSuccessResponse(res, result, messages.DeleteSuccess);
            } else {
                sendFailResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
};
