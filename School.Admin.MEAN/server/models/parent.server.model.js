﻿var mongoose = require('mongoose');

var parentSchema = new mongoose.Schema({

    userRef: { type: mongoose.Schema.Types.ObjectId, ref: 'user', required: true },
    studentRef: { type: mongoose.Schema.Types.ObjectId, ref: 'student' },

    fatherName: String,
    fatherOccupation: String,
    fatherContactNumber: String,
    fatherEmail: String,

    motherName: String,
    motherOccupation: String,
    motherContactNumber: String,
    motherEmail: String,

    isActive: Boolean,
    isDeleted: Boolean,

    createdBy: {
        type: mongoose.Schema.Types.ObjectId
    },
    createdDate: {
        type: Date
    },
    updatedBy: {
        type: mongoose.Schema.Types.ObjectId
    },
    updatedDate: Date

});

mongoose.model('parent', parentSchema);