﻿module.exports = function (app) {

    var holidayController = require('../controllers/holiday.server.controller');
    app.get('/holiday/index', holidayController.index);
    app.get('/holiday', holidayController.index);

    var holidayApiController = require('../controllers/api/holiday.server.api.controller');
    //get
    app.get('/api/holiday/getById', holidayApiController.getById);
    //app.get('/api/holiday/count', holidayApiController.count);
    app.get('/api/holiday/getAll', holidayApiController.getAll);
    //post
    app.post('/api/holiday/add', holidayApiController.add);
    app.post('/api/holiday/update', holidayApiController.update);
    app.post('/api/holiday/removeById', holidayApiController.removeById);
};