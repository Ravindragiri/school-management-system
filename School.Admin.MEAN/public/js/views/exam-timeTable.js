﻿var app = angular.module('schoolApp', ['ngTouch', 'ui.grid', 'ui.grid.expandable', 'ui.grid.selection', 'ui.grid.pinning']);

app
    .directive("datepicker", function ($timeout) {
        return {
            restrict: "A",
            require: "ngModel",
            link: function (scope, elem, attrs, ngModelCtrl) {
                var updateModel = function (dateText) {
                    scope.$apply(function () {
                        ngModelCtrl.$setViewValue(dateText);
                    });
                };
                var options = {
                    dateFormat: "dd/mm/yy",
                    onSelect: function (dateText) {
                        updateModel(dateText);
                    }
                };
                $timeout(function () {
                    elem.datepicker(options);
                });
            }
        }
    })
    //.directive("datepicker", function ($timeout) {

    //    function link(scope, element, attrs) {
    //        // CALL THE "datepicker()" METHOD USING THE "element" OBJECT.
    //        $timeout(function () {
    //            element.datepicker({
    //                dateFormat: "dd/mm/yy"
    //            });
    //        });
    //    }

    //    return {
    //        require: 'ngModel',
    //        link: link,
    //        transclude: true
    //    };
    //})
    .controller('ngExamTimeTableController', ['ExamTimeTableService', '$scope', function (ExamTimeTableService, $scope) {

        $scope.addMode = true;

        $scope.gridOptions = {
            excludeProperties: '__metadata',
            enableRowSelection: true,
            expandableRowTemplate: '<div ui-grid="row.entity.subGridOptions" style="height:140px;"></div>',
            expandableRowHeight: 150,
            expandableRowScope: {
                subGridVariable: 'subGridScopeVariable'
            }
        };

        $scope.gridOptions.columnDefs = [

            { name: 'class', cellTemplate: '<div class="ui-grid-cell-contents">{{ COL_FIELD }}</div>' },
            { name: 'examType', pinnedLeft: true, cellTemplate: '<div class="ui-grid-cell-contents">{{ COL_FIELD }}</div>' },

            { name: 'edit', cellTemplate: '<button type="button" class="btn primary" ng-click="grid.appScope.editExamTimeTable(row)">Edit</button>' },
            { name: 'delete', field: '_id', cellTemplate: '<button type="button" class="btn primary" ng-click="grid.appScope.deleteExamTimeTable(row)">Delete</button>' }
        ]

        $scope.insertExamTimeTable = function () {

            if ($scope.examDTO) {
                var data = {
                    examDTO: $scope.examDTO
                };
                ExamTimeTableService.create(data)
                    .then(function success(response) {
                        $scope.message = 'Exam TimeTable added!';
                        $scope.errorMessage = '';
                    },
                    function error(response) {
                        $scope.errorMessage = 'Error adding exam TimeTable!';
                        $scope.message = '';
                    });
            }
            else {
                $scope.errorMessage = 'Please enter a name!';
                $scope.message = '';
            }
        };

        $scope.load = function () {
            ExamTimeTableService.readAll().then(function (response) {
                var data = response.data;
                for (i = 0; i < data.length; i++) {

                    data[i].subGridOptions = {
                        columnDefs: [
                            { name: 'subject', field: 'subject' },
                            { name: 'examDate', field: 'examDate' },
                            { name: 'timeStart', field: 'timeStart' },
                            { name: 'timeEnd', field: 'timeEnd' }
                        ],
                        data: data[i].timeTableList
                    };
                }
                $scope.gridOptions.data = data;
            });
        }

        $scope.load();

        $scope.update = function () {

            var data = {
                examDTO: $scope.examDTO
            }
            ExamTimeTableService.update(data).then(function (response) {
                
                if (response != null) {
                    var status = response.statusCode;

                    switch (status) {
                        case 200:
                            $scope.addMode = true;
                            alert("Record has been updated successfully.");
                            break;
                        case 0:
                            alert("Your request cannot be processed now due to some technical issue. please try again later.");
                            break;
                        case -1:
                        case 400:
                        case 401:
                        case 204:
                            alert(response.message);
                            break;
                        default:
                    }
                }
            });
        }

        $scope.cancel = function () {
            $scope.examDTO = {
                examType: '',
                class: '',
                timeTableList: []
            };
            $scope.addMode = true;
        }



        $scope.deleteExamTimeTable = function (row) {
            if (row.entity._id) {

                ExamTimeTableService.delete(row.entity._id).then(function (response) {
                    
                    if (response != null) {
                        var status = response.statusCode;

                        switch (status) {
                            case 200:
                                $scope.gridOptions.data = response.data;
                                var index = $scope.gridOptions.data.indexOf(row.entity);
                                $scope.gridOptions.data.splice(index, 1);
                                alert("Record has been deleted successfully.");
                                break;
                            case 0:
                                alert("Your request cannot be processed now due to some technical issue. please try again later.");
                                break;
                            case -1:
                            case 400:
                            case 401:
                            case 204:
                                alert(response.message);
                                break;
                            default:
                        }
                    }
                });
            }
        };

        $scope.examDTO = {
            examType: '',
            class: '',
            timeTableList: []
        };

        $scope.add = function () {

            $scope.examDTO.timeTableList.push({
                subject: '',
                examDate: '',
                timeStart: '',
                timeEnd: ''
            });
        }

        $scope.editExamTimeTable = function (row) {

            if (row.entity) {
                $scope.examDTO = row.entity;
                //var index = $scope.gridOptions.data.indexOf(row.entity);
                $scope.addMode = false;
            }
        };

        $scope.del = function (i) {
            $scope.examDTO.timeTableList.splice(i, 1);
        }

        $scope.search = function () {
            var data = {
                examType: $scope.examDTO.examType,
                class: $scope.examDTO.class
            };

            ExamTimeTableService.getExamTimeTableByExamTypeAndClass(data).then(function (response) {
                $scope.examDTO = response.data;
            });
        }

    }]);

(function () {

    angular.module('schoolApp')
        .service('ExamTimeTableService', ['$http', ExamTimeTableService]);

    function ExamTimeTableService($http) {

        var self = this;
        var baseUrl = 'http://localhost:1238';

        //var objectName = 'products';

        self.readAll = function () {
            return $http({
                method: 'GET',
                url: baseUrl + '/api/exam/TimeTable/getAllExamTimeTableInfo',
                //url: baseUrl + objectName
            }).then(function (response) {
                return response.data;
            });
        };

        self.getExamTimeTableByExamTypeAndClass = function (data) {
            return $http({
                method: 'GET',
                url: baseUrl + '/api/exam/timeTable/getExamTimeTableByExamTypeAndClass',
                params: data,
                //url: baseUrl + objectName
            }).then(function (response) {
                return response.data;
            }).catch(function (response) {
                console.error('Error occurred:', response.status, response.data);
            });
        };

        self.readOne = function (id) {
            return $http({
                method: 'GET',
                url: baseUrl + objectName + '/' + id
            }).then(function (response) {
                return response.data;
            });
        };

        self.create = function (data) {
            return $http({
                method: 'POST',
                //url: baseUrl + objectName,
                url: baseUrl + '/api/exam/TimeTable/add',
                data: data,
                params: {
                    returnObject: true
                },
            }).then(function (response) {
                return response.data;
            });
        };

        self.update = function (data) {
            return $http({
                method: 'POST',
                //url: baseUrl + objectName + '/' + id,
                url: 'http://localhost:1238/api/exam/timeTable/update',
                data: data
            }).then(function (response) {
                return response.data;
            });
        };

        self.delete = function (id) {
            return $http({
                method: 'POST',
                //url: baseUrl + objectName + '/' + id
                url: 'http://localhost:1238/api/exam/timeTable/removeById' + '?id=' + id
            }).then(function (response) {
                return response.data;
            });
        };

    }
} ());