﻿appDefaults =
    {
        OrderBy: "distance_DESC",
        Page: 1,
        PageSize: 10,
        UserType: "1"
    };

statusCode = {

    SYSTEM_ERROR: { text: 'SYSTEM_ERROR', value: 500 },
    ServerValidationFailed: { text: 'ServerValidationFailed', value: -10 },
    NotFound: { text: 'NotFound', value: 204 },
    FKConflict: { text: 'FKConflict', value: -8 },
    UnauthorizedAccess: { text: 'UnauthorizedAccess', value: 401 },
    DeleteFailed: { text: 'DeleteFailed', value: -6 },
    AlreadyExists: { text: 'AlreadyExists', value: -5 },
    Invalid: { text: 'Invalid', value: -4 },
    FailedRedirect: { text: 'FailedRedirect', value: -3 },
    ErrorRedirect: { text: 'ErrorRedirect', value: -2 },
    Error: { text: 'Error', value: -1 },
    Failed: { text: 'Failed', value: 0 },
    Success: { text: 'Success', value: 200 },
    Redirect: { text: 'Redirect', value: 300 }
};

messageStateEnum = {
    Draft: "0",
    Normal: "1"
};

userTypeEnum = {
    student: "1",
    teacher: "2",
    officeStaff: "3",
    parent: "4"
};

loginTypeEnum = {
    LocalAccount: "0",
    SocialAccount: "1"
};

examResultEnum = {
    Fail: "0",
    Pass: "1"
};

messages = {
    SaveSuccess: "Record has been saved successfully.",
    UpdateSuccess: "Record has been updated successfully.",
    DeleteSuccess: "Record has been deleted successfully.",
    ReadSuccess: "Record has been retrieved successfully.",
    SaveFailed: "We're unable to save your record.",
    DeleteFailed: "We're unable to delete your record.",
    ErrorProcessing: "The system encountered an error while processing your request.",
    ErrorProcessingRefNumber: "The system encountered an error while processing your request. Reference number: {0}",
    UserAlreadyExists: "The user is already registered!",
    UserRegisteredSuccessfully: "The user is registered successfully!",
    UserRegistrationFailed: "Registration failed.",
    RecordNotFound: "Record with Id '{0}' not found.",
    InvalidLoginCredentials: "Login failed. Invalid Login credentials.",
    LoggedInSuccess: "You logged in successfully.",
    CurrentPasswordIncorrect: "The password you entered is incorrect, please retype your current password.",
    PasswordIncorrect: "The password you entered is incorrect.",
    MaxLoginAttempts: "Too many failed login attempts.",
    UserDoesnotExist: "User does not exist!",
    organDoesnotExist: "organ does not exist!",
    RequiredFieldMissing: "Required field is missing! {0}",
    RequiredFieldEmail: "The field email is required.",
    EmailNotRegistered: "Email not registered!",
    ResetPasswordEmailSent: "A link to reset your password has been sent. Please Check your email.",
    UsernameNotAvailable: "User already exists with same username.",
    EmailNotAvailable: "User already exists with same email.",
    MessageSentSuccessfully: "Your message was sent successfully.",
    MessageNotSent: "Your message was not sent.",
    NoMessagesFound: "No messages found.",
    NotFound: "No Content Found!",
    BadRequest: "Bad request , try again please !"
};

classes = [
    {
        value: "First",
        text: "First",
        intake: "60",
        fees: "15000"
    },
    {
        value: "Second",
        text: "Second",
        intake: "60",
        fees: "15000"
    },
    {
        value: "Third",
        text: "Third",
        intake: "60",
        fees: "15000"
    },
    {
        value: "Fourth",
        text: "Fourth",
        intake: "60",
        fees: "15000"
    },
    {
        value: "Fifth",
        text: "Fifth",
        intake: "60",
        fees: "15000"
    },
    {
        value: "Sixth",
        text: "Sixth",
        intake: "60",
        fees: "15000"
    },
    {
        value: "Seventh",
        text: "Seventh",
        intake: "60",
        fees: "15000"
    }
];

subjects = [
    {
        value: 'Maths',
        text: 'Maths'
    },
    {
        value: 'Gujarati',
        text: 'Gujarati'
    },
    {
        value: 'Paryavaran',
        text: 'Paryavaran'
    },
    {
        value: 'English',
        text: 'English'
    },
    {
        value: 'Hindi',
        text: 'Hindi'
    },
    {
        value: 'Sanskrit',
        text: 'Sanskrit'
    },
    {
        value: 'Vigyan',
        text: 'Vigyan'
    }
];

sections = [
    {
        value: 'A',
        text: 'A'
    },
    {
        value: 'B',
        text: 'B'
    },
    {
        value: 'C',
        text: 'C'
    }
];

markTypes = {
    Oral: {
        id: 1,
        text: 'Oral'
    },
    Writing: {
        id: 2,
        text: 'Writing'
    }
};

qualifications = [
    {
        "value": "BA",
        "text": "BA"
    }, {
        "value": "MA",
        "text": "MA"
    }, {
        "value": "B.Ed",
        "text": "B.Ed"
    }, {
        "value": "M.Ed",
        "text": "M.Ed"
    }
];

bloodGroups = [
    {
        "value": "A+",
        "text": "A+"
    },
    {
        "value": "A-",
        "text": "A-"
    },
    {
        "value": "B+",
        "text": "B+"
    },
    {
        "value": "B-",
        "text": "B-"
    },
    {
        "value": "AB+",
        "text": "AB+"
    },
    {
        "value": "AB+",
        "text": "AB-"
    },
    {
        "value": "O+",
        "text": "O+"
    },
    {
        "value": "O-",
        "text": "O-"
    }
]

examTypes = [
    {
        "value": "First",
        "text": "First"
    },
    {
        "value": "Second",
        "text": "Second"
    },
    {
        "value": "Final",
        "text": "Final"
    }
]

designations = [
    {
        "value": "Head Teacher",
        "text": "Head Teacher"
    },
    {
        "value": "Teacher",
        "text": "Teacher"
    }
]

indiaStates = [
    {
        "value": "AN",
        "text": "Andaman and Nicobar Islands"
    },
    {
        "value": "AP",
        "text": "Andhra Pradesh"
    },
    {
        "value": "AR",
        "text": "Arunachal Pradesh"
    },
    {
        "value": "AS",
        "text": "Assam"
    },
    {
        "value": "BR",
        "text": "Bihar"
    },
    {
        "value": "CG",
        "text": "Chandigarh"
    },
    {
        "value": "CH",
        "text": "Chhattisgarh"
    },
    {
        "value": "DH",
        "text": "Dadra and Nagar Haveli"
    },
    {
        "value": "DD",
        "text": "Daman and Diu"
    },
    {
        "value": "DL",
        "text": "Delhi"
    },
    {
        "value": "GA",
        "text": "Goa"
    },
    {
        "value": "GJ",
        "text": "Gujarat"
    },
    {
        "value": "HR",
        "text": "Haryana"
    },
    {
        "value": "HP",
        "text": "Himachal Pradesh"
    },
    {
        "value": "JK",
        "text": "Jammu and Kashmir"
    },
    {
        "value": "JH",
        "text": "Jharkhand"
    },
    {
        "value": "KA",
        "text": "Karnataka"
    },
    {
        "value": "KL",
        "text": "Kerala"
    },
    {
        "value": "LD",
        "text": "Lakshadweep"
    },
    {
        "value": "MP",
        "text": "Madhya Pradesh"
    },
    {
        "value": "MH",
        "text": "Maharashtra"
    },
    {
        "value": "MN",
        "text": "Manipur"
    },
    {
        "value": "ML",
        "text": "Meghalaya"
    },
    {
        "value": "MZ",
        "text": "Mizoram"
    },
    {
        "value": "NL",
        "text": "Nagaland"
    },
    {
        "value": "OR",
        "text": "Odisha"
    },
    {
        "value": "PY",
        "text": "Puducherry"
    },
    {
        "value": "PB",
        "text": "Punjab"
    },
    {
        "value": "RJ",
        "text": "Rajasthan"
    },
    {
        "value": "SK",
        "text": "Sikkim"
    },
    {
        "value": "TN",
        "text": "Tamil Nadu"
    },
    {
        "value": "TS",
        "text": "Telangana"
    },
    {
        "value": "TR",
        "text": "Tripura"
    },
    {
        "value": "UP",
        "text": "Uttar Pradesh"
    },
    {
        "value": "UK",
        "text": "Uttarakhand"
    },
    {
        "value": "WB",
        "text": "West Bengal"
    }
]

markValues = {
    A: {
        id: 1,
        text: 'A'
    },
    B: {
        id: 2,
        text: 'B'
    }
};

noticeTypes = {
    Oral: {
        id: 1,
        text: 'Oral'
    },
    Writing: {
        id: 2,
        text: 'Writing'
    }
};

paymentStatusTypes = [
    {
        text: 'Paid',
        value: 'Paid'
    },
    {
        text: 'Unpaid',
        value: 'Unpaid'
    }
];

paymentMethods = [
    {
        text: 'Cash',
        value: 'Cash'
    },
    {
        text: 'Check',
        value: 'Check'
    },
    {
        text: 'Card',
        value: 'Card'
    }
];

