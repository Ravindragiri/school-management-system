﻿var config = require('../config/config');
require('../school.common/appConstants');

exports.index = function (req, res) {

    res.render('parent/index', {
        pTitle: 'parent List | School',
        students: []
    });
};