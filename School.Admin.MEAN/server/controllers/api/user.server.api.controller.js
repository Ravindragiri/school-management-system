﻿var userDal = require('../../school.dal/user.dal.js');
require('../../school.common/helpers/responseHelper');
var Random = require('../../utils/random');

var mongoose = require('mongoose');
var user = mongoose.model('user');
require('../../school.common/appConstants');

exports.add = function (req, res) {

    if (req.body.userDTO) {
        var userModel = new user(req.body.userDTO);

        userDal.add(userModel)
            .then(function (result) {
                if (result) {
                    sendSuccessResponse(res, result);
                } else {
                    sendErrorResponse(res);
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
    else {
        sendBadRequestResponse(res);
    }
};

exports.createNewUser = function (req, res) {

    var userModel = req.body.userModel;

    userDal.createNewUser(userModel)
        .then(function (result) {
            if (result) {
                sendSuccessResponse(res, result);
            } else {
                sendFailResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
};

exports.removeById = function (req, res) {

    var userId = req.query.userId;
    //var loggedInUserId =;

    if (userId) {

        var userUpdateCondition = { $and: [] };
        userUpdateCondition.$and.push({
            _id: userId
        });

        var userUpdateSet = {
            isDeleted: true,
            updatedBy: userId,
            updatedDate: new Date()
        };

        userDal.update(userUpdateCondition, userUpdateSet)
            .then(function (result) {
                if (result) {
                    sendSuccessResponse(res, result, messages.DeleteSuccess);
                } else {
                    sendSuccessResponse(res, result, messages.DeleteFailed);
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
    else {
        sendBadRequestResponse(res);
    }
};

exports.getUserByEmail = function (req, res) {

    if (req.query.email) {
        var email = req.query.email;

        userDal.getUserByEmail(email)
            .then(function (result) {
                if (result) {
                    sendSuccessResponse(res, result);
                } else {
                    sendNoContentResponse(res);
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
    else {
        sendBadRequestResponse(res);
    }
}

exports.getUserByEmailPassword = function (req, res) {

    if (req.body.loginDTO) {
        var email = req.body.loginDTO.email;
        var password = req.body.loginDTO.password;

        userDal.getUserByEmailPassword(email, password)
            .then(function (result) {
                if (result) {
                    sendSuccessResponse(res, result);
                } else {
                    sendNoContentResponse(res);
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
    else {
        sendBadRequestResponse(res);
    }
}

exports.getUserById = function (req, res) {

    if (req.query.userId) {
        var userId = req.query.userId;

        userDal.getUserById(userId)
            .then(function (result) {
                if (result) {
                    //var avatarUrl = getSocialAccountAvatarUrl(result.socialAccountType, result.socialAccountId);
                    //if (avatarUrl) {
                    //    result.avatarUrl = avatarUrl;
                    //}

                    sendSuccessResponse(res, result);
                } else {
                    sendNoContentResponse(res);
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
    else {
        sendBadRequestResponse(res);
    }
}

exports.getUsernameById = function (req, res) {

    if (req.query.userId) {
        var userId = req.query.userId;

        userDal.getUsernameById(userId)
            .then(function (result) {
                if (result) {
                    sendSuccessResponse(res, result);
                } else {
                    sendNoContentResponse(res);
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
    else {
        sendBadRequestResponse(res);
    }
}

exports.getUserIdByUsername = function (req, res) {

    if (req.query.username) {
        var username = req.query.username;

        userDal.getUserIdByUsername(username)
            .then(function (result) {
                if (result) {
                    sendSuccessResponse(res, result);
                } else {
                    sendNoContentResponse(res);
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
    else {
        sendBadRequestResponse(res);
    }
}

exports.authenticateUser = function (req, res) {
    var username = req.body.username;
    var password = req.body.password;

    userDal.authenticateUser(username, password, reason)
        .then(function (result) {

            if (err) throw err;

            // login was successful if we have a user
            if (result) {
                // handle login success
                console.log('login success');
                return;
            }

            // otherwise we can determine why we failed
            var reasons = User.failedLogin;
            switch (reason) {
                case reasons.NOT_FOUND:
                case reasons.PASSWORD_INCORRECT:
                    // note: these cases are usually treated the same - don't tell
                    // the user *why* the login failed, only that it did
                    break;
                case reasons.MAX_ATTEMPTS:
                    // send email or otherwise notify user that account is
                    // temporarily locked
                    break;
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
}

exports.authenticateMobileUser = function (req, res) {

    if (!req.body.loginDTO)
        return sendBadRequestResponse(res);

    var loginDTO = req.body.loginDTO;

    if (loginDTO.loginType == loginTypeEnum.LocalAccount) {

        if (loginDTO.username && loginDTO.password) {
            userDal.authenticateMobileUser(loginDTO.username, loginDTO.password)
                .then(function (result) {
                    res.send(result);
                    //if (user) {
                    //    sendSuccessResponse(res, user, messages.LoggedInSuccess);
                    //} else {
                    //    sendFailResponse(res, messages.InvalidLoginCredentials);
                    //}
                })
                .catch(function (err) {
                    console.log(err);
                    sendErrorResponse(res);
                });
        }
        else {
            sendBadRequestResponse(res);
        }
    }
    else {
        return sendBadRequestResponse(res);
    }
};

exports.authenticateWebUser = function (req, res) {
    var loginDTO = req.body.loginDTO;

    userDal.authenticateWebUser(loginDTO)
        .then(function (result) {

            if (result) {
                sendSuccessResponse(res, result, messages.LoggedInSuccess);
            } else {
                sendFailResponse(res, messages.InvalidLoginCredentials);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
};

exports.requestMobileVerificationCode = function (req, res) {
    var mobileVerification = req.body.mobileVerification;

    userDal.requestMobileVerificationCode(mobileVerification)
        .then(function (result) {
            if (result) {
                sendSuccessResponse(res, result);
            } else {
                sendFailResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
}

exports.changePassword = function (req, res) {

    var userId = req.body.userId;
    var oldPassword = req.body.oldPassword;
    var newPassword = req.body.newPassword;

    if (userId && oldPassword && newPassword) {

        var userUpdateCondition = { $and: [] };
        userUpdateCondition.$and.push({
            _id: userId,
            password: oldPassword
        });

        var userUpdateSet = {
            password: newPassword,
            updatedBy: userId,
            updatedDate: new Date()
        };

        userDal.update(userUpdateCondition, userUpdateSet)
            .then(function (result) {
                if (result) {
                    sendSuccessResponse(res, result, messages.UpdateSuccess);
                } else {
                    sendSuccessResponse(res, result, messages.CurrentPasswordIncorrect);
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
    else {
        sendBadRequestResponse(res);
    }
};

exports.forgetPassword = function (req, res) {
    if (!req.body.email)
        return sendBadRequestResponse(res);

    var email = req.body.email;
    userDal.getUserByEmail(email)
        .then(function (user) {
            if (user) {

                var userUpdateCondition = { _id: user._id.toString() };
                var userUpdateSet = {
                    password: Random.generateRandomString(8)
                };

                userDal.update(userUpdateCondition, userUpdateSet)
                    .then(function (user) {

                        if (user) {
                            //TODO
                            //send resetPassword email
                            sendSuccessResponse(res, '', messages.ResetPasswordEmailSent);
                        }
                        else {
                            sendFailResponse(res);
                        }
                    })
                    .catch(function (err) {
                        console.log(err);
                        sendErrorResponse(res);
                    });
            } else {
                sendFailResponse(res, messages.EmailNotRegistered);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
};

exports.updateProfile = function (req, res) {
    var mobileUserDTO = req.body.mobileUserDTO;

    userDal.getUserById(mobileUserDTO.Id)
        .then(function (userDTO) {

            if (userDTO) {
                if (userDTO.Id > 0) {
                    if (userDTO.password == mobileUserDTO.password) {

                        //update profile photo TODO
                        userDTO.firstName = mobileUserDTO.firstName;
                        userDTO.lastName = mobileUserDTO.lastName;
                        userDTO.phoneNumber = mobileUserDTO.phoneNumber;
                        //address TODO

                        userDal.update(userDTO, userId)
                            .then(function (user) {

                                if (user) {
                                    sendSuccessResponse(res, result, messages.UpdateSuccess);
                                }
                                else {
                                    sendFailResponse(res, messages.PasswordIncorrect);
                                }
                            })
                            .catch(function (err) {
                                console.log(err);
                                sendErrorResponse(res);
                            });
                    }
                }
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
};

exports.updatePassword = function (req, res) {

    console.log(req.body);
    if (req.body.updatePasswordDTO) {
        var userModel = new user(req.body.updatePasswordDTO);
        var newPassword = req.body.updatePasswordDTO.newPassword;

        userDal.updatePassword(userModel, newPassword)
            .then(function (user) {
                if (user) {
                    sendSuccessResponse(res, user, messages.UserRegisteredSuccessfully);
                    //uploadAvatar(files, user._id.toString());
                } else {
                    sendFailResponse(res, messages.UserRegistrationFailed);
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
    else {
        sendBadRequestResponse(res);
    }
};

exports.updateEmail = function (req, res) {

    if (req.body.updateEmailDTO) {
        var userModel = new user(req.body.updateEmailDTO);

        userDal.updateEmail(userModel)
            .then(function (user) {
                if (user) {
                    sendSuccessResponse(res, user, messages.UserRegisteredSuccessfully);
                    //uploadAvatar(files, user._id.toString());
                } else {
                    sendFailResponse(res, messages.UserRegistrationFailed);
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
    else {
        sendBadRequestResponse(res);
    }
};
