﻿var timeTableDal = require('../../school.dal/class.timeTable.dal.js');
require('../../school.common/helpers/responseHelper');

var mongoose = require('mongoose');
var timeTable = mongoose.model('classRoutine');
require('../../school.common/appConstants');

exports.add = function (req, res) {

    if (req.body.classTimeTableDTO) {
        var timeTableModel = new timeTable(req.body.classTimeTableDTO);

        timeTableDal.add(timeTableModel)
            .then(function (result) {
                if (result) {
                    sendSuccessResponse(res, result);
                } else {
                    sendErrorResponse(res);
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
    else {
        sendBadRequestResponse(res);
    }
};

exports.removeById = function (req, res) {

    if (req.query.timeTableId) {
        var className = req.query.className;
        var section = req.query.section;
        var timeTableId = req.query.timeTableId;

        timeTableDal.removeById(className, section, timeTableId)
            .then(function (result) {
                if (result) {
                    sendSuccessResponse(res, result, messages.DeleteSuccess);
                } else {
                    sendFailResponse(res);
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
    else {
        sendBadRequestResponse(res);
    }
};

exports.getTimeTableById = function (req, res) {

    if (req.query.timeTableId) {
        var timeTableId = req.query.timeTableId;

        timeTableDal.getClassTimeTableById(timeTableId)
            .then(function (result) {
                if (result) {

                    sendSuccessResponse(res, result);
                } else {
                    sendNotFoundResponse(res);
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
    else {
        sendBadRequestResponse(res);
    }
}

exports.getClassTimeTableByClassAndSection = function (req, res) {

    if (req.query.class && req.query.section) {
        var className = req.query.class;
        var section = req.query.section;
        timeTableDal.getClassTimeTableByClassAndSection(className, section)
            .then(function (result) {
                if (result) {

                    sendSuccessResponse(res, result);
                } else {
                    sendNotFoundResponse(res);
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
}

exports.getAllTimeTables = function (req, res) {

    timeTableDal.getAllTimeTables()
        .then(function (result) {
            if (result) {

                sendSuccessResponse(res, result);
            } else {
                sendNotFoundResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
}

exports.update = function (req, res) {

    if (req.body.mobileTimeTableDTO) {
        var mobileTimeTableDTO = req.body.mobileTimeTableDTO;

        timeTableDal.getTimeTableById(mobileTimeTableDTO.Id)
            .then(function (classTimeTableDTO) {

                if (classTimeTableDTO) {
                    if (classTimeTableDTO.Id > 0) {
                        if (classTimeTableDTO.password == mobileTimeTableDTO.password) {

                            //update profile photo TODO
                            classTimeTableDTO.firstName = mobileTimeTableDTO.firstName;
                            classTimeTableDTO.lastName = mobileTimeTableDTO.lastName;
                            classTimeTableDTO.phoneNumber = mobileTimeTableDTO.phoneNumber;
                            //address TODO

                            timeTableDal.update(classTimeTableDTO, timeTableId)
                                .then(function (timeTable) {

                                    if (timeTable) {
                                        sendSuccessResponse(res, result, messages.UpdateSuccess);
                                    }
                                    else {
                                        sendFailResponse(res, messages.PasswordIncorrect);
                                    }
                                })
                                .catch(function (err) {
                                    console.log(err);
                                    sendErrorResponse(res);
                                });
                        }
                    }
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
    else {
        sendBadRequestResponse(res);
    }
};

exports.getTimeTableByDay = function (req, res) {

    if (req.query.day) {
        var day = req.query.day;

        timeTableDal.getTimeTableByDay(day)
            .then(function (result) {
                if (result) {

                    sendSuccessResponse(res, result);
                } else {
                    sendNotFoundResponse(res);
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
    else {
        sendBadRequestResponse(res);
    }
};