﻿var config = require('../config/config.js');
require('../school.common/appConstants');

exports.index = function (req, res) {
    res.render('invoice/index', {
        pTitle: 'Invoice List | School'
    });
};