﻿module.exports = function (app) {

    var search = require('../controllers/search.server.controller');
    app.get('/search/result', search.result);
};