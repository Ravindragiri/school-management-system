﻿var Q = require('q');
var mongoose = require('mongoose'),
    async = require('async');

require('../models/invoice.server.model');
require('../models/user.server.model');

var _invoice = mongoose.model('invoice');
var _user = mongoose.model('user');

var ObjectId = mongoose.Types.ObjectId;

require('../../server/school.common/helpers/deferredHelper');

var dal = {};

dal.getAllInvoiceInfo = getAllInvoiceInfo;
dal.updateInvoice = updateInvoice;
dal.removeInvoice = removeInvoice;
dal.insert = insert;

module.exports = dal;

function insert(invoiceModel, paymentModel) {

    var paymentDal = require('./payment.dal.js');
    var deferred = Q.defer();

    async.waterfall(
        [
            function (callback) {

                invoiceModel.createdDate = new Date();

                add(invoiceModel)
                    .then(function (invoice) {

                        if (invoice) {

                            paymentModel.invoiceRef = invoice._id;
                            paymentModel.createdBy = invoice._id;
                            paymentModel.createdDate = new Date();
                            callback(null, paymentModel);
                        } else {
                            deferred.resolve();
                        }
                    })
                    .catch(function (err) {
                        if (err) {
                            console.log(err);
                            deferred.reject(err);
                        }
                    });
            },
            function (paymentModel, callback) {
                paymentDal.add(paymentModel)
                    .then(function (payment) {

                        if (payment) {
                            deferred.resolve(payment);
                        } else {
                            deferred.resolve();
                        }
                    })
                    .catch(function (err) {
                        if (err) {
                            console.log(err);
                            callback(err, invoiceModel);
                            //deferred.reject(err);
                        }
                    });
            }
        ], function (errs, results) {
            if (errs) {
                async.each(results, rollback, function () {
                    console.log('Rollback done.');
                    //res.status(400).send(errs);
                });
            } else {
                console.log('Done.');
                deferred.resolve(results);
            }
        });

    function rollback(doc, callback) {
        if (!doc) { callback(); }
        else {
            _payment.findByIdAndRemove(doc._id, function (err, doc) {
                console.log('Rolled-back document: ' + doc);
                callback();
            });
        }
    }
    return deferred.promise;
}

function getAllInvoiceInfo() {

    var deferred = Q.defer();

    _invoice
        .find({})
        .populate({ path: 'userRef' })
        .populate({ path: 'subjects' })
        //.populate({ path: 'followers' })
        //.populate({ path: 'favorites' })
        //.populate({ path: 'messages' })
        //.populate({ path: 'profileVisitors' })
        //.populate({ path: 'sharedProfiles' })
        .lean()
        .exec(function (err, result) {
            execHelper(deferred, err, result);
        });
    return deferred.promise;
}

function updateInvoice(invoiceDTO, invoiceID, invoiceId) {

    var invoice = new _invoice(invoiceDTO);
    invoice._id = new ObjectId;
    invoice.createdBy = invoiceId;
    invoice.createdDate = new Date();

    var deferred = Q.defer();

    _invoice
        .findByIdAndUpdate({ _id: invoiceId }, { $push: { "invoices": invoice } }, { safe: true, upsert: true }, function (err, result) {
            updateHelper(deferred, err, result);
        });
    return deferred.promise;
}

function removeInvoice(invoiceModel, invoiceId) {

    var deferred = Q.defer();

    invoiceModel.find({ _id: invoiceId }, function (err, invoice) {
        if (err) {
            deferred.reject(err);
        }

        // delete him
        invoice.remove(function (err) {
            if (err) {
                deferred.reject(err);
            }

            console.log('invoice successfully deleted!');
        });
    });
    return deferred.promise;
}

function add(invoiceModel) {

    var deferred = Q.defer();

    invoiceModel
        .save(function (err, result) {
            createHelper(deferred, err, result);
        });

    return deferred.promise;
}