﻿var Q = require('q');
var mongoose = require('mongoose')

require('../models/student.server.model');
require('../models/user.server.model');

var _student = mongoose.model('student');
var _user = mongoose.model('user');

var ObjectId = mongoose.Types.ObjectId;

require('../../server/school.common/helpers/deferredHelper');

var dal = {};

dal.getAllPassedStudents = getAllPassedStudents;
dal.getAllFailedStudents = getAllFailedStudents;
dal.promote = promote;
dal.promoteAll = promoteAll;

module.exports = dal;

function getAllPassedStudents(classId, examType) {

    var deferred = Q.defer();

    _student
        .aggregate(
        {
            $match: {
                $or: [
                    { "exams.examType": { $eq: examType } },
                    { 'exams.class': classId },
                    { 'exams.result': examResultEnum.Pass },
                ]
            }
        },
        { $unwind: '$exams' },
        //{ $match: { '_id': ObjectId(studentId) } },
        { $lookup: { from: "users", localField: "userRef", foreignField: "_id", as: "user" } },
        { $unwind: "$user" },
        {
            $project: {
                "_id": 1,

                userId: "$user._id",
                firstName: "$user.firstName",
                lastName: "$user.lastName",
                birthDate: "$user.birthDate",

                "exams._id": 1,
                "exams.examType": 1,

                "exams.class": 1,
                "exams.section": 1,
                "exams.rollNo": 1,

                "exams.subjects": 1,
                "exams.result": 1,
                "exams.division": 1,
                "exams.percentage": 1,
                "exams.rank": 1
            }
        })
        .exec(function (err, result) {
            execHelper(deferred, err, result);
        });
    return deferred.promise;
}

function getAllFailedStudents(classId, examType) {

    var deferred = Q.defer();

    _student
        .aggregate(
        {
            $match: {
                $or: [
                    { "exams.examType": { $eq: examType } },
                    { 'exams.class': classId },
                    { 'exams.result': examResultEnum.Fail },
                ]
            }
        },
        { $unwind: '$exams' },
        //{ $match: { '_id': ObjectId(studentId) } },
        { $lookup: { from: "users", localField: "userRef", foreignField: "_id", as: "user" } },
        { $unwind: "$user" },
        {
            $project: {
                "_id": 1,

                userId: "$user._id",
                firstName: "$user.firstName",
                lastName: "$user.lastName",
                birthDate: "$user.birthDate",

                "exams._id": 1,
                "exams.examType": 1,

                "exams.class": 1,
                "exams.section": 1,
                "exams.rollNo": 1,

                "exams.subjects": 1,
                "exams.result": 1,
                "exams.division": 1,
                "exams.percentage": 1,
                "exams.rank": 1
            }
        })
        .exec(function (err, result) {
            execHelper(deferred, err, result);
        });
    return deferred.promise;
}

function promote(promotionUpdateCondition, promotionUpdateSet) {

    var deferred = Q.defer();

    _student.findOneAndUpdate(promotionUpdateCondition, { $set: promotionUpdateSet }, { new: true }, function (err, promotion) {
        if (err) {
            deferred.reject(err);
        }
        if (promotion) {
            promotion = _.pick(promotion.toObject(), ['_id', 'userRef', 'subjects']);
            deferred.resolve(promotion);
        } else {
            deferred.resolve();
        }
    });
    return deferred.promise;
}