﻿module.exports = function (app) {

    var invoiceController = require('../controllers/invoice.server.controller');
    app.get('/invoice/index', invoiceController.index);
    app.get('/invoice', invoiceController.index);

    var invoiceApiController = require('../controllers/api/invoice.server.api.controller');
    //get
    app.get('/api/invoice/getAllInvoiceInfo', invoiceApiController.getAllInvoiceInfo);
    //post
    app.post('/api/invoice/insert', invoiceApiController.insert);
    app.post('/api/invoice/update', invoiceApiController.update);
    app.post('/api/invoice/remove', invoiceApiController.remove);
    
};