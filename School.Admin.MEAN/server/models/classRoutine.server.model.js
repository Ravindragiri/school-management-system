﻿var mongoose = require('mongoose');

var classRoutineSchema = new mongoose.Schema({

    class: String,
    section: String,

    timeTableList: [{
        subject: String,
        day: String,
        timeStart: String,
        timeEnd: String
    }],
    academicYear: String,

    createdBy: {
        type: mongoose.Schema.Types.ObjectId
    },
    createdDate: {
        type: Date
    },
    updatedBy: {
        type: mongoose.Schema.Types.ObjectId
    },
    updatedDate: Date
});
//exports.classRoutineSchema = classRoutineSchema;
mongoose.model('classRoutine', classRoutineSchema);