﻿var contactUsDal = require('../../school.dal/contactUs.dal.js');
require('../../school.common/helpers/responseHelper');

var mongoose = require('mongoose');
var contactUs = mongoose.model('contactUs');

exports.sendMessage = function (req, res) {
    var contactUsModel = new contactUs(req.body);

    contactUsModel.createdDate = new Date();

    contactUsDal.sendMessage(contactUsModel)
        .then(function (result) {
            if (result) {
                sendSuccessResponse(res, result);
            } else {
                sendFailResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
};