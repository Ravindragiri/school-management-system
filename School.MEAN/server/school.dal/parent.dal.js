﻿var Q = require('q');
var mongoose = require('mongoose')

require('../models/student.server.model');
require('../models/user.server.model');

var _student = mongoose.model('student');
var _user = mongoose.model('user');

var ObjectId = mongoose.Types.ObjectId;

var dal = {};

dal.insert = insert;
dal.getParentDetails = getParentDetails;
dal.getParentDetailsById = getParentDetailsById;
dal.loadEmail = loadEmail;
dal.searchParent = searchParent;
dal.getUserName = getUserName;

function insert() {
}

function getParentDetails(){
}

function getParentDetailsById(){
}

function loadEmail(){
}

function searchParent(){
}

function getUserName(){
}

module.exports = dal;