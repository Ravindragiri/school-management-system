﻿var mongoose = require('mongoose');

//TODO check webhook is post or get
var webhookSchema = new mongoose.Schema({

    amount: String,
    buyer: String,      //Buyer's email
    buyer_name: String, //Fees charged by Instamojo
    buyer_phone: String,    
    currency: String,
    fees: String,
    longurl: String,    //URL related to the payment request
    mac: String,        //Message Authentication code of this webhook request

    payment_id: String,
    payment_request_id: String,
    purpose: String,
    shorturl: String,   //Short URL of the payment request
    status: String
});

mongoose.model('webhook', webhookSchema);