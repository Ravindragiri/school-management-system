﻿module.exports = function (app) {

    var contactUs = require('../controllers/contactUs.server.controller');
    //app.post('/contactUs/sendMessage', contactUs.sendMessage);

    app.get('/contactUs/contact', contactUs.index);
    app.get('/contactUs', contactUs.index);
    app.get('/contact', contactUs.index);
};