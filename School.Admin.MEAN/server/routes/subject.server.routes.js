﻿module.exports = function (app) {

    var subjectApiController = require('../controllers/api/subject.server.api.controller');
    app.get('/api/subject/getAllSubjects', subjectApiController.getAllSubjects);
};