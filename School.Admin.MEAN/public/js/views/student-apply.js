﻿/***** angular ngStudentApplyController starts *****/
var app = angular.module('schoolApp', ['ngTouch', 'ui.grid']);

app
    .controller('ngStudentApplyController', ['StudentService', '$scope', '$http',
        function (StudentService, $scope, $http) {

            $scope.gridOptions = {
                columnDefs: [
                    { name: 'Current Class', field: 'currentClass', cellTemplate: '<div class="ui-grid-cell-contents">{{ COL_FIELD }}</div>' },
                    { name: 'Current Section', field: 'currentSection', cellTemplate: '<div class="ui-grid-cell-contents">{{ COL_FIELD }}</div>' },
                    { name: 'Apply Roll No', cellTemplate: '<button type="button" class="btn btn-xs" ng-click="grid.appScope.applyRollNo(row)">Apply</button>' },
                    { name: 'Apply Registration No', field: 'code', cellTemplate: '<button type="button" class="btn btn-xs" ng-click="grid.appScope.deleteStudent(row)">Apply</button>' }
                ],
                excludeProperties: '__metadata',
            };

            $scope.validateRegisterForm = function () {
                $scope.registrationFormReset = false;
                $scope.showRegisterValidation = function () { return $scope.registrationForm.$invalid; };
            }

            //$scope.reset = function () {
            //    $scope.form = angular.copy(reviewFormFields());
            //    $scope.registrationFormReset = true;
            //    $scope.reviewForm.$setPristine();
            //}

            $scope.register = function () {

                //$scope.validateRegisterForm();
                $("#registrationForm").validate();

                //if ($scope.registrationForm.$valid) {
                if ($('#registrationForm').valid()) {

                    $scope.loading = true;

                    var postData = {
                        __RequestVerificationToken: angular.element("input[name='__RequestVerificationToken']").val(),
                        UserModel: $scope.UserModel,
                        ServiceProviderModel: $scope.ServiceProviderModel
                    };

                    $.ajax({
                        type: "POST",
                        url: registerUrl,
                        data: postData,
                        dataType: "json",
                        accept: 'application/json',
                        beforeSend: function () {

                        },
                        success: function (response) {

                            if (response != null) {
                                var status = response.statusCode;

                                switch (status) {
                                    case 1:
                                        //var objTravel = JSON.parse(response.data);
                                        break;
                                    case 0:
                                        alert(response.message);
                                        break;
                                    case -10:
                                        $scope.showServerErrorMessages(response, response.serverErrors);
                                        break;
                                    case -500:
                                        alert(response.message)
                                        //window.location.href = errorPage;
                                        break;
                                    default:
                                }
                            }
                        },
                        error: function (xhr, status, errorThrown) {
                            alert("An error occurred while processing your request.");
                        }
                    });

                    //
                }
            };

            $scope.showServerErrorMessages = function (response, errors) {

                $.each(errors, function (index, error) {

                    $scope.$apply(function () {

                        $scope.invalidelements[error.Element] = true;
                        $('label[ng-show="invalidelements.' + error.Element + '"]').css('display', 'inline-block');
                        $('.' + error.Element).focus();
                    });
                });
            }

            $scope.load = function () {
                StudentService.readAll().then(function (response) {
                    $scope.gridOptions.data = response.data;
                    //$scope.gridOptions.data = JSON.parse(response.data);
                });
            }

            $scope.load();

            $scope.applyRollNo = function (row) {
                if (row.entity) {
                    var data = {
                        studentId: row.entity.currentClass,
                        sectionId: row.entity.currentSection
                    }
                    //var index = $scope.gridOptions.data.indexOf(row.entity);
                    $scope.addMode = false;
                    StudentService.applyRollNo(data).then(function (response) {
                    });
                }
            };

            $scope.deleteStudent = function (field) {
                //var index = $scope.gridOptions.data.indexOf(row.entity);
            };
        }]);
/***** angular ngStudentApplyController ends *****/

(function () {

    angular.module('schoolApp')
        .service('StudentService', ['$http', StudentService]);

    function StudentService($http) {

        var self = this;
        var baseUrl = 'http://localhost:1238/api/student/getTotalClassAndSectionInfo';

        //var objectName = 'products';

        self.readAll = function () {
            return $http({
                method: 'GET',
                url: baseUrl,
                //url: baseUrl + objectName
            }).then(function (response) {
                return response.data;
            });
        };

        self.readOne = function (id) {
            return $http({
                method: 'GET',
                url: baseUrl + objectName + '/' + id
            }).then(function (response) {
                return response.data;
            });
        };

        self.create = function (data) {
            return $http({
                method: 'POST',
                url: baseUrl + objectName,
                data: data,
                params: {
                    returnObject: true
                },
            }).then(function (response) {
                return response.data;
            });
        };

        self.applyRollNo = function (data) {
            return $http({
                method: 'POST',
                //url: baseUrl + objectName + '/' + id,
                url: 'http://localhost:1238/api/student/applyRollNo',
                data: data
            }).then(function (response) {
                return response.data;
            });
        };

        self.delete = function (id) {
            return $http({
                method: 'POST',
                url: baseUrl + objectName + '/' + id
            }).then(function (response) {
                return response.data;
            });
        };

    }
} ());