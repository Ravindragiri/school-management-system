﻿var teacherDal = require('../../../server/school.dal/teacher.dal.js');
require('../../school.common/helpers/responseHelper');

var mongoose = require('mongoose'),
    //fs = require('fs-extra'),
    //util = require("util"),
    moment = require("moment"),
    async = require('async');
//path = require("path");

var _teacher = mongoose.model('teacher');
var _user = mongoose.model('user');

function formatServerDate(candidateDTO) {
    if (candidateDTO.userRef && candidateDTO.userRef.birthDate) {
        candidateDTO.userRef.birthDate = moment(candidateDTO.userRef.birthDate).format("DD/MM/YYYY");
    }
    return candidateDTO;
}

function parseClientDate(candidateDTO) {
    if (candidateDTO.userRef && candidateDTO.userRef.birthDate) {
        candidateDTO.userRef.birthDate = moment(candidateDTO.userRef.birthDate, 'DD/MM/YYYY').toDate();
    }
    return candidateDTO;
}

exports.getSubjectNames = function (req, res) {

    if (req.query.userId) {
        var userId = req.query.userId;

        teacherDal.getSubjectNames(userId)
            .then(function (result) {
                if (result) {
                    sendSuccessResponse(res, result);
                } else {
                    sendNoContentResponse(res);
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
    else {
        send404Response(res);
    }
}

exports.insert = function (req, res) {

    if (req.body.teacherDTO) {
        var userDal = require('../../school.dal/user.dal.js');
        var teacherDTO = req.body.teacherDTO;
        teacherDTO = parseClientDate(teacherDTO);
        var teacherModel = new _teacher(teacherDTO);
        var userModel = new _user(teacherDTO.userRef);

        async.waterfall(
            [
                function (callback) {
                    userDal.checkUserNameExist(userModel.username)
                        .then(function (result) {
                            if (result) {
                                sendFailResponse(res, messages.UsernameNotAvailable);
                            }
                            else {
                                callback(null);
                            }
                        });
                },
                function (callback) {
                    userDal.checkEmailExist(userModel.email)
                        .then(function (result) {
                            if (result) {
                                sendFailResponse(res, messages.EmailNotAvailable);
                            }
                            else {
                                callback(null);
                            }
                        });
                },
                function (callback) {

                    teacherDal.insert(teacherModel, userModel)
                        .then(function (user) {
                            if (user) {
                                sendSuccessResponse(res, user, messages.UserRegisteredSuccessfully);
                                //uploadAvatar(files, user._id.toString());
                            } else {
                                sendFailResponse(res, messages.UserRegistrationFailed);
                            }
                        })
                        .catch(function (err) {
                            console.log(err);
                            sendErrorResponse(res);
                        });
                }
            ], function (errs, results) {
                if (errs) {
                    console.log(err);
                    sendErrorResponse(res);
                } else {
                    console.log('Done.');
                }
            });
    }
    else {
        sendBadRequestResponse(res);
    }
};

exports.mobileRegister = function (req, res) {
    var userDal = require('../../school.dal/user.dal.js');

    var teacherModel = new _teacher(req.body.data.teacherDTO);
    var userModel = new _user(req.body.data.userRef);

    async.waterfall(
        [
            function (callback) {
                userDal.checkUserNameExist(userModel.username)
                    .then(function (result) {
                        if (result) {
                            sendFailResponse(res, messages.UsernameNotAvailable);
                        }
                        else {
                            callback(null);
                        }
                    });
            },
            function (callback) {
                userDal.checkEmailExist(userModel.email)
                    .then(function (result) {
                        if (result) {
                            sendFailResponse(res, messages.EmailNotAvailable);
                        }
                        else {
                            callback(null);
                        }
                    });
            },
            function (callback) {

                //userModel.createdDate = new Date();

                teacherDal.mobileRegister(userModel)
                    .then(function (user) {
                        if (user) {
                            sendSuccessResponse(res, user, messages.UserRegisteredSuccessfully);
                            //uploadAvatar(files, user._id.toString());
                        } else {
                            sendFailResponse(res, messages.UserRegistrationFailed);
                        }
                    })
                    .catch(function (err) {
                        console.log(err);
                        sendErrorResponse(res);
                    });
            }
        ], function (errs, results) {
            if (errs) {
                console.log(err);
                sendErrorResponse(res);
            } else {
                console.log('Done.');
            }
        });

};

exports.remove = function (req, res) {

    console.log(req.query);

    if (req.query.id && req.query.userId) {

        var teacherId = req.query.id;
        var userId = req.query.userId;

        teacherDal.remove(teacherId, userId)
            .then(function (result) {
                if (result) {
                    sendSuccessResponse(res, result);
                } else {
                    sendNoContentResponse(res);
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
    else {
        sendBadRequestResponse(res);
    }
}

uploadAvatar = function (files, userId) {
    if (userId) {
        if (files.image) {
            if (files.image.name && files.image.name.length > 0) {
                var newFileName = userId + path.extname(files.image.name);
                if (!fs.existsSync(path.join(__dirname, util.format('../../../public/uploads/avatar/%s', userId)))) {
                    fs.mkdirSync(path.join(__dirname, util.format('../../../public/uploads/avatar/%s', userId)));
                }
                var tempPath = files.image.path,
                    targetPath = path.join(__dirname, util.format('../../../public/uploads/avatar/%s/%s', userId, newFileName));

                fs.copy(tempPath, targetPath, function (err) {
                    if (err) { console.log(err); }
                    else { console.log("Upload completed!"); }
                });
            }
        }
    }
}

var formidable = require('formidable');

exports.update = function (req, res) {

    if (req.body.teacherDTO) {
        var userDal = require('../../school.dal/user.dal.js');
        var teacherDTO = req.body.teacherDTO;
        teacherDTO = parseClientDate(teacherDTO);
        var updateProfileModel = new _user(teacherDTO.userRef);
        var teacherModel = new _teacher(teacherDTO);
        var avatarUrl;

        teacherDal.mobileUpdateProfile(updateProfileModel, teacherModel, avatarUrl)
            .then(function (result) {
                if (result) {
                    sendSuccessResponse(res, result);
                } else {
                    sendFailResponse(res);
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
    else {
        sendErrorResponse(res);
    }
};

exports.updateProfile = function (req, res) {

    if (req.body.teacherDTO) {
        var userDal = require('../../school.dal/user.dal.js');
        var teacherDTO = req.body.teacherDTO;
        teacherDTO = parseClientDate(teacherDTO);
        var updateProfileModel = new _user(teacherDTO.userRef);
        var teacherModel = new _teacher(teacherDTO);
        var avatarUrl;

        teacherDal.updateProfile(updateProfileModel, teacherModel, avatarUrl)
            .then(function (result) {
                if (result) {
                    sendSuccessResponse(res, result);
                } else {
                    sendFailResponse(res);
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
    else {
        sendErrorResponse(res);
    }
};

exports.getTeacherByUserId = function (req, res) {

    console.log(req.query.userId);
    if (req.query.userId) {
        var userId = req.query.userId;

        teacherDal.getTeacherByUserId(userId)
            .then(function (result) {
                if (result) {
                    sendSuccessResponse(res, result);
                } else {
                    sendFailResponse(res);
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
    else {
        sendBadRequestResponse(res);
    }
}

exports.getTeacherById = function (req, res) {

    if (req.query.teacherId) {
        var teacherId = req.query.teacherId;

        teacherDal.getTeacherById(teacherId)
            .then(function (result) {
                if (result) {
                    sendSuccessResponse(res, result);
                } else {
                    sendNoContentResponse(res);
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
    else {
        sendBadRequestResponse(res);
    }
}

exports.getAllTeacherInfo = function (req, res) {

    //sendSuccessResponse(res, JSON.stringify(getSubjectList()));

    teacherDal.getAllTeacherInfo()
        .then(function (result) {
            if (result) {
                result = formatServerDate(result);
                sendSuccessResponse(res, result);
            } else {
                sendNoContentResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
}

exports.getTeacherContactDetailById = function (req, res) {

    if (req.query.teacherId) {
        var teacherId = req.query.teacherId;

        teacherDal.getTeacherContactDetailById(teacherId)
            .then(function (result) {
                if (result) {
                    sendSuccessResponse(res, result);
                } else {
                    sendNoContentResponse(res);
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
    else {
        sendBadRequestResponse();
    }
}

exports.count = function (req, res) {

    teacherDal.count()
        .then(function (result) {
            if (result) {
                sendSuccessResponse(res, result);
            } else {
                sendFailResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
};

exports.getTeacherProfileInfo = function (req, res) {

    if (req.query.userId) {
        var userId = req.query.userId;

        teacherDal.getTeacherProfileInfo(userId)
            .then(function (result) {
                if (result) {
                    sendSuccessResponse(res, result);
                } else {
                    sendNoContentResponse(res);
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
    else {
        sendBadRequestResponse();
    }
}

exports.getTeachersByAnyCriteria = function (req, res) {

    var searchCriteria = {
        userType: (req.query.userType) ? parseInt(req.query.userType) : appDefaults.UserType,
        searchText: req.query.title,

        page: (req.query.page) ? parseInt(req.query.page) : appDefaults.Page,
        pageSize: (req.query.pageSize) ? parseInt(req.query.pageSize) : appDefaults.PageSize,
        orderBy: req.query.orderBy ? req.query.orderBy : appDefaults.OrderBy
    };

    teacherDal.getTeachersByAnyCriteria(searchCriteria)
        .then(function (result) {
            if (result) {
                sendSuccessResponse(res, result);
            } else {
                sendNoContentResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            return sendErrorResponse(res);
        });
}

exports.count = function (req, res) {

    teacherDal.count()
        .then(function (result) {
            if (result) {
                sendSuccessResponse(res, result);
            } else {
                sendFailResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
};

exports.getTeacherAvatar = function (req, res) {

    var teacherId = req.query.teacherId;

    teacherDal.getTeacherAvatar(teacherId)
        .then(function (result) {
            if (result) {
                sendSuccessResponse(res, result);
            } else {
                sendFailResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
}
