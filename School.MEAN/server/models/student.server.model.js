﻿var mongoose = require('mongoose');

var studentSchema = new mongoose.Schema({

    user: { type: mongoose.Schema.Types.ObjectId, ref: 'user', required: true },

    admissionRegistrationNo: String,
    joinedDate: String,

    //firstName: String,
    //lastName: String,
    sex: String,
    //birthDate: String,
    caste: String,
    religion: String,
    busfacility: String,

    currentClass: String,
    currentSection: String,
    currentRollno: String,

    addmissionRegisterRef: { type: mongoose.Schema.Types.ObjectId, ref: 'addmissionRegister', required: true },

    currentSubjects: [require('../models/subject.server.model').subjectSchema],
    exams: [require('../models/exam.server.model').examSchema],

    createdBy: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
    },
    createdDate: {
        type: Date,
        required: true
    },
    updatedBy: {
        type: mongoose.Schema.Types.ObjectId
    },
    updatedDate: Date
});

mongoose.model('student', studentSchema);