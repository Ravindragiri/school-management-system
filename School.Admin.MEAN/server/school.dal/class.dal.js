﻿var Q = require('q');
var mongoose = require('mongoose');

require('../models/class.server.model');
require('../models/user.server.model');

var userDal = require('../../server/school.dal/user.dal.js');

var _classModel = mongoose.model('class');
var _user = mongoose.model('user');

require('../../server/school.common/helpers/deferredHelper');

var dal = {};

dal.getClassSection = getClassSection;
dal.getClassSubject = getClassSubject;
dal.getClassStudent = getClassStudent;

module.exports = dal;

function getClassSection(classId) {

}

function getClassSubject(classId) {

}

function getClassStudent(classId) {

    var deferred = Q.defer();

    _classModel
        .findOne({ 'currentClass': classId })
        .populate({ path: 'user' })
        .populate({ path: 'exams' })
        //.populate({ path: 'followers' })
        //.populate({ path: 'favorites' })
        //.populate({ path: 'messages' })
        //.populate({ path: 'profileVisitors' })
        //.populate({ path: 'sharedProfiles' })
        .select('_id province city address pinCode avatarUrl exams')
        .exec(function (err, result) {
            execHelper(deferred, err, result);
        });
    return deferred.promise;
}