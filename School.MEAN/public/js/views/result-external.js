﻿var app = angular.module('schoolApp', ['ngTouch']);

app.controller('ngResultExternalController', function ($scope, $http) {

    $scope.resultDTO = {

        standard: "Seventh",
        section: "A",
        examDate: "JUN-JUL 2018",
        currentRollNo: "21",
        studentName: "Goswami Ravindragiri Gopalgiri",
        externalTotal: "700",
        internalTotal: "200",
        grandTotal: "900",
        creditTotal: "100",
        sgpa: "7.1",
        result: "FIRST",
        lastDateForVerification: "10/03/2018",
        subjects: [
            {
                text: "Hindi",
                internal: {
                    marksObtainedTheory: 69,
                    marksObtainedPractical: 19,
                    GP: "5",
                    CR: "20"
                },
                external: {
                    marksObtainedTheory: 69,
                    marksObtainedPractical: 19,
                    GP: "5",
                    CR: "20"
                }
            },
            {
                text: "Maths",
                internal: {
                    marksObtainedTheory: 61,
                    marksObtainedPractical: 21,
                    GP: "15",
                    CR: "21"
                },
                external: {
                    marksObtainedTheory: 69,
                    marksObtainedPractical: 19,
                    GP: "5",
                    CR: "20"
                }
            }

        ]
    };
});