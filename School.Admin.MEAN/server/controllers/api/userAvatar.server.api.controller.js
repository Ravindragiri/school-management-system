﻿var userAvatarDal = require('../../../server/school.dal/userAvatar.dal.js');
require('../../school.common/helpers/responseHelper');

exports.add = function (req, res) {
    var userAvatarModel = req.body.userAvatarModel;

    userAvatarDal.add(userAvatarModel)
        .then(function (result) {
            if (result) {
                sendSuccessResponse(res, result);
            } else {
                sendFailResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
};

exports.getUserAvatar = function (req, res) {
    var userId = req.query.userId;

    userAvatarDal.getUserAvatar(userId)
        .then(function (result) {
            if (result) {
                sendSuccessResponse(res, result);
            } else {
                sendFailResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
}

exports.update = function (req, res) {
    var userAvatarModel = req.body.userAvatarModel;

    userAvatarDal.update(userAvatarModel)
        .then(function (result) {
            if (result) {
                sendSuccessResponse(res, result);
            } else {
                sendFailResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
};

