﻿updateHelper = function (deferred, err, result) {

    if (err) {
        console.log(err);
        deferred.reject(err);
    }

    if (result) {
        deferred.resolve(result);
    } else {
        deferred.resolve();
    }
}

execHelper = function (deferred, err, result) {

    if (err) {
        console.log(err);
        deferred.reject(err);
    }
    if (result) {
        deferred.resolve(result);
    } else {
        deferred.resolve();
    }
}

createHelper = function (deferred, err, result) {

    if (err) {
        console.log(err);
        deferred.reject(err);
    }
    if (result) {
        deferred.resolve(result);
    } else {
        deferred.resolve();
    }
}
