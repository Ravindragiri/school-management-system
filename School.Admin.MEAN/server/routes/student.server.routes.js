﻿module.exports = function (app) {

    var studentController = require('../controllers/student.server.controller');
    app.get('/student/index', studentController.index);
    app.get('/student', studentController.index);
    app.get('/student/register', studentController.renderRegister);
    app.get('/student/profile', studentController.renderProfile);
    app.get('/student/myProfile', studentController.renderMyProfile);

    var studentApiController = require('../controllers/api/student.server.api.controller');
    //post
    app.post('/api/student/insert', studentApiController.insert);
    app.post('/api/student/mobileRegister', studentApiController.mobileRegister);
    app.post('/api/student/mobileUpdateProfile', studentApiController.mobileUpdateProfile);
    app.post('/api/student/update', studentApiController.update);
    app.post('/api/student/updateProfile', studentApiController.updateProfile);
    app.post('/api/student/remove', studentApiController.remove);
    //get
    app.get('/api/student/getStudentContactDetailById', studentApiController.getStudentContactDetailById);
    app.get('/api/student/getStudentsByAnyCriteria', studentApiController.getStudentsByAnyCriteria);
    app.get('/api/student/getStudentById', studentApiController.getStudentById);
    app.get('/api/student/getStudentByUserId', studentApiController.getStudentByUserId);
    app.get('/api/student/getAllStudentInfo', studentApiController.getAllStudentInfo);
    app.get('/api/student/searchStudents', studentApiController.searchStudents);
    app.get('/api/student/getStudentAvatar', studentApiController.getStudentAvatar);
    app.get('/api/student/getAllStudentsByClassAndSection', studentApiController.getAllStudentsByClassAndSection);
    app.get('/api/student/count', studentApiController.count);
};