﻿//$(document).ready(function () {
//    $('#myTab').on('click', '.nav-tabs a', function () {
//        // set a special class on the '.dropdown' element
//        $(this).closest('.dropdown').addClass('dontClose');
//    })

//    $('#menu1').on('hide.bs.dropdown', function (e) {
//        if ($(this).hasClass('dontClose')) {
//            e.preventDefault();
//        }
//        $(this).removeClass('dontClose');
//    });
//});

// create angular controller

var app = angular.module('schoolApp', ['ngTouch']);

app.controller('ngTeacherMyProfileController', function ($scope, $http) {

    $scope.getContent = function (obj) {
        return obj.value + " " + obj.text;
    };

    var baseUrl = 'http://localhost:1238'

    $scope.teacherDTO = {
        userRef: {}
    };

    $scope.readOne = function () {

        var userId = $("#userId").val();
        //var userId = "5a80a7e1fd268e214c4556a6";

        if (userId) {
            $http({
                method: 'GET',
                url: baseUrl + '/api/teacher/getTeacherByUserId' + '?userId=' + userId
            }).then(function (response) {
                $scope.teacherDTO = response.data.data;
                $scope.addMode = false;
            });
        }
    };
    $scope.readOne();

    $scope.updateProfile = function () {

        var data = {
            teacherDTO: $scope.teacherDTO
        }
        return $http({
            method: 'POST',
            //url: baseUrl + objectName + '/' + id,
            url: baseUrl + '/api/teacher/updateProfile',
            data: data
        }).then(function (response) {

        });
    };

    $scope.updatePassword = function () {

        var data = {
            updatePasswordDTO: {
                _id: $scope.teacherDTO.userRef._id,
                //_id: "5a80a7e1fd268e214c4556a6",
                password: $scope.updatePasswordDTO.password,
                newPassword: $scope.updatePasswordDTO.newPassword,
            }
        }
        $http({
            method: 'POST',
            //url: baseUrl + objectName + '/' + id,
            url: baseUrl + '/api/user/updatePassword',
            data: data
        }).then(function (response) {

        });
    };

    $scope.updateEmail = function () {

        var data = {
            updateEmailDTO: {
                _id: $scope.teacherDTO.userRef._id,
                //_id: "5a80a7e1fd268e214c4556a6",
                password: $scope.updateEmailDTO.password,
                email: $scope.updateEmailDTO.email,
            }
        };

        $http({
            method: 'POST',
            //url: baseUrl + objectName + '/' + id,
            url: baseUrl + '/api/user/updateEmail',
            data: data
        }).then(function (response) {

        });
    };

});

$(function () {

    $("#btnChangePersonalInfo").on('click', function () {
        $('[href=#changePersonalInfo]').tab('show');
    });

    $("#btnChangeEmail").on('click', function () {
        $('[href=#changeEmail]').tab('show');
    });

    $("#btnChangePassword").on('click', function () {
        $('[href=#changePassword]').tab('show');
    });
});
