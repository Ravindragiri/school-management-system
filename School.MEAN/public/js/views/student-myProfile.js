﻿//$(document).ready(function () {
//    $('#myTab').on('click', '.nav-tabs a', function () {
//        // set a special class on the '.dropdown' element
//        $(this).closest('.dropdown').addClass('dontClose');
//    })

//    $('#menu1').on('hide.bs.dropdown', function (e) {
//        if ($(this).hasClass('dontClose')) {
//            e.preventDefault();
//        }
//        $(this).removeClass('dontClose');
//    });
//});

// create angular controller

var app = angular.module('schoolApp', ['ngTouch']);

app.controller('ngStudentMyProfileController', function ($scope, $http) {

    $scope.getContent = function (obj) {
        return obj.value + " " + obj.text;
    };

    var baseUrl = 'http://localhost:1238'

    $scope.studentDTO = {};
    $scope.readOne = function () {

        var userId = $("#userId").val();
        //var userId = "5a7c98ed89737c24c04ca012";

        $http({
            method: 'GET',
            url: baseUrl + '/api/student/getStudentByUserId?userId=' + userId
        }).then(function (response) {
            $scope.studentDTO = response.data.data;
            $scope.addMode = false;
        });
    };
    $scope.readOne();

    $scope.updateProfile = function () {

        var data = {
            studentDTO: $scope.studentDTO
        }
        return $http({
            method: 'POST',
            //url: baseUrl + objectName + '/' + id,
            url: baseUrl + '/api/student/updateProfile',
            data: data
        }).then(function (response) {

        });
    };

    $scope.updatePassword = function () {

        var data = {
            updatePasswordDTO: {
                studentId: $scope.studentDTO._id,
                userId: $scope.studentDTO.userRef._id,
                password: $scope.studentDTO.password,
                newPassword: $scope.studentDTO.newPassword,
            }
        }
        $http({
            method: 'POST',
            //url: baseUrl + objectName + '/' + id,
            url: baseUrl + '/api/user/updatePassword',
            data: data
        }).then(function (response) {

        });
    };

    $scope.updateEmail = function () {

        var data = {
            updateEmailDTO: {
                studentId: $scope.studentDTO._id,
                userId: $scope.studentDTO.userRef._id,
                password: $scope.studentDTO.password,
                email: $scope.studentDTO.email,
            }
        }
        $http({
            method: 'POST',
            //url: baseUrl + objectName + '/' + id,
            url: baseUrl + '/api/user/updateEmail',
            data: data
        }).then(function (response) {

        });
    };

});

$(function () {

    $("#btnChangePersonalInfo").on('click', function () {
        $('[href=#changePersonalInfo]').tab('show');
    });

    $("#btnChangeEmail").on('click', function () {
        $('[href=#changeEmail]').tab('show');
    });

    $("#btnChangePassword").on('click', function () {
        $('[href=#changePassword]').tab('show');
    });
});