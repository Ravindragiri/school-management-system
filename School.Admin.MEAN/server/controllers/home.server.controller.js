﻿'use strict';

var config = require('../config/config');

exports.index = function (req, res) {

    res.render('home/index', {
        pTitle: 'Home | School'
    });
};

