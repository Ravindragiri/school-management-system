﻿var examDal = require('../../../server/school.dal/student.exam.dal.js');
require('../../school.common/helpers/responseHelper');

var mongoose = require('mongoose');
    //fs = require('fs-extra'),
    //util = require("util"),
//path = require("path");

var _exam = mongoose.model('exam');
var _user = mongoose.model('user');


exports.add = function (req, res) {

    var examModel = req.body.examModel;

    _exam.add(examModel)
        .then(function (result) {
            if (result) {
                sendSuccessResponse(res, result);
            } else {
                sendErrorResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
}

exports.removeById = function (req, res) {
    var examId = req.body.examId;

    _exam.removeById(examId)
        .then(function (result) {
            if (result) {
                sendSuccessResponse(res, result, messages.DeleteSuccess);
            } else {
                sendFailResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
};

exports.update = function (req, res) {

    console.log(req.body);

    if (req.body.studentDTO) {

        var studentDTO = req.body.studentDTO;
        var loggedInUserId;

        examDal.update(studentDTO, loggedInUserId)
            .then(function (result) {

                if (result) {
                    sendSuccessResponse(res, result, messages.UpdateSuccess);
                }
                else {
                    sendFailResponse(res, messages.PasswordIncorrect);
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
    else {
        sendBadRequestResponse(res);
    }
};

exports.getExamBySubject = function (req, res) {

    if (req.query.studentId && req.query.subjectCode) {

        var studentId = req.query.studentId;
        var examType = req.query.examType;
        var subjectCode = req.query.subjectCode;

        examDal.getExamBySubject(studentId, examType, subjectCode)
            .then(function (result) {
                if (result) {
                    sendSuccessResponse(res, result);
                } else {
                    sendNoContentResponse(res);
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
    else {
        sendBadRequestResponse(res);
    }
}

exports.getExamByDate = function (req, res) {
    var examDate = req.query.examDate;

    _exam.getExamByDate(examDate)
        .then(function (result) {
            if (result) {
                sendSuccessResponse(res, result);
            } else {
                sendNoContentResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
}

exports.getExamById = function (req, res) {
    var examId = req.query.examId;

    examDal.getExamById(examId)
        .then(function (result) {
            if (result) {
                sendSuccessResponse(res, result);
            } else {
                sendNoContentResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
}

exports.addMarksheet = function (req, res) {

    if (req.body.data.examDTO && req.body.data.studentId) {

        var examModel = _exam(req.body.data.examDTO);
        var studentId = req.body.data.studentId;
        var userId = req.body.data.userId;

        examDal.insertMarks(examModel, studentId, userId)
            .then(function (result) {
                if (result) {
                    sendSuccessResponse(res, result);
                } else {
                    sendErrorResponse(res);
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
    else {
        
    }
}

exports.getMarksheet = function (req, res) {

    if (req.query.studentId && req.query.examType) {

        var studentId = req.query.studentId;
        var classId = req.query.classId;
        var examType = req.query.examType;

        examDal.getStudentMarksDetails(studentId, classId, examType)
            .then(function (result) {
                if (result) {
                    sendSuccessResponse(res, result);
                } else {
                    sendNoContentResponse(res);
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
    else {
        sendBadRequestResponse(res);
    }
}

exports.getAllStudentMarks = function (req, res) {

    if (req.query.examType) {

        var classId = req.query.classId;
        var examType = req.query.examType;

        examDal.getAllStudentMarks(classId, examType)
            .then(function (result) {
                if (result) {
                    sendSuccessResponse(res, result);
                } else {
                    sendNoContentResponse(res);
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
    else {
        sendBadRequestResponse(res);
    }
}

exports.smsExamMarks = function (req, res) {

    var examId = req.query.examId;
    var classId = req.query.classId;
    var receiverType = req.query.receiverType;  //student or parent
}

exports.remove = function (req, res) {
    
    console.log(req.query); 

    if (req.query.examId && req.query.studentId) {
        var examId = req.query.examId;
        var studentId = req.query.studentId;

        examDal.remove(studentId, examId)
            .then(function (result) {
                if (result) {
                    sendSuccessResponse(res, result);
                } else {
                    sendNoContentResponse(res);
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
    else {
        sendBadRequestResponse(res);
    }
}

