﻿var Q = require('q');
var mongoose = require('mongoose')

require('../models/userAvatar.server.model');
var userAvatar = mongoose.model('userAvatar');

var dal = {};

dal.createUserAvatar = createUserAvatar;
dal.getUserAvatar = getUserAvatar;
dal.updateUserAvatar = updateUserAvatar;

module.exports = dal;

function createUserAvatar(userModel) {

    var deferred = Q.defer();

    userModel
        .save(function (err, result) {
            if (err) {
                console.log(err);
                deferred.reject(err);
            }

            if (result) {
                deferred.resolve(result);
            } else {
                deferred.resolve();
            }
        });

    return deferred.promise;
}

function getUserAvatar(userId) {

    var deferred = Q.defer();

    _userAvatar
        .findOne({ '_id': userId })
        .exec(function (err, userAvatar) {

            if (err) {
                console.log(err);
                deferred.reject(err);
            }
            if (userAvatar) {
                deferred.resolve(userAvatar);
            } else {
                deferred.resolve();
            }
        });
    return deferred.promise;
}

function updateUserAvatar(userModel, userId) {

    var deferred = Q.defer();

    userModel.findOneAndUpdate({ _id: userId }, { $set: { avatarUrl: userModel.avatarUrl } }, { new: true }, function (err, user) {
        if (err) {
            console.log(err);
            deferred.reject(err);
        }
        if (user) {
            deferred.resolve(user);
            console.log('user avatar successfully deleted!');
        } else {
            deferred.resolve();
        }


    });
    return deferred.promise;
}