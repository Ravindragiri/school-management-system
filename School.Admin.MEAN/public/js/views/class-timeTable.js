﻿var app = angular.module('schoolApp', ['ngTouch', 'ui.grid', 'ui.grid.expandable', 'ui.grid.selection', 'ui.grid.pinning']);

app.controller('ngClassTimeTableController', ['ClassTimeTableService', '$scope', function (ClassTimeTableService, $scope) {

    $scope.addMode = true;

    $scope.gridOptions = {
        excludeProperties: '__metadata',
        enableRowSelection: true,
        expandableRowTemplate: '<div ui-grid="row.entity.subGridOptions" style="height:140px;"></div>',
        expandableRowHeight: 150,
        expandableRowScope: {
            subGridVariable: 'subGridScopeVariable'
        }
    };
    $scope.gridOptions.columnDefs = [
        { name: 'class', pinnedLeft: true, cellTemplate: '<div class="ui-grid-cell-contents">{{ COL_FIELD }}</div>' },
        { name: 'section', cellTemplate: '<div class="ui-grid-cell-contents">{{ COL_FIELD }}</div>' },
        { name: 'edit', cellTemplate: '<button type="button" class="btn btn-xs" ng-click="grid.appScope.editTimeTable(row)">Edit</button>' },
        { name: 'delete', field: '_id', cellTemplate: '<button type="button" class="btn btn-xs" ng-click="grid.appScope.deleteTimeTable(row)">Delete</button>' }
    ];

    $scope.load = function () {
        ClassTimeTableService.readAll().then(function (response) {
            var data = response.data;
            for (i = 0; i < data.length; i++) {

                data[i].subGridOptions = {
                    columnDefs: [
                        { name: 'subject', field: 'subject' },
                        { name: 'day', field: 'day' },
                        { name: 'timeStart', field: 'timeStart' },
                        { name: 'timeEnd', field: 'timeEnd' }
                    ],
                    data: data[i].timeTableList
                };
            }
            $scope.gridOptions.data = data;
        });
    }
    $scope.load();

    $scope.update = function () {
        var data = {
            classDTO: $scope.classDTO
        }
        ClassTimeTableService.update(data).then(function (response) {
            
            if (response != null) {
                var status = response.statusCode;

                switch (status) {
                    case 200:
                        $scope.addMode = true;
                        alert("Record has been updated successfully.");
                        break;
                    case 0:
                        alert("Your request cannot be processed now due to some technical issue. please try again later.");
                        break;
                    case -1:
                    case 400:
                    case 401:
                    case 204:
                        alert(response.message);
                        break;
                    default:
                }
            }
        });
    }

    $scope.cancel = function () {
        $scope.classDTO = {
            class: '',
            section: '',
            timeTableList: []
        };
        $scope.addMode = true;
    }

    $scope.editTimeTable = function (row) {
        if (row.entity) {
            $scope.classDTO = row.entity;
            //var index = $scope.gridOptions.data.indexOf(row.entity);
            $scope.addMode = false;
        }
    };

    $scope.deleteTimeTable = function (row) {
        if (row.entity) {
            var id = row.entity._id;
            ClassTimeTableService.delete(id).then(function (response) {
                if (response != null) {
                    var status = response.statusCode;

                    switch (status) {
                        case 200:
                            alert("Record has been deleted successfully.");
                            break;
                        case 0:
                            alert("Your request cannot be processed now due to some technical issue. please try again later.");
                            break;
                        case -1:
                        case 400:
                        case 401:
                        case 204:
                            alert(response.message);
                            break;
                        default:
                    }
                }
            });
        }
    };



    $scope.classDTO = {
        class: '',
        section: '',
        timeTableList: []
    };

    $scope.add = function () {
        $scope.classDTO.timeTableList.push({
            subject: '',
            day: '',
            timeStart: '',
            timeEnd: ''
        });
    }

    $scope.insert = function () {
        var data = {
            classDTO: $scope.classDTO
        }
        ClassTimeTableService.create(data).then(function (response) {
            if (response != null) {
                var status = response.statusCode;

                switch (status) {
                    case 200:
                        alert("Record has been saved successfully.");
                        break;
                    case 0:
                        alert("Your request cannot be processed now due to some technical issue. please try again later.");
                        break;
                    case -1:
                    case 400:
                    case 401:
                    case 204:
                        alert(response.message);
                        break;
                    default:
                }
            }
        });
    }

    $scope.del = function (i) {
        $scope.classDTO.timeTableList.splice(i, 1);
    }

    $scope.search = function () {
        var data = {
            class: $scope.classDTO.class,
            section: $scope.classDTO.section
        };

        ClassTimeTableService.getClassTimeTableByClassAndSection(data).then(function (response) {
            $scope.classDTO = response.data;
        });
    }
}]);


(function () {

    angular.module('schoolApp')
        .service('ClassTimeTableService', ['$http', ClassTimeTableService]);

    function ClassTimeTableService($http) {

        var self = this;
        var baseUrl = 'http://localhost:1238';

        //var objectName = 'products';

        self.readAll = function () {
            return $http({
                method: 'GET',
                url: baseUrl + '/api/class/timeTable/getAll',
                //url: baseUrl + objectName
            }).then(function (response) {
                return response.data;
            });
        };

        self.getClassTimeTableByClassAndSection = function (data) {
            return $http({
                method: 'GET',
                url: baseUrl + '/api/class/timeTable/getByClassAndSection',
                params: data,
                //url: baseUrl + objectName
            }).then(function (response) {
                return response.data;
            }).catch(function (response) {
                console.error('Error occurred:', response.status, response.data);
            });
        };

        self.readOne = function (id) {
            return $http({
                method: 'GET',
                url: baseUrl + objectName + '/' + id
            }).then(function (response) {
                return response.data;
            });
        };

        self.create = function (data) {
            return $http({
                method: 'POST',
                //url: baseUrl + objectName,
                url: 'http://localhost:1238/api/class/timeTable/add',
                data: data,
                params: {
                    returnObject: true
                },
            }).then(function (response) {
                return response.data;
            });
        };

        self.update = function (data) {
            return $http({
                method: 'POST',
                //url: baseUrl + objectName + '/' + id,
                url: 'http://localhost:1238/api/class/timeTable/update',
                data: data
            }).then(function (response) {
                return response.data;
            });
        };

        self.delete = function (id) {
            return $http({
                method: 'POST',
                //url: baseUrl + objectName + '/' + id
                url: 'http://localhost:1238/api/class/timeTable/removeById' + '?id=' + id
            }).then(function (response) {
                return response.data;
            });
        };

    }
} ());