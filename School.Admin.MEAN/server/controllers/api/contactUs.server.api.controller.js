﻿var contactUsDal = require('../../school.dal/contactUs.dal.js');
require('../../school.common/helpers/responseHelper');

exports.sendMessage = function (req, res) {

    if (req.body.contactUsDTO) {

        var contactUsDTO = req.body.contactUsDTO;
        contactUsDTO.createdDate = new Date();

        contactUsDal.sendMessage(contactUsDTO)
            .then(function (result) {
                if (result) {
                    sendSuccessResponse(res, result);
                } else {
                    sendFailResponse(res);
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
};