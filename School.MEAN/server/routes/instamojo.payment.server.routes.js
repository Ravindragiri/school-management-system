﻿module.exports = function (app) {

    var instamojoController = require('../controllers/instamojo.server.controller');
    app.get('/instamojo/success', instamojoController.sucess);
    

    var instamojoApiController = require('../controllers/api/instamojo.server.api.controller');
    app.post('/api/payment/createRequest', instamojoApiController.createRequest);
    app.post('/instamojo/webhook', instamojoApiController.webhook);
};