﻿var timeTableDal = require('../../school.dal/class.timeTable.dal.js');
require('../../school.common/helpers/responseHelper');

var mongoose = require('mongoose');
var timeTable = mongoose.model('classRoutine');
require('../../school.common/appConstants');

exports.add = function (req, res) {

    console.log(req.body.classDTO);

    if (req.body.classDTO) {
        var timeTableModel = new timeTable(req.body.classDTO);

        timeTableDal.add(timeTableModel)
            .then(function (result) {
                if (result) {
                    sendSuccessResponse(res, result);
                } else {
                    sendErrorResponse(res);
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
    else {
        sendBadRequestResponse(res);
    }
};

exports.removeById = function (req, res) {

    if (req.query.id) {
        var timeTableId = req.query.id;

        timeTableDal.removeById(timeTableId)
            .then(function (result) {
                if (result) {
                    sendSuccessResponse(res, result, messages.DeleteSuccess);
                } else {
                    sendFailResponse(res);
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
    else {
        sendBadRequestResponse(res);
    }
};

exports.getTimeTableById = function (req, res) {

    if (req.query.timeTableId) {
        var timeTableId = req.query.timeTableId;

        timeTableDal.getClassTimeTableById(timeTableId)
            .then(function (result) {
                if (result) {

                    sendSuccessResponse(res, result);
                } else {
                    sendNoContentResponse(res);
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
    else {
        sendBadRequestResponse(res);
    }
}

exports.getClassTimeTableByClassAndSection = function (req, res) {

    if (req.query.class && req.query.section) {
        var className = req.query.class;
        var section = req.query.section;
        timeTableDal.getClassTimeTableByClassAndSection(className, section)
            .then(function (result) {
                if (result) {

                    sendSuccessResponse(res, result);
                } else {
                    sendNoContentResponse(res);
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
}

exports.getAllTimeTables = function (req, res) {

    timeTableDal.getAllTimeTables()
        .then(function (result) {
            if (result) {

                sendSuccessResponse(res, result);
            } else {
                sendNoContentResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
}

exports.update = function (req, res) {

    console.log(req.body);

    if (req.body.classDTO) {

        var classDTO = req.body.classDTO;
        var loggedInUserId;

        timeTableDal.update(classDTO, loggedInUserId)
            .then(function (result) {

                if (result) {
                    sendSuccessResponse(res, result, messages.UpdateSuccess);
                }
                else {
                    sendFailResponse(res, messages.PasswordIncorrect);
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
    else {
        sendBadRequestResponse(res);
    }
};

exports.getTimeTableByDay = function (req, res) {

    if (req.query.day) {
        var day = req.query.day;

        timeTableDal.getTimeTableByDay(day)
            .then(function (result) {
                if (result) {

                    sendSuccessResponse(res, result);
                } else {
                    sendNoContentResponse(res);
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
    else {
        sendBadRequestResponse(res);
    }
};