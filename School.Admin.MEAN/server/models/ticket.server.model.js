﻿var mongoose = require('mongoose');

var ticketSchema = new mongoose.Schema({

    title: String,
    code: String,
    status: String,
    priority: String,
    description: String,
    createdBy: {
        type: mongoose.Schema.Types.ObjectId
    },
    createdDate: {
        type: Date
    }
});

mongoose.model('ticket', ticketSchema);