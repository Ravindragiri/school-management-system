﻿module.exports = function (app) {

    var error = require('../controllers/error.server.controller');

    app.get('/error/index', error.index);
    app.get('/error', error.index);
};