﻿module.exports = function (app) {

    var teacherController = require('../controllers/teacher.server.controller');
    app.get('/teacher/index', teacherController.index);
    app.get('/teacher', teacherController.index);
    app.get('/teacher/register', teacherController.renderRegister);
    app.get('/teacher/myProfile', teacherController.renderMyProfile);
};