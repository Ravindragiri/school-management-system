﻿var mongoose = require('mongoose');

var examTimeTableSchema = new mongoose.Schema({

    code: String,
    name: String,
    description: String,
    examType: String,
    class: String,
    section: String,

    timeTableList: [{
        subject: String,
        examDate: Date,
        timeStart: String,
        timeEnd: String
    }],

    acedamicYear: String,

    createdBy: {
        type: mongoose.Schema.Types.ObjectId
    },
    createdDate: {
        type: Date
    },
    updatedBy: {
        type: mongoose.Schema.Types.ObjectId
    },
    updatedDate: Date
});

exports.examTimeTableSchema = examTimeTableSchema;
mongoose.model('examTimeTable', examTimeTableSchema);