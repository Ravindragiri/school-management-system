﻿var app = angular.module('schoolApp', ['ngTouch']);

app.controller('ngClassTimeTableController', ['ClassTimeTableService', '$scope', function (ClassTimeTableService, $scope) {

    $scope.search = function () {

        $scope.timeTableList = [];
        var data = {
            class: $scope.class,
            section: $scope.section
        }

        ClassTimeTableService.getClassTimeTableByClassAndSection(data).then(function (response) {
            $scope.classDTO = response.data;
        });
    }
}]);

(function () {

    angular.module('schoolApp')
        .service('ClassTimeTableService', ['$http', ClassTimeTableService]);

    function ClassTimeTableService($http) {

        var self = this;
        var baseUrl = 'http://localhost:1238';

        //var objectName = 'products';

        self.readAll = function (data) {
            return $http({
                method: 'GET',
                params: data,
                url: baseUrl + '/api/class/timeTable/getClassTimeTableByClassAndSection'
            }).then(function (response) {
                return response.data;
            });
        };

        self.getClassTimeTableByClassAndSection = function (data) {
            return $http({
                method: 'GET',
                url: baseUrl + '/api/class/timeTable/getByClassAndSection',
                params: data,
                //url: baseUrl + objectName
            }).then(function (response) {
                return response.data;
            }).catch(function (response) {
                console.error('Error occurred:', response.status, response.data);
            });
        };
    }
} ());