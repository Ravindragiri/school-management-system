﻿var mongoose = require('mongoose');

var feesRegisterSchema = new mongoose.Schema({

    receiptNumber: String,
    receivedDate: Date,
    formFee: Number,
    libraryFee: Number,
    labFee: Number,
    developmentFee: Number,
    computerFee: Number,
    sportFee: Number,
    latefee: Number,
    total: Number,

    createdBy: {
        type: mongoose.Schema.Types.ObjectId
    },
    createdDate: {
        type: Date
    },
    updatedBy: {
        type: mongoose.Schema.Types.ObjectId
    },
    updatedDate: Date
});

mongoose.model('feesRegister', feesRegisterSchema);