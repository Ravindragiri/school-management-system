﻿var mongoose = require("mongoose");
var userModel = require('../server/models/user.server.model');
var server = require('../server');
require('../test/common/test.result.handler');

//Require the dev-dependencies
var chai = require('chai');
var chaiHttp = require('chai-http');
var should = chai.should();

chai.use(chaiHttp);

describe('Users', function () {
    
    //userModel.collection.drop();

    //getOfficeStaffById starts
    /*
    it('getOfficeStaffById #1', function (done) {
        chai.request(server)
            .get('/api/officeStaff/getOfficeStaffById?officeStaffID=5a6a0dc5a06beb21006f9968')
            .end(function (err, res) {
                getOfficeStaffByIdResultWithData(res);
                done();
            });
    });

    it('getOfficeStaffById #2', function (done) {
        chai.request(server)
            .get('/api/officeStaff/getOfficeStaffById')
            .end(function (err, res) {
                resultWithBadRequest(res);
                done();
            });
    });

    it('getOfficeStaffById #3', function (done) {
        chai.request(server)
            .get('/api/officeStaff/getOfficeStaffById?')
            .end(function (err, res) {
                resultWithBadRequest(res);
                done();
            });
    });

    it('getOfficeStaffById #4', function (done) {
        chai.request(server)
            .get('/api/officeStaff/getOfficeStaffById?officeStaffID=577e0fa784fd15ec5db7d6c1')
            .end(function (err, res) {
                resultNotFoundWithoutMessage(res);
                done();
            });
    });
    */
    //getOfficeStaffById ends

    //getOfficeStaffContactDetailById starts
    /*
    it('getOfficeStaffContactDetailById #1', function (done) {
        chai.request(server)
            .get('/api/officeStaff/getOfficeStaffContactDetailById?officeStaffID=5a6a0dc5a06beb21006f9968')
      .end(function (err, res) {
            getOfficeStaffContactDetailByIdResultWithData(res);
            done();
        });
    });
    
    it('getOfficeStaffContactDetailById #2', function (done) {
        chai.request(server)
      .get('/api/officeStaff/getOfficeStaffContactDetailById')
      .end(function (err, res) {
            resultWithBadRequest(res);
            done();
        });
    });
    
    it('getOfficeStaffContactDetailById #3', function (done) {
        chai.request(server)
      .get('/api/officeStaff/getOfficeStaffContactDetailById?')
      .end(function (err, res) {
            resultWithBadRequest(res);
            done();
        });
    });
    
    it('getOfficeStaffContactDetailById #4', function (done) {
        chai.request(server)
            .get('/api/officeStaff/getOfficeStaffContactDetailById?officeStaffID=5a6a0dc5a06beb21006f9968')
      .end(function (err, res) {
            getOfficeStaffContactDetailByIdResultWithData(res);
            done();
        });
    });
    
    it('getOfficeStaffContactDetailById #5', function (done) {
        chai.request(server)
            .get('/api/officeStaff/getOfficeStaffContactDetailById?officeStaffID=577e0fa784fd15ec5db7d6c1')
      .end(function (err, res) {
            resultNotFoundWithoutMessage(res);
            done();
        });
    });
    */
    //getOfficeStaffContactDetailById ends

    //count starts
    /*
    it('count #1', function (done) {
        chai.request(server)
            .get('/api/officeStaff/count')
            .end(function (err, res) {
                resultSuccess(res);
                done();
            });
    });
    */
    //count ends

    //getOfficeStaffProfileInfo starts

    it('getOfficeStaffProfileInfo #1', function (done) {
        chai.request(server)
            .get('/api/officeStaff/getOfficeStaffProfileInfo?userID=5a6a0dc5a06beb21006f9969')
            .end(function (err, res) {
                getOfficeStaffByIdResultWithData(res);
                done();
            });
    });

    it('getOfficeStaffById #2', function (done) {
        chai.request(server)
            .get('/api/officeStaff/getOfficeStaffProfileInfo')
            .end(function (err, res) {
                resultWithBadRequest(res);
                done();
            });
    });

    it('getOfficeStaffById #3', function (done) {
        chai.request(server)
            .get('/api/officeStaff/getOfficeStaffProfileInfo?')
            .end(function (err, res) {
                resultWithBadRequest(res);
                done();
            });
    });

    it('getOfficeStaffById #4', function (done) {
        chai.request(server)
            .get('/api/officeStaff/getOfficeStaffProfileInfo?userID=5a6a0dc5a06beb21006f9969')
            .end(function (err, res) {
                resultNotFoundWithoutMessage(res);
                done();
            });
    });

    //getOfficeStaffProfileInfo ends
});