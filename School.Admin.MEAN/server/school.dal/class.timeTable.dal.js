﻿var Q = require('q');
var mongoose = require('mongoose'),
    async = require('async');

require('../models/classRoutine.server.model');
require('../models/user.server.model');

var userDal = require('../../server/school.dal/user.dal.js');

var _classTimeTable = mongoose.model('classRoutine');
var _user = mongoose.model('user');

require('../../server/school.common/helpers/deferredHelper');

var dal = {};

dal.add = add;
dal.getClassTimeTableById = getClassTimeTableById;
dal.getAllTimeTables = getAllTimeTables;
dal.getClassTimeTableByClassAndSection = getClassTimeTableByClassAndSection;
dal.getTimeTableByDay = getTimeTableByDay;
dal.getClassTimeTableContactDetailById = getClassTimeTableContactDetailById;
dal.register = register;
dal.mobileRegister = mobileRegister;
dal.getClassTimeTablesByAnyCriteria = getClassTimeTablesByAnyCriteria;
dal.getAllClassTimeTableInfo = getAllClassTimeTableInfo;
dal.getClassTimeTableProfileInfo = getClassTimeTableProfileInfo;
dal.getClassTimeTableByUserId = getClassTimeTableByUserId;
dal.count = count;
dal.update = update;
//dal.insertTimeTable = insertTimeTable;
//dal.displayTimeTable = displayTimeTable;
//dal.searchTimeTable = searchTimeTable;
dal.removeById = removeById;

module.exports = dal;

function add(classTimeTableModel) {

    var deferred = Q.defer();

    classTimeTableModel
        .save(function (err, result) {
            if (err) deferred.reject(err);

            if (result) {
                deferred.resolve(result);
            } else {
                deferred.resolve();
            }
        });

    return deferred.promise;
}

function getAllTimeTables() {

    var deferred = Q.defer();

    _classTimeTable
        .find({})
        .exec(function (err, result) {
            execHelper(deferred, err, result);
        });
    return deferred.promise;
}

function getClassTimeTableByClassAndSection(className, section) {

    var deferred = Q.defer();

    _classTimeTable
        .findOne({
            $and: [
                { 'class': className },
                { 'section': section }
            ]
        })
        .exec(function (err, result) {
            execHelper(deferred, err, result);
        });
    return deferred.promise;
}

function getTimeTableByDay(day) {

    var deferred = Q.defer();

    _classTimeTable
        .findOne({ 'timeTableList.day': day })
        .exec(function (err, result) {
            execHelper(deferred, err, result);
        });
    return deferred.promise;
}

function getClassTimeTableById(_id) {

    var deferred = Q.defer();

    _classTimeTable
        .findOne({ '_id': _id })
        .exec(function (err, result) {
            execHelper(deferred, err, result);
        });
    return deferred.promise;
}

function getClassTimeTableByUserId(userId) {

    var deferred = Q.defer();

    _classTimeTable
        .findOne({})
        .populate({
            path: 'userRef',
            match: { _id: userId },
        })
        .exec(function (err, result) {
            execHelper(deferred, err, result);
        });
    return deferred.promise;
}

function getClassTimeTableContactDetailById(classTimeTableId) {

    var deferred = Q.defer();

    _classTimeTable
        .find({ _id: classTimeTableId })
        .populate({ path: 'userRef' })
        .exec(function (err, result) {
            execHelper(deferred, err, result);
        });
    return deferred.promise;
}

function register(classTimeTableModel, userModel) {

    //var userDal = require('./user.dal.js');
    var deferred = Q.defer();

    async.waterfall(
        [
            function (callback) {

                userModel.createdDate = new Date();

                userDal.add(userModel)
                    .then(function (user) {

                        if (user) {

                            //classTimeTableModel.user._id = user._id;
                            classTimeTableModel.userRef = user._id;
                            classTimeTableModel.createdBy = user._id;
                            classTimeTableModel.createdDate = new Date();
                            callback(null, classTimeTableModel);
                        } else {
                            deferred.resolve();
                        }
                    })
                    .catch(function (err) {
                        if (err) deferred.reject(err);
                    });
            },
            function (classTimeTableModel, callback) {
                add(classTimeTableModel)
                    .then(function (classTimeTable) {

                        if (classTimeTable) {
                            deferred.resolve(classTimeTable);
                        } else {
                            deferred.resolve();
                        }
                    })
                    .catch(function (err) {
                        if (err) deferred.reject(err);
                    });
            }
        ], function (errs, results) {
            if (errs) {
                async.each(results, rollback, function () {
                    console.log('Rollback done.');
                    res.status(400).send(errs);
                });
            } else {
                console.log('Done.');
                deferred.resolve(results);
            }
        });

    function rollback(doc, callback) {
        if (!doc) { callback(); }
        else {
            userModel.findByIdAndRemove(doc._id, function (err, doc) {
                console.log('Rolled-back document: ' + doc);
                callback();
            });
        }
    }
    return deferred.promise;
}

function mobileRegister(classTimeTableModel, userModel) {

    //var userDal = require('./user.dal.js');
    var deferred = Q.defer();

    async.waterfall(
        [
            function (callback) {

                userModel.createdDate = new Date();

                userDal.add(userModel)
                    .then(function (user) {

                        if (user) {

                            classTimeTableModel.userRef = user._id;
                            classTimeTableModel.createdBy = user._id;
                            classTimeTableModel.createdDate = new Date();
                            callback(null, classTimeTableModel);
                        } else {
                            deferred.resolve();
                        }
                    })
                    .catch(function (err) {
                        if (err) deferred.reject(err);
                    });
            },
            function (classTimeTableModel, callback) {
                add(classTimeTableModel)
                    .then(function (classTimeTable) {

                        if (classTimeTable) {
                            deferred.resolve(classTimeTable);
                        } else {
                            deferred.resolve();
                        }
                    })
                    .catch(function (err) {
                        if (err) deferred.reject(err);
                    });
            }
        ], function (errs, results) {
            if (errs) {
                async.each(results, rollback, function () {
                    console.log('Rollback done.');
                    res.status(400).send(errs);
                });
            } else {
                console.log('Done.');
                deferred.resolve(results);
            }
        });

    function rollback(doc, callback) {
        if (!doc) { callback(); }
        else {
            userModel.findByIdAndRemove(doc._id, function (err, doc) {
                console.log('Rolled-back document: ' + doc);
                callback();
            });
        }
    }
    return deferred.promise;
}

function getClassTimeTablesByAnyCriteria(searchCriteria, searchText) {

    var deferred = Q.defer();

    _classTimeTable
        //.find({
        //    loc: {
        //        $near: searchCriteria.coords,
        //        $maxDistance: searchCriteria.maxDistance
        //    }
        //})
        .find({
            '$or': [
                { designation: new RegExp(searchText, 'i') },
                { qualification: new RegExp(searchText, 'i') }
            ]
        })
        .populate({ path: 'userRef' })
        //.populate({ path: 'classs' })
        //.populate({ path: 'followers' })
        //.populate({ path: 'favorites' })
        //.populate({ path: 'messages' })
        //.populate({ path: 'profileVisitors' })
        //.populate({ path: 'sharedProfiles' })
        .exec(function (err, result) {
            execHelper(deferred, err, result);
        });
    return deferred.promise;
}

function getAllClassTimeTableInfo() {

    var deferred = Q.defer();

    _classTimeTable
        .find({})
        .exec(function (err, result) {
            execHelper(deferred, err, result);
        });
    return deferred.promise;
}

function getClassTimeTableProfileInfo(userId) {

    var deferred = Q.defer();

    _classTimeTable
        .findOne({})
        .populate({
            path: 'user',
            match: { _id: userId },
        })
        .exec(function (err, result) {
            execHelper(deferred, err, result);
        });
    return deferred.promise;
}

function count() {

    var deferred = Q.defer();

    _classTimeTable
        .count({})
        .exec(function (err, result) {
            execHelper(deferred, err, result);
        });
    return deferred.promise;
}

function update(classTimeTableDTO, loggedInUserId) {

    var deferred = Q.defer();

    _classTimeTable.findById({ _id: classTimeTableDTO._id }, function (err, data) {
        if (err) {
            console.log(err);
            deferred.reject(err);
        }
        if (data) {

            data.timeTableList = [];
            //data.updatedBy = loggedInUserId;
            data.updatedDate = new Date();

            data.save(function (err, result) {
                var classTimeTableModel = new _classTimeTable(classTimeTableDTO);
                var timeTableList = classTimeTableModel.timeTableList;

                timeTableList.forEach(function (timeTable) {

                    if (!timeTable._id) {
                        timeTable._id = new ObjectId;
                    }
                    //timeTable.updatedBy = loggedInUserId;
                    //timeTable.updatedDate = new Date();

                    _classTimeTable
                        .findByIdAndUpdate({ _id: classTimeTableModel._id }, { $push: { "timeTableList": timeTable } }, { safe: true, upsert: true }, function (err, result) {
                            if (err) {
                                console.log(err);
                                deferred.reject(err);
                            }
                            if (result) {
                                deferred.resolve(result);
                            } else {
                                deferred.resolve();
                            }
                        });
                });
            });
        }
    });
    return deferred.promise;
}

function insertTimeTable() {
}

function displayTimeTable() {
}

function searchTimeTable(classType, studentClass, section) {
}

function removeById(timeTableId) {

    var deferred = Q.defer();

    _classTimeTable.findById({ _id: timeTableId }, function (err, data) {

        if (err) {
            console.log(err);
            deferred.reject(err);
        }
        if (data) {

            data.timeTableList = [];
            //exam.updatedBy = loggedInUserId;
            data.updatedDate = new Date();

            data.save(function (err, timeTable) {
                if (err) {
                    deferred.reject(err);
                }
                if (timeTable) {
                    console.log('timeTable successfully deleted!');
                    deferred.resolve(timeTable);
                } else {
                    deferred.resolve();
                }
            });
        }
    });
    return deferred.promise;
}