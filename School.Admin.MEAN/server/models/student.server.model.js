﻿var mongoose = require('mongoose');

var studentSchema = new mongoose.Schema({

    userRef: { type: mongoose.Schema.Types.ObjectId, ref: 'user', required: true },
    parentRef: { type: mongoose.Schema.Types.ObjectId, ref: 'parent' },
    dormitoryRef: { type: mongoose.Schema.Types.ObjectId, ref: 'dormitory' },

    admissionRegistrationNo: String,
    joinedDate: String,
    busFacility: String,
    birthCertificateNo: String,

    currentClass: String,
    currentSection: String,
    currentRollNo: String,

    admissionRegisterRef: { type: mongoose.Schema.Types.ObjectId, ref: 'admissionRegister' },

    currentSubjects: [require('../models/subject.server.model').subjectSchema],
    exams: [require('../models/exam.server.model').examSchema],
    attendances: [require('../models/attendance.server.model').attendanceSchema],

    status: String,
    isDeleted: Boolean,

    createdBy: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
    },
    createdDate: {
        type: Date,
        required: true
    },
    updatedBy: {
        type: mongoose.Schema.Types.ObjectId
    },
    updatedDate: Date
});
exports.studentSchema = studentSchema;
mongoose.model('student', studentSchema);