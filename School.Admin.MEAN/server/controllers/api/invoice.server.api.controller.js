﻿var invoiceDal = require('../../../server/school.dal/invoice.dal.js');
require('../../school.common/helpers/responseHelper');

var mongoose = require('mongoose'),
    //fs = require('fs-extra'),
    //util = require("util"),
    async = require('async'),
    moment = require("moment");
//path = require("path");

var _invoice = mongoose.model('invoice');
var _payment = mongoose.model('payment');
var _user = mongoose.model('user');

function formatServerDate(candidateDTO) {
    if (candidateDTO.createdDate) {
        candidateDTO.createdDate = moment(candidateDTO.createdDate).format("DD/MM/YYYY");
    }
    return candidateDTO;
}

function parseClientDate(candidateDTO) {
    if (candidateDTO.createdDate) {
        candidateDTO.createdDate = moment(candidateDTO.createdDate, 'DD/MM/YYYY').toDate();
    }
    return candidateDTO;
}

exports.insert = function (req, res) {

    if (req.body.invoiceDTO) {

        var invoiceDTO = req.body.invoiceDTO;
        invoiceDTO = parseClientDate(invoiceDTO);
        var invoiceModel = new _invoice(invoiceDTO);
        var paymentModel = new _payment(invoiceDTO);

        invoiceDal.insert(invoiceModel, paymentModel)
            .then(function (result) {
                if (result) {
                    sendSuccessResponse(res, result);
                } else {
                    sendErrorResponse(res);
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
    else {
        sendBadRequestResponse(res);
    }
}

exports.update = function (req, res) {
    if (req.body.invoiceDTO) {
        var invoiceDTO = req.body.invoiceDTO;
        invoiceDTO = parseClientDate(invoiceDTO);
        invoiceDal.getInvoiceById(invoiceDTO.Id)
            .then(function (invoiceModel) {

                if (invoiceModel) {
                    if (invoiceModel.Id > 0) {

                        invoiceModel.title = invoiceDTO.title;
                        invoiceModel.desc = invoiceDTO.desc;
                        invoiceModel.amount = invoiceDTO.amount;
                        invoiceModel.status = invoiceDTO.status;
                        invoiceModel.date = invoiceDTO.date;

                        invoiceDal.updateInvoice(invoiceModel)
                            .then(function (user) {

                                if (user) {
                                    sendSuccessResponse(res, result, messages.UpdateSuccess);
                                }
                                else {
                                    sendFailResponse(res, messages.PasswordIncorrect);
                                }
                            })
                            .catch(function (err) {
                                console.log(err);
                                sendErrorResponse(res);
                            });
                    }
                }
            })
            .catch(function (err) {
                console.log(err);
                sendErrorResponse(res);
            });
    }
    else {
        sendBadRequestResponse(res);
    }
};

exports.remove = function (req, res) {
    var invoiceModel = req.body.invoiceModel;
    var invoiceId = req.body.invoiceId;

    invoiceDal.deleteInvoice(invoiceModel, invoiceId)
        .then(function (result) {
            if (result) {
                sendSuccessResponse(res, result, messages.DeleteSuccess);
            } else {
                sendFailResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
};

exports.getAllInvoiceInfo = function (req, res) {

    invoiceDal.getAllInvoiceInfo()
        .then(function (result) {
            if (result) {
                for (j = 0; j < result.length; j++) {
                    result[j] = formatServerDate(result[j])
                }
                sendSuccessResponse(res, result);
            } else {
                invoice
                sendNoContentResponse(res);
            }
        })
        .catch(function (err) {
            console.log(err);
            sendErrorResponse(res);
        });
}
