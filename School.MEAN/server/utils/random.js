var Random = {
    generateRandomString : generateRandomString
};

var possibleChars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
function generateRandomString(length) {
    var result = '';
    for (var i = length; i > 0; --i) result += possibleChars[Math.floor(Math.random() * possibleChars.length)];
    return result;
}

module.exports = Random;