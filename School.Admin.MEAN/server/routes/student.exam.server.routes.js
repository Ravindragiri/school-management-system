﻿module.exports = function (app) {

    var studentExamController = require('../controllers/student.exam.server.controller');
    app.get('/student/exam/index', studentExamController.index);
    app.get('/student/exam/marks', studentExamController.renderStudentExamMarks);
    app.get('/student/exam/timeTable', studentExamController.renderTimeTable);
    app.get('/student/exam/result', studentExamController.renderResult);

    var studentExamApiController = require('../controllers/api/student.exam.server.api.controller');
    //get
    app.get('/api/student/exam/getMarksheet', studentExamApiController.getMarksheet);
    app.get('/api/student/exam/getAllStudentMarks', studentExamApiController.getAllStudentMarks);
    app.get('/api/student/exam/getExamBySubject', studentExamApiController.getExamBySubject);
    //app.get('/api/student/exam/getAllNearByExams', studentExamApiController.getAllNearByExams);
    //app.get('/api/student/exam/getNearExams', studentExamApiController.getNearExams);
    //app.get('/api/student/exam/getExamDetail', studentExamApiController.getExamDetail);
    //app.get('/api/student/exam/searchExams', studentExamApiController.searchExams);
    //post
    app.post('/api/student/exam/addMarksheet', studentExamApiController.addMarksheet);
    //app.post('/api/student/exam/add', studentExamApiController.add);
    app.post('/api/student/exam/update', studentExamApiController.update);
    app.post('/api/student/exam/remove', studentExamApiController.remove);
    //app.post('/api/student/exam/removeById', studentExamApiController.removeById);
    //app.get('/api/student/exam/getExamsByUserId', studentExamApiController.getExamsByUserId);
};