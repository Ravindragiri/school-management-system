﻿module.exports = function (app) {

    var faq = require('../controllers/faq.server.controller');
    app.get('/faq/index', faq.index);
};