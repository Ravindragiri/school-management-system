﻿module.exports = function (app) {

    var studentController = require('../controllers/student.server.controller');
    app.get('/student/index', studentController.index);
    app.get('/student/register', studentController.renderRegister);
    app.get('/student/profile', studentController.renderProfile);
    app.get('/student/myProfile', studentController.renderMyProfile);
    app.get('/student/result-external', studentController.renderResultExternal);
    app.get('/student/result-internal', studentController.renderResultInternal);

    var studentApiController = require('../controllers/api/student.server.api.controller');
    app.post('/api/student/register', studentApiController.register);
    app.post('/api/student/mobileRegister', studentApiController.mobileRegister);
    app.post('/api/student/mobileUpdateProfile', studentApiController.mobileUpdateProfile);
    app.get('/api/student/getStudentContactDetailById', studentApiController.getStudentContactDetailById);
    app.get('/api/student/getStudentsByAnyCriteria', studentApiController.getStudentsByAnyCriteria);
    app.get('/api/student/getStudentById', studentApiController.getStudentById);
    app.get('/api/student/searchStudents', studentApiController.searchStudents);
    app.get('/api/student/getStudentAvatar', studentApiController.getStudentAvatar);
};