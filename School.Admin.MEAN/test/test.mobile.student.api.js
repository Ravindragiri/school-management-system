﻿var mongoose = require("mongoose");
var userModel = require('../server/models/user.server.model');
var server = require('../app');
require('../test/common/test.result.handler');

//Require the dev-dependencies
var chai = require('chai');
var chaiHttp = require('chai-http');
var should = chai.should();

chai.use(chaiHttp);

describe('Users', function () {
    
    //userModel.collection.drop();
    
    //searchMarks starts
    /*    
    it('searchMarks #1', function (done) {
        chai.request(server)
      .get('/api/student/mark/searchMarks?userType=1')
      //.get('/api/student/mark/searchMarks')
      //.query({ userType: 1 })
      .end(function (err, res) {
            resultWithBadRequest(res);
            done();
        });
    });
    
    it('searchMarks #2', function (done) {
        chai.request(server)
      .get('/api/student/mark/searchMarks?userType=0')
      .end(function (err, res) {
            resultWithBadRequest(res);
            done();
        });
    });
    
    it('searchMarks #3', function (done) {
        chai.request(server)
      .get('/api/student/mark/searchMarks?userType=')
      .end(function (err, res) {
            resultWithBadRequest(res);
            done();
        });
    });
    
    it('searchMarks #4', function (done) {
        chai.request(server)
      .get('/api/student/mark/searchMarks?userType=100')
      .end(function (err, res) {
            resultWithError(res);            
            done();
        });
    });
    
    it('searchMarks #5', function (done) {
        chai.request(server)
      .get('/api/student/mark/searchMarks')
      .end(function (err, res) {
            resultWithBadRequest(res);
            done();
        });
    });
    
    it('searchMarks #6', function (done) {
        chai.request(server)
      .get('/api/student/mark/searchMarks?userType=1')
      .end(function (err, res) {
            resultWithBadRequest(res);
            done();
        });
    });

    //create case resultEmptyArray

    it('searchMarks #7', function (done) {
        chai.request(server)
      .get('/api/student/mark/searchMarks?userType=0&title=Liver')
      .end(function (err, res) {
            resultReceiverArray(res);
            done();
        });
    });
      
    it('searchMarks #8', function (done) {
        chai.request(server)
      .get('/api/student/mark/searchMarks?userType=1&title=Liver')
      .end(function (err, res) {
            resultEmptyArray(res);
            done();
        });
    });
    
    it('searchMarks #9', function (done) {
        chai.request(server)
      .get('/api/student/mark/searchMarks?userType=1&title=Hair')
      .end(function (err, res) {
            resultEmptyArray(res);
            done();
        });
    });
    */
    //searchMarks ends

    //getMarkDetail starts
    /*
    it('getMarkDetail #1', function (done) {
        chai.request(server)
      .get('/api/student/mark/getMarkDetail?userID=57a0b3d3d18e5fc725041c18')
      //.get('/api/student/mark/getMarkDetail')
      //.query({ userType: 1 })
      .end(function (err, res) {
            getMarkDetailResultWithData(res);
            done();
        });
    });

    it('getMarkDetail #2', function (done) {
        chai.request(server)
      .get('/api/student/mark/getMarkDetail')
      //.get('/api/student/mark/getMarkDetail')
      //.query({ userType: 1 })
      .end(function (err, res) {
            resultWithBadRequest(res);
            done();
        });
    });

    it('getMarkDetail #3', function (done) {
        chai.request(server)
      .get('/api/student/mark/getMarkDetail?')
      //.get('/api/student/mark/getMarkDetail')
      //.query({ userType: 1 })
      .end(function (err, res) {
            resultWithBadRequest(res);
            done();
        });
    });

    it('getMarkDetail #4', function (done) {
        chai.request(server)
      .get('/api/student/mark/getMarkDetail?userID=57a0b3d3d18e5fc725041c18')
      //.get('/api/student/mark/getMarkDetail')
      //.query({ userType: 1 })
      .end(function (err, res) {
            getMarkDetailResultWithData(res);
            done();
        });
    });

    it('getMarkDetail #5', function (done) {
        chai.request(server)
      .get('/api/student/mark/getMarkDetail?userID=577e0fa784fd15ec5db7d6c1')
      //.get('/api/student/mark/getMarkDetail')
      //.query({ userType: 1 })
      .end(function (err, res) {
            resultNotFound(res);
            done();
        });
    });
    */
    //getMarkDetail ends

    //getStudentContactDetailById starts
    /*
    it('getStudentContactDetailById #1', function (done) {
        chai.request(server)
      .get('/api/student/getStudentContactDetailById?userID=57a0b3d3d18e5fc725041c18')
      .end(function (err, res) {
            getStudentContactDetailByIdResultWithData(res);
            done();
        });
    });
    
    it('getStudentContactDetailById #2', function (done) {
        chai.request(server)
      .get('/api/student/getStudentContactDetailById')
      .end(function (err, res) {
            resultWithBadRequest(res);
            done();
        });
    });
    
    it('getStudentContactDetailById #3', function (done) {
        chai.request(server)
      .get('/api/student/getStudentContactDetailById?')
      .end(function (err, res) {
            resultWithBadRequest(res);
            done();
        });
    });
    
    it('getStudentContactDetailById #4', function (done) {
        chai.request(server)
      .get('/api/student/getStudentContactDetailById?userID=57a0b3d3d18e5fc725041c18')
      .end(function (err, res) {
            getStudentContactDetailByIdResultWithData(res);
            done();
        });
    });
    
    it('getStudentContactDetailById #5', function (done) {
        chai.request(server)
      .get('/api/student/getStudentContactDetailById?userID=577e0fa784fd15ec5db7d6c1')
      .end(function (err, res) {
            resultNotFoundWithoutMessage(res);
            done();
        });
    });
    */
    //getStudentContactDetailById ends
    
    //add marks starts
    /*
    it('add marks #1', function (done) {
        chai.request(server)
      .post('/api/student/mark/add')
      .send({
            "marks" : [
                {
                    "title": "Intestines",
                    "description": null
                }],
            "userID": "57a0b3d3d18e5fc725041c18"
        })
        .end(function (err, res) {
            
            singleResultWithData(res);
            done();
        });
    });
    
    it('add marks #2', function (done) {
        chai.request(server)
      .post('/api/student/mark/add')
      .send({
            "marks" : [
                {
                    "title": "Intestines",
                    "description": null
                }],
            "userID": ""
        })
        .end(function (err, res) {
            resultWithBadRequest(res);
            done();
        });
    });
    
    it('add marks #3', function (done) {
        chai.request(server)
      .post('/api/student/mark/add')
      .send({
            "marks" : [
                {
                    "title": "Intestines",
                    "description": null
                }],
            "userID": null
        })
        .end(function (err, res) {
            resultWithBadRequest(res);
            done();
        });
    });
    
    it('add marks #4', function (done) {
        chai.request(server)
      .post('/api/student/mark/add')
      .send({
            "marks" : [],
            "userID": "57a0b3d3d18e5fc725041c18"
        })
        .end(function (err, res) {
            resultWithBadRequest(res);
            done();
        });
    });
    
    it('add marks #5', function (done) {
        chai.request(server)
      .post('/api/student/mark/add')
      .send({
            "marks" : null,
            "userID": "57a0b3d3d18e5fc725041c18"
        })
        .end(function (err, res) {
            resultWithBadRequest(res);
            done();
        });
    });
    
    it('add marks #6', function (done) {
        chai.request(server)
      .post('/api/student/mark/add')
      .send({
            "marks" : "",
            "userID": "57a0b3d3d18e5fc725041c18"
        })
        .end(function (err, res) {
            resultWithBadRequest(res);
            done();
        });
    });
    
    it('add marks #7', function (done) {
        chai.request(server)
      .post('/api/student/mark/add')
      .send({
            "userID": "57a0b3d3d18e5fc725041c18"
        })
        .end(function (err, res) {
            resultWithBadRequest(res);
            done();
        });
    });
    
    it('add marks #8', function (done) {
        chai.request(server)
      .post('/api/student/mark/add')
      .send({
            "marks" : ""
        })
        .end(function (err, res) {
            resultWithBadRequest(res);
            done();
        });
    });
    
    it('add marks #9', function (done) {
        chai.request(server)
        .post('/api/student/mark/add')
        .end(function (err, res) {
            resultWithBadRequest(res);
            done();
        });
    });
    */
    //add marks ends

    //mark remove starts
    /*
    it('mark remove #1', function (done) {
        chai.request(server)
        .post('/api/student/mark/remove')
        .send({
            "userID": "",
            "markTitle" : ""
        })
        .end(function (err, res) {
            resultWithBadRequest(res);
            done();
        });
    });
    
    it('mark remove #2', function (done) {
        chai.request(server)
        .post('/api/student/mark/remove')
        .send({
            "userID": "57a0b3d3d18e5fc725041c18",
            "markTitle" : ""
        })
        .end(function (err, res) {
            resultWithBadRequest(res);
            done();
        });
    });
    
    it('mark remove #2', function (done) {
        chai.request(server)
        .post('/api/student/mark/remove')
        .send({
            "userID": "",
            "markTitle" : "Liver"
        })
        .end(function (err, res) {
            resultWithBadRequest(res);
            done();
        });
    });

    it('mark remove #4', function (done) {
        chai.request(server)
        .post('/api/student/mark/remove')
        .send({
            "userID": null,
            "markTitle" : null
        })
        .end(function (err, res) {
            resultWithBadRequest(res);
            done();
        });
    });

    it('mark remove #5', function (done) {
        chai.request(server)
        .post('/api/student/mark/remove')
        .send({
            "userID": "57a0b3d3d18e5fc725041c18",
            "markTitle" : null
        })
        .end(function (err, res) {
            resultWithBadRequest(res);
            done();
        });
    });

    it('mark remove #6', function (done) {
        chai.request(server)
        .post('/api/student/mark/remove')
        .send({
            "userID": null,
            "markTitle" : "Liver"
        })
        .end(function (err, res) {
            resultWithBadRequest(res);
            done();
        });
    });

    it('mark remove #6', function (done) {
        chai.request(server)
        .post('/api/student/mark/remove')
        .send({
            "userID": "57a0b3d3d18e5fc725041c18",
            "markTitle" : "Intestines"
        })
        .end(function (err, res) {
            resultSuccess(res);
            done();
        });
    });

    it('mark remove #7', function (done) {
        chai.request(server)
        .post('/api/student/mark/remove')
        .send({
            "userID": "57a0b3d3d18e5fc725041c18",
            "markTitle" : "Liver"
        })
        .end(function (err, res) {
            resultSuccess(res);
            done();
        });
    });
    */
    //mark remove ends

    //mark remove by ID starts
    /*
    it('mark removeByID #1', function (done) {
        chai.request(server)
        .post('/api/student/mark/removeByID')
        .send({
            "userID": "57a0b3d3d18e5fc725041c18",
            "markID" : "579cd7040261fe9427ee9b58"
        })
        .end(function (err, res) {
            resultEmpty(res);
            done();
        });
    });
    */
    //mark remove by ID ends
});