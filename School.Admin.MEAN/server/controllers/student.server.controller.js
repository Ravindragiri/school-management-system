﻿var gcm = require('node-gcm'),
    config = require('../config/config.js');

require('../school.common/appConstants');

exports.index = function (req, res) {
    res.render('student/index', {
        pTitle: 'Students List | Organ Student'
    });
};

exports.renderRegister = function (req, res) {

    res.render('student/register', {
        countries: [],
        pTitle: 'Students List | Organ Student'
    });
};

exports.verifyCaptcha = function (req, res, next) {
    //returns true is valid - the captcha value must sent in the captch field 
    //the error message is in the errDesc key of the captcha object 
    if (!req.session.captcha.valid)
        return res.send(401, "Captcha does not match");

    res.send("Captcha matched! Well done :)");
};

exports.renderProfile = function (req, res) {

    res.render('student/profile', {
        profileVisitors: getProfileVisitors(),
        messages: getMessages(),
        pTitle: 'Profile | Organ Student'
    });
};

exports.renderMyProfile = function (req, res) {

    res.render('student/myProfile', {
        profileVisitors: getProfileVisitors(),
        messages: getMessages(),
        pTitle: 'My Profile | Organ Student',
        bloodGroups: bloodGroups,
        examTypes: examTypes,
        provinces: indiaStates,
        designations: designations,
        qualifications: qualifications,
        standards: classes
    });
};

function notify(response, request) {
    console.log("Request handler 'notify' was called");
    
    var androidReceivers = {};
    
    var respond = function () {
        var message = new gcm.Message();
        var sender = new gcm.Sender(config.gcm_apikey);
        var registrationIds = [];
        
        // Filling the Ids to broadcast the notification
        for (var i = 0; i < androidReceivers.length; i++) {
            var obj = androidReceivers[i];
            for (var key in obj) {
                if (key == "id_gcm") {
                    registrationIds.push('' + obj[key]);
                }
            }
        }
        
        // Optional
        message.addData('key1', 'message1');
        message.addData('key2', 'message2');
        message.collapseKey = 'demo';
        message.delayWhileIdle = true;
        message.timeToLive = 3;
        
        console.log(util.inspect(registrationIds));
        /**
		 * Parameters: message-literal, registrationIds-array, No. of retries, callback-function
		 */
		sender.send(message, registrationIds, 4, function (err, result) {
            console.log(result);
        });
        
        
        response.end(JSON.stringify(androidReceivers));
    }
    
    //db.all("SELECT rowid,id_gcm FROM andro_devices", function (err, rows) {
    //    if (rows != undefined) {
    //        androidReceivers = rows;
    //    } else {
    //        androidReceivers['error'] = 'Error, could not fetch android devices data...';
    //        console.log(err);
    //    }
    //    respond();
    //});

}

