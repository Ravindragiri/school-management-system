﻿var mongoose = require('mongoose');

var userSchema = new mongoose.Schema({

    registrationVerificationMethodType: Number,

    firstName: String,
    lastName: String,
    birthDate: String,
    phoneNumber: String,
    bloodGroup: String,

    username: String,
    email: String,
    emailConfirmed: Boolean,
    password: String,
    passwordHash: String,
    passwordSalt: String,

    address: String,
    city: String,
    //district: String,
    state: String,
    pin: String,

    avatarUrl: String,
    avatarMimeType: String,
    avatarFileName: String,

    isActive: Boolean,
    isDeleted: Boolean,
    isProfileIncomplete: Boolean,

    securityStamp: String,
    phoneNumberConfirmed: String,
    twoFactorEnabled: String,
    lockoutEndDate: String,
    accessFailedCount: Number,
    userType: String,

    loginAttempts: { type: Number, required: true, default: 0 },
    lockUntil: { type: Number },

    createdBy: {
        type: mongoose.Schema.Types.ObjectId,
    },
    createdDate: {
        type: Date,
    },
    updatedBy: {
        type: mongoose.Schema.Types.ObjectId
    },
    updatedDate: Date
});

exports.userSchema = userSchema;
mongoose.model('user', userSchema);
