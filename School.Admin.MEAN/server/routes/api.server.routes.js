﻿var _user = require('../controllers/users/user.authorization');

module.exports = function (app) {

    var userApiController = require('../controllers/api/user.server.api.controller');
    //post
    app.post('/api/user/add', userApiController.add);
    app.post('/api/user/changePassword', userApiController.changePassword);
    app.post('/api/user/authenticateMobileUser', userApiController.authenticateMobileUser);
    app.post('/api/user/removeById', userApiController.removeById);
    app.post('/api/user/forgetPassword', userApiController.forgetPassword);
    app.post('/api/user/getUserByEmailPassword', userApiController.getUserByEmailPassword);
    app.post('/api/user/updatePassword', userApiController.updatePassword);
    app.post('/api/user/updateEmail', userApiController.updateEmail);
    //get
    app.get('/api/user/getUserByEmail', userApiController.getUserByEmail);
    app.get('/api/user/getUsernameById', userApiController.getUsernameById);
    app.get('/api/user/getUserById', userApiController.getUserById);
    app.get('/api/user/getUserIdByUsername', userApiController.getUserIdByUsername);
}