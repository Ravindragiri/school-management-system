﻿var Q = require('q');
var mongoose = require('mongoose'),
    _ = require('lodash'),
    async = require('async');

require('../models/officeStaff.server.model');
require('../models/user.server.model');

var ObjectId = mongoose.Types.ObjectId;

var userDal = require('../../server/school.dal/user.dal.js');

var _officeStaffs = mongoose.model('officeStaff');
var _user = mongoose.model('user');

require('../../server/school.common/helpers/deferredHelper');

var dal = {};

dal.add = add;
dal.getOfficeStaffById = getOfficeStaffById;
dal.getOfficeStaffContactDetailById = getOfficeStaffContactDetailById;
dal.insert = insert;
dal.mobileRegister = mobileRegister;
dal.getOfficeStaffsByAnyCriteria = getOfficeStaffsByAnyCriteria;
dal.getOfficeStaffProfileInfo = getOfficeStaffProfileInfo;
dal.getOfficeStaffByUserId = getOfficeStaffByUserId;
dal.count = count;
dal.updateOfficeStaff = updateOfficeStaff;
dal.getAllOfficeStaffInfo = getAllOfficeStaffInfo;
dal.update = update;
dal.updateProfile = updateProfile;
dal.remove = remove;

module.exports = dal;

function add(officeStaffModel) {

    var deferred = Q.defer();

    officeStaffModel
        .save(function (err, result) {
            createHelper(deferred, err, result);
        });

    return deferred.promise;
}

function getOfficeStaffById(_id) {

    var deferred = Q.defer();

    _officeStaffs
        .findOne({ '_id': _id })
        .populate({ path: 'userRef' })
        //.populate({ path: 'exams' })
        //.populate({ path: 'followers' })
        //.populate({ path: 'favorites' })
        //.populate({ path: 'messages' })
        //.populate({ path: 'profileVisitors' })
        //.populate({ path: 'sharedProfiles' })
        .exec(function (err, result) {
            execHelper(deferred, err, result);
        });
    return deferred.promise;
}

function getOfficeStaffByUserId(userId) {

    var deferred = Q.defer();

    _officeStaffs
        .findOne({ userRef: ObjectId(userId)})
        .populate({
            path: 'userRef',
            select: '_id firstName lastName birthDate phoneNumber bloodGroup email username address city province pinCode'
        })
        .select('_id designation qualification experience userRef')
        .exec(function (err, result) {
            execHelper(deferred, err, result);
        });
    return deferred.promise;
}

function getOfficeStaffContactDetailById(officeStaffId) {

    var deferred = Q.defer();

    _officeStaffs
        .find({ _id: officeStaffId })
        .populate({ path: 'userRef' })
        .exec(function (err, result) {
            execHelper(deferred, err, result);
        });
    return deferred.promise;
}

function insert(officeStaffModel, userModel) {

    //var userDal = require('./user.dal.js');
    var deferred = Q.defer();

    async.waterfall(
        [
            function (callback) {

                userModel.createdDate = new Date();

                userDal.add(userModel)
                    .then(function (user) {

                        if (user) {

                            //officeStaffModel.user._id = user._id;
                            officeStaffModel.userRef = user._id;
                            officeStaffModel.createdBy = user._id;
                            officeStaffModel.createdDate = new Date();
                            callback(null, officeStaffModel);
                        } else {
                            deferred.resolve();
                        }
                    })
                    .catch(function (err) {
                        if (err) deferred.reject(err);
                    });
            },
            function (officeStaffModel, callback) {
                add(officeStaffModel)
                    .then(function (officeStaff) {

                        if (officeStaff) {
                            deferred.resolve(officeStaff);
                        } else {
                            deferred.resolve();
                        }
                    })
                    .catch(function (err) {
                        if (err) deferred.reject(err);
                    });
            }
        ], function (errs, results) {
            if (errs) {
                async.each(results, rollback, function () {
                    console.log('Rollback done.');
                    res.status(400).send(errs);
                });
            } else {
                console.log('Done.');
                deferred.resolve(results);
            }
        });

    function rollback(doc, callback) {
        if (!doc) { callback(); }
        else {
            userModel.findByIdAndRemove(doc._id, function (err, doc) {
                console.log('Rolled-back document: ' + doc);
                callback();
            });
        }
    }
    return deferred.promise;
}

function mobileRegister(officeStaffModel, userModel) {

    //var userDal = require('./user.dal.js');
    var deferred = Q.defer();

    async.waterfall(
        [
            function (callback) {

                userModel.createdDate = new Date();

                userDal.add(userModel)
                    .then(function (user) {

                        if (user) {

                            officeStaffModel.userRef = user._id;
                            officeStaffModel.createdBy = user._id;
                            officeStaffModel.createdDate = new Date();
                            callback(null, officeStaffModel);
                        } else {
                            deferred.resolve();
                        }
                    })
                    .catch(function (err) {
                        if (err) deferred.reject(err);
                    });
            },
            function (officeStaffModel, callback) {
                add(officeStaffModel)
                    .then(function (officeStaff) {

                        if (officeStaff) {
                            deferred.resolve(officeStaff);
                        } else {
                            deferred.resolve();
                        }
                    })
                    .catch(function (err) {
                        if (err) deferred.reject(err);
                    });
            }
        ], function (errs, results) {
            if (errs) {
                async.each(results, rollback, function () {
                    console.log('Rollback done.');
                    res.status(400).send(errs);
                });
            } else {
                console.log('Done.');
                deferred.resolve(results);
            }
        });

    function rollback(doc, callback) {
        if (!doc) { callback(); }
        else {
            userModel.findByIdAndRemove(doc._id, function (err, doc) {
                console.log('Rolled-back document: ' + doc);
                callback();
            });
        }
    }
    return deferred.promise;
}

function getOfficeStaffsByAnyCriteria(searchCriteria, searchText) {

    var deferred = Q.defer();

    _officeStaffs
        //.find({
        //    loc: {
        //        $near: searchCriteria.coords,
        //        $maxDistance: searchCriteria.maxDistance
        //    }
        //})
        .find({
            '$or': [
                { designation: new RegExp(searchText, 'i') },
                { qualification: new RegExp(searchText, 'i') }
            ]
        })
        .populate({ path: 'userRef' })
        //.populate({ path: 'exams' })
        //.populate({ path: 'followers' })
        //.populate({ path: 'favorites' })
        //.populate({ path: 'messages' })
        //.populate({ path: 'profileVisitors' })
        //.populate({ path: 'sharedProfiles' })
        .exec(function (err, result) {
            execHelper(deferred, err, result);
        });
    return deferred.promise;
}

function getOfficeStaffProfileInfo(userId) {

    var deferred = Q.defer();

    _officeStaffs
        .findOne({})
        .populate({
            path: 'user',
            match: { _id: userId },
        })
        .exec(function (err, result) {
            execHelper(deferred, err, result);
        });
    return deferred.promise;
}

function count() {

    var deferred = Q.defer();

    _officeStaffs
        .count({})
        .exec(function (err, result) {
            execHelper(deferred, err, result);
        });
    return deferred.promise;
}

function update(updateProfileModel, officeStaffModel, avatarUrl) {

    var deferred = Q.defer();

    var userModel = updateProfileModel.userRef;

    async.waterfall(
        [
            function (callback) {

                _user.findOne({ _id: updateProfileModel._id }, function (err, user) {
                    if (!err) {
                        if (user) {

                            user.email = updateProfileModel.email ? updateProfileModel.email : user.email;
                            user.username = updateProfileModel.username ? updateProfileModel.username : user.username;
                            user.password = updateProfileModel.password ? updateProfileModel.password : user.password;

                            user.firstName = updateProfileModel.firstName ? updateProfileModel.firstName : user.firstName;
                            user.lastName = updateProfileModel.lastName ? updateProfileModel.lastName : user.lastName;
                            user.birthName = updateProfileModel.birthName ? updateProfileModel.birthName : user.birthName;
                            user.phoneNumber = updateProfileModel.phoneNumber ? updateProfileModel.phoneNumber : user.phoneNumber;
                            user.bloodGroup = updateProfileModel.bloodGroup ? updateProfileModel.bloodGroup : user.bloodGroup;

                            user.address = updateProfileModel.address ? updateProfileModel.address : user.address;
                            user.city = updateProfileModel.city ? updateProfileModel.city : user.city;
                            user.province = updateProfileModel.province ? updateProfileModel.province : user.province;
                            user.pinCode = updateProfileModel.pinCode ? updateProfileModel.pinCode : user.pinCode;

                            user.qualification = updateProfileModel.qualification ? updateProfileModel.qualification : user.qualification;
                            user.designation = updateProfileModel.designation ? updateProfileModel.designation : user.designation;
                            user.experience = updateProfileModel.experience ? updateProfileModel.experience : user.experience;

                            //user.country = updateProfileModel.country ? updateProfileModel.country : user.country;
                            //user.countryISO2 = updateProfileModel.countryISO2 ? updateProfileModel.countryISO2 : user.countryISO2;
                            //user.countryDialCode = updateProfileModel.countryDialCode ? updateProfileModel.countryDialCode : user.countryDialCode;

                            user.updatedBy = updateProfileModel._id;
                            user.updatedDate = new Date();
                        }
                        //user.status = request.status;
                        user.save(function (err, result) {
                            if (err) {
                                console.log(err);
                                deferred.reject(err);
                            }
                            if (result._id) {
                                callback(null, updateProfileModel);
                            }
                        });
                    }
                });
            },
            function (officeStaffId, callback) {
                _officeStaffs.findOne({ _id: officeStaffModel._id }, function (err, officeStaff) {
                    if (!err) {
                        if (officeStaff) {

                            officeStaff.designation = officeStaffModel.designation ? officeStaffModel.designation : officeStaff.designation;
                            officeStaff.qualification = officeStaffModel.qualification ? officeStaffModel.qualification : officeStaff.qualification;
                            officeStaff.experience = officeStaffModel.experience ? officeStaffModel.experience : officeStaff.experience;

                            officeStaff.updatedBy = officeStaffModel.userRef._id;
                            officeStaff.updatedDate = new Date();;
                        }
                        officeStaff.save(function (err, result) {
                            if (err) {
                                console.log(err);
                                deferred.reject(err);
                            }
                            if (result._id) {
                                callback(null, officeStaffModel.officeStaffId);
                            }
                        });
                    }
                    else {
                        console.log(err);
                        deferred.reject(err);
                    }
                });
            }
        ], function (errs, results) {
            if (errs) {
                async.each(results, rollback, function () {
                    console.log('Rollback done.');
                    res.status(400).send(errs);
                });
            } else {
                console.log('Done.');
                deferred.resolve(results);
            }
        });

    function rollback(doc, callback) {
        if (!doc) { callback(); }
        else {
            //userModel.findByIdAndRemove(doc._id, function (err, doc) {
            //    console.log('Rolled-back document: ' + doc);
            //    callback();
            //});
        }
    }
    return deferred.promise;
}

function updateProfile(updateProfileModel, officeStaffModel, avatarUrl) {

    var deferred = Q.defer();

    var userModel = updateProfileModel.userRef;

    async.waterfall(
        [
            function (callback) {

                _user.findOne({ _id: updateProfileModel._id }, function (err, user) {
                    if (!err) {
                        if (user) {

                            //user.email = updateProfileModel.email ? updateProfileModel.email : user.email;
                            //user.username = updateProfileModel.username ? updateProfileModel.username : user.username;
                            //user.password = updateProfileModel.password ? updateProfileModel.password : user.password;

                            user.firstName = updateProfileModel.firstName ? updateProfileModel.firstName : user.firstName;
                            user.lastName = updateProfileModel.lastName ? updateProfileModel.lastName : user.lastName;
                            user.birthName = updateProfileModel.birthName ? updateProfileModel.birthName : user.birthName;
                            user.phoneNumber = updateProfileModel.phoneNumber ? updateProfileModel.phoneNumber : user.phoneNumber;
                            user.bloodGroup = updateProfileModel.bloodGroup ? updateProfileModel.bloodGroup : user.bloodGroup;

                            user.address = updateProfileModel.address ? updateProfileModel.address : user.address;
                            user.city = updateProfileModel.city ? updateProfileModel.city : user.city;
                            user.province = updateProfileModel.province ? updateProfileModel.province : user.province;
                            user.pinCode = updateProfileModel.pinCode ? updateProfileModel.pinCode : user.pinCode;

                            user.qualification = updateProfileModel.qualification ? updateProfileModel.qualification : user.qualification;
                            user.designation = updateProfileModel.designation ? updateProfileModel.designation : user.designation;
                            user.experience = updateProfileModel.experience ? updateProfileModel.experience : user.experience;

                            //user.country = updateProfileModel.country ? updateProfileModel.country : user.country;
                            //user.countryISO2 = updateProfileModel.countryISO2 ? updateProfileModel.countryISO2 : user.countryISO2;
                            //user.countryDialCode = updateProfileModel.countryDialCode ? updateProfileModel.countryDialCode : user.countryDialCode;

                            user.updatedBy = updateProfileModel._id;
                            user.updatedDate = new Date();
                        }
                        //user.status = request.status;
                        user.save(function (err, result) {
                            if (err) {
                                console.log(err);
                                deferred.reject(err);
                            }
                            if (result._id) {
                                callback(null, updateProfileModel);
                            }
                        });
                    }
                });
            },
            function (officeStaffId, callback) {
                _officeStaffs.findOne({ _id: officeStaffModel._id }, function (err, officeStaff) {
                    if (!err) {
                        if (officeStaff) {

                            officeStaff.designation = officeStaffModel.designation ? officeStaffModel.designation : officeStaff.designation;
                            officeStaff.qualification = officeStaffModel.qualification ? officeStaffModel.qualification : officeStaff.qualification;
                            officeStaff.experience = officeStaffModel.experience ? officeStaffModel.experience : officeStaff.experience;

                            officeStaff.updatedBy = officeStaffModel.userRef._id;
                            officeStaff.updatedDate = new Date();;
                        }
                        officeStaff.save(function (err, result) {
                            if (err) {
                                console.log(err);
                                deferred.reject(err);
                            }
                            if (result._id) {
                                callback(null, officeStaffModel.officeStaffId);
                            }
                        });
                    }
                    else {
                        console.log(err);
                        deferred.reject(err);
                    }
                });
            }
        ], function (errs, results) {
            if (errs) {
                async.each(results, rollback, function () {
                    console.log('Rollback done.');
                    res.status(400).send(errs);
                });
            } else {
                console.log('Done.');
                deferred.resolve(results);
            }
        });

    function rollback(doc, callback) {
        if (!doc) { callback(); }
        else {
            //userModel.findByIdAndRemove(doc._id, function (err, doc) {
            //    console.log('Rolled-back document: ' + doc);
            //    callback();
            //});
        }
    }
    return deferred.promise;
}

function getAllOfficeStaffInfo() {

    var deferred = Q.defer();

    _officeStaffs
        .find({})
        .populate({
            path: 'userRef',
            select: '_id firstName lastName birthDate phoneNumber bloodGroup email username address city province pinCode'
        })
        //.populate({ path: 'subjects' })
        //.populate({ path: 'followers' })
        //.populate({ path: 'favorites' })
        //.populate({ path: 'messages' })
        //.populate({ path: 'profileVisitors' })
        //.populate({ path: 'sharedProfiles' })
        .exec(function (err, result) {
            execHelper(deferred, err, result);
        });
    return deferred.promise;
}

function updateOfficeStaff(officeStaffUpdateCondition, officeStaffUpdateSet) {

    var deferred = Q.defer();

    _officeStaffs.findOneAndUpdate(officeStaffUpdateCondition, { $set: officeStaffUpdateSet }, { new: true }, function (err, officeStaff) {
        if (err) {
            deferred.reject(err);
        }
        if (officeStaff) {
            officeStaff = _.pick(officeStaff.toObject(), ['_id', 'officeStaffname', 'email', 'isProfileIncomplete', 'firstName', 'phoneNumber', 'province', 'city',
                'address', 'pinCode', 'countryDialCode', 'avatarUrl', 'exams']);
            deferred.resolve(officeStaff);
        } else {
            deferred.resolve();
        }
    });
    return deferred.promise;
}

function remove(officeStaffId, userId) {

    var userDal = require('./user.dal.js');
    var deferred = Q.defer();

    async.waterfall(
        [
            function (callback) {

                //userModel.createdDate = new Date();

                var loggedInUserId;

                var userUpdateCondition = { $and: [] };
                userUpdateCondition.$and.push({
                    _id: userId
                });

                var userUpdateSet = {
                    isDeleted: true,
                    //updatedBy: loggedInUserId,
                    updatedDate: new Date()
                };

                userDal.update(userUpdateCondition, userUpdateSet)
                    .then(function (user) {

                        if (user) {

                            callback(null, officeStaffId);
                        } else {
                            deferred.resolve();
                        }
                    })
                    .catch(function (err) {
                        if (err) deferred.reject(err);
                    });
            },
            function (officeStaffId, callback) {

                var loggedInUserId;

                var officeStaffUpdateCondition = { $and: [] };
                officeStaffUpdateCondition.$and.push({
                    _id: officeStaffId
                });

                var officeStaffUpdateSet = {
                    isDeleted: true,
                    //updatedBy: loggedInUserId,
                    updatedDate: new Date()
                };

                updateOfficeStaff(officeStaffUpdateCondition, officeStaffUpdateSet)
                    .then(function (officeStaff) {

                        if (officeStaff) {
                            deferred.resolve(officeStaff);
                        } else {
                            deferred.resolve();
                        }
                    })
                    .catch(function (err) {
                        if (err) {
                            callback(err, userModel);
                            //deferred.reject(err);
                        }
                    });
            }
        ], function (errs, results) {
            if (errs) {
                async.each(results, rollback, function () {
                    console.log('Rollback done.');
                    //res.status(400).send(errs);
                });
            } else {
                console.log('Done.');
                deferred.resolve(results);
            }
        });

    function rollback(doc, callback) {
        if (!doc) { callback(); }
        else {
            _user.findOne({ _id: updateProfileModel.userId }, function (err, user) {
                if (!user) {

                    user.isDeleted = false;
                    user.updatedBy = updateProfileModel._id;
                    user.updatedDate = new Date();
                }
                user.save(function (err, doc) {
                    console.log('Rolled-back document: ' + doc);
                    callback();
                });
            });
        }
    }
    return deferred.promise;
}