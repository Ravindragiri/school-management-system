﻿module.exports = function (app) {

    var contactUsApiController = require('../controllers/api/contactUs.server.api.controller');
    app.post('/api/contactUs/sendMessage', contactUsApiController.sendMessage);

    var userApiController = require('../controllers/api/user.server.api.controller');
    //post
    app.post('/api/user/add', userApiController.add);
    app.post('/api/user/changePassword', userApiController.changePassword);
    app.post('/api/user/authenticateMobileUser', userApiController.authenticateMobileUser);
    app.post('/api/user/removeById', userApiController.removeById);
    app.post('/api/user/forgetPassword', userApiController.forgetPassword);
    //get
    app.get('/api/user/getUserByEmail', userApiController.getUserByEmail);
    app.get('/api/user/getUsernameById', userApiController.getUsernameById);
    app.get('/api/user/getUserById', userApiController.getUserById);

    var officeStaffApiController = require('../controllers/api/officeStaff.server.api.controller');
    //post
    app.post('/api/officeStaff/register', officeStaffApiController.register);
    app.post('/api/officeStaff/mobileRegister', officeStaffApiController.mobileRegister);
    app.post('/api/officeStaff/mobileUpdateProfile', officeStaffApiController.mobileUpdateProfile);
    //get
    app.get('/api/officeStaff/getOfficeStaffContactDetailById', officeStaffApiController.getOfficeStaffContactDetailById);
    app.get('/api/officeStaff/getOfficeStaffsByAnyCriteria', officeStaffApiController.getOfficeStaffsByAnyCriteria);
    app.get('/api/officeStaff/getOfficeStaffById', officeStaffApiController.getOfficeStaffById);
    app.get('/api/officeStaff/getOfficeStaffByUserId', officeStaffApiController.getOfficeStaffByUserId);
    //app.get('/api/officeStaff/searchOfficeStaffs', officeStaffApiController.searchOfficeStaffs);
    app.get('/api/officeStaff/getOfficeStaffAvatar', officeStaffApiController.getOfficeStaffAvatar);
    app.get('/api/officeStaff/count', officeStaffApiController.count);

    var teacherApiController = require('../controllers/api/teacher.server.api.controller');
    //post
    app.post('/api/teacher/register', teacherApiController.register);
    app.post('/api/teacher/mobileRegister', teacherApiController.mobileRegister);
    app.post('/api/teacher/mobileUpdateProfile', teacherApiController.mobileUpdateProfile);
    //get
    app.get('/api/teacher/getTeacherContactDetailById', teacherApiController.getTeacherContactDetailById);
    app.get('/api/teacher/getTeachersByAnyCriteria', teacherApiController.getTeachersByAnyCriteria);
    app.get('/api/teacher/getTeacherById', teacherApiController.getTeacherById);
    app.get('/api/teacher/getTeacherByUserId', teacherApiController.getTeacherByUserId);
    //app.get('/api/teacher/searchTeachers', teacherApiController.searchTeachers);
    app.get('/api/teacher/getTeacherAvatar', teacherApiController.getTeacherAvatar);
    app.get('/api/teacher/count', teacherApiController.count);

    //var studentExamApiController = require('../controllers/api/student.exam.server.api.controller');
    //app.post('/api/student/exam/add', studentExamApiController.add);
    //app.post('/api/student/exam/update', studentExamApiController.update);
    //app.get('/api/student/exam/getAllNearByExams', studentExamApiController.getAllNearByExams);
    //app.get('/api/student/exam/getNearExams', studentExamApiController.getNearExams);
    //app.get('/api/student/exam/getExamDetail', studentExamApiController.getExamDetail);
    //app.get('/api/student/exam/searchExams', studentExamApiController.searchExams);
    //app.post('/api/student/exam/remove', studentExamApiController.remove);
    //app.post('/api/student/exam/removeByID', studentExamApiController.removeByID);
    //app.get('/api/student/exam/getExamsByUserID', studentExamApiController.getExamsByUserID);
}