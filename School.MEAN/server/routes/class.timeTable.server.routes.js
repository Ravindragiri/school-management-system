﻿module.exports = function (app) {

    var classTimeTableController = require('../controllers/class.TimeTable.server.controller');
    app.get('/class/TimeTable/index', classTimeTableController.index);
    app.get('/class/TimeTable', classTimeTableController.index);

    var classTimeTableApiController = require('../controllers/api/class.timeTable.server.api.controller');
    //get
    app.get('/api/class/timeTable/getClassTimeTableByClassAndSection', classTimeTableApiController.getClassTimeTableByClassAndSection);
    app.get('/api/class/timeTable/getAllTimeTables', classTimeTableApiController.getAllTimeTables);
    app.get('/api/class/timeTable/getTimeTableById', classTimeTableApiController.getTimeTableById);
    app.get('/api/class/timeTable/getTimeTableByDay', classTimeTableApiController.getTimeTableByDay);
    app.get('/api/class/timeTable/removeById', classTimeTableApiController.removeById);
    
}